import random

def input_sequence(nev, rate_hz):

    # sequence is per 25ns, i.e. rate is translated into a mean period of bunch crossings
    fraction_bc = rate_hz/40000000.

    seq = []

    for i in range(0,nev):
        if random.random()<=fraction_bc:
            seq.append(1)
        else:
            seq.append(0)

    return seq


def measure_rate(seq):

    nl1a=0.
    ntot=0.
    for l1a in seq:
        ntot = ntot+1.
        if l1a==1:
            nl1a = nl1a+1.
    return nl1a/ntot*40000000.

def apply_bunchpattern(seq, patternfile):
    #read pattern file
    f = open(patternfile,'r')
    lines = f.readlines()
    for i in range(0,len(lines)):
        lines[i] = lines[i].strip()

    # apply coincidence
    bcid=0
    for i in range(0,len(seq)):
        if (seq[i]==1):
            if (lines[bcid]=='0'):
                seq[i]=0
        bcid=bcid+1
        if (bcid>3563):
            bcid=bcid-3564

    return seq

def apply_deadtime(seq, simple_deadtime, complex_level, complex_rate, start_level):

    sdt = 0
    fillcount = complex_rate
    bucketsize = complex_level
    level=start_level

    out_seq = []

    for t in seq:
        # put one token into bucket when fillcount reaches zero
        if fillcount>0:
            fillcount=fillcount-1
        else:
            if level<bucketsize:
                level=level+1
            fillcount=complex_rate
            
        # decrease simple deadtime count
        if sdt>0:
            sdt=sdt-1

        # make trigger decision
        l1a=0
        if (t==1 and sdt==0 and level>0):
            # issue L1A
            l1a=1
            sdt=simple_deadtime+1
            level=level-1

        out_seq.append(l1a)

    return out_seq


def format_pattern(seq, triggerpattern, emptypattern):

    ret_seq = []
    mult = 0
    for t in seq:
        if t==1:
            # print out earlier empty patterns
            if mult>0:
                ret_seq.append(str(emptypattern)+" "+str(mult))
                mult=0
            # print out trigger
            ret_seq.append(str(triggerpattern)+" 1")
        else:
            mult=mult+1
    # print out remaining empty patterns
    if mult>0:
        ret_seq.append(str(emptypattern)+" "+str(mult))

    return ret_seq


# input rate
input_rate_hz = 200000.

# simple deadtime
simpledt = 5

# complex deadtime parameters
complex_rate = 415
complex_level = 7

# max = 1000000, 294*3564-3 gives you the max length for a pattern repetitive with the LHC turns
nev = 294*3564-3

patternfile = "ltp_50ns_624b+1small_598_16_576_72bpi11inj_b.dat"

ts = input_sequence(nev, input_rate_hz)
ts = apply_bunchpattern(ts, patternfile)
ts = apply_deadtime(ts, simpledt, complex_level, complex_rate, 0)
print "# input rate = " + str(input_rate_hz) + " Hz"
print "# simple deadtime: " + str(simpledt)
print "# complex deadtime: level = " + str(complex_level) + ", rate = " + str(complex_rate)
print "# rate = " + str(measure_rate(ts))
print "# number of events: " + str(nev)
out_seq = format_pattern(ts, "1111 1111 1111 1111", "0000 0000 0000 0000")

#print out_seq
for l in out_seq:
    print l

    

