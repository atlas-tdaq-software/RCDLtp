#ifndef RCDLTP_H
#define RCDLTP_H

//******************************************************************************
// file: RCDLtp.h
// desc: library for Ltp module
//
// Author: Thilo Pauly  (thilo.pauly@cern.ch)
//
//******************************************************************************

#include <string>
#include <vector>
#include "RCDVme/RCDVme.h"

using namespace std;

namespace RCD {

  // Ltp class -----------------------------------------------------------------
  class LTP {

  public:
    LTP() {
      m_busy_channels.clear();
    }
    ~LTP() {}

    // low-level interface:
    std::string GetError(int errcode);
    int Reset();
    int Open(u_int base);
    int Close();
    int ReadConfigurationEEPROM(u_int& board_man, u_int& board_revision, u_int& board_id);
    bool CheckCERNID();

    // performs basic test to increase the likeliness that the vme
    // module is actually an LTP module
    bool CheckLTP();

    enum error_codes {
      ERROR_INVALID_INTERRUPT_VECTOR = 9901,
      ERROR_INVALID_IO_LOCAL_SEL     = 9902,
      ERROR_INVALID_ORB_SEL          = 9903,
      ERROR_INVALID_COUNTER_SEL      = 9904,
      ERROR_INVALID_TTYPE_REG        = 9905,
      ERROR_INVALID_TTYPE_FIFODELAY  = 9906,
      ERROR_INVALID_TTYPE_FIFOSRC    = 9907,
      ERROR_INVALID_CALREQ_SRC       = 9908,
      ERROR_INVALID_CALREQ_PRESET    = 9909,
      ERROR_CALREQ_LINKSTATUS        = 9910,
      ERROR_INVALID_BUSY_LEMOOUT_SRC = 9911,
      ERROR_INVALID_BUSY_DEAD_SRC    = 9912,
      ERROR_INVALID_PG_RUNMODE       = 9913,
      ERROR_TOO_LONG_PATTERN         = 9914
    };

    enum channel           {L1A, TR1, TR2, TR3, BG0, BG1, BG2, BG3}; 
    enum orbit_source      {ORB_ERROR,ORB_NO_SOURCE,ORB_INTERNAL,ORB_FP_LEMO_INPUT,ORB_PATGEN};
    enum counter_selection {CNT_ERROR, CNT_NO_SOURCE, CNT_ORBIT, CNT_ORBIT_ECR,
			    CNT_BG0, CNT_BG1, CNT_BG2, CNT_BG3, 
			    CNT_L1A, CNT_TR1, CNT_TR2, CNT_TR3,
			    CNT_L1A_ECR, CNT_TR1_ECR, CNT_TR2_ECR, CNT_TR3_ECR};
    enum busy_lemo_output   {BUSY_ERROR, BUSY_ONLY, BUSY_OR_DEAD, BUSY_TIMER_INH, BUSY_PG_INH};
    enum busy_dead_source  {DEAD_ERROR, DEAD_L1A, DEAD_TR1, DEAD_TR2, DEAD_TR3};
    enum ttype_file_addr   {TTYPE_FILE_ERROR, TTYPE_FILE_0, TTYPE_FILE_1, TTYPE_FILE_2, TTYPE_FILE_3};
    enum ttype_delay       {TTYPE_DELAY_ERROR, TTYPE_DELAY_2BC, TTYPE_DELAY_3BC, TTYPE_DELAY_4BC, TTYPE_DELAY_5BC};
    enum ttype_fifotrigger {TTYPE_FIFO_ERROR, TTYPE_FIFO_L1A, TTYPE_FIFO_TR1, TTYPE_FIFO_TR2, TTYPE_FIFO_TR3};
    enum cal_source        {CAL_ERROR, CAL_LINK, CAL_PRESET, CAL_FP, CAL_PG};
    enum pg_op_mode        {PG_ERROR, PG_PRE_LOAD, PG_RUNNING, PG_FIXED, PG_NOTUSED};
    enum io_source         {IO_ERROR,IO_NO_SOURCE,IO_FP_LEMO,IO_FP_LEMO_PFN,IO_PATGEN};

    // ----------------
    // VMEbus Interrupt
    // ----------------

    int  VMEInt_write_request_level(u_short w);
    int  VMEInt_enable                      ();
    int  VMEInt_disable                     ();
    int  VMEInt_enable_source_0             ();
    int  VMEInt_disable_source_0            ();
    int  VMEInt_enable_source_1             ();
    int  VMEInt_disable_source_1            ();
    int  VMEInt_set_vector             (int w);
    int  VMEInt_read_request_level(u_short* w);
    bool VMEInt_is_enabled                  ();
    bool VMEInt_source_0_is_enabled         ();
    bool VMEInt_source_1_is_enabled         ();
    bool VMEInt_request_is_pending          ();
    bool VMEInt_request_is_asserted         ();
    bool VMEInt_source_0_is_active          ();
    bool VMEInt_source_1_is_active          ();
    int  VMEInt_read_vector       (u_short* w);

    // -----------------------------
    // I/O - L1A, Test Triggers, BGo
    // -----------------------------

    u_short CSR(channel c);

    int IO_write       (channel c, u_short w);
    int IO_read       (channel c, u_short* w);

    // Set methods
    int  IO_linkout_from_linkin   (channel c);
    int  IO_linkout_from_local    (channel c);
    int  IO_lemoout_from_linkin   (channel c);
    int  IO_lemoout_from_local    (channel c);
    int  IO_local_no_source       (channel c);
    int  IO_local_lemo_direct      (channel c);
    int  IO_local_lemo_pfn         (channel c);
    int  IO_local_pg              (channel c);
    int  IO_enable_busy_gating    (channel c);
    int  IO_enable_inhibit_gating (channel c);
    int  IO_enable_pg_gating      (channel c);
    int  IO_disable_busy_gating   (channel c);
    int  IO_disable_inhibit_gating(channel c);
    int  IO_disable_pg_gating     (channel c);
    int  IO_pfn_width_1bc         (channel c);
    int  IO_pfn_width_2bc         (channel c);
    int  IO_sw_pulse              (channel c);

    // Get Methods
    bool L1A_link_is_active                ();
    bool IO_linkin_is_active      (channel c);
    bool IO_linkout_is_active     (channel c);
    bool IO_local_is_active       (channel c);
    bool IO_linkout_is_from_local (channel c);
    bool IO_linkout_is_from_linkin(channel c);
    bool IO_lemoout_is_from_local (channel c);
    bool IO_lemoout_is_from_linkin(channel c);

    int  IO_local_selection       (channel c, io_source* source);
    bool IO_busy_gating_is_enabled(channel c);
    bool IO_inhibit_timer_gating_is_enabled(channel c);
    bool IO_pg_gating_is_enabled  (channel c);
    bool IO_pfn_width_is_1bc      (channel c);
    bool IO_pfn_width_is_2bc      (channel c);
   
    // -----------
    // Bunch Clock
    // -----------

    int  BC_write        (u_short w);
    int  BC_read        (u_short* w);

    // Set Methods
    int  BC_linkout_from_linkin   ();
    int  BC_linkout_from_local    ();
    int  BC_lemoout_from_linkin   ();
    int  BC_lemoout_from_local    ();
    int  BC_local_intern          ();
    int  BC_local_lemo             ();

    // Get Methods
    bool BC_linkout_is_from_local ();
    bool BC_linkout_is_from_linkin();
    bool BC_lemoout_is_from_local ();
    bool BC_lemoout_is_from_linkin();
    bool BC_local_is_from_lemoin  ();
    bool BC_local_is_from_intern  ();
    bool BC_external_is_running   ();
    bool BC_selected_is_running   ();

    // -----
    // Orbit
    // -----
    int ORB_write     (u_short w);
    int ORB_read     (u_short* w);

    // Set Methods
    int ORB_linkout_from_linkin();
    int ORB_linkout_from_local ();
    int ORB_lemoout_from_linkin();
    int ORB_lemoout_from_local ();
    int ORB_local_no           ();
    int ORB_local_intern       ();
    int ORB_local_lemo          ();
    int ORB_local_pg           ();

    int COUNTER_L1A            ();
    int COUNTER_TR1            ();
    int COUNTER_TR2            ();
    int COUNTER_TR3            ();
    int COUNTER_BG0            ();
    int COUNTER_BG1            ();
    int COUNTER_BG2            ();
    int COUNTER_BG3            ();
    int COUNTER_orb            ();
    int COUNTER_orb_ecr        ();
    int COUNTER_L1A_ecr        ();
    int COUNTER_TR1_ecr        ();
    int COUNTER_TR2_ecr        ();
    int COUNTER_TR3_ecr        ();
    int COUNTER_none           ();


    // Get Methods
    bool ORB_linkout_is_from_local ();
    bool ORB_linkout_is_from_linkin();
    bool ORB_lemoout_is_from_local ();
    bool ORB_lemoout_is_from_linkin();

    int ORB_local_selection(enum orbit_source* source );

    int COUNTER_selection(enum counter_selection* source );

    bool ORB_selected_is_running();

    int INH_writeShadow(u_short w)  ;
    int INH_readShadow(u_short* w)  ;

    int COUNTER_reset              ();
    int COUNTER_read_lsw (u_short* w);
    int COUNTER_read_msw (u_short* w);
    int COUNTER_read_32  (u_int* a);
    int COUNTER_read_8_24(u_short* a8, u_int* a24);

    // ------------
    // Trigger Type
    // ------------

    // Set Methods
    int  TTYPE_write_regfile0(u_short w);
    int  TTYPE_write_regfile1(u_short w);
    int  TTYPE_write_regfile2(u_short w);
    int  TTYPE_write_regfile3(u_short w);

    int  TTYPE_write     (u_short w);
    int  TTYPE_linkout_from_linkin();
    int  TTYPE_linkout_from_local ();
    int  TTYPE_eclout_from_linkin ();
    int  TTYPE_eclout_from_local  ();
    int  TTYPE_local_from_pg      ();
    int  TTYPE_local_from_vme     ();
    int  TTYPE_enable_eclout      ();
    int  TTYPE_disable_eclout     ();
    int  TTYPE_regfile_address_0  ();
    int  TTYPE_regfile_address_1  ();
    int  TTYPE_regfile_address_2  ();
    int  TTYPE_regfile_address_3  ();
    int  TTYPE_fifodelay_2bc      ();
    int  TTYPE_fifodelay_3bc      ();
    int  TTYPE_fifodelay_4bc      ();
    int  TTYPE_fifodelay_5bc      ();
    int  TTYPE_fifotrigger_L1A    ();
    int  TTYPE_fifotrigger_TR1    ();
    int  TTYPE_fifotrigger_TR2    ();
    int  TTYPE_fifotrigger_TR3    ();
    int  TTYPE_reset_fifo         ();


    // Get Methods
    int  TTYPE_read_regfile0   (u_short* w);
    int  TTYPE_read_regfile1   (u_short* w);
    int  TTYPE_read_regfile2   (u_short* w);
    int  TTYPE_read_regfile3   (u_short* w);
    int  TTYPE_read            (u_short* w);
    bool TTYPE_linkout_is_from_local     ();
    bool TTYPE_linkout_is_from_linkin    ();
    bool TTYPE_eclout_is_from_local      ();
    bool TTYPE_eclout_is_from_linkin     ();
    bool TTYPE_local_is_controlled_by_vme();
    bool TTYPE_local_is_controlled_by_pg ();
    bool TTYPE_eclout_is_disabled        ();

    int TTYPE_regfile_address(enum ttype_file_addr* addr  );
    int TTYPE_fifodelay      (enum ttype_delay* del       );
    int TTYPE_fifotrigger    (enum ttype_fifotrigger* src );

    bool TTYPE_fifo_is_full ();
    bool TTYPE_fifo_is_empty();

    int TTYPE_write_fifo();
    int TTYPE_read_fifo (u_short* w);

    // ------------
    // Turn Counter
    // ------------

    int TURN_write_selector_code (u_short w);
    int TURN_read_selector_code (u_short* w);
    int TURN_orbit_lemoout_from_orbit      ();
    int TURN_orbit_lemoout_from_turn       ();
    bool TURN_orbit_lemoout_is_from_orbit  ();
    bool TURN_orbit_lemoout_is_from_turn   ();


    // -------------------
    // Calibration Request
    // -------------------

    // Set Methods
    int CAL_write        (u_short w);
    int CAL_source_link           ();
    int CAL_source_preset         ();
    int CAL_source_fp             ();
    int CAL_source_pg             ();
    int CAL_enable_testpath       ();
    int CAL_disable_testpath      ();
    int CAL_set_preset_value (int w);

    // Get Methods
    int CAL_read (u_short* w);

    bool CAL_testpath_is_enabled();

    int CAL_source(enum cal_source* src );

    int CAL_get_preset_value(int* w);
    int CAL_get_linkstatus  (int* w);

    // ----
    // BUSY
    // ----

    // Set Methods
    int BUSY_write          (u_short w);
    int BUSY_linkout_from_linkin     ();
    int BUSY_linkout_from_local      ();
    int BUSY_local_with_linkin       ();
    int BUSY_local_without_linkin    ();
    int BUSY_local_with_ttlin        ();
    int BUSY_local_without_ttlin     ();
    int BUSY_local_with_nimin        ();
    int BUSY_local_without_nimin     ();
    int BUSY_enable_constant_level   ();
    int BUSY_disable_constant_level  ();
    int BUSY_local_with_deadtime     ();
    int BUSY_local_without_deadtime  ();
    int BUSY_lemoout_busy_only       ();
    int BUSY_lemoout_busy_or_deadtime();
    int BUSY_lemoout_inhibit         ();
    int BUSY_lemoout_pg_inhibit      ();
    int BUSY_deadtime_source_L1A     ();
    int BUSY_deadtime_source_TR1     ();
    int BUSY_deadtime_source_TR2     ();
    int BUSY_deadtime_source_TR3     ();
    int BUSY_deadtime_duration  (int n);
    int BUSY_enable_from_pg          ();
    int BUSY_disable_from_pg         ();

    // Get Methods
    int BUSY_read          (u_short* w);
    int BUSY_read_status   (u_short* w);

    bool BUSY_linkout_is_from_local        ();
    bool BUSY_linkout_is_from_linkin       ();
    bool BUSY_linkin_is_summed_with_local  ();
    bool BUSY_ttlin_is_summed_with_local   ();
    bool BUSY_nimin_is_summed_with_local   ();
    bool BUSY_deadtime_is_summed_with_local();
    bool BUSY_constant_level_is_asserted   ();
    bool BUSY_from_pg_is_enabled           ();

    int BUSY_lemoout_source (enum busy_lemo_output* source  );
    int BUSY_deadtime_source(enum busy_dead_source* source );

    int BUSY_get_deadtime_duration(int* dur );

    bool BUSY_linkin_is_busy  ();
    bool BUSY_linkout_is_busy ();
    bool BUSY_ttlin_is_busy   ();
    bool BUSY_nimin_is_busy   ();
    bool BUSY_internal_is_busy();

    // -----------------
    // Pattern Generator
    // -----------------
    int PG_write                    (u_short w);
    int PG_runmode_load                      ();
    int PG_runmode_fixedvalue                ();
    int PG_runmode_cont                      ();
    int PG_runmode_notused                   ();
    int PG_runmode_singleshot_vme            ();
    int PG_runmode_singleshot_orb            ();
    int PG_runmode_singleshot_bgo            ();
    int PG_start                             ();
    int PG_reset                             (); 
    int PG_get_fixed_value         (u_short* w);
    int PG_set_fixed_value          (u_short w);
    int PG_load_memory_from_file (std::string*);
    int PG_load_memory(std::vector<std::string>&);
    int PG_read_from_memory                  ();
    int PG_read                    (u_short* w);
    int PG_read_address_counter_MSW(u_short* w);
    int PG_read_address_counter_LSW(u_short* w);
    int PG_read_address_counter      (u_int* w);

    int PG_runmode (enum pg_op_mode* mode );

    bool PG_is_in_continuous_mode                 ();
    bool PG_is_in_singleshot_mode                 ();
    bool PG_single_shot_trigger_source_is_external();
    bool PG_single_shot_trigger_source_is_vme     ();
    bool PG_external_source_is_BGo3               ();
    bool PG_external_source_is_orbit              ();
    bool PG_is_idle                               ();

    // --------------------------
    // Predefined Operation Modes
    // --------------------------
    int OM_slave();                             // running in slave mode
    int OM_master_lemo();                       // running in master mode with local lemo inputs
    int OM_master_patgen(std::string& file);    // running in master mode with pattern generator
    int OM_master_RPCPulseL1A(u_int l1a_delay); // master RPC mode: test pulse followed by L1A
    int OM_GenerateECR();                       // generates ECR with pattern generator

    void OM_slave_print_description();    
    void OM_master_lemo_print_description();  
    void OM_master_patgen_print_description(std::string& pgfilename); 
    void OM_master_RPCPulseL1A_print_description(u_int l1a_delay); 

    // -----------------------
    // Software busy mechanism
    // -----------------------
    int createBusyChannel();
    bool setBusyChannel(unsigned int c);
    bool releaseBusyChannel(unsigned int c);
    bool getBusyStatus(unsigned int c, bool& status);
    bool getGlobalBusyStatus(bool& status);
    void dumpSWBusy();
    void clearSWBusy();
  private:
    void update_hwbusy();
    

  private:


    // helper methods
    int  pow(int a, int k);
    int  bintoint(char* a);
    int  bin4toint(char* a1,char* a2,char* a3,char* a4);
    int  setBits(u_int reg, u_short mask, u_short bits);
    int  getBitsAndPrint(u_int reg, u_short mask, u_short* bits);
    int  getBits(u_int reg, u_short mask, u_short* bits);
    int  testBits(u_int reg, u_short mask, bool* result);
    std::string printBinary16(u_short n);
    int  delayPatterns(u_short& p0, u_short& p1);


    // software busy channels
    std::vector<bool> m_busy_channels;

    // register addresses
    static const u_short REG_20BITCNT_LSW   = 0xF2; // RW W 16 Address Counter Shadow register low
    static const u_short REG_20BITCNT_MSW   = 0xF0; // RW W 4  Address Counter Shadow register high
    static const u_short REG_MEMIO          = 0xE8; // RW W 16 Pattern Generator Memory I/O
    static const u_short REG_FIXVAL         = 0xE6; // RW W 16 Fixed Value Output Register
    
    static const u_short REG_PATGEN_CSR     = 0xE4; // RW W 5  Pattern Generator Control & Status Reg.
    static const u_short REG_SINGLSHOT      = 0xE2; //  W W -  Start Pattern Generator
    static const u_short REG_SW_RST         = 0xE0; //  W W -  Reset FSM / data-less function

    static const u_short REG_BUSY_SR        = 0xD4; //  R R 5  BUSY Status Register
    static const u_short REG_BUSY_CR        = 0xD2; // RW W 16 BUSY Control Register
    static const u_short REG_CAL_CSR        = 0xD0; // RW W 8  CALIBRATION Control & Status Register

    static const u_short REG_FIFO_RST       = 0xCC; //  W W - Reset FIFO / data_less function
    static const u_short REG_FIFO_IO        = 0xCA; // RW W 8 Trigger Type FIFO Buffer I/O
    static const u_short REG_TTYPE_CSR      = 0xC8; // RW W 10 TRIG_TYPE Control & Status Register
    static const u_short REG_TTYPE_REGFILE3 = 0xC6; // RW W 8 Locally stored Trigger Type Register 3
    static const u_short REG_TTYPE_REGFILE2 = 0xC4; // RW W 8 Locally stored Trigger Type Register 2
    static const u_short REG_TTYPE_REGFILE1 = 0xC2; // RW W 8 Locally stored Trigger Type Register 1
    static const u_short REG_TTYPE_REGFILE0 = 0xC0; // RW W 8 Locally stored Trigger Type Register 0

    static const u_short REG_TURN_CR        = 0xBC; // RW W 5 Turn Counter Control & Status Register
    static const u_short REG_32BITCNT_LSW   = 0xBA; //  R R 16 Event Counter low
    static const u_short REG_32BITCNT_MSW   = 0xB8; //  R R 16 Event Counter high
    static const u_short REG_32BITCNT_RST   = 0xB6; //  W W - Reset Counter / data_less function
    static const u_short REG_INH_TIMER      = 0xB4; // RW W 12 Inhibit Timer Duration Counter
    static const u_short REG_ORB_CSR        = 0xB2; // RW W 8 ORBIT Control & Status Register
    static const u_short REG_BC_CSR         = 0xB0; // RW W 3 BC Control & Status Register

    static const u_short REG_BG3_CSR        = 0xA6; // RW W 8 BGo3 Control & Status Register
    static const u_short REG_BG2_CSR        = 0xA4; // RW W 8 BGo2 Control & Status Register
    static const u_short REG_BG1_CSR        = 0xA2; // RW W 8 BGo1 Control & Status Register
    static const u_short REG_BG0_CSR        = 0xA0; // RW W 8 BGo0 Control & Status Register
    
    static const u_short REG_TR3_CSR        = 0x96; // RW W 8 Test_Trig_3 Control & Status Register
    static const u_short REG_TR2_CSR        = 0x94; // RW W 8 Test_Trig_2 Control & Status Register
    static const u_short REG_TR1_CSR        = 0x92; // RW W 8 Test_Trig_1 Control & Status Register
    static const u_short REG_L1A_CSR        = 0x90; // RW W 8 L1A Control & Status Register
    
    static const u_short REG_INTID          = 0x86; // RW W 16 VME Intrp Status/ID register VME IF
    static const u_short REG_INTCSR         = 0x84; // RW W 16 VME Interrupter CSR
    static const u_short REG_SWIRQ          = 0x82; //  W W - VME Interrupt by soft function
    static const u_short REG_SWRST          = 0x80; //  W W - Reset module / data_less function
    static const u_short REG_Cfg_EEPROM     = 0x00; // R(W) W 16 LSBytes in every Long Word. See specs.

    // mask -- VMEInterupt
    static const u_short MSK_VMEINT_LVL     = 0x0007;
    static const u_short MSK_VMEINT_EN      = 0x0008;
    static const u_short MSK_VMEINT_S0_EN   = 0x0010;
    static const u_short MSK_VMEINT_S1_EN   = 0x0020;
    static const u_short MSK_VMEINT_PEND    = 0x0040;
    static const u_short MSK_VMEINT_SQ_STS  = 0x0080;
    static const u_short MSK_VMEINT_S0      = 0x0001;
    static const u_short MSK_VMEINT_S2      = 0x0002;
    static const u_short MSK_VMEINT_VEC     = 0x00FC;

    // data -- VMEInterrupt
    static const u_short VMEINT_EN  = 0x0008;
    static const u_short VMEINT_DIS = 0x0000;

    static const u_short VMEINT_S0_EN  = 0x0010;
    static const u_short VMEINT_S0_DIS = 0x0000;
    static const u_short VMEINT_S1_EN  = 0x0020;
    static const u_short VMEINT_S1_DIS = 0x0000;

    static const u_short VMEINT_PEND   = 0x0040;
    static const u_short VMEINT_ASSERT = 0x0080;

    static const u_short VMEINT_S0_ACT = 0x0001;
    static const u_short VMEINT_S1_ACT = 0x0002;

    // masks -- PG
    static const u_short MSK_PG_RUNMODE    = 0x001f;
    static const u_short MSK_PG_R          = 0xf91f;
    static const u_short MSK_PG_ADDR       = 0x0100;
    static const u_short MSK_PG_AC_MSW     = 0x000f;
    static const u_short MSK_PG_AC_LSW     = 0xffff;

    static const u_short MSK_PG_OPMode     = 0x0003;
    static const u_short MSK_PG_SS_CONT    = 0x0004;
    static const u_short MSK_PG_SS_TRIG    = 0x0008;
    static const u_short MSK_PG_SS_EXT     = 0x0010;

    // data -- PG
    static const u_short PG_RUNMODE_LOAD    = 0x0000;
    static const u_short PG_RUNMODE_RUN     = 0x0001;
    static const u_short PG_RUNMODE_FIXED   = 0x0002;
    static const u_short PG_RUNMODE_NOTUSED = 0x0003;  
    static const u_short PG_RUNMODE_CONT    = 0x0005;  
    static const u_short PG_RUNMODE_SS_VME  = 0x0001;
    static const u_short PG_RUNMODE_SS_ORB  = 0x0009;
    static const u_short PG_RUNMODE_SS_BGO  = 0x0019;

    static const u_int PG_MAXPATTERN = 0xfffff;

    // masks -- i/o
    static const u_short  MSK_SIG_W                  = 0x00ff;
    static const u_short  MSK_SIG_L1A_R              = 0x03ff; // read mask for L1A
    static const u_short  MSK_SIG_NOTL1A_R           = 0x07ff; // read mask for all inputs but L1A
    static const u_short  MSK_SIG_LINKOUT            = 0x0001;
    static const u_short  MSK_SIG_LEMOOUT             = 0x0002;
    static const u_short  MSK_SIG_LOCALSRC           = 0x000C;
    static const u_short  MSK_SIG_BUSY               = 0x0010;
    static const u_short  MSK_SIG_INHIBIT            = 0x0020;
    static const u_short  MSK_SIG_PG                 = 0x0040;
    static const u_short  MSK_SIG_PFN                = 0x0080; 
    static const u_short  MSK_SIG_PULSE              = 0x8000; 

    static const u_short  MSK_SIG_L1A_LINK_STS       = 0x0100;
    static const u_short  MSK_SIG_L1A_LOCAL_STS      = 0x0200;
    static const u_short  MSK_SIG_NOTL1A_LINKIN_STS  = 0x0100;
    static const u_short  MSK_SIG_NOTL1A_LINKOUT_STS = 0x0200;
    static const u_short  MSK_SIG_NOTL1A_LOCAL_STS   = 0x0400;

    // data -- i/o
    static const u_short SIG_LINKOUT_LINK   = 0x0000;
    static const u_short SIG_LINKOUT_LOCAL  = 0x0001;
    static const u_short SIG_LEMOOUT_LINK   = 0x0000;
    static const u_short SIG_LEMOOUT_LOCAL  = 0x0002;
    static const u_short SIG_LOCALSRC_NO    = 0x0000;
    static const u_short SIG_LOCALSRC_FP    = 0x0004;
    static const u_short SIG_LOCALSRC_PFN   = 0x0008;
    static const u_short SIG_LOCALSRC_PG    = 0x000c;
    static const u_short SIG_GATING_NO      = 0x0000;
    static const u_short SIG_GATING_BUSY    = 0x0010;
    static const u_short SIG_GATING_INHIBIT = 0x0020;
    static const u_short SIG_GATING_PG      = 0x0040;
    static const u_short SIG_PFN_1BC        = 0x0000;
    static const u_short SIG_PFN_2BC        = 0x0080;
    static const u_short SIG_PULSE          = 0x8000;
    
    // masks -- BC
    static const u_short MSK_BC_W          = 0x0007;
    static const u_short MSK_BC_R          = 0x0307;
    static const u_short MSK_BC_LINKOUT    = 0x0001;
    static const u_short MSK_BC_LEMOOUT     = 0x0002;
    static const u_short MSK_BC_LOCALSRC   = 0x0004;
    static const u_short MSK_BC_STATUS_EXT = 0x0100;
    static const u_short MSK_BC_STATUS_SEL = 0x0200;

    // data -- BC
    static const u_short BC_LINKOUT_LINK   = 0x0000;
    static const u_short BC_LINKOUT_LOCAL  = 0x0001;
    static const u_short BC_LEMOOUT_LINK   = 0x0000;
    static const u_short BC_LEMOOUT_LOCAL  = 0x0002;
    static const u_short BC_LOCAL_INTERN   = 0x0000;
    static const u_short BC_LOCAL_LEMO      = 0x0004;

    // masks -- Orbit
    static const u_short MSK_ORB_W          = 0x00ff;
    static const u_short MSK_ORB_R          = 0x01ff;
    static const u_short MSK_ORB_LINKOUT    = 0x0001;
    static const u_short MSK_ORB_LEMOOUT     = 0x0002;
    static const u_short MSK_ORB_LOCALSRC   = 0x000c;
    static const u_short MSK_ORB_CNTRSRC    = 0x00f0;
    static const u_short MSK_ORB_STATUS_SEL = 0x0100;
    
    // data -- Orbit
    static const u_short ORB_LINKOUT_LINK    = 0x0000;
    static const u_short ORB_LINKOUT_LOCAL   = 0x0001;
    static const u_short ORB_LEMOOUT_LINK    = 0x0000;
    static const u_short ORB_LEMOOUT_LOCAL   = 0x0002;
    static const u_short ORB_LOCAL_NO        = 0x0000;
    static const u_short ORB_LOCAL_INTERN    = 0x0004;
    static const u_short ORB_LOCAL_LEMO       = 0x0008;
    static const u_short ORB_LOCAL_PG        = 0x000c;

    static const u_short ORB_CNTRSRC_NONE    = 0x0000;
    static const u_short ORB_CNTRSRC_ORB     = 0x0020;
    static const u_short ORB_CNTRSRC_ORB_ECR = 0x0030;
    static const u_short ORB_CNTRSRC_BG0     = 0x0040;
    static const u_short ORB_CNTRSRC_BG1     = 0x0050;
    static const u_short ORB_CNTRSRC_BG2     = 0x0060;
    static const u_short ORB_CNTRSRC_BG3     = 0x0070;
    static const u_short ORB_CNTRSRC_L1A     = 0x0080;
    static const u_short ORB_CNTRSRC_TR1     = 0x0090;
    static const u_short ORB_CNTRSRC_TR2     = 0x00A0;
    static const u_short ORB_CNTRSRC_TR3     = 0x00B0;
    static const u_short ORB_CNTRSRC_L1A_ECR = 0x00C0;
    static const u_short ORB_CNTRSRC_TR1_ECR = 0x00D0;
    static const u_short ORB_CNTRSRC_TR2_ECR = 0x00E0;
    static const u_short ORB_CNTRSRC_TR3_ECR = 0x00F0;

    // masks - Inhibit Timer Shadow Register
    static const u_short MSK_INH_W           = 0x0fff;

    // masks - Turn counter
    static const u_short MSK_TURN_W          = 0x001f;
    static const u_short MSK_TURN_SELCODE    = 0x000f;
    static const u_short MSK_TURN_SEL        = 0x0010;
    
    // data - turn counter
    static const u_short TURN_ORBIT          = 0x0000;
    static const u_short TURN_SEL            = 0x0010;

    // masks - trigger type register file
    static const u_short MSK_TTYPE_FILE       = 0x00ff;
    // masks - trigger type
    static const u_short MSK_TTYPE_W          = 0x03ff;
    static const u_short MSK_TTYPE_R          = 0x33ff;
    
    static const u_short MSK_TTYPE_LINKOUT    = 0x0001;
    static const u_short MSK_TTYPE_ECLOUT     = 0x0002;
    static const u_short MSK_TTYPE_LOCAL      = 0x0004;
    static const u_short MSK_TTYPE_ECL_ENABLE = 0x0008;
    static const u_short MSK_TTYPE_FILEADDR   = 0x0030;
    static const u_short MSK_TTYPE_DELAY      = 0x00C0;
    static const u_short MSK_TTYPE_FIFOSRC    = 0x0300;
    static const u_short MSK_TTYPE_FIFO_EMPTY = 0x2000; 
    static const u_short MSK_TTYPE_FIFO_FULL  = 0x1000; 
    
    // data - trigger type
    static const u_short TTYPE_LINKOUT_LINK   = 0x0000; 
    static const u_short TTYPE_LINKOUT_LOCAL  = 0x0001; 
    static const u_short TTYPE_ECLOUT_LINK    = 0x0000; 
    static const u_short TTYPE_ECLOUT_LOCAL   = 0x0002; 
    static const u_short TTYPE_LOCAL_PG       = 0x0000; 
    static const u_short TTYPE_LOCAL_VME      = 0x0004; 
    static const u_short TTYPE_ECL_EN         = 0x0000; 
    static const u_short TTYPE_ECL_DIS        = 0x0008; 
    static const u_short TTYPE_FILEADDR_0     = 0x0000; 
    static const u_short TTYPE_FILEADDR_1     = 0x0010; 
    static const u_short TTYPE_FILEADDR_2     = 0x0020; 
    static const u_short TTYPE_FILEADDR_3     = 0x0030; 
    static const u_short TTYPE_DELAY_2        = 0x0000; 
    static const u_short TTYPE_DELAY_3        = 0x0040; 
    static const u_short TTYPE_DELAY_4        = 0x0080; 
    static const u_short TTYPE_DELAY_5        = 0x00C0; 
    static const u_short TTYPE_FIFOSRC_L1A    = 0x0000; 
    static const u_short TTYPE_FIFOSRC_TR1    = 0x0100; 
    static const u_short TTYPE_FIFOSRC_TR2    = 0x0200; 
    static const u_short TTYPE_FIFOSRC_TR3    = 0x0300; 

    // masks - calibration request
    static const u_short MSK_CAL_W          = 0x0077;
    static const u_short MSK_CAL_R          = 0x0777;
    static const u_short MSK_CAL_SRC        = 0x0003;
    static const u_short MSK_CAL_TESTPATH   = 0x0004;
    static const u_short MSK_CAL_PRESET     = 0x0070;
    static const u_short MSK_CAL_LINKSTATUS = 0x0700;

    // data - calibration request
    static const u_short CAL_SRC_LINK     = 0x0000;
    static const u_short CAL_SRC_PRESET   = 0x0001;
    static const u_short CAL_SRC_FP       = 0x0002;
    static const u_short CAL_SRC_PG       = 0x0003;
    static const u_short CAL_TESTPATH_EN  = 0x0004;
    static const u_short CAL_TESTPATH_DIS = 0x0000;

    // masks - busy cr
    static const u_short MSK_BUSYCR_W       = 0xF3FF;
    static const u_short MSK_BUSYCR_R       = 0xF3FF;
    static const u_short MSK_BUSYCR_LINKOUT = 0x0001;
    static const u_short MSK_BUSYCR_SUMLINK = 0x0002;
    static const u_short MSK_BUSYCR_SUMTTL  = 0x0004;
    static const u_short MSK_BUSYCR_SUMNIM  = 0x0008;
    static const u_short MSK_BUSYCR_CONST   = 0x0010;
    static const u_short MSK_BUSYCR_SUMDEAD = 0x0020;
    static const u_short MSK_BUSYCR_LEMOOUT  = 0x00C0;
    static const u_short MSK_BUSYCR_DEADSRC = 0x0300;
    static const u_short MSK_BUSYCR_DEADDUR = 0x7000;
    static const u_short MSK_BUSYCR_PG      = 0x8000;

    // data busy cr
    static const u_short BUSY_LINKOUT_LINK       = 0x0000;
    static const u_short BUSY_LINKOUT_LOCAL      = 0x0001;
    static const u_short BUSY_SUMLINK_EN         = 0x0002;
    static const u_short BUSY_SUMLINK_DIS        = 0x0000;
    static const u_short BUSY_SUMTTL_EN          = 0x0004;
    static const u_short BUSY_SUMTTL_DIS         = 0x0000;
    static const u_short BUSY_SUMNIM_EN          = 0x0008;
    static const u_short BUSY_SUMNIM_DIS         = 0x0000;
    static const u_short BUSY_CONST_EN           = 0x0010;
    static const u_short BUSY_CONST_DIS          = 0x0000;
    static const u_short BUSY_SUMDEAD_EN         = 0x0020;
    static const u_short BUSY_SUMDEAD_DIS        = 0x0000;
    static const u_short BUSY_LEMOOUT_BUSYONLY   = 0x0000;
    static const u_short BUSY_LEMOOUT_BUSYORDEAD = 0x0040;
    static const u_short BUSY_LEMOOUT_INH        = 0x0080;
    static const u_short BUSY_LEMOOUT_PGINH      = 0x00C0;
    static const u_short BUSY_DEADSRC_L1A        = 0x0000;
    static const u_short BUSY_DEADSRC_TR1        = 0x0100;
    static const u_short BUSY_DEADSRC_TR2        = 0x0200;
    static const u_short BUSY_DEADSRC_TR3        = 0x0300;
    static const u_short BUSY_PG_EN              = 0x8000;
    static const u_short BUSY_PG_DIS             = 0x0000;
    
    static const u_short BUSY_DEADDUR_POS = 12;

    // masks - busy sr
    static const u_short MSK_BUSYSR_R           = 0x001F;
    static const u_short MSK_BUSYSR_CTPIN       = 0x0001;
    static const u_short MSK_BUSYSR_CTPOUT      = 0x0002;
    static const u_short MSK_BUSYSR_TTL         = 0x0004;
    static const u_short MSK_BUSYSR_NIM         = 0x0008;
    static const u_short MSK_BUSYSR_INTERN      = 0x0010;

    // -- VMEbus driver/library and master mapping
    VME*			m_vme;
    VMEMasterMap*		m_vmm;
    static const u_int		VME_SIZE	= 0x0100;

    // -- general usage parameters
    u_short			m_dats;
  };

}	// namespace RCD

#endif // RCDLTP_H
