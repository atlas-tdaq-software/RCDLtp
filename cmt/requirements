package RCDLtp

author  Thilo.Pauly@cern.ch
manager Thilo.Pauly@cern.ch

public
use Boost * TDAQCExternal
use TDAQPolicy
use rcc_time_stamp
use rcc_error
use RCDMenu
use RCDVme
use ROSGetInput
use owl
use cmdl

macro generate-config-include-dirs "${TDAQ_INST_PATH}/share/data/RCDLtp"

private
        
library RCDLtp                  "RCDLtp.cc"
application menuRCDLtp	        "test/menuRCDLtp.cc"
application setupLTPClock       "test/setupLTPClock.cc"
application ltp_test_standalone	"test/ltp_test_standalone.cc"
application ltp_test_bench	"test/ltp_test_bench.cc"
application BGoTesterForPer     "test/BGoTesterForPer.cc"
application stressLTPReader     "test/stressLTPReader.cc"

#----------------------------------------------------------
# This is for the shared libraries:

macro RCDLtp_shlibflags                 "-lRCDVme -lrcc_time_stamp -lrcc_error"
macro menuRCDLtp_dependencies	        RCDLtp
macro menuRCDLtplinkopts                "-lRCDLtp -lRCDMenu -lcmdline"
macro setupLTPClock_dependencies	RCDLtp
macro setupLTPClocklinkopts             "-lRCDLtp -lRCDMenu -lcmdline -lomnithread -lomniORB4 -lers"
macro ltp_test_standalone_dependencies	vme_rcc
macro ltp_test_standalonelinkopts       "-lvme_rcc -lrcc_time_stamp -lgetinput"
macro ltp_test_bench_dependencies	vme_rcc
macro ltp_test_benchlinkopts            "-lvme_rcc -lrcc_time_stamp -lgetinput"
macro BGoTesterForPer_dependencies	RCDLtp
macro BGoTesterForPerlinkopts "-lRCDLtp -lowl -lomnithread -lomniORB4 -lers -lboost_date_time-$(boost_libsuffix)"
macro stressLTPReaderlinkopts "-lRCDVme -lvme_rcc -lcmdline -lomnithread -lomniORB4 -lers -lboost_date_time-$(boost_libsuffix)"
macro stressLTPReader_dependencies	RCDLtp
#----------------------------------------------------------

public

apply_pattern install_libs files = "libRCDLtp.so"
apply_pattern install_apps files = "menuRCDLtp"
apply_pattern install_apps files = "setupLTPClock"
apply_pattern install_apps files = "ltp_test_standalone"
apply_pattern install_apps files = "ltp_test_bench"
apply_pattern install_apps files = "BGoTesterForPer"
apply_pattern install_apps files = "stressLTPReader"
apply_pattern install_data name=data     src_dir="../data"      files="*.dat" target_dir="data"

#----------------------------------------------------------

