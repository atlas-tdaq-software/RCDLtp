//******************************************************************************
// file: menuRCDLtp.cc
// desc: menu for test of library for LTP
//******************************************************************************

#include <fstream>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <list>
#include <errno.h>
#include <cstdlib>

#include "RCDMenu/RCDMenu.h"
#include "RCDLtp/RCDLtp.h"
#include "DFDebug/DFDebug.h"
#include "cmdl/cmdargs.h"

using namespace RCD;



// global variable
LTP* ltp;

int n_channels = 0;
u_short selchannel;
std::string pgfilename;
std::string conffile;
u_int l1a_delay;

u_int base = 0;


class LTPOpen : public MenuItem {
public:
  LTPOpen() { setName("Open LTP"); }
  int action() {
    int status = 0;

    ltp = new LTP();

    status = ltp->Open(base);

    u_int board_man = 0;
    u_int board_revision = 0;
    u_int board_id = 0;
    status |= ltp->ReadConfigurationEEPROM(board_man, board_revision, board_id);

    printf("\n");
    printf(" Manufacturer: 0x%08x (CERN = 0x00080030)\n", board_man);
    printf(" Revision    : 0x%08x \n", board_revision);
    printf(" Id          : 0x%08x \n", board_id);
    printf("\n");

    bool isltp = ltp->CheckLTP();
    if (isltp) {
      printf(" LTP detected at base address 0x%06x\n", base);
    } else {
      printf(" ERROR: LTP check failed. Check base address 0x%06x.\n", base);
    }

    return status;
  }
};

//------------------------------------------------------------------------------
class LTPClose : public MenuItem {
public:
  LTPClose() { setName("Close LTP"); }
  int action() {
    int status = ltp->Close();
    delete ltp;
    return(status);
  }
};


//------------------------------------------------------------------------------

class LTPReset : public MenuItem {
public:
  LTPReset() { setName("Reset LTP"); }
  int action() {
    return(ltp->Reset());
  }
};

//------------------------------------------------------------------------------
class LTPIOChannel : public MenuItem {
public:
  LTPIOChannel() { setName("Select I/O Channel"); }
  int action() {
    int rtnv = 0;

    std::printf("\n");
    std::printf("*** Select I/O Channel\n\n");
    std::printf("Current i/o channel selection: %d \n", selchannel);
    
    int sel = enterInt("0-quit, 1-L1A, 2-T1, 3-T2, 4-T3, 5-BGo0, 6-BGo1, 7-BGo2, 8-BGo3, 9-all",0,9);
    
    switch (sel) {
    case 0:  // do nothing
      break; 
    default: 
      selchannel = sel;
      break;
    }

    std::cout << std::endl 
	      << "Chosen i/o channel = " << selchannel << " " <<  std::endl;
    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPIOWrite : public MenuItem {
public:
  LTPIOWrite() { setName("Write to Register of Selected Channel"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Write to register of selected channel\n\n");
    u_short w = (u_short) enterHex("Please enter value [hex] ",0x0000,0x00ff);

    switch (selchannel) {
    case 1: rtnv = ltp->IO_write(LTP::L1A, w); break;
    case 2: rtnv = ltp->IO_write(LTP::TR1, w); break;
    case 3: rtnv = ltp->IO_write(LTP::TR2, w); break;
    case 4: rtnv = ltp->IO_write(LTP::TR3, w); break;
    case 5: rtnv = ltp->IO_write(LTP::BG0, w); break;
    case 6: rtnv = ltp->IO_write(LTP::BG1, w); break;
    case 7: rtnv = ltp->IO_write(LTP::BG2, w); break;
    case 8: rtnv = ltp->IO_write(LTP::BG3, w); break;
    case 9: rtnv = ltp->IO_write(LTP::L1A, w); 
      rtnv = ltp->IO_write(LTP::TR1, w);
      rtnv = ltp->IO_write(LTP::TR2, w);      
      rtnv = ltp->IO_write(LTP::TR3, w);      
      rtnv = ltp->IO_write(LTP::BG0, w);
      rtnv = ltp->IO_write(LTP::BG1, w);      
      rtnv = ltp->IO_write(LTP::BG2, w);      
      rtnv = ltp->IO_write(LTP::BG3, w);
      break;    
    }
    
    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPIORead : public MenuItem {
public:
  LTPIORead() { setName("Read from Register of Selected Channel"); }
  int action() {
    int rtnv = 0;
    u_short w;

    switch (selchannel) {
    case 1: 
      rtnv = ltp->IO_read(LTP::L1A, &w); 
      std::printf("    Read value = 0x%04x \n", w);
      break;
    case 2: 
      rtnv = ltp->IO_read(LTP::TR1, &w);
      break;
      std::printf("    Read value = 0x%04x \n", w);
    case 3: 
      rtnv = ltp->IO_read(LTP::TR2, &w);
      std::printf("    Read value = 0x%04x \n", w);
      break;
    case 4: 
      rtnv = ltp->IO_read(LTP::TR3, &w);
      std::printf("    Read value = 0x%04x \n", w);
      break;
    case 5: 
      rtnv = ltp->IO_read(LTP::BG0, &w);
      std::printf("    Read value = 0x%04x \n", w);
      break;
    case 6: 
      rtnv = ltp->IO_read(LTP::BG1, &w);
      std::printf("    Read value = 0x%04x \n", w);
      break;
    case 7: 
      rtnv = ltp->IO_read(LTP::BG2, &w);
      std::printf("    Read value = 0x%04x \n", w);
      break;
    case 8: 
      rtnv = ltp->IO_read(LTP::BG3, &w);
      std::printf("    Read value = 0x%04x \n", w);
      break;
    case 9: 
      rtnv = ltp->IO_read(LTP::L1A, &w); 
      std::printf("    Read value for L1A = 0x%04x \n", w);
      rtnv = ltp->IO_read(LTP::TR1, &w);
      std::printf("    Read value for TR1 = 0x%04x \n", w);
      rtnv = ltp->IO_read(LTP::TR2, &w);      
      std::printf("    Read value for TR2 = 0x%04x \n", w);
      rtnv = ltp->IO_read(LTP::TR3, &w);      
      std::printf("    Read value for TR3 = 0x%04x \n", w);
      rtnv = ltp->IO_read(LTP::BG0, &w);
      std::printf("    Read value for BG0 = 0x%04x \n", w);
      rtnv = ltp->IO_read(LTP::BG1, &w);      
      std::printf("    Read value for BG1 = 0x%04x \n", w);
      rtnv = ltp->IO_read(LTP::BG2, &w);      
      std::printf("    Read value for BG2 = 0x%04x \n", w);
      rtnv = ltp->IO_read(LTP::BG3, &w);
      std::printf("    Read value for BG3 = 0x%04x \n", w);
      break;    
    }
    
    return(rtnv);
  }
};

//------------------------------------------------------------------------------
// Helper class for IOStatus

void printStatus(LTP::channel c) {
    // print status
    std::cout << "\n";
    std::cout << "*** Status of " ;
    if (c==LTP::L1A) std::cout << "L1A";
    if (c==LTP::TR1) std::cout << "TestTrigger 1";
    if (c==LTP::TR2) std::cout << "TestTrigger 2";
    if (c==LTP::TR3) std::cout << "TestTrigger 3";
    if (c==LTP::BG0) std::cout << "BGo-0";
    if (c==LTP::BG1) std::cout << "BGo-1";
    if (c==LTP::BG2) std::cout << "BGo-2";
    if (c==LTP::BG3) std::cout << "BGo-3";
    std::cout << " channel *******************\n\n";

    // Link Out
    if (ltp->IO_linkout_is_from_local(c)) std::printf("  Link out from local sources\n");
    else   std::printf("  Link out from link in\n");
    if (ltp->IO_lemoout_is_from_local(c)) std::printf("  LEMO out from local sources\n");
    else   std::printf("  LEMO out from link in\n");
    LTP::io_source source;
    ltp->IO_local_selection(c, &source);
    if      (source == LTP::IO_NO_SOURCE ) std::printf("  Local source: none\n");
    else if (source == LTP::IO_FP_LEMO    ) std::printf("  Local source: LEMO input direct\n");
    else if (source == LTP::IO_FP_LEMO_PFN) std::printf("  Local source: LEMO input via pulse-forming network\n");
    else if (source == LTP::IO_PATGEN    ) std::printf("  Local source: pattern generator\n");
    else {
      ERR_TEXT(DFDB_RCDLTP,"LTPIOStatus::action() -- wrong bits for local source "); 
    }
    
    if (ltp->IO_busy_gating_is_enabled(c)) std::printf("  Busy gating: enabled\n");
    else   std::printf("  Busy gating: disabled\n");
    if (ltp->IO_inhibit_timer_gating_is_enabled(c)) std::printf("  Inhibit Timer gating: enabled\n");
    else   std::printf("  Inhibit Timer gating: disabled\n");
    if (ltp->IO_pg_gating_is_enabled(c)) std::printf("  Pattern generator gating: enabled\n");
    else   std::printf("  Pattern generator gating: disabled\n");
    if (ltp->IO_pfn_width_is_1bc(c)) std::printf("  Pulse forming network duration: 1 BC\n");
    else   std::printf("  Pulse forming network duration: 2 BC\n");

    // L1A status is different from the other signals:
    if (c==LTP::L1A) {
      if (ltp->L1A_link_is_active()) std::printf("  Link: active\n");
      else   std::printf("  Link: not active\n");
    }
    else {
      if (ltp->IO_linkin_is_active(c)) std::printf("  Link in: active\n");
      else   std::printf("  Link in: not active\n");
      if (ltp->IO_linkout_is_active(c)) std::printf("  Link out: active\n");
      else   std::printf("  Link out: not active\n");
    }
    if (ltp->IO_local_is_active(c)) std::printf("  Local: active\n");
    else   std::printf("  Local: not active\n");

}


//------------------------------------------------------------------------------
class LTPIOStatus: public MenuItem {
public:
  LTPIOStatus() { setName("Channel Status"); }
  int action() {
    int rtnv = 0;

    // get channel
    switch (selchannel) {
    case 1: printStatus(LTP::L1A); break;
    case 2: printStatus(LTP::TR1); break;
    case 3: printStatus(LTP::TR2); break;
    case 4: printStatus(LTP::TR3); break;
    case 5: printStatus(LTP::BG0); break;
    case 6: printStatus(LTP::BG1); break;
    case 7: printStatus(LTP::BG2); break;
    case 8: printStatus(LTP::BG3); break;
    case 9: 
      printStatus(LTP::L1A);      
      printStatus(LTP::TR1);
      printStatus(LTP::TR2);
      printStatus(LTP::TR3);
      printStatus(LTP::BG0);
      printStatus(LTP::BG1);
      printStatus(LTP::BG2);
      printStatus(LTP::BG3);
      break;    
    }

    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPIOISource: public MenuItem {
public:
  LTPIOISource() { setName("Select Source for Link Out"); }
  int action() {
    int rtnv = 0;

    std::printf("\n");
    std::printf("*** Select Source for Link Out\n\n");
    int sel = enterInt("0-quit, 1-link, 2-local",0,2);
    
    switch (sel) {
    case 0: break;
    case 1:
      switch (selchannel) {
      case 1: rtnv = ltp->IO_linkout_from_linkin(LTP::L1A); break;
      case 2: rtnv = ltp->IO_linkout_from_linkin(LTP::TR1); break;
      case 3: rtnv = ltp->IO_linkout_from_linkin(LTP::TR2); break;
      case 4: rtnv = ltp->IO_linkout_from_linkin(LTP::TR3); break;
      case 5: rtnv = ltp->IO_linkout_from_linkin(LTP::BG0); break;
      case 6: rtnv = ltp->IO_linkout_from_linkin(LTP::BG1); break;
      case 7: rtnv = ltp->IO_linkout_from_linkin(LTP::BG2); break;
      case 8: rtnv = ltp->IO_linkout_from_linkin(LTP::BG3); break;
      case 9: 
	rtnv = ltp->IO_linkout_from_linkin(LTP::L1A); 
	rtnv = ltp->IO_linkout_from_linkin(LTP::TR1);
	rtnv = ltp->IO_linkout_from_linkin(LTP::TR2);      
	rtnv = ltp->IO_linkout_from_linkin(LTP::TR3);      
	rtnv = ltp->IO_linkout_from_linkin(LTP::BG0);
	rtnv = ltp->IO_linkout_from_linkin(LTP::BG1);      
	rtnv = ltp->IO_linkout_from_linkin(LTP::BG2);      
	rtnv = ltp->IO_linkout_from_linkin(LTP::BG3);
	break;    
      }
      break;
    case 2:
      switch (selchannel) {
      case 1: rtnv = ltp->IO_linkout_from_local(LTP::L1A); break;
      case 2: rtnv = ltp->IO_linkout_from_local(LTP::TR1); break;
      case 3: rtnv = ltp->IO_linkout_from_local(LTP::TR2); break;
      case 4: rtnv = ltp->IO_linkout_from_local(LTP::TR3); break;
      case 5: rtnv = ltp->IO_linkout_from_local(LTP::BG0); break;
      case 6: rtnv = ltp->IO_linkout_from_local(LTP::BG1); break;
      case 7: rtnv = ltp->IO_linkout_from_local(LTP::BG2); break;
      case 8: rtnv = ltp->IO_linkout_from_local(LTP::BG3); break;
      case 9: 
	rtnv = ltp->IO_linkout_from_local(LTP::L1A); 
	rtnv = ltp->IO_linkout_from_local(LTP::TR1);
	rtnv = ltp->IO_linkout_from_local(LTP::TR2);      
	rtnv = ltp->IO_linkout_from_local(LTP::TR3);      
	rtnv = ltp->IO_linkout_from_local(LTP::BG0);
	rtnv = ltp->IO_linkout_from_local(LTP::BG1);      
	rtnv = ltp->IO_linkout_from_local(LTP::BG2);      
	rtnv = ltp->IO_linkout_from_local(LTP::BG3);
	break;    
      }
      break;
    }
    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPIOOSource: public MenuItem {
public:
  LTPIOOSource() { setName("Select Source for LEMO Output"); }
  int action() {
    int rtnv = 0;
      
    std::printf("\n");
    std::printf("*** Select Source for LEMO Output\n\n");
    int sel = enterInt("0-quit, 1-link, 2-local",0,2);

    switch (sel) {
    case 0: break;
    case 1:
      switch (selchannel) {
      case 1: rtnv = ltp->IO_lemoout_from_linkin(LTP::L1A); break;
      case 2: rtnv = ltp->IO_lemoout_from_linkin(LTP::TR1); break;
      case 3: rtnv = ltp->IO_lemoout_from_linkin(LTP::TR2); break;
      case 4: rtnv = ltp->IO_lemoout_from_linkin(LTP::TR3); break;
      case 5: rtnv = ltp->IO_lemoout_from_linkin(LTP::BG0); break;
      case 6: rtnv = ltp->IO_lemoout_from_linkin(LTP::BG1); break;
      case 7: rtnv = ltp->IO_lemoout_from_linkin(LTP::BG2); break;
      case 8: rtnv = ltp->IO_lemoout_from_linkin(LTP::BG3); break;
      case 9: 
	rtnv = ltp->IO_lemoout_from_linkin(LTP::L1A); 
	rtnv = ltp->IO_lemoout_from_linkin(LTP::TR1);
	rtnv = ltp->IO_lemoout_from_linkin(LTP::TR2);      
	rtnv = ltp->IO_lemoout_from_linkin(LTP::TR3);      
	rtnv = ltp->IO_lemoout_from_linkin(LTP::BG0);
	rtnv = ltp->IO_lemoout_from_linkin(LTP::BG1);      
	rtnv = ltp->IO_lemoout_from_linkin(LTP::BG2);      
	rtnv = ltp->IO_lemoout_from_linkin(LTP::BG3);
	break;    
      }
      break;
    case 2:
      switch (selchannel) {
      case 1: rtnv = ltp->IO_lemoout_from_local(LTP::L1A); break;
      case 2: rtnv = ltp->IO_lemoout_from_local(LTP::TR1); break;
      case 3: rtnv = ltp->IO_lemoout_from_local(LTP::TR2); break;
      case 4: rtnv = ltp->IO_lemoout_from_local(LTP::TR3); break;
      case 5: rtnv = ltp->IO_lemoout_from_local(LTP::BG0); break;
      case 6: rtnv = ltp->IO_lemoout_from_local(LTP::BG1); break;
      case 7: rtnv = ltp->IO_lemoout_from_local(LTP::BG2); break;
      case 8: rtnv = ltp->IO_lemoout_from_local(LTP::BG3); break;
      case 9: 
	rtnv = ltp->IO_lemoout_from_local(LTP::L1A); 
	rtnv = ltp->IO_lemoout_from_local(LTP::TR1);
	rtnv = ltp->IO_lemoout_from_local(LTP::TR2);      
	rtnv = ltp->IO_lemoout_from_local(LTP::TR3);      
	rtnv = ltp->IO_lemoout_from_local(LTP::BG0);
	rtnv = ltp->IO_lemoout_from_local(LTP::BG1);      
	rtnv = ltp->IO_lemoout_from_local(LTP::BG2);      
	rtnv = ltp->IO_lemoout_from_local(LTP::BG3);
	break;    
      }
      break;
    }
    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPIOLocalISource   : public MenuItem {
public:
  LTPIOLocalISource() { setName("Select Source for Local Signal"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Select Source for Local Signal\n\n");
    std::printf("   FP  = Front Panel\n");
    std::printf("   PFN = Pulse Forming Network\n");
    std::printf("   PG  = Pattern Generator\n\n");

    std::printf("Please select:\n");
    int sel = enterInt("0-quit, 1-no source, 2-FP, 3-FP:PFN, 4-PG, ",0,4);

    switch (sel) {
    case 0: break;
    case 1:  // no source
      switch (selchannel) {
      case 1: rtnv = ltp->IO_local_no_source(LTP::L1A); break;
      case 2: rtnv = ltp->IO_local_no_source(LTP::TR1); break;
      case 3: rtnv = ltp->IO_local_no_source(LTP::TR2); break;
      case 4: rtnv = ltp->IO_local_no_source(LTP::TR3); break;
      case 5: rtnv = ltp->IO_local_no_source(LTP::BG0); break;
      case 6: rtnv = ltp->IO_local_no_source(LTP::BG1); break;
      case 7: rtnv = ltp->IO_local_no_source(LTP::BG2); break;
      case 8: rtnv = ltp->IO_local_no_source(LTP::BG3); break;
      case 9: 
	rtnv = ltp->IO_local_no_source(LTP::L1A); 
	rtnv = ltp->IO_local_no_source(LTP::TR1);
	rtnv = ltp->IO_local_no_source(LTP::TR2);      
	rtnv = ltp->IO_local_no_source(LTP::TR3);      
	rtnv = ltp->IO_local_no_source(LTP::BG0);
	rtnv = ltp->IO_local_no_source(LTP::BG1);      
	rtnv = ltp->IO_local_no_source(LTP::BG2);      
	rtnv = ltp->IO_local_no_source(LTP::BG3);
	break;    
      }
      break;
    case 2: // FP direct
      switch (selchannel) {
      case 1: rtnv = ltp->IO_local_lemo_direct(LTP::L1A); break;
      case 2: rtnv = ltp->IO_local_lemo_direct(LTP::TR1); break;
      case 3: rtnv = ltp->IO_local_lemo_direct(LTP::TR2); break;
      case 4: rtnv = ltp->IO_local_lemo_direct(LTP::TR3); break;
      case 5: rtnv = ltp->IO_local_lemo_direct(LTP::BG0); break;
      case 6: rtnv = ltp->IO_local_lemo_direct(LTP::BG1); break;
      case 7: rtnv = ltp->IO_local_lemo_direct(LTP::BG2); break;
      case 8: rtnv = ltp->IO_local_lemo_direct(LTP::BG3); break;
      case 9: rtnv = ltp->IO_local_lemo_direct(LTP::L1A); 
	rtnv = ltp->IO_local_lemo_direct(LTP::TR1);
	rtnv = ltp->IO_local_lemo_direct(LTP::TR2);      
	rtnv = ltp->IO_local_lemo_direct(LTP::TR3);      
	rtnv = ltp->IO_local_lemo_direct(LTP::BG0);
	rtnv = ltp->IO_local_lemo_direct(LTP::BG1);      
	rtnv = ltp->IO_local_lemo_direct(LTP::BG2);      
	rtnv = ltp->IO_local_lemo_direct(LTP::BG3);
	break;    
      }
      break;
    case 3: // FP:PFN
      switch (selchannel) {
      case 1: rtnv = ltp->IO_local_lemo_pfn(LTP::L1A); break;
      case 2: rtnv = ltp->IO_local_lemo_pfn(LTP::TR1); break;
      case 3: rtnv = ltp->IO_local_lemo_pfn(LTP::TR2); break;
      case 4: rtnv = ltp->IO_local_lemo_pfn(LTP::TR3); break;
      case 5: rtnv = ltp->IO_local_lemo_pfn(LTP::BG0); break;
      case 6: rtnv = ltp->IO_local_lemo_pfn(LTP::BG1); break;
      case 7: rtnv = ltp->IO_local_lemo_pfn(LTP::BG2); break;
      case 8: rtnv = ltp->IO_local_lemo_pfn(LTP::BG3); break;
      case 9: rtnv = ltp->IO_local_lemo_pfn(LTP::L1A); 
	rtnv = ltp->IO_local_lemo_pfn(LTP::TR1);
	rtnv = ltp->IO_local_lemo_pfn(LTP::TR2);      
	rtnv = ltp->IO_local_lemo_pfn(LTP::TR3);      
	rtnv = ltp->IO_local_lemo_pfn(LTP::BG0);
	rtnv = ltp->IO_local_lemo_pfn(LTP::BG1);      
	rtnv = ltp->IO_local_lemo_pfn(LTP::BG2);      
	rtnv = ltp->IO_local_lemo_pfn(LTP::BG3);
	break;    
      }
      break;
    case 4:  // PG
      switch (selchannel) {
      case 1: rtnv = ltp->IO_local_pg(LTP::L1A); break;
      case 2: rtnv = ltp->IO_local_pg(LTP::TR1); break;
      case 3: rtnv = ltp->IO_local_pg(LTP::TR2); break;
      case 4: rtnv = ltp->IO_local_pg(LTP::TR3); break;
      case 5: rtnv = ltp->IO_local_pg(LTP::BG0); break;
      case 6: rtnv = ltp->IO_local_pg(LTP::BG1); break;
      case 7: rtnv = ltp->IO_local_pg(LTP::BG2); break;
      case 8: rtnv = ltp->IO_local_pg(LTP::BG3); break;
      case 9: rtnv = ltp->IO_local_pg(LTP::L1A); 
	rtnv = ltp->IO_local_pg(LTP::TR1);
	rtnv = ltp->IO_local_pg(LTP::TR2);      
	rtnv = ltp->IO_local_pg(LTP::TR3);      
	rtnv = ltp->IO_local_pg(LTP::BG0);
	rtnv = ltp->IO_local_pg(LTP::BG1);      
	rtnv = ltp->IO_local_pg(LTP::BG2);      
	rtnv = ltp->IO_local_pg(LTP::BG3);
	break;    
      }
      break;
    }
    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPIOPFN   : public MenuItem {
public:
  LTPIOPFN() { setName("Duration of Pulse Forming Network"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Duration of Pulse Forming Network\n\n");

    int sel = enterInt("0-quit, 1-1BC, 2-2BC ",0,2);

    switch (sel) {
    case 0: break;
    case 1:
      switch (selchannel) {
      case 1: rtnv = ltp->IO_pfn_width_1bc(LTP::L1A); break;
      case 2: rtnv = ltp->IO_pfn_width_1bc(LTP::TR1); break;
      case 3: rtnv = ltp->IO_pfn_width_1bc(LTP::TR2); break;
      case 4: rtnv = ltp->IO_pfn_width_1bc(LTP::TR3); break;
      case 5: rtnv = ltp->IO_pfn_width_1bc(LTP::BG0); break;
      case 6: rtnv = ltp->IO_pfn_width_1bc(LTP::BG1); break;
      case 7: rtnv = ltp->IO_pfn_width_1bc(LTP::BG2); break;
      case 8: rtnv = ltp->IO_pfn_width_1bc(LTP::BG3); break;
      case 9: rtnv = ltp->IO_pfn_width_1bc(LTP::L1A); 
	rtnv = ltp->IO_pfn_width_1bc(LTP::TR1);
	rtnv = ltp->IO_pfn_width_1bc(LTP::TR2);      
	rtnv = ltp->IO_pfn_width_1bc(LTP::TR3);      
	rtnv = ltp->IO_pfn_width_1bc(LTP::BG0);
	rtnv = ltp->IO_pfn_width_1bc(LTP::BG1);      
	rtnv = ltp->IO_pfn_width_1bc(LTP::BG2);      
	rtnv = ltp->IO_pfn_width_1bc(LTP::BG3);
	break;    
      }
      break;
    case 2:
      switch (selchannel) {
      case 1: rtnv = ltp->IO_pfn_width_2bc(LTP::L1A); break;
      case 2: rtnv = ltp->IO_pfn_width_2bc(LTP::TR1); break;
      case 3: rtnv = ltp->IO_pfn_width_2bc(LTP::TR2); break;
      case 4: rtnv = ltp->IO_pfn_width_2bc(LTP::TR3); break;
      case 5: rtnv = ltp->IO_pfn_width_2bc(LTP::BG0); break;
      case 6: rtnv = ltp->IO_pfn_width_2bc(LTP::BG1); break;
      case 7: rtnv = ltp->IO_pfn_width_2bc(LTP::BG2); break;
      case 8: rtnv = ltp->IO_pfn_width_2bc(LTP::BG3); break;
      case 9: rtnv = ltp->IO_pfn_width_2bc(LTP::L1A); 
	rtnv = ltp->IO_pfn_width_2bc(LTP::TR1);
	rtnv = ltp->IO_pfn_width_2bc(LTP::TR2);      
	rtnv = ltp->IO_pfn_width_2bc(LTP::TR3);      
	rtnv = ltp->IO_pfn_width_2bc(LTP::BG0);
	rtnv = ltp->IO_pfn_width_2bc(LTP::BG1);      
	rtnv = ltp->IO_pfn_width_2bc(LTP::BG2);      
	rtnv = ltp->IO_pfn_width_2bc(LTP::BG3);
	break;    
      }
      break;
    }
    return(rtnv);
  }
};


//------------------------------------------------------------------------------
class LTPIOPulse   : public MenuItem {
public:
  LTPIOPulse() { setName("Software pulse (not possible for L1A)"); }
  int action() {

    int npulse = enterInt("Number of pulses: ",0,100000);

    int rtnv = 0;
    for (int i = 0; i < npulse; ++i) {
      switch (selchannel) {
      case 1: rtnv = -1; break;
      case 2: rtnv = ltp->IO_sw_pulse(LTP::TR1); break;
      case 3: rtnv = ltp->IO_sw_pulse(LTP::TR2); break;
      case 4: rtnv = ltp->IO_sw_pulse(LTP::TR3); break;
      case 5: rtnv = ltp->IO_sw_pulse(LTP::BG0); break;
      case 6: rtnv = ltp->IO_sw_pulse(LTP::BG1); break;
      case 7: rtnv = ltp->IO_sw_pulse(LTP::BG2); break;
      case 8: rtnv = ltp->IO_sw_pulse(LTP::BG3); break;
      case 9: 
	rtnv = ltp->IO_sw_pulse(LTP::TR1);
	rtnv = ltp->IO_sw_pulse(LTP::TR2);      
	rtnv = ltp->IO_sw_pulse(LTP::TR3);      
	rtnv = ltp->IO_sw_pulse(LTP::BG0);
	rtnv = ltp->IO_sw_pulse(LTP::BG1);      
	rtnv = ltp->IO_sw_pulse(LTP::BG2);      
	rtnv = ltp->IO_sw_pulse(LTP::BG3);
	break;
      }
    }
    return(rtnv);
  }
};



//------------------------------------------------------------------------------
class LTPIOBusy  : public MenuItem {
public:
  LTPIOBusy() { setName("Enable Busy Gating"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Enable Busy Gating\n\n");
    int sel = enterInt("0-quit, 1-disable, 2-enable",0,2);

    switch (sel) {
    case 0: break;
    case 1:
      switch (selchannel) {
      case 1: rtnv = ltp->IO_disable_busy_gating(LTP::L1A); break;
      case 2: rtnv = ltp->IO_disable_busy_gating(LTP::TR1); break;
      case 3: rtnv = ltp->IO_disable_busy_gating(LTP::TR2); break;
      case 4: rtnv = ltp->IO_disable_busy_gating(LTP::TR3); break;
      case 5: rtnv = ltp->IO_disable_busy_gating(LTP::BG0); break;
      case 6: rtnv = ltp->IO_disable_busy_gating(LTP::BG1); break;
      case 7: rtnv = ltp->IO_disable_busy_gating(LTP::BG2); break;
      case 8: rtnv = ltp->IO_disable_busy_gating(LTP::BG3); break;
      case 9: rtnv = ltp->IO_disable_busy_gating(LTP::L1A); 
	rtnv = ltp->IO_disable_busy_gating(LTP::TR1);
	rtnv = ltp->IO_disable_busy_gating(LTP::TR2);      
	rtnv = ltp->IO_disable_busy_gating(LTP::TR3);      
	rtnv = ltp->IO_disable_busy_gating(LTP::BG0);
	rtnv = ltp->IO_disable_busy_gating(LTP::BG1);      
	rtnv = ltp->IO_disable_busy_gating(LTP::BG2);      
	rtnv = ltp->IO_disable_busy_gating(LTP::BG3);
	break;    
      }
      break;
    case 2:
      switch (selchannel) {
      case 1: rtnv = ltp->IO_enable_busy_gating(LTP::L1A); break;
      case 2: rtnv = ltp->IO_enable_busy_gating(LTP::TR1); break;
      case 3: rtnv = ltp->IO_enable_busy_gating(LTP::TR2); break;
      case 4: rtnv = ltp->IO_enable_busy_gating(LTP::TR3); break;
      case 5: rtnv = ltp->IO_enable_busy_gating(LTP::BG0); break;
      case 6: rtnv = ltp->IO_enable_busy_gating(LTP::BG1); break;
      case 7: rtnv = ltp->IO_enable_busy_gating(LTP::BG2); break;
      case 8: rtnv = ltp->IO_enable_busy_gating(LTP::BG3); break;
      case 9: rtnv = ltp->IO_enable_busy_gating(LTP::L1A); 
	rtnv = ltp->IO_enable_busy_gating(LTP::TR1);
	rtnv = ltp->IO_enable_busy_gating(LTP::TR2);      
	rtnv = ltp->IO_enable_busy_gating(LTP::TR3);      
	rtnv = ltp->IO_enable_busy_gating(LTP::BG0);
	rtnv = ltp->IO_enable_busy_gating(LTP::BG1);      
	rtnv = ltp->IO_enable_busy_gating(LTP::BG2);      
	rtnv = ltp->IO_enable_busy_gating(LTP::BG3);
	break;    
      }
      break;
    }
    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPIOInhibit: public MenuItem {
public:
  LTPIOInhibit() { setName("Enable Inhibit Timer Gating"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Enable Inhibit Timer Gating\n\n");
    int sel = enterInt("0-quit, 1-disable, 2-enable",0,2);

    switch (sel) {
    case 0: break;
    case 1:
      switch (selchannel) {
      case 1: rtnv = ltp->IO_disable_inhibit_gating(LTP::L1A); break;
      case 2: rtnv = ltp->IO_disable_inhibit_gating(LTP::TR1); break;
      case 3: rtnv = ltp->IO_disable_inhibit_gating(LTP::TR2); break;
      case 4: rtnv = ltp->IO_disable_inhibit_gating(LTP::TR3); break;
      case 5: rtnv = ltp->IO_disable_inhibit_gating(LTP::BG0); break;
      case 6: rtnv = ltp->IO_disable_inhibit_gating(LTP::BG1); break;
      case 7: rtnv = ltp->IO_disable_inhibit_gating(LTP::BG2); break;
      case 8: rtnv = ltp->IO_disable_inhibit_gating(LTP::BG3); break;
      case 9: rtnv = ltp->IO_disable_inhibit_gating(LTP::L1A); 
	rtnv = ltp->IO_disable_inhibit_gating(LTP::TR1);
	rtnv = ltp->IO_disable_inhibit_gating(LTP::TR2);      
	rtnv = ltp->IO_disable_inhibit_gating(LTP::TR3);      
	rtnv = ltp->IO_disable_inhibit_gating(LTP::BG0);
	rtnv = ltp->IO_disable_inhibit_gating(LTP::BG1);      
	rtnv = ltp->IO_disable_inhibit_gating(LTP::BG2);      
	rtnv = ltp->IO_disable_inhibit_gating(LTP::BG3);
	break;    
      }
      break;
    case 2:
      switch (selchannel) {
      case 1: rtnv = ltp->IO_enable_inhibit_gating(LTP::L1A); break;
      case 2: rtnv = ltp->IO_enable_inhibit_gating(LTP::TR1); break;
      case 3: rtnv = ltp->IO_enable_inhibit_gating(LTP::TR2); break;
      case 4: rtnv = ltp->IO_enable_inhibit_gating(LTP::TR3); break;
      case 5: rtnv = ltp->IO_enable_inhibit_gating(LTP::BG0); break;
      case 6: rtnv = ltp->IO_enable_inhibit_gating(LTP::BG1); break;
      case 7: rtnv = ltp->IO_enable_inhibit_gating(LTP::BG2); break;
      case 8: rtnv = ltp->IO_enable_inhibit_gating(LTP::BG3); break;
      case 9: rtnv = ltp->IO_enable_inhibit_gating(LTP::L1A); 
	rtnv = ltp->IO_enable_inhibit_gating(LTP::TR1);
	rtnv = ltp->IO_enable_inhibit_gating(LTP::TR2);      
	rtnv = ltp->IO_enable_inhibit_gating(LTP::TR3);      
	rtnv = ltp->IO_enable_inhibit_gating(LTP::BG0);
	rtnv = ltp->IO_enable_inhibit_gating(LTP::BG1);      
	rtnv = ltp->IO_enable_inhibit_gating(LTP::BG2);      
	rtnv = ltp->IO_enable_inhibit_gating(LTP::BG3);
	break;    
      }
      break;
    }
    return(rtnv);
  }
    
};

//------------------------------------------------------------------------------
class LTPIOPG    : public MenuItem {
public:
  LTPIOPG() { setName("Enable Pattern Generator Gating"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Enable Pattern Generator Gating\n\n");
    int sel = enterInt("0-quit, 1-disable, 2-enable",0,2);

    switch (sel) {
    case 0: break;
    case 1:
      switch (selchannel) {
      case 1: rtnv = ltp->IO_disable_pg_gating(LTP::L1A); break;
      case 2: rtnv = ltp->IO_disable_pg_gating(LTP::TR1); break;
      case 3: rtnv = ltp->IO_disable_pg_gating(LTP::TR2); break;
      case 4: rtnv = ltp->IO_disable_pg_gating(LTP::TR3); break;
      case 5: rtnv = ltp->IO_disable_pg_gating(LTP::BG0); break;
      case 6: rtnv = ltp->IO_disable_pg_gating(LTP::BG1); break;
      case 7: rtnv = ltp->IO_disable_pg_gating(LTP::BG2); break;
      case 8: rtnv = ltp->IO_disable_pg_gating(LTP::BG3); break;
      case 9: rtnv = ltp->IO_disable_pg_gating(LTP::L1A); 
	rtnv = ltp->IO_disable_pg_gating(LTP::TR1);
	rtnv = ltp->IO_disable_pg_gating(LTP::TR2);      
	rtnv = ltp->IO_disable_pg_gating(LTP::TR3);      
	rtnv = ltp->IO_disable_pg_gating(LTP::BG0);
	rtnv = ltp->IO_disable_pg_gating(LTP::BG1);      
	rtnv = ltp->IO_disable_pg_gating(LTP::BG2);      
	rtnv = ltp->IO_disable_pg_gating(LTP::BG3);
	break;    
      }
      break;
    case 2:
      switch (selchannel) {
      case 1: rtnv = ltp->IO_enable_pg_gating(LTP::L1A); break;
      case 2: rtnv = ltp->IO_enable_pg_gating(LTP::TR1); break;
      case 3: rtnv = ltp->IO_enable_pg_gating(LTP::TR2); break;
      case 4: rtnv = ltp->IO_enable_pg_gating(LTP::TR3); break;
      case 5: rtnv = ltp->IO_enable_pg_gating(LTP::BG0); break;
      case 6: rtnv = ltp->IO_enable_pg_gating(LTP::BG1); break;
      case 7: rtnv = ltp->IO_enable_pg_gating(LTP::BG2); break;
      case 8: rtnv = ltp->IO_enable_pg_gating(LTP::BG3); break;
      case 9: rtnv = ltp->IO_enable_pg_gating(LTP::L1A); 
	rtnv = ltp->IO_enable_pg_gating(LTP::TR1);
	rtnv = ltp->IO_enable_pg_gating(LTP::TR2);      
	rtnv = ltp->IO_enable_pg_gating(LTP::TR3);      
	rtnv = ltp->IO_enable_pg_gating(LTP::BG0);
	rtnv = ltp->IO_enable_pg_gating(LTP::BG1);      
	rtnv = ltp->IO_enable_pg_gating(LTP::BG2);      
	rtnv = ltp->IO_enable_pg_gating(LTP::BG3);
	break;    
      }
      break;
    }
    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPPGLoadFile    : public MenuItem {
public:
  LTPPGLoadFile() { setName("Load Pattern File into Memory"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    rtnv = ltp->PG_load_memory_from_file(&pgfilename);
    
    if (rtnv != 0) {
      std::printf(" ERROR: could not load pattern from file %s\n", pgfilename.c_str());
    }
    else {
    std::printf("*** Load Pattern File into memory\n\n");
    }

    // reset pattern generator
    rtnv |= ltp->PG_reset();

    return(rtnv);
  }
};


//------------------------------------------------------------------------------
class LTPPGReadFixedValue    : public MenuItem {
public:
  LTPPGReadFixedValue() { setName("Read Fixed Value Register"); }
  int action() {
    u_short val;
    std::printf("\n");
    std::printf("*** Read Fixed Value Register\n\n");
    int rtnv = ltp->PG_get_fixed_value(&val);
    std::printf("   Read value %04x from fixed value register\n",val);
    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPPGWriteFixedValue    : public MenuItem {
public:
  LTPPGWriteFixedValue() { setName("Write to Fixed Value Register"); }
  int action() {
    u_short val;
    std::printf("\n");
    std::printf("*** Write to Fixed Value Register\n\n");
    ltp->PG_get_fixed_value(&val);
    std::printf("   Read value %04x from fixed value register\n",val);
    std::printf("Please enter new value [hex]: ");
    std::cin >> std::hex >> val >> std::dec;
    int rtnv = ltp->PG_set_fixed_value(val);
    return rtnv;
  }
};

//------------------------------------------------------------------------------
class LTPPGStatus    : public MenuItem {
public:
  LTPPGStatus() { setName("Status"); }
  int action() {

    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Pattern Generator Status ****************\n\n");
    
    enum LTP::pg_op_mode mode;
    ltp->PG_runmode(&mode);
    if      (mode == LTP::PG_PRE_LOAD) std::printf("  Operation Mode: pre-defined o/p or load memory\n");
    else if (mode == LTP::PG_RUNNING)  std::printf("  Operation Mode: running\n");
    else if (mode == LTP::PG_FIXED)    std::printf("  Operation Mode: fixed programmed values\n");
    else if (mode == LTP::PG_NOTUSED)  std::printf("  Operation Mode: not used\n");
    else {
      std::printf("  PG Operation Mode: ERROR\n");
    }

    if (ltp->PG_is_in_continuous_mode())                  std::printf("  PG is in: continuous mode \n");
    else                                                  std::printf("  PG is in: single-shot mode\n");
    if (ltp->PG_single_shot_trigger_source_is_external()) std::printf("  Single-shot trigger source is: external \n");
    else                                                  std::printf("  Single-shot trigger source is: VME\n");
    if (ltp->PG_external_source_is_BGo3())                std::printf("  External single-shot trigger source is: BGo-3\n");
    else                                                  std::printf("  External single-shot trigger source is: selected Orbit source\n");
    return(rtnv);
  }
};


//------------------------------------------------------------------------------
class LTPPGSetFileName    : public MenuItem {
public:

  LTPPGSetFileName() { setName("Set Pattern File Name"); }
  int action() {
//     // default filenames:
//     char filea[500] = "pg_A.dat";
//     char fileb[500] = "pg_B.dat" ;
//     char filec[500] = "pg_C.dat" ;

    int rtnv = 0;
    
    std::printf("\n");
    std::printf("*** Set Pattern File Name\n\n");
    std::printf("  The pattern file contains the patterns that will be loaded\n");
    std::printf("  into the memory of the pattern generator. It contains the\n");
    std::printf("  the bit pattern in 4 4-bit blocks and the multiplicity of\n");
    std::printf("  that pattern. Comment lines start with #. Here an example:\n");
    std::printf("\n");
    std::printf("  #                     M \n");
    std::printf("  #                     u \n");
    std::printf("  #                     l \n");
    std::printf("  #                     t \n");
    std::printf("  #                     i \n");
    std::printf("  #                     p \n");
    std::printf("  #                     l \n");
    std::printf("  #  ICC C     TT     O i \n");
    std::printf("  # Bnaa aBBB BTT     r c \n");
    std::printf("  # uhll lGGG GYYT TTLb i \n");
    std::printf("  # siRR Rooo oPPT TT1i t \n");
    std::printf("  # yb21 0321 0103 21At y \n");
    std::printf("  #                       \n");
    std::printf("    1000 0100 0001 1111 3 \n");
    std::printf("    0000 0110 0001 1100 10 \n");
    std::printf("\n\n");
    std::printf("Current filename: %s  (relative to current directory)\n", pgfilename.c_str());
//     std::printf("Available: FileA: %s\n", filea);
//     std::printf("           FileB: %s\n", fileb);
//     std::printf("           FileC: %s\n", filec);
    std::printf("\n");

    int sel = enterInt("0-quit, 1-enter file",0,1);
    switch (sel) {
    case 0: 
      break;
    case 1:
      std::printf("\n");
      std::printf("Please enter file name (relative to current directory): ");
      std::cin >> pgfilename;
      break;
    }
    std::printf("\n");
    std::printf("Selected filename: %s", pgfilename.c_str());

    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPConfSetFileName    : public MenuItem {
public:

  LTPConfSetFileName() { setName("Set Pattern File Name"); }
  int action() {

    int rtnv = 0;
    
    std::printf("\n");
    std::printf("*** Set Configuration File Name\n\n");
    std::printf("Current filename: %s  (relative to current directory)\n", conffile.c_str());
    std::printf("\n");

    int sel = enterInt("0-quit, 1-enter file",0,1);
    switch (sel) {
    case 0: 
      break;
    case 1:
      std::printf("\n");
      std::printf("Please enter file name (relative to current directory): ");
      std::cin >> conffile;
      break;
    }
    std::printf("\n");
    std::printf("Selected filename: %s", conffile.c_str());

    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPPGStart    : public MenuItem {
public:
  LTPPGStart() { setName("Start Pattern Generator"); }
  int action() {
    int rtnv = ltp->PG_start();
    return(rtnv);
  }
};


//------------------------------------------------------------------------------
class LTPPGTrigger    : public MenuItem {
public:
  LTPPGTrigger() { setName("Trigger Pattern Generator by VME"); }
  int action() {
    int rtnv(0);

    int n = enterInt("Number of sequences: ", 0, 1000000);
    rtnv = ltp->PG_reset();
    for (int i = 0; i<n; ++i) {
      rtnv |= ltp->PG_start();
      // wait until sequence is finished
      do {} while(!ltp->PG_is_idle());
    }

    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPPGReset    : public MenuItem {
public:
  LTPPGReset() { setName("Reset Pattern Generator FSM"); }
  int action() {
    int rtnv = ltp->PG_reset();
    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPPGReadMemory    : public MenuItem {
public:
  LTPPGReadMemory() { setName("Read Memory"); }
  int action() {
    int rtnv = 0;

    // stop pattern generator
    rtnv |= ltp->PG_runmode_load();

    // reset pattern generator
    rtnv |= ltp->PG_reset();

    // read number of values
    u_int count = 0;
    rtnv |= ltp->PG_read_address_counter(&count);

    // reset pattern generator
    rtnv |= ltp->PG_reset();

    std::printf("\n Number of patterns in memory: %d\n\n",count);

    // decide whether to print them or to continue
    int sel = enterInt("0-quit, 1-read&print ",0,1);
    std::printf("\n\n");

    switch (sel) {
    case 0: break;
    case 1: 
      rtnv |= ltp->PG_read_from_memory();
      break;
    }

    std::printf("\n\n");
    return(rtnv);
  }
};

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
class LTPPGRun    : public MenuItem {
public:

  LTPPGRun() { setName("Set Run Mode"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Set Run Mode\n\n");
    std::printf("  - fixed:       Use fix value register levels\n");
    std::printf("  - cont:        Continously run from memory\n");
    std::printf("  - single:vme   Single shot mode triggered by VME\n");
    std::printf("  - single:orbit Single shot mode triggered by Orbit\n");
    std::printf("  - single:BGo3  Single shot mode triggered by BGo-3\n");
    std::printf("\n");

    u_int mode = enterInt("0-off, 1-fixed, 2-cont, 3-single:vme, 4-single:orbit, 5-single:BGo3 ",0,5);

    // reset state machine
    ltp->PG_reset();

    switch(mode) {
    case 0: // switch off or load memory
      rtnv = ltp->PG_runmode_load();
      break;
    case 1: // fixed value register
      rtnv = ltp->PG_runmode_fixedvalue();
      break;
    case 2: // continous
      rtnv = ltp->PG_runmode_cont();
      break;
    case 3: // single shot triggered by vme
      rtnv = ltp->PG_runmode_singleshot_vme();
      break;
    case 4: // single shot triggered by orbit source
      rtnv = ltp->PG_runmode_singleshot_orb();
      break;
    case 5: // single shot triggerd by BGo-3
      rtnv = ltp->PG_runmode_singleshot_bgo();
      break;
    }
    
    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPBCWrite : public MenuItem {
public:
  LTPBCWrite() { setName("Write to BC Register"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Write to BC Register\n\n");
    u_short w = (u_short) enterHex("Please enter value [hex] ",0x0000,0x00ff);
    return rtnv = ltp->BC_write(w);
  }
};

//------------------------------------------------------------------------------
class LTPBCRead  : public MenuItem {
public:
  LTPBCRead() { setName("Read from BC Register"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Read from BC Register\n\n");
    u_short w;
    rtnv = ltp->BC_read(&w);

    std::printf("    Read value = 0x%04x \n", w);

    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPBCStatus : public MenuItem {
public:
  LTPBCStatus() { setName("BC Status"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** BC Status *******************************\n\n");
    
    if (ltp->BC_linkout_is_from_local())          std::printf("  BC linkout is from: local\n");
    else                                          std::printf("  BC linkout is from: link in\n");
    if (ltp->BC_lemoout_is_from_local())           std::printf("  BC LEMO out is from: local\n");
    else                                          std::printf("  BC LEMO out is from: link in\n");
    if (ltp->BC_local_is_from_lemoin()) std::printf("  BC local source is from: LEMO in\n");
    else                                          std::printf("  BC local source is from: internal\n");
    if (ltp->BC_external_is_running())            std::printf("  External LEMO BC clock is running\n");
    else                                          std::printf("  External LEMO BC clock is NOT running\n");
    if (ltp->BC_selected_is_running())            std::printf("  Selected BC clock is running\n");
    else                                          std::printf("  Selected BC clock is NOT running\n");

    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPBCLinkInputSource : public MenuItem {
public:
  LTPBCLinkInputSource() { setName("Select BC Source for Link Out"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Select BC Source for Link Out\n\n");
    int sel = enterInt("0-quit, 1-link, 2-local",0,2);
    switch (sel) {
    case 0: return(0)                           ; break;
    case 1: rtnv = ltp->BC_linkout_from_linkin(); break;
    case 2: rtnv = ltp->BC_linkout_from_local (); break;
    }
    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPBCLemoInputSource : public MenuItem {
public:
  LTPBCLemoInputSource() { setName("Select Source for BC LEMO Output"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Select Source for BC LEMO Output\n\n");
    int sel = enterInt("0-quit, 1-link, 2-local",0,2);
    switch (sel) {
    case 0: return(0)                          ; break;
    case 1: rtnv = ltp->BC_lemoout_from_linkin(); break;
    case 2: rtnv = ltp->BC_lemoout_from_local (); break;
    }
    return(rtnv);
  }
};
//------------------------------------------------------------------------------
class  LTPBCLocalSource: public MenuItem {
public:
  LTPBCLocalSource() { setName("Select Source for Local BC"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Select Source for Local BC\n\n");
    int sel = enterInt("0-quit, 1-intern, 2-LEMO ip",0,2);
    switch (sel) {
    case 0: return(0)                    ; break;
    case 1: rtnv = ltp->BC_local_intern(); break;
    case 2: rtnv = ltp->BC_local_lemo   (); break;
    }
    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPOrbitWrite : public MenuItem {
public:
  LTPOrbitWrite() { setName("Write to Orbit Register"); }
  int action() {
    std::printf("\n");
    std::printf("*** Write to Orbit Register\n\n");
    int rtnv = 0;
    u_short w = (u_short) enterHex("Please enter value [hex] ",0x0000,0x00ff);
    return rtnv = ltp->ORB_write(w);
  }
};

//------------------------------------------------------------------------------
class LTPOrbitRead  : public MenuItem {
public:
  LTPOrbitRead() { setName("Read from Orbit Register"); }
  int action() {
    int rtnv = 0;
    u_short w;
    rtnv = ltp->ORB_read(&w);
    std::printf("    Read value = 0x%04x \n", w);
    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPOrbitStatus : public MenuItem {
public:
  LTPOrbitStatus() { setName("Orbit Status"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Orbit Status ****************************\n\n");
    
    if (ltp->ORB_linkout_is_from_local())          std::printf("  Orbit linkout is from: local\n");
    else                                           std::printf("  Orbit linkout is from: link in\n");
    if (ltp->ORB_lemoout_is_from_local())           std::printf("  Orbit LEMO out is from: local\n");
    else                                           std::printf("  Orbit LEMO out is from: link in\n");

    enum LTP::orbit_source sel;
    ltp->ORB_local_selection(&sel);
    if      (sel == LTP::ORB_NO_SOURCE)    std::printf("  Selected orbit source is: no source\n");
    else if (sel == LTP::ORB_INTERNAL)     std::printf("  Selected orbit source is: internal\n");
    else if (sel == LTP::ORB_FP_LEMO_INPUT) std::printf("  Selected orbit source is: front-panel LEMO input\n");
    else if (sel == LTP::ORB_PATGEN)       std::printf("  Selected orbit source is: pattern generator\n");
    else {
      std::printf("  Selected orbit source is: ERROR\n");
    }
    if (ltp->ORB_selected_is_running())            std::printf("  Selected Orbit clock is running\n");
    else                                           std::printf("  Selected Orbit clock is NOT running\n");
    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPOrbitLinkInputSource : public MenuItem {
public:
  LTPOrbitLinkInputSource() { setName("Select Source for Orbit Link Out"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Select Source for Orbit Link Out\n\n");
    int sel = enterInt("0-quit, 1-link, 2-local",0,2);
    switch (sel) {
    case 0: return(0)                            ; break;
    case 1: rtnv = ltp->ORB_linkout_from_linkin(); break;
    case 2: rtnv = ltp->ORB_linkout_from_local (); break;
    }
    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPOrbitLemoInputSource : public MenuItem {
public:
  LTPOrbitLemoInputSource() { setName("Select Source for Orbit LEMO Output"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Select Source for Orbit LEMO Output\n\n");
    int sel = enterInt("0-quit, 1-link, 2-local",0,2);
    switch (sel) {
    case 0: return(0)                           ; break;
    case 1: rtnv = ltp->ORB_lemoout_from_linkin(); break;
    case 2: rtnv = ltp->ORB_lemoout_from_local (); break;
    }
    return(rtnv);
  }
};
//------------------------------------------------------------------------------
class  LTPOrbitLocalSource: public MenuItem {
public:
  LTPOrbitLocalSource() { setName("Select Source for Local Orbit"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Select Source for Local Orbit\n\n");
    int sel = enterInt("0-quit, 1-no source, 2-intern, 3-LEMO ip, 4-PG",0,4);
    switch (sel) {
    case 0: return(0)                     ; break;
    case 1: rtnv = ltp->ORB_local_no    (); break;
    case 2: rtnv = ltp->ORB_local_intern(); break;
    case 3: rtnv = ltp->ORB_local_lemo   (); break;
    case 4: rtnv = ltp->ORB_local_pg    (); break;
    }
    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPCounterSource: public MenuItem {
public:
  LTPCounterSource() { setName("Select Counter Source and Reset Type"); }
  int action() {
    int rtnv = 0;

    std::printf("\n\n");
    std::printf("*** Select Counter Source and Reset Type\n\n");
    // get current counter selection
    enum LTP::counter_selection csel;
    ltp->COUNTER_selection(&csel);
    std::printf("  Current selection is: ");
    if      (csel == LTP::CNT_NO_SOURCE) std::printf(" No source \n");
    else if (csel == LTP::CNT_ORBIT    ) std::printf(" Orbit (32-bit) \n");
    else if (csel == LTP::CNT_ORBIT_ECR) std::printf(" Orbit (32-bit) reset by ECR(BGo-1)\n");
    else if (csel == LTP::CNT_BG0      ) std::printf(" BGo-0 (32-bit)\n");
    else if (csel == LTP::CNT_BG1      ) std::printf(" BGo-1 (32-bit)\n");
    else if (csel == LTP::CNT_BG2      ) std::printf(" BGo-2 (32-bit)\n");
    else if (csel == LTP::CNT_BG3      ) std::printf(" BGo-3 (32-bit)\n");
    else if (csel == LTP::CNT_L1A      ) std::printf(" L1A (32-bit)\n");
    else if (csel == LTP::CNT_TR1      ) std::printf(" Test Trigger 1 (32-bit)\n");
    else if (csel == LTP::CNT_TR2      ) std::printf(" Test Trigger 2 (32-bit)\n");
    else if (csel == LTP::CNT_TR3      ) std::printf(" Test Trigger 3 (32-bit)\n");
    else if (csel == LTP::CNT_L1A_ECR  ) std::printf(" L1A (24-bit) reset by ECR, 8-bit ECR counter\n");
    else if (csel == LTP::CNT_TR1_ECR  ) std::printf(" Test Trigger 1 (24-bit) reset by ECR, 8-bit ECR counter\n");
    else if (csel == LTP::CNT_TR2_ECR  ) std::printf(" Test Trigger 2 (24-bit) reset by ECR, 8-bit ECR counter\n"); 
    else if (csel == LTP::CNT_TR3_ECR  ) std::printf(" Test Trigger 3 (24-bit) reset by ECR, 8-bit ECR counter\n");
    else {
      std::printf("  Selected counter source is: ERROR\n");
    }
    
    std::printf("\n");
    std::printf("\n");
    std::printf("Select Signal to be counted:\n");
    int sel = enterInt("0-quit, 1-L1A, 2-Test1, 3-Test2, 4-Test3,\n5-BGo0, 6-BGo1, 7-BGo2, 8-BGo3, 9-Orbit, 10-none", 0, 10);
    // if L1/test trigger or orbit, then ask for reset type
    bool ecr_reset = false;
    if ( ((sel>=1) && (sel<=4)) || (sel==9) ) {
      std::printf("\nSelect reset type:");
      int reset = enterInt("0-VME, 1-ECR(BGo-1)",0,1);
      std::printf("\n");
      if (reset==1) ecr_reset = true;
      else ecr_reset = false;
    }

    switch (sel) {
    case  0: return(0)                     ; break;
    case  1: 
      if (ecr_reset) rtnv = ltp->COUNTER_L1A_ecr(); 
      else           rtnv = ltp->COUNTER_L1A    ();
      break;
    case  2: 
      if (ecr_reset) rtnv = ltp->COUNTER_TR1_ecr(); 
      else           rtnv = ltp->COUNTER_TR1    ();
      break;
    case  3: 
      if (ecr_reset) rtnv = ltp->COUNTER_TR2_ecr(); 
      else           rtnv = ltp->COUNTER_TR2    ();
      break;
    case  4: 
      if (ecr_reset) rtnv = ltp->COUNTER_TR3_ecr(); 
      else           rtnv = ltp->COUNTER_TR3    ();
      break;
    case  5: rtnv = ltp->COUNTER_BG0 (); break;
    case  6: rtnv = ltp->COUNTER_BG1 (); break;
    case  7: rtnv = ltp->COUNTER_BG2 (); break;
    case  8: rtnv = ltp->COUNTER_BG3 (); break;
    case  9: 
      if (ecr_reset) rtnv = ltp->COUNTER_orb_ecr(); 
      else           rtnv = ltp->COUNTER_orb    ();
      break;
    case  10: 
      rtnv = ltp->COUNTER_none ();
      break;
    }
    return(rtnv);
  }
};


//------------------------------------------------------------------------------
class LTPBusyStatus : public MenuItem {
public:
  LTPBusyStatus() { setName("Busy Status"); }
  int action() {
    int rtnv = 0;

    std::printf("\n");
    std::printf("*** Busy Status  ****************************\n\n");
    if (ltp->BUSY_linkout_is_from_local() )     std::printf("  Local busy drives link out busy\n");
    else                                        std::printf("  Link in busy drives link out busy\n");
    if (ltp->BUSY_linkin_is_summed_with_local() ) std::printf("  Link in summed with local busy: enabled\n");
    else                                        std::printf("  Link in summed with local busy: disabled\n");
    if (ltp->BUSY_ttlin_is_summed_with_local() )  std::printf("  TTL in summed with local busy: enabled\n");
    else                                        std::printf("  TTL in summed with local busy: disabled\n");
    if (ltp->BUSY_nimin_is_summed_with_local() )  std::printf("  NIM in summed with local busy: enabled\n");
    else                                        std::printf("  NIM in summed with local busy: disabled\n");
    if (ltp->BUSY_deadtime_is_summed_with_local() ) std::printf("  Dead time summed with local busy: enabled\n");
    else                                        std::printf("  Dead time summed with local busy: disabled\n");
    if (ltp->BUSY_constant_level_is_asserted() )   std::printf("  Constant busy level asserted: enabled\n");
    else                                        std::printf("  Constant busy level asserted: disabled\n");
    if (ltp->BUSY_from_pg_is_enabled() )       std::printf("  Busy from pattern generator: enabled\n");
    else                                        std::printf("  Busy from pattern generator: disabled\n");

    enum LTP::busy_lemo_output lemosel;
    ltp->BUSY_lemoout_source(&lemosel);
    std::printf("  Busy LEMO output selection: ");
    if      (lemosel == LTP::BUSY_ONLY      ) std::printf(" busy only\n");
    else if (lemosel == LTP::BUSY_OR_DEAD   ) std::printf(" busy or deadtime\n");
    else if (lemosel == LTP::BUSY_TIMER_INH ) std::printf(" timer inhibit\n");
    else if (lemosel == LTP::BUSY_PG_INH    ) std::printf(" pattern generator\n");
    else {
      std::printf("  Busy LEMO output selection: ERROR\n");
    }

    enum LTP::busy_dead_source deadsel;
    ltp->BUSY_deadtime_source(&deadsel);
    std::printf("  Busy deadtime trigger source selection: ");
    if      (deadsel == LTP::DEAD_L1A ) std::printf(" \n");
    else if (deadsel == LTP::DEAD_TR1 ) std::printf(" \n");
    else if (deadsel == LTP::DEAD_TR2 ) std::printf(" \n");
    else if (deadsel == LTP::DEAD_TR3 ) std::printf(" \n");
    else {
      std::printf("  Deadtime trigger selection: ERROR\n");
    }

    int readdur; 
    ltp->BUSY_get_deadtime_duration(&readdur);
    int dur = readdur + 4;
    std::printf("  Current deadtime duration: %d BC  (Value in register: %d)\n",dur, readdur); 

    std::printf("\n");
    if (ltp->BUSY_linkin_is_busy())   std::printf("  CTP link in busy is:      active\n");
    else                              std::printf("  CTP link in busy is:  not active\n");
    if (ltp->BUSY_linkout_is_busy())  std::printf("  CTP link out busy is:     active\n");
    else                              std::printf("  CTP link out busy is: not active\n");
    if (ltp->BUSY_ttlin_is_busy())      std::printf("  TTL busy is:              active\n");
    else                              std::printf("  TTL busy is:          not active\n");
    if (ltp->BUSY_nimin_is_busy())      std::printf("  NIM busy is:              active\n");
    else                              std::printf("  NIM busy is:          not active\n");
    if (ltp->BUSY_internal_is_busy()) std::printf("  Internal busy is:         active\n");
    else                              std::printf("  Internal busy is:     not active\n");
 
    return rtnv;
 
  }
};

//------------------------------------------------------------------------------
class LTPBusyWrite : public MenuItem {
public:
  LTPBusyWrite() { setName("Write to Busy CR Register"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Write to Busy CR Register\n\n");
    u_short w = (u_short) enterHex("Please enter value [hex] ",0x0000,0x00ff);
    return rtnv = ltp->BUSY_write(w);
  }
};

//------------------------------------------------------------------------------
class LTPBusyRead  : public MenuItem {
public:
  LTPBusyRead() { setName("Read from Busy CR Register"); }
  int action() {
    int rtnv = 0;
    u_short w;
    rtnv = ltp->BUSY_read(&w);
    std::printf("    Read value = 0x%04x \n", w);
    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPBusyReadStatus  : public MenuItem {
public:
  LTPBusyReadStatus() { setName("Read from Busy Status Register"); }
  int action() {
    int rtnv = 0;
    u_short w;
    rtnv = ltp->BUSY_read_status(&w);
    std::printf("    Read value = 0x%04x \n", w);
    return(rtnv);
  }
};


//------------------------------------------------------------------------------
class LTPBusyLinkInputSource : public MenuItem {
public:
  LTPBusyLinkInputSource() { setName("Select Source to drive Link"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Select Source to drive Link\n\n");
    int sel = enterInt("0-quit, 1-link, 2-local",0,2);
    switch (sel) {
    case 0: return(0)                           ; break;
    case 1: rtnv = ltp->BUSY_linkout_from_linkin(); break;
    case 2: rtnv = ltp->BUSY_linkout_from_local (); break;
    }
    return(rtnv);
  }
};
//------------------------------------------------------------------------------
class LTPBusySumLink : public MenuItem {
public:
  LTPBusySumLink() { setName("Link Busy to be Summed with Local Busy"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Link Busy to be Summed with Local Busy\n\n");
    int sel = enterInt("0-quit, 1-enable, 2-disable",0,2);
    switch (sel) {
    case 0: return(0)                           ; break;
    case 1: rtnv = ltp->BUSY_local_with_linkin   (); break;
    case 2: rtnv = ltp->BUSY_local_without_linkin(); break;
    }
    return(rtnv);
  }
};
//------------------------------------------------------------------------------
class LTPBusySumTTL : public MenuItem {
public:
  LTPBusySumTTL() { setName("TTL Input to be Summed with Local Busy"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** TTL Input to be Summed with Local Busy\n\n");
    int sel = enterInt("0-quit, 1-enable, 2-disable",0,2);
    switch (sel) {
    case 0: return(0)                           ; break;
    case 1: rtnv = ltp->BUSY_local_with_ttlin   (); break;
    case 2: rtnv = ltp->BUSY_local_without_ttlin(); break;
    }
    return(rtnv);
  }
};
//------------------------------------------------------------------------------
class LTPBusySumNIM : public MenuItem {
public:
  LTPBusySumNIM() { setName("NIM Input to be Summed with Local Busy"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** NIM Input to be Summed with Local Busy\n\n");
    int sel = enterInt("0-quit, 1-enable, 2-disable",0,2);
    switch (sel) {
    case 0: return(0)                           ; break;
    case 1: rtnv = ltp->BUSY_local_with_nimin   (); break;
    case 2: rtnv = ltp->BUSY_local_without_nimin(); break;
    }
    return(rtnv);
  }
};
//------------------------------------------------------------------------------
class LTPBusyConstant : public MenuItem {
public:
  LTPBusyConstant() { setName("Assert a Constant Busy Level"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Assert a Constant Busy Level\n\n");
    int sel = enterInt("0-quit, 1-enable, 2-disable",0,2);
    switch (sel) {
    case 0: return(0)                            ; break;
    case 1: rtnv = ltp->BUSY_enable_constant_level (); break;
    case 2: rtnv = ltp->BUSY_disable_constant_level(); break;
    }
    return(rtnv);
  }
};
//------------------------------------------------------------------------------
class LTPBusySumDead : public MenuItem {
public:
  LTPBusySumDead() { setName("Dead-Time to be Summed with Local Busy"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Dead-Time to be Summed with Local Busy\n\n");
    int sel = enterInt("0-quit, 1-enable, 2-disable",0,2);
    switch (sel) {
    case 0: return(0)                                ; break;
    case 1: rtnv = ltp->BUSY_local_with_deadtime   (); break;
    case 2: rtnv = ltp->BUSY_local_without_deadtime(); break;
    }
    return(rtnv);
  }
};
//------------------------------------------------------------------------------
class LTPBusyLEMOSource : public MenuItem {
public:
  LTPBusyLEMOSource() { setName("Select Source for Busy LEMO Output"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Select Source for Busy LEMO Output\n\n");
    int sel = enterInt("0-quit, 1-Busy Only, 2-Busy OR Dead-time, 3-Timer Inhibit, 4-PG Inhibit",0,4);
    switch (sel) {
    case 0: return(0)                                 ; break;
    case 1: rtnv = ltp->BUSY_lemoout_busy_only       (); break;
    case 2: rtnv = ltp->BUSY_lemoout_busy_or_deadtime(); break;
    case 3: rtnv = ltp->BUSY_lemoout_inhibit         (); break;
    case 4: rtnv = ltp->BUSY_lemoout_pg_inhibit      (); break;
    }
    return(rtnv);
  }
};
//------------------------------------------------------------------------------
class LTPBusyDeadSource : public MenuItem {
public:
  LTPBusyDeadSource() { setName("Select Dead-time Trigger Source"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Select Dead-time Trigger Source\n\n");
    int sel = enterInt("0-quit, 1-L1A, 2-Test1, 3-Test2, 4-Test3",0,4);
    switch (sel) {
    case 0: return(0)                             ; break;
    case 1: rtnv = ltp->BUSY_deadtime_source_L1A(); break;
    case 2: rtnv = ltp->BUSY_deadtime_source_TR1(); break;
    case 3: rtnv = ltp->BUSY_deadtime_source_TR2(); break;
    case 4: rtnv = ltp->BUSY_deadtime_source_TR3(); break;
    }
    return(rtnv);
  }
};
//------------------------------------------------------------------------------
class LTPBusyDeadDuration : public MenuItem {
public:
  LTPBusyDeadDuration() { setName("Select Dead Time Duration"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Select Dead Time Duration\n\n");
    int sel = enterInt("0-quit, n-(number of BC minus 4)",0,7);
    switch (sel) {
    case 0: return(0)                               ; break;
    default: rtnv = ltp->BUSY_deadtime_duration(sel); break;
    }
    return(rtnv);
  }
};
//------------------------------------------------------------------------------
class LTPBusyPG : public MenuItem {
public:
  LTPBusyPG() { setName("Busy from Pattern Generator"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Busy from Pattern Generator\n\n");
    int sel = enterInt("0-quit, 1-enable, 2-disable",0,2);
    switch (sel) {
    case 0: return(0)                         ; break;
    case 1: rtnv = ltp->BUSY_enable_from_pg() ; break;
    case 2: rtnv = ltp->BUSY_disable_from_pg(); break;
    }
    return(rtnv);
  }
};


//------------------------------------------------------------------------------
class LTPCalWrite : public MenuItem {
public:
  
  LTPCalWrite() { setName("Write to Calibration Request Register"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Write to Calibration Request Register\n\n");
    u_short w = (u_short) enterHex("Please enter value [hex] ",0x0000,0x00ff);
    return rtnv = ltp->CAL_write(w);
  }
};

//------------------------------------------------------------------------------
class LTPCalRead  : public MenuItem {
public:
  LTPCalRead() { setName("Read from Calibration Request Register"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Read from Calibration Request Register\n\n");
    u_short w;
    rtnv = ltp->CAL_read(&w);
    std::printf("    Read value = 0x%04x \n", w);
    return(rtnv);
  }
};


//------------------------------------------------------------------------------
class LTPCalStatus : public MenuItem {
public:
  LTPCalStatus() { setName("Status"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** CALREQ Status ***************************\n\n");
    
    enum LTP::cal_source src;
    ltp->CAL_source(&src);
    if      (src == LTP::CAL_LINK)   std::printf("  Selected source: link\n");
    else if (src == LTP::CAL_PRESET) std::printf("  Selected source: preset values\n");
    else if (src == LTP::CAL_FP)     std::printf("  Selected source: front-panel input\n");
    else if (src == LTP::CAL_PG)     std::printf("  Selected source: pattern generator\n");
    else {
      std::printf("  Selected source: ERROR\n");
    }

    if (ltp->CAL_testpath_is_enabled()) std::printf("  CAL testpath as output to front-panel: enabled\n");
    else                                std::printf("  CAL testpath as output to front-panel: disabled\n");

    int w;
    ltp->CAL_get_preset_value(&w);
    std::printf("  Preset value: %d\n",w); 
    ltp->CAL_get_linkstatus(&w);
    std::printf("  Link status: %d\n",w); 
    
    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPCalSource : public MenuItem {
public:
  LTPCalSource() { setName("Select Source for Calibration Request"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Select Source for Calibration Request\n\n");
    int sel = enterInt("0-quit, 1-link, 2-pre-set, 3-FP ip, 4=PG",0,4);
    switch (sel) {
    case 0: return(0)                          ; break;
    case 1: rtnv = ltp->CAL_source_link      (); break;
    case 2: rtnv = ltp->CAL_source_preset    (); break;
    case 3: rtnv = ltp->CAL_source_fp        (); break;
    case 4: rtnv = ltp->CAL_source_pg        (); break;
    }
    return(rtnv);
  }
};
//------------------------------------------------------------------------------
class LTPCalTestPath : public MenuItem {
public:
  LTPCalTestPath() { setName("Enable Test-Path as Output to Front-Panel"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Enable Test-Path as Output to Front-Panel\n\n");
    int sel = enterInt("0-quit, 1-enable, 2-disable",0,2);
    switch (sel) {
    case 0: return(0)                         ; break;
    case 1: rtnv = ltp->CAL_enable_testpath (); break;
    case 2: rtnv = ltp->CAL_disable_testpath(); break;
    }
    return(rtnv);
  }
};
//------------------------------------------------------------------------------
class LTPCalWritePreset : public MenuItem {
public:
  LTPCalWritePreset() { setName("Set Preset Value"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Set Preset Value\n\n");
    int sel = enterInt("Please enter value: ",0,7);
    rtnv = ltp->CAL_set_preset_value(sel);
    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPTTypeWrite : public MenuItem {
public:
  LTPTTypeWrite() { setName("Write to Trigger Type Register"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Write to Trigger Type Register\n\n");
    u_short w = (u_short) enterHex("Please enter value [hex] ",0x0000,0x00ff);
    return rtnv = ltp->TTYPE_write(w);
  }
};
//------------------------------------------------------------------------------
class LTPTTypeRead : public MenuItem {
public:
  LTPTTypeRead() { setName("Read from Trigger Type Register"); }
  int action() {
    int rtnv = 0;
    u_short w;
    rtnv = ltp->TTYPE_read(&w);
    std::printf("    Read value = 0x%04x \n", w);

    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPTTypeStatus : public MenuItem {
public:
  LTPTTypeStatus() { setName("Trigger Type Status"); }
  int action() {
    int rtnv = 0;

    std::printf("\n");
    std::printf("*** TType Status ****************************\n\n");
    
    if (ltp->TTYPE_linkout_is_from_local())     std::printf("  TTYPE linkout is from: local\n");
    else                                        std::printf("  TTYPE linkout is from: link in\n");
    if (ltp->TTYPE_eclout_is_from_local())       std::printf("  TTYPE front-panel output is from: local\n");
    else                                        std::printf("  TTYPE front-panel output is from: link in\n");
    if (ltp->TTYPE_local_is_controlled_by_vme()) std::printf("  TTYPE local source is controlled by: CR bits\n");
    else                                        std::printf("  TTYPE local source is controlled by: pattern generator\n");
    if (ltp->TTYPE_eclout_is_disabled())    std::printf("  TTYPE ecl output: disabled\n");
    else                                        std::printf("  TTYPE ecl output: enabled\n");
 
    enum LTP::ttype_file_addr addr;
    ltp->TTYPE_regfile_address(&addr);
    if      (addr == LTP::TTYPE_FILE_0) std::printf("  Selected TTYPE register file: 0\n");
    else if (addr == LTP::TTYPE_FILE_1) std::printf("  Selected TTYPE register file: 1\n");
    else if (addr == LTP::TTYPE_FILE_2) std::printf("  Selected TTYPE register file: 2\n");
    else if (addr == LTP::TTYPE_FILE_3) std::printf("  Selected TTYPE register file: 3\n");
    else {
                                        std::printf("  Selected TTYPE register file: ERROR\n");
    }

    enum LTP::ttype_delay del;
    ltp->TTYPE_fifodelay(&del);
    if      (del == LTP::TTYPE_DELAY_2BC) std::printf("  TTYPE write delay: 2 BC\n");
    else if (del == LTP::TTYPE_DELAY_3BC) std::printf("  TTYPE write delay: 3 BC\n");
    else if (del == LTP::TTYPE_DELAY_4BC) std::printf("  TTYPE write delay: 4 BC\n");
    else if (del == LTP::TTYPE_DELAY_5BC) std::printf("  TTYPE write delay: 5 BC\n");
    else {
                                          std::printf("  TTYPE write delay: ERROR\n");
    }

    enum LTP::ttype_fifotrigger src;
    ltp->TTYPE_fifotrigger(&src);

    if      (src == LTP::TTYPE_FIFO_L1A) std::printf("  TTYPE fifo source: L1A\n");
    else if (src == LTP::TTYPE_FIFO_TR1) std::printf("  TTYPE fifo source: Test Trigger 1\n");
    else if (src == LTP::TTYPE_FIFO_TR2) std::printf("  TTYPE fifo source: Test Trigger 2\n");
    else if (src == LTP::TTYPE_FIFO_TR3) std::printf("  TTYPE fifo source: Test Trigger 3\n");
    else {
                                         std::printf("  TTYPE fifo source: ERROR\n");
    }

    if (ltp->TTYPE_fifo_is_full())       std::printf("  TType fifo is: full\n");
    else                                 std::printf("  TType fifo is: not full\n");
    if (ltp->TTYPE_fifo_is_empty())      std::printf("  TType fifo is: empty\n");
    else                                 std::printf("  TType fifo is: not empty\n");

    u_short w0, w1, w2, w3;
    rtnv |= ltp->TTYPE_read_regfile0(&w0);
    rtnv |= ltp->TTYPE_read_regfile1(&w1);
    rtnv |= ltp->TTYPE_read_regfile2(&w2);
    rtnv |= ltp->TTYPE_read_regfile3(&w3);
    std::printf("\n");
    std::printf("  TType register file 0: 0x%02x\n",w0);
    std::printf("  TType register file 1: 0x%02x\n",w1);
    std::printf("  TType register file 2: 0x%02x\n",w2);
    std::printf("  TType register file 3: 0x%02x\n",w3);

    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPTTypeRegisterFiles : public MenuItem {
public:
  LTPTTypeRegisterFiles() { setName("Trigger Type Register Files"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Trigger Type Register Files\n");
    std::printf("\n");
    int sel = enterInt("0-quit, 1-read, 2-write",0,2);
    u_short w0, w1, w2, w3;

    switch(sel) {
    case 0:
      return(0);
      break;
    case 1:
      rtnv = ltp->TTYPE_read_regfile0(&w0);
      rtnv = ltp->TTYPE_read_regfile1(&w1);
      rtnv = ltp->TTYPE_read_regfile2(&w2);
      rtnv = ltp->TTYPE_read_regfile3(&w3);
      
      std::printf("\n");
      std::printf("Trig-Type register file 0: %02x\n",w0);
      std::printf("Trig-Type register file 1: %02x\n",w1);
      std::printf("Trig-Type register file 2: %02x\n",w2);
      std::printf("Trig-Type register file 3: %02x\n",w3);
      std::printf("\n");
      break;
    case 2:
      std::printf("\n");
      w0 = enterHex("Please enter value for register file 0: ",0x00, 0xff);
      w1 = enterHex("Please enter value for register file 1: ",0x00, 0xff);
      w2 = enterHex("Please enter value for register file 2: ",0x00, 0xff);
      w3 = enterHex("Please enter value for register file 3: ",0x00, 0xff);
      std::printf("\n");
      rtnv = ltp->TTYPE_write_regfile0(w0);
      rtnv = ltp->TTYPE_write_regfile1(w1);
      rtnv = ltp->TTYPE_write_regfile2(w2);
      rtnv = ltp->TTYPE_write_regfile3(w3);
      break;
    }
    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPTTypeLinkInputSource : public MenuItem {
public:
  LTPTTypeLinkInputSource() { setName("Select Source for Trigger Type Link Output"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Select Source for Trigger Type Link Out\n\n");
    int sel = enterInt("0-quit, 1-link, 2-local",0,2);
    switch (sel) {
    case 0: return(0);                               break;
    case 1: rtnv = ltp->TTYPE_linkout_from_linkin(); break;
    case 2: rtnv = ltp->TTYPE_linkout_from_local (); break;
    }
    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPTTypeECLSource : public MenuItem {
public:
  LTPTTypeECLSource() { setName("Select Source for Trigger Type ECL Output"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Select Source for Trigger Type ECL Output\n\n");
    int sel = enterInt("0-quit, 1-link, 2-local",0,2);
    switch (sel) {
    case 0: return(0);                               break;
    case 1: rtnv = ltp->TTYPE_eclout_from_linkin(); break;
    case 2: rtnv = ltp->TTYPE_eclout_from_local (); break;
    }
    return(rtnv);
  }
};
//------------------------------------------------------------------------------
class LTPTTypeLocalSource : public MenuItem {
public:
  LTPTTypeLocalSource() { setName("Select Source for Local Trigger Type"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Select Source for Local Trigger Type\n\n");
    int sel = enterInt("0-quit, 1-PG, 2-CR Bits",0,2);
    switch (sel) {
    case 0: return(0);                          break;
    case 1: rtnv = ltp->TTYPE_local_from_pg (); break;
    case 2: rtnv = ltp->TTYPE_local_from_vme(); break;
    }
    return(rtnv);
  }
};
//------------------------------------------------------------------------------
class LTPTTypeECLEnable : public MenuItem {
public:
  LTPTTypeECLEnable() { setName("Enable ECL Output"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Enable ECL Output\n\n");
    int sel = enterInt("0-quit, 1-enable, 2-disable",0,2);
    switch (sel) {
    case 0: return(0);                       break;
    case 1: rtnv = ltp->TTYPE_enable_eclout (); break;
    case 2: rtnv = ltp->TTYPE_disable_eclout(); break;
    }
    return(rtnv);
  }
};
//------------------------------------------------------------------------------
class LTPTTypeFileAddress : public MenuItem {
public:
  LTPTTypeFileAddress() { setName("Select Trigger Type Register File"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Select Trigger Type Register File\n\n");
    int sel = enterInt("0-quit, 1-File0, 2-File1, 3-File2, 4-File3",0,4);
    switch (sel) {
    case 0: return(0);                             break;
    case 1: rtnv = ltp->TTYPE_regfile_address_0(); break;
    case 2: rtnv = ltp->TTYPE_regfile_address_1(); break;
    case 3: rtnv = ltp->TTYPE_regfile_address_2(); break;
    case 4: rtnv = ltp->TTYPE_regfile_address_3(); break;
    }
    return(rtnv);
  }
};
//------------------------------------------------------------------------------
class LTPTTypeDelay : public MenuItem {
public:
  LTPTTypeDelay() { setName("Select Fifo Write Delay After Trigger"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Select Fifo Write Delay After Trigger\n\n");
    int sel = enterInt("0-quit, 1-2BC, 2-3BC, 3-4BC, 4-5BC",0,4);
    switch (sel) {
    case 0: return(0);                      break;
    case 1: rtnv = ltp->TTYPE_fifodelay_2bc(); break;
    case 2: rtnv = ltp->TTYPE_fifodelay_3bc(); break;
    case 3: rtnv = ltp->TTYPE_fifodelay_4bc(); break;
    case 4: rtnv = ltp->TTYPE_fifodelay_5bc(); break;
    }
    return(rtnv);
  }
};
//------------------------------------------------------------------------------
class LTPTTypeTriggerSource : public MenuItem {
public:
  LTPTTypeTriggerSource() { setName("Select Trigger Source for Trigger Type Fifo Writing"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Select Trigger Source for Trigger Type Fifo Writing\n\n");
    int sel = enterInt("0-quit, 1-L1A, 2-Test1, 3-Test2, 4-Test3",0,4);
    switch (sel) {
    case 0: return(0);                          break;
    case 1: rtnv = ltp->TTYPE_fifotrigger_L1A(); break;
    case 2: rtnv = ltp->TTYPE_fifotrigger_TR1(); break;
    case 3: rtnv = ltp->TTYPE_fifotrigger_TR2(); break;
    case 4: rtnv = ltp->TTYPE_fifotrigger_TR3(); break;
    }
    return(rtnv);
  }
};

//------------------------------------------------------------------------------
class LTPTTypeFifo : public MenuItem {
public:
  LTPTTypeFifo() { setName("Trigger Type Fifo"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Trigger Type Fifo\n\n");
    u_short w;
    // status
    char empty[5] = "";
    char full[5] = "";
    if (!ltp->TTYPE_fifo_is_empty()) std::sprintf(empty,"not ");
    if (!ltp->TTYPE_fifo_is_full()) std::sprintf(full,"not ");

    std::printf("Fifo is %sempty and %sfull\n",empty, full);

    int sel = enterInt("0-quit, 1-reset, 2-read once, 3-dump, 4-write",0,4);
    std::list<int> fifo;
    switch (sel) {
    case 0: return(0); break;
    case 1: rtnv = ltp->TTYPE_reset_fifo  (); break;
    case 2: 
      rtnv = ltp->TTYPE_read_fifo (&w); 
      std::printf("Read word 0x%02x (%d) from trigger type FIFO\n", w, w);
      break;
    case 3: 
      while (!ltp->TTYPE_fifo_is_empty()) {
	ltp->TTYPE_read_fifo(&w);
	fifo.push_back(w);
      }
      for (std::list<int>::const_iterator it = fifo.begin();
	   it != fifo.end(); ++it) {
	std::printf(" %02x\n",*it);
      }
      std::printf("Fifo contained %zu elements\n",fifo.size());
      std::printf("\n");
      break;
    case 4: rtnv = ltp->TTYPE_write_fifo  (); break;
    }
    return(rtnv);
  }
};


//------------------------------------------------------------------------------
class LTPCounterReset: public MenuItem {
public:
  LTPCounterReset() { setName("Reset Counter"); }
  int action() {
    std::printf("\n");
    std::printf("*** Reset Counter\n\n");
    ltp->COUNTER_reset();
    return(0);
  }
};
//------------------------------------------------------------------------------
class LTPCounterStatus: public MenuItem {
public:
  LTPCounterStatus() { setName("Counter Status"); }
  int action() {

    std::printf("\n");
    u_short m,l;
    u_int c;
    ltp->COUNTER_read_lsw(&l);
    ltp->COUNTER_read_msw(&m);
    c = m*0x10000 + l;

    std::printf("*** 32-Bit Counter  *************************\n\n");
    // get current counter selection
    enum LTP::counter_selection csel;
    ltp->COUNTER_selection(&csel);
    std::printf("  Current selection is: ");
    if      (csel == LTP::CNT_NO_SOURCE) std::printf(" No source \n");
    else if (csel == LTP::CNT_ORBIT    ) std::printf(" Orbit (32-bit) \n");
    else if (csel == LTP::CNT_ORBIT_ECR) std::printf(" Orbit (32-bit) reset by ECR(BGo-1)\n");
    else if (csel == LTP::CNT_BG0      ) std::printf(" BGo-0 (32-bit)\n");
    else if (csel == LTP::CNT_BG1      ) std::printf(" BGo-1 (32-bit)\n");
    else if (csel == LTP::CNT_BG2      ) std::printf(" BGo-2 (32-bit)\n");
    else if (csel == LTP::CNT_BG3      ) std::printf(" BGo-3 (32-bit)\n");
    else if (csel == LTP::CNT_L1A      ) std::printf(" L1A (32-bit)\n");
    else if (csel == LTP::CNT_TR1      ) std::printf(" Test Trigger 1 (32-bit)\n");
    else if (csel == LTP::CNT_TR2      ) std::printf(" Test Trigger 2 (32-bit)\n");
    else if (csel == LTP::CNT_TR3      ) std::printf(" Test Trigger 3 (32-bit)\n");
    else if (csel == LTP::CNT_L1A_ECR  ) std::printf(" L1A (24-bit) reset by ECR, 8-bit ECR counter\n");
    else if (csel == LTP::CNT_TR1_ECR  ) std::printf(" Test Trigger 1 (24-bit) reset by ECR, 8-bit ECR counter\n");
    else if (csel == LTP::CNT_TR2_ECR  ) std::printf(" Test Trigger 2 (24-bit) reset by ECR, 8-bit ECR counter\n"); 
    else if (csel == LTP::CNT_TR3_ECR  ) std::printf(" Test Trigger 3 (24-bit) reset by ECR, 8-bit ECR counter\n");
    else {
      std::printf("  Selected counter source is: ERROR\n");
    }

    std::printf("\n");
    std::printf("  Counter value: 0x%08x = (dec)%d\n",c,c);

    return(0);
  }
};

//------------------------------------------------------------------------------
class LTPTurnSelector: public MenuItem {
public:
  LTPTurnSelector() { setName("Turn Counter Selector"); }
  int action() {
    std::printf("\n");
    std::printf("*** Turn Counter Selector\n\n");
    int rtnv = 0;
    u_short turncode;
    int sel = enterInt("0-quit, 1-read, 2-enter(hex)",0,2);
    switch (sel) {
    case 0: 
      return(0);
      break;
    case 1: 
      rtnv = ltp->TURN_read_selector_code(&turncode);
      std::printf("    Read value = 0x%04x \n", turncode);
      break;
    case 2: turncode = (u_short) enterHex("Please enter turn selector code (hex): ",0x0,0xf);
      rtnv = ltp->TURN_write_selector_code(turncode);
      break;
    }
    return(rtnv);
  }
};
//------------------------------------------------------------------------------
class LTPOrbitOutLemoSelection: public MenuItem {
public:
  LTPOrbitOutLemoSelection() { setName("Select Source for Orbit LEMO Output"); }
  int action() {
    std::printf("\n");
    std::printf("*** Select Source for Orbit LEMO Output\n\n");
    int rtnv = 0;
    int sel = enterInt("0-quit, 1-orbit, 2-turn",0,2);
    switch (sel) {
    case 0: return(0);                       break;
    case 1: rtnv = ltp->TURN_orbit_lemoout_from_orbit(); break;
    case 2: rtnv = ltp-> TURN_orbit_lemoout_from_turn(); break;
    }
    return(rtnv);
  }
};
//------------------------------------------------------------------------------
class LTPInhibit: public MenuItem {
public:
  LTPInhibit() { setName("Inhibit Shadow Register"); }
  int action() {
    int rtnv = 0;
    std::printf("\n");
    std::printf("*** Inhibit Shadow Register\n\n");
    u_short shadow;
    int sel = enterInt("0-quit, 1-read, 2-enter(hex)",0,2);
    switch (sel) {
    case 0: 
      return(0);
      break;
    case 1: 
      rtnv = ltp->INH_readShadow(&shadow); 
      std::printf("    Read value = 0x%04x \n", shadow);
      break;
    case 2: 
      shadow = (u_short) enterHex("Please enter timer value (hex): ",0x0,0xfff);
      rtnv = ltp->INH_writeShadow(shadow);
      std::printf("    Read value = 0x%04x \n", shadow);
      break;
    }
    return(rtnv);
  }
};
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
class LTPCTPMode : public MenuItem {
public:
  LTPCTPMode() { setName("Slave Mode"); }
  int action() {
    int rtnv = 0;

    ltp->OM_slave_print_description();
    int sel = enterInt("0-quit, 1-Configure",0,1);
    
    switch (sel) {
    case 0:  // do nothing
      break; 
    case 1:

      rtnv = ltp->OM_slave();

      ltp->OM_slave_print_description();

      break;
    }
    return rtnv;
  }
};

//------------------------------------------------------------------------------
class LTPLocalMode : public MenuItem {
public:
  LTPLocalMode() { setName("Master Mode (LEMO)"); }
  int action() {
    int rtnv = 0;

    ltp->OM_master_lemo_print_description();

    int sel = enterInt("0-quit, 1-Configure",0,1);
    
    switch (sel) {
    case 0:  // do nothing
      break; 
    case 1:

      rtnv = ltp->OM_master_lemo();

      ltp->OM_master_lemo_print_description();
      break;
    }
    return rtnv;
  }
};
//------------------------------------------------------------------------------
class LTPPGMode : public MenuItem {
public:
  LTPPGMode() { setName("Master Mode (Pattern Generator)"); }
  int action() {
    int rtnv = 0;

    ltp->OM_master_patgen_print_description(pgfilename);

    int sel = enterInt("0-quit, 1-Configure, 2-Change Filename",0,2);
    
    switch (sel) {
    case 0:  // do nothing
      break; 
    case 1:
      rtnv = ltp->OM_master_patgen(pgfilename);
      if (rtnv!=0) {
	std::cerr << std::endl << "ERROR: please check patgen file" << std::endl;
      } else {
	rtnv |= ltp->PG_start();
      ltp->OM_master_patgen_print_description(pgfilename);
      }

      break;
    case 2:
      LTPPGSetFileName h;
      h.action();
      break;
    }
    return rtnv;
  }
};


//------------------------------------------------------------------------------
class LTPRPCPulseL1A : public MenuItem {
public:
  LTPRPCPulseL1A() { setName("RPC Master Mode: pulse followed by L1A"); }
  int action() {
   LTPRPCPulseL1A_start:
    int rtnv = 0;

    ltp->OM_master_RPCPulseL1A_print_description(l1a_delay);

    int sel = enterInt("0-quit, 1-Configure, 2-Change L1A_Delay",0,2);
    
    switch (sel) {
    case 0:  // do nothing
      break; 
    case 1:
      ltp->OM_master_RPCPulseL1A(l1a_delay);
      ltp->OM_master_RPCPulseL1A_print_description(l1a_delay);
      break;
    case 2:
      l1a_delay = enterInt("L1A_Delay in BC",6,100);
      std::printf("Current value: L1A_delay = %d BC\n", l1a_delay);
      goto LTPRPCPulseL1A_start;
      break;
    }
    return rtnv;
  }
};

//------------------------------------------------------------------------------
class LTPToggleConstBusy : public MenuItem {
public:
  LTPToggleConstBusy() { setName("Toggle Const Busy"); }
  int action() {
    int rtnv = 0;

    bool busy_asserted = ltp->BUSY_constant_level_is_asserted();
    if (busy_asserted) {
      std::printf(" Current status: BUSY\n");
    }
    else {
      std::printf(" Current status: NOT BUSY\n");
    }

    int sel = enterInt("0-quit, 1-Toggle",0,1);
    
    switch (sel) {
    case 0:  // do nothing
      break; 
    case 1:
      if (busy_asserted) {
	ltp->BUSY_disable_constant_level();
      }
      else {
	ltp->BUSY_enable_constant_level();
      }
      busy_asserted = ltp->BUSY_constant_level_is_asserted();
      if (busy_asserted) {
	std::printf(" New status: BUSY\n");
      }
      else {
	std::printf(" New status: NOT BUSY\n");
      }
      
      break;
    }
    return rtnv;
  }
};


//------------------------------------------------------------------------------
class LTPGenerateECR : public MenuItem {
public:
  LTPGenerateECR() { setName("Generate ECR (overwrites BGo-1 settings)"); }
  int action() {
    int rtnv = 0;

    std::printf(" Generates ECR (BGo-1) pulse with 1ms BUSY before and after\n");
    std::printf(" CAUTION: overwrites BGo-1 settings!\n");

    int sel = enterInt("0-quit, 1-Generate ECR",0,1);
    
    switch (sel) {
    case 0:  // do nothing
      break; 
    case 1:
      ltp->OM_GenerateECR();
      break;
    }
    return rtnv;
  }
};


//------------------------------------------------------------------------------

class LTPStatus : public MenuItem {
public:
  LTPStatus() { setName("Global Status"); }
  int action() {
    int rtnv = 0;

    LTPBCStatus statusBC;
    statusBC.action();

    LTPOrbitStatus statusOrbit;
    statusOrbit.action();

    // set current channel to all channels for the dump
    // reset to old value afterwards
    u_short oldselchannel = selchannel;
    selchannel = 9;
    LTPIOStatus statusIO;
    statusIO.action();
    selchannel = oldselchannel;

    LTPBusyStatus statusBusy;
    statusBusy.action();

    LTPTTypeStatus statusTType;
    statusTType.action();

    LTPCalStatus statusCal;
    statusCal.action();

    LTPCounterStatus statusCounter;
    statusCounter.action();

    LTPPGStatus statusPG;
    statusPG.action();

    std::printf("*********************************************\n");


    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class LTPDump : public MenuItem {
public:
  LTPDump() { setName("Register Dump"); }
  int action() {
    int rtnv = 0;

    u_short w = 0x0000;
    rtnv |= ltp->IO_read(LTP::L1A, &w);    u_short w_l1a = w;
    rtnv |= ltp->IO_read(LTP::TR1, &w);    u_short w_tr1 = w;
    rtnv |= ltp->IO_read(LTP::TR2, &w);    u_short w_tr2 = w;
    rtnv |= ltp->IO_read(LTP::TR3, &w);    u_short w_tr3 = w;
    
    rtnv |= ltp->BC_read(&w);     u_short w_bc  = w;
    rtnv |= ltp->ORB_read(&w);    u_short w_orb = w;

    rtnv |= ltp->IO_read(LTP::BG0, &w);    u_short w_bg0 = w;
    rtnv |= ltp->IO_read(LTP::BG1, &w);    u_short w_bg1 = w;
    rtnv |= ltp->IO_read(LTP::BG2, &w);    u_short w_bg2 = w;
    rtnv |= ltp->IO_read(LTP::BG3, &w);    u_short w_bg3 = w;

    rtnv |= ltp->BUSY_read(&w);                   u_short w_busy = w;
    rtnv |= ltp->BUSY_read_status(&w);            u_short w_busy_status = w;
    
    rtnv |= ltp->CAL_read(&w);         u_short w_cal = w;
    rtnv |= ltp->INH_readShadow(&w);   u_short w_inh = w;
    rtnv |= ltp->COUNTER_read_lsw(&w); u_short w_count_lsw = w;
    rtnv |= ltp->COUNTER_read_msw(&w); u_short w_count_msw = w;

    rtnv |= ltp->TTYPE_read(&w);          u_short w_ttype = w;
    rtnv |= ltp->TTYPE_read_regfile0(&w); u_short w_ttype_rf0 = w;
    rtnv |= ltp->TTYPE_read_regfile1(&w); u_short w_ttype_rf1 = w;
    rtnv |= ltp->TTYPE_read_regfile2(&w); u_short w_ttype_rf2 = w;
    rtnv |= ltp->TTYPE_read_regfile3(&w); u_short w_ttype_rf3 = w;

    rtnv |= ltp->TURN_read_selector_code(&w);     u_short w_turn = w;
    rtnv |= ltp->PG_get_fixed_value(&w);          u_short w_pg_fixed = w;
    rtnv |= ltp->PG_read(&w);                     u_short w_pg = w;
    rtnv |= ltp->PG_read_address_counter_MSW(&w); u_short w_pg_ac_msw = w;
    rtnv |= ltp->PG_read_address_counter_LSW(&w); u_short w_pg_ac_lsw = w;


    printf("\n\n\n");
    printf("  ----------\n");
    printf("   LTP DUMP \n");
    printf("  ----------\n\n\n");
    printf(" L1A             0x%04x \n", w_l1a);
    printf(" Test Trigger 1  0x%04x \n", w_tr1);
    printf(" Test Trigger 2  0x%04x \n", w_tr2);
    printf(" Test Trigger 3  0x%04x \n", w_tr3);
    printf("\n");
    printf(" BC Clock        0x%04x \n", w_bc);
    printf(" Orbit           0x%04x \n", w_orb);
    printf("\n");
    printf(" BGo-0 (BCR)     0x%04x \n", w_bg0);
    printf(" BGo-1 (ECR)     0x%04x \n", w_bg1);
    printf(" BGo-2           0x%04x \n", w_bg2);
    printf(" BGo-3           0x%04x \n", w_bg3);
    printf("\n");
    printf(" Busy CSR        0x%04x \n", w_busy);
    printf(" Busy Status     0x%04x \n", w_busy_status);
    printf("\n");
    printf(" Inhibit Timer   0x%04x \n", w_inh);
    printf(" Cal Request     0x%04x \n", w_cal);
    printf("\n");
    printf(" Counter    0x%04x %04x\n", w_count_msw, w_count_lsw);
    printf("\n");
    printf(" Trigger Type    0x%04x \n", w_ttype);
    printf("    TType File0  0x%04x \n", w_ttype_rf0);
    printf("    TType File1  0x%04x \n", w_ttype_rf1);
    printf("    TType File2  0x%04x \n", w_ttype_rf2);
    printf("    TType File3  0x%04x \n", w_ttype_rf3);
    printf("\n");
    printf(" Turn Selector   0x%04x \n", w_turn);
    printf("\n");
    printf(" PatGen CSR               0x%04x \n", w_pg);
    printf(" PatGen fixed values      0x%04x \n", w_pg_fixed);
    printf(" PatGen Address Counter 0x%1x %04x \n", w_pg_ac_msw, w_pg_ac_lsw);
    printf(" PatGen filename          %s \n", pgfilename.c_str());
    printf(" Configuration filename   %s \n", conffile.c_str());

    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class LTPConfigureFromFile : public MenuItem {
public:
  LTPConfigureFromFile() { setName("Configure from File"); }
  int action() {
    int rtnv = 0;

      
    // declare variables for registers

    u_short w_l1a;
    u_short w_tr1;
    u_short w_tr2;
    u_short w_tr3;
    
    u_short w_bc ;
    u_short w_orb;

    u_short w_bg0;
    u_short w_bg1;
    u_short w_bg2;
    u_short w_bg3;

    u_short w_busy;
    u_short w_busy_status;
    
    u_short w_cal;
    u_short w_inh;
    u_short w_count_lsw;
    u_short w_count_msw;

    u_short w_ttype;
    u_short w_ttype_rf0;
    u_short w_ttype_rf1;
    u_short w_ttype_rf2;
    u_short w_ttype_rf3;

    u_short w_turn;
    u_short w_pg_fixed;
    u_short w_pg;
    u_short w_pg_ac_msw;
    u_short w_pg_ac_lsw;

    printf("\n");
    printf(" Current configuration file name: %s\n", conffile.c_str());
    printf("\n");

    int sel = enterInt("0-quit, 1-ConfigureFromFile, 2-Change Filename",0,2);
    
    switch (sel) {
    case 0:  // do nothing
      break; 
    case 1:
      {
	// open file
	std::ifstream tfile(conffile.c_str(), ios::in);
	if (!tfile.is_open()) {
	  exit(-1);
	}
	else {
	  std::cout << "Loading file " << conffile.c_str() << " into memory" << std::endl;

	  char buffer[200];
	  // ignore the first 3 lines
	  tfile.getline(buffer,200);
	  tfile.getline(buffer,200);
	  tfile.getline(buffer,200);
	  tfile.getline(buffer,200);

	  // read from file
	  u_int intread;
	  tfile.getline(buffer,200);
	  sscanf(buffer, "L1A %04x", &intread);
	  w_l1a = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "TestTrigger1 %04x", &intread);
	  w_tr1 = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "TestTrigger2 %04x", &intread);
	  w_tr2 = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "TestTrigger3 %04x", &intread);
	  w_tr3 = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "BC %04x", &intread);
	  w_bc = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "Orbit %04x", &intread);
	  w_orb = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "BGo-0 %04x", &intread);
	  w_bg0 = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "BGo-1 %04x", &intread);
	  w_bg1 = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "BGo-2 %04x", &intread);
	  w_bg2 = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "BGo-3 %04x", &intread);
	  w_bg3 = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "BusyCSR %04x", &intread);
	  w_busy = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "BusyStatus %04x", &intread);
	  w_busy_status = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "Inhibit %04x", &intread);
	  w_inh = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "CalReq %04x", &intread);
	  w_cal = static_cast<u_short>(intread);
	  u_int intread2;
	  tfile.getline(buffer,200);
	  sscanf(buffer, "Counter %04x %04x", &intread, &intread2);
	  w_count_lsw = static_cast<u_short>(intread);
	  w_count_msw = static_cast<u_short>(intread2);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "TTypeCSR %04x", &intread);
	  w_ttype = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "TTypeFile0 %04x", &intread);
	  w_ttype_rf0 = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "TTypeFile1 %04x", &intread);
	  w_ttype_rf1 = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "TTypeFile2 %04x", &intread);
	  w_ttype_rf2 = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "TTypeFile3 %04x", &intread);
	  w_ttype_rf3 = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "TurnSelector %04x", &intread);
	  w_turn = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "PG %04x", &intread);
	  w_pg = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "PGFixed %04x", &intread);
	  w_pg_fixed = static_cast<u_short>(intread);
	  tfile.getline(buffer,200);
	  sscanf(buffer, "PGAddressCounter %1x %04x", &intread, &intread2);
	  w_pg_ac_lsw = static_cast<u_short>(intread);
	  w_pg_ac_msw = static_cast<u_short>(intread2);

	  tfile.getline(buffer,200);
	  char helper[200];
	  sscanf(buffer, "PGFilename %s", helper);
	  pgfilename = helper;

	  tfile.getline(buffer,200);
	  sscanf(buffer, "ConfFilename %s", helper);
	  conffile = helper;
	
	  printf("\n\n");
	  printf(" ----------------\n");
	  printf("  Read from File \n");
	  printf(" ----------------\n");
	  printf(" L1A             0x%04x \n", w_l1a);
	  printf(" Test Trigger 1  0x%04x \n", w_tr1);
	  printf(" Test Trigger 2  0x%04x \n", w_tr2);
	  printf(" Test Trigger 3  0x%04x \n", w_tr3);
	  printf("\n");
	  printf(" BC Clock        0x%04x \n", w_bc);
	  printf(" Orbit           0x%04x \n", w_orb);
	  printf("\n");
	  printf(" BGo-0 (BCR)     0x%04x \n", w_bg0);
	  printf(" BGo-1 (ECR)     0x%04x \n", w_bg1);
	  printf(" BGo-2           0x%04x \n", w_bg2);
	  printf(" BGo-3           0x%04x \n", w_bg3);
	  printf("\n");
	  printf(" Busy CSR        0x%04x \n", w_busy);
	  printf(" Busy Status     0x%04x \n", w_busy_status);
	  printf("\n");
	  printf(" Inhibit Timer   0x%04x \n", w_inh);
	  printf(" Cal Request     0x%04x \n", w_cal);
	  printf("\n");
	  printf(" Counter    0x%04x %04x\n", w_count_msw, w_count_lsw);
	  printf("\n");
	  printf(" Trigger Type    0x%04x \n", w_ttype);
	  printf("    TType File0  0x%04x \n", w_ttype_rf0);
	  printf("    TType File1  0x%04x \n", w_ttype_rf1);
	  printf("    TType File2  0x%04x \n", w_ttype_rf2);
	  printf("    TType File3  0x%04x \n", w_ttype_rf3);
	  printf("\n");
	  printf(" Turn Selector   0x%04x \n", w_turn);
	  printf("\n");
	  printf(" PatGen CSR               0x%04x \n", w_pg);
	  printf(" PatGen fixed values      0x%04x \n", w_pg_fixed);
	  printf(" PatGen Address Counter 0x%1x %04x \n", w_pg_ac_msw, w_pg_ac_lsw);
	  printf(" PatGen filename          %s \n", pgfilename.c_str());
	  printf(" Configuration filename   %s \n", conffile.c_str());

	}
	tfile.close();

	// write to registers
	rtnv |= ltp->IO_write(LTP::L1A, w_l1a);    
	rtnv |= ltp->IO_write(LTP::TR1, w_tr1);    
	rtnv |= ltp->IO_write(LTP::TR2, w_tr2);    
	rtnv |= ltp->IO_write(LTP::TR3, w_tr3);    

	rtnv |= ltp->BC_write(w_bc);     
	rtnv |= ltp->ORB_write(w_orb);    

	rtnv |= ltp->IO_write(LTP::BG0, w_bg0);    
	rtnv |= ltp->IO_write(LTP::BG1, w_bg1);    
	rtnv |= ltp->IO_write(LTP::BG2, w_bg2);    
	rtnv |= ltp->IO_write(LTP::BG3, w_bg3);    

	rtnv |= ltp->BUSY_write(w_busy);            
    
	rtnv |= ltp->CAL_write(w_cal);         
	rtnv |= ltp->INH_writeShadow(w_inh);   

	rtnv |= ltp->TTYPE_write(w_ttype);          
	rtnv |= ltp->TTYPE_write_regfile0(w_ttype_rf0); 
	rtnv |= ltp->TTYPE_write_regfile1(w_ttype_rf1); 
	rtnv |= ltp->TTYPE_write_regfile2(w_ttype_rf2); 
	rtnv |= ltp->TTYPE_write_regfile3(w_ttype_rf3); 

	rtnv |= ltp->TURN_write_selector_code(w_turn);     
	rtnv |= ltp->PG_set_fixed_value(w_pg_fixed);          

	// if pattern generator CSR register
	// was not in running mode, then just set the value
	if ( (w_pg & 0x0003) != 1) {
	  std::printf("Pattern generator value corresponds to <not running>.\n");
	  std::printf("Writing to PG CSR register\n");
	  rtnv |= ltp->PG_write(w_pg);                     
	}
	else {
	  std::printf("Pattern generator value corresponds to <running>.\n");
	  std::printf("Loading to file memory and start\n");
	  // else load memory with file and set to running mode
	  ltp->PG_reset();
	  rtnv |= ltp->PG_load_memory_from_file(&pgfilename);
	  if (rtnv != 0) {
	    std::printf(" ERROR: could not open file %s\n", pgfilename.c_str());
	    return(rtnv);
	  }
	  rtnv |= ltp->PG_write(w_pg);                     
	  rtnv |= ltp->PG_start();
	}

	break;
      }
    case 2:
      LTPConfSetFileName h;
      h.action();
      break;
    }

    return(rtnv);
  }
};

//------------------------------------------------------------------------------

class LTPDumpToFile : public MenuItem {
public:
  LTPDumpToFile() { setName("Register Dump to File"); }
  int action() {
    int rtnv = 0;
      
    printf("\n");
    printf(" Current configuration file name: %s\n", conffile.c_str());
    printf("\n");

    int sel = enterInt("0-quit, 1-DumpToFile, 2-Change Filename",0,2);
    
    switch (sel) {
    case 0:  // do nothing
      break; 
    case 1:
      {
	// open file
	FILE* file;
	file = fopen(conffile.c_str(), "w");
	if (!file) {
	  printf("\n *** ERROR opening file %s for writing *** \n", conffile.c_str());
	  exit(-1);
	}

	// read values from registers

	u_short w = 0x0000;
	rtnv |= ltp->IO_read(LTP::L1A, &w);    u_short w_l1a = w;
	rtnv |= ltp->IO_read(LTP::TR1, &w);    u_short w_tr1 = w;
	rtnv |= ltp->IO_read(LTP::TR2, &w);    u_short w_tr2 = w;
	rtnv |= ltp->IO_read(LTP::TR3, &w);    u_short w_tr3 = w;
    
	rtnv |= ltp->BC_read(&w);     u_short w_bc  = w;
	rtnv |= ltp->ORB_read(&w);    u_short w_orb = w;

	rtnv |= ltp->IO_read(LTP::BG0, &w);    u_short w_bg0 = w;
	rtnv |= ltp->IO_read(LTP::BG1, &w);    u_short w_bg1 = w;
	rtnv |= ltp->IO_read(LTP::BG2, &w);    u_short w_bg2 = w;
	rtnv |= ltp->IO_read(LTP::BG3, &w);    u_short w_bg3 = w;

	rtnv |= ltp->BUSY_read(&w);            u_short w_busy = w;
	rtnv |= ltp->BUSY_read_status(&w);     u_short w_busy_status = w;
    
	rtnv |= ltp->CAL_read(&w);         u_short w_cal = w;
	rtnv |= ltp->INH_readShadow(&w);   u_short w_inh = w;
	rtnv |= ltp->COUNTER_read_lsw(&w); u_short w_count_lsw = w;
	rtnv |= ltp->COUNTER_read_msw(&w); u_short w_count_msw = w;

	rtnv |= ltp->TTYPE_read(&w);          u_short w_ttype = w;
	rtnv |= ltp->TTYPE_read_regfile0(&w); u_short w_ttype_rf0 = w;
	rtnv |= ltp->TTYPE_read_regfile1(&w); u_short w_ttype_rf1 = w;
	rtnv |= ltp->TTYPE_read_regfile2(&w); u_short w_ttype_rf2 = w;
	rtnv |= ltp->TTYPE_read_regfile3(&w); u_short w_ttype_rf3 = w;

	rtnv |= ltp->TURN_read_selector_code(&w);     u_short w_turn = w;
	rtnv |= ltp->PG_get_fixed_value(&w);          u_short w_pg_fixed = w;
	rtnv |= ltp->PG_read(&w);                     u_short w_pg = w;
	rtnv |= ltp->PG_read_address_counter_MSW(&w); u_short w_pg_ac_msw = w;
	rtnv |= ltp->PG_read_address_counter_LSW(&w); u_short w_pg_ac_lsw = w;

	// write to file

	fprintf(file, "------------------\n");
	fprintf(file, "LTP Configuration\n");
	fprintf(file, "Do not change!!! \n");
	fprintf(file, "------------------\n");
	fprintf(file, "L1A %04x\n", w_l1a);
	fprintf(file, "TestTrigger1 %04x\n", w_tr1);
	fprintf(file, "TestTrigger2 %04x\n", w_tr2);
	fprintf(file, "TestTrigger3 %04x\n", w_tr3);
	fprintf(file, "BC %04x\n", w_bc);
	fprintf(file, "Orbit %04x\n", w_orb);
	fprintf(file, "BGo-0 %04x\n", w_bg0);
	fprintf(file, "BGo-1 %04x\n", w_bg1);
	fprintf(file, "BGo-2 %04x\n", w_bg2);
	fprintf(file, "BGo-3 %04x\n", w_bg3);
	fprintf(file, "BusyCSR %04x\n", w_busy);
	fprintf(file, "BusyStatus %04x\n", w_busy_status);
	fprintf(file, "Inhibit %04x\n", w_inh);
	fprintf(file, "CalReq %04x\n", w_cal);
	fprintf(file, "Counter %04x %04x\n", w_count_msw, w_count_lsw);
	fprintf(file, "TTypeCSR %04x\n", w_ttype);
	fprintf(file, "TTypeFile0 %04x\n", w_ttype_rf0);
	fprintf(file, "TTypeFile1 %04x\n", w_ttype_rf1);
	fprintf(file, "TTypeFile2 %04x\n", w_ttype_rf2);
	fprintf(file, "TTypeFile3 %04x\n", w_ttype_rf3);
	fprintf(file, "TurnSelector %04x\n", w_turn);
	fprintf(file, "PG %04x\n", w_pg);
	fprintf(file, "PGFixed %04x\n", w_pg_fixed);
	fprintf(file, "PGAddressCounter %1x %04x\n", w_pg_ac_msw, w_pg_ac_lsw);
	fprintf(file, "PGFilename %s\n", pgfilename.c_str());
	fprintf(file, "ConfFilename %s\n", conffile.c_str());

	// close file
	fclose(file);
	break;
      }
    case 2:
      LTPConfSetFileName h;
      h.action();
      break;

    }

    return(rtnv);
  }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
class LTPAddChannel : public MenuItem {
public:
  LTPAddChannel() { setName("Add sw busy channel"); }
  int action() {
    ltp->createBusyChannel();
    ++n_channels;
    int status = 0;
    return(status);
  }
};

//------------------------------------------------------------------------------
class LTPSetChannel : public MenuItem {
public:
  LTPSetChannel() { setName("Set busy for channel"); }
  int action() {
    int status = 0;

    int sel = enterInt("channel number",0,n_channels-1);
    
    status = ltp->setBusyChannel(sel);

    return(status);
  }
};

//------------------------------------------------------------------------------
class LTPReleaseChannel : public MenuItem {
public:
  LTPReleaseChannel() { setName("Release busy for channel"); }
  int action() {
    int status = 0;
    int sel = enterInt("channel number",0,n_channels-1);
    
    status = ltp->releaseBusyChannel(sel);

    return(status);
  }
};

//------------------------------------------------------------------------------
class LTPSWBusyDump : public MenuItem {
public:
  LTPSWBusyDump() { setName("Dump software busy status"); }
  int action() {
    int status = 0;
    ltp->dumpSWBusy();
    return(status);
  }
};

//------------------------------------------------------------------------------

int main(int argc, char** argv) {

  CmdArgStr  base_cmd('a', "address", "vme-base-address",  "VME base address in hex", CmdArg::isOPT);
  base_cmd = "";

  CmdLine  cmd(*argv, &base_cmd, nullptr);
  CmdArgvIter  arg_iter(--argc, ++argv);
  cmd.description("Command-line driven program to communicate with an LTP board");
  cmd.parse(arg_iter);

  if (base_cmd.isNULL()) {
    // no argument given, ask for base address
    std::string in;
    std::cout << "<base>?" << std::endl;
    getline(cin,in);
    if(sscanf(in.c_str(),"%x",&base) == EOF) exit(EINVAL);
    if(base < 0x100) base = base << 16;
  } else {
    // get first argument, the base address
    std::string in(base_cmd);
    sscanf(argv[1], "%x", &base);
    if(base < 0x100) base = base << 16;
  }

  printf(" VME base adresss = 0x%06x\n", base);

  // set defaults:
  selchannel = 1; 

  l1a_delay = 10;

  // set default filename
  pgfilename = "pg_A.dat";
  conffile = "conf.dat";

  Menu	menu("LTP Main Menu");

  // generate menu
  menu.init(new LTPOpen);

  Menu menuCases("Predefined Modes/Actions");
  menuCases.add(new LTPCTPMode);
  menuCases.add(new LTPLocalMode);
  menuCases.add(new LTPPGMode);
  menuCases.add(new LTPRPCPulseL1A);
  menuCases.add(new LTPToggleConstBusy);
  menuCases.add(new LTPGenerateECR);
  menu.add(&menuCases);

  Menu menuBC("Bunch Clock");
  menuBC.add(new LTPBCStatus         );
  menuBC.add(new LTPBCLinkInputSource);
  menuBC.add(new LTPBCLemoInputSource );
  menuBC.add(new LTPBCLocalSource    );
  menuBC.add(new LTPBCWrite          );
  menuBC.add(new LTPBCRead           );
  menu.add(&menuBC);

  Menu menuOrbit("Orbit");
  menuOrbit.add(new LTPOrbitStatus         );
  menuOrbit.add(new LTPOrbitLinkInputSource);
  menuOrbit.add(new LTPOrbitLemoInputSource );
  menuOrbit.add(new LTPOrbitLocalSource    );
  menuOrbit.add(new LTPOrbitWrite);
  menuOrbit.add(new LTPOrbitRead);
  menu.add(&menuOrbit);


  Menu menuIO("I/O - L1A, Test Trigger, BGo");
  menuIO.add(new LTPIOChannel);      // select io channel
  menuIO.add(new LTPIOStatus);       // status
  menuIO.add(new LTPIOISource);      // input source for CTP link out
  menuIO.add(new LTPIOOSource);      // LEMO output source
  menuIO.add(new LTPIOLocalISource); // source for local input
  menuIO.add(new LTPIOPFN);          // PFN duration
  menuIO.add(new LTPIOBusy);         // enable Busy gating
  menuIO.add(new LTPIOInhibit);      // enable Inhibit Timer gating
  menuIO.add(new LTPIOPG);           // enable Pattern Generator gating
  menuIO.add(new LTPIOPulse);        // produce software pulse
  menuIO.add(new LTPIOWrite);        // write to register
  menuIO.add(new LTPIORead);         // read from register
  menu.add(&menuIO);

  Menu menuBusy("Busy");
  menuBusy.add(new LTPBusyLinkInputSource);
  menuBusy.add(new LTPBusySumLink);
  menuBusy.add(new LTPBusySumTTL);
  menuBusy.add(new LTPBusySumNIM);
  menuBusy.add(new LTPBusyConstant);
  menuBusy.add(new LTPBusySumDead);
  menuBusy.add(new LTPBusyLEMOSource);
  menuBusy.add(new LTPBusyDeadSource );
  menuBusy.add(new LTPBusyDeadDuration);
  menuBusy.add(new LTPBusyPG);
  menuBusy.add(new LTPBusyStatus);
  menuBusy.add(new LTPBusyWrite);
  menuBusy.add(new LTPBusyRead);
  menu.add(&menuBusy);

  Menu menuTType("Trigger Type");
  menuTType.add(new LTPTTypeStatus         );
  menuTType.add(new LTPTTypeRegisterFiles  );
  menuTType.add(new LTPTTypeLinkInputSource);
  menuTType.add(new LTPTTypeECLSource      );
  menuTType.add(new LTPTTypeLocalSource    );
  menuTType.add(new LTPTTypeECLEnable      );
  menuTType.add(new LTPTTypeFileAddress    );
  menuTType.add(new LTPTTypeDelay          );
  menuTType.add(new LTPTTypeTriggerSource  );
  menuTType.add(new LTPTTypeFifo          );
  menuTType.add(new LTPTTypeWrite          );
  menuTType.add(new LTPTTypeRead           );
  menu.add(&menuTType);


  Menu menuCal("Calibration Request");
  menuCal.add(new LTPCalStatus     );
  menuCal.add(new LTPCalSource     );
  menuCal.add(new LTPCalTestPath   );
  menuCal.add(new LTPCalWritePreset);
  menuCal.add(new LTPCalWrite      );
  menuCal.add(new LTPCalRead       );
  menu.add(&menuCal);

  Menu menuCounter("32-bit Counter");
  menuCounter.add(new LTPCounterStatus);
  menuCounter.add(new LTPCounterReset );
  menuCounter.add(new LTPCounterSource);
  menu.add(&menuCounter);

  Menu menuPG("Pattern Generator");
  menuPG.add(new LTPPGStatus);             // set file name
  menuPG.add(new LTPPGSetFileName);        // set file name
  menuPG.add(new LTPPGLoadFile);           // load file into memory
  menuPG.add(new LTPPGRun);                // set run mode
  menuPG.add(new LTPPGStart);              // start sequence
  menuPG.add(new LTPPGTrigger);            // multiple trigger for single shot
  menuPG.add(new LTPPGReset);              // reset pattern generator
  menuPG.add(new LTPPGReadMemory);         // reset pattern generator
  menuPG.add(new LTPPGReadFixedValue);     // read fixed value register
  menuPG.add(new LTPPGWriteFixedValue);    // write fixed value register
  menu.add(&menuPG);

  Menu menuMisc("Miscellaneous");
  menuMisc.add(new LTPTurnSelector);
  menuMisc.add(new LTPOrbitOutLemoSelection);
  menuMisc.add(new LTPInhibit);
  menu.add(&menuMisc);

  Menu menuGlobal("Global Configuration");
  menuGlobal.add(new LTPStatus);
  menuGlobal.add(new LTPDump);
  menuGlobal.add(new LTPDumpToFile);
  menuGlobal.add(new LTPConfigureFromFile);
  menu.add(&menuGlobal);

  menu.add(new LTPReset);

  Menu menuSWBusy("Test SW Busy");
  menuSWBusy.add(new LTPAddChannel);
  menuSWBusy.add(new LTPSetChannel);
  menuSWBusy.add(new LTPReleaseChannel);
  menuSWBusy.add(new LTPSWBusyDump);
  menu.add(&menuSWBusy);

  menu.exit(new LTPClose);

  // start menu
  menu.execute();

  return 0;
}


