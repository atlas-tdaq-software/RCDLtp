/****************************************************************/
/* file: ltp_test_standalone.cc                                 */
/*                                                              */
/* Program to test ATLAS LTP modules in stand alone mode 	*/
/*								*/
/* Author: Per Gallno						*/
/* Ported from CATY to C by: Markus Joos, PH/ESS                */
/*                                                              */
/*******C 2007 - A nickel program worth a dime*******************/

//==============================================================
// The module is of type D16/A24 (AM = 39, 3A, 3D, 3E) standard
// or D16/A16 (AM = 29, 2D)
//==============================================================

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include "rcc_error/rcc_error.h"
#include "rcc_time_stamp/tstamp.h"
#include "ROSGetInput/get_input.h"
#include "DFDebug/DFDebug.h"
#include "vme_rcc/vme_rcc.h"

//Macros
#define PTO      {char cc[9]; printf("Press<return> to continue\n"); fgets(cc, 9, stdin);}
#define PTO2     {char cc[9]; fgets(cc, 9, stdin);}


//Constants
#define LTP_BASE            0xFF1000
#define MANUFID             0x20
#define BOARDID             0x30
#define REVISION            0x40
#define SIGNUM              42
#define CERNID              0x00080030

#define REG_20BITCNT_LSW    0xF2
#define REG_20BITCNT_MSW    0xF0
#define REG_MEMIO           0xE8
#define REG_FIXVAL          0xE6
#define REG_PATGEN_CSR      0xE4
#define REG_SINGLSHOT       0xE2
#define REG_SW_RST          0xE0
#define REG_BUSY_SR         0xD4
#define REG_BUSY_CR         0xD2
#define REG_CAL_CSR         0xD0
#define REG_FIFO_RST        0xCC
#define REG_FIFO_IO         0xCA
#define REG_TTYPE_CSR       0xC8
#define REG_TTYPE_REGFILE3  0xC6
#define REG_TTYPE_REGFILE2  0xC4
#define REG_TTYPE_REGFILE1  0xC2
#define REG_TTYPE_REGFILE0  0xC0
#define REG_TURN_CR         0xBC
#define REG_32BITCNT_LSW    0xBA
#define REG_32BITCNT_MSW    0xB8
#define REG_32BITCNT_RST    0xB6
#define REG_INH_TIMER       0xB4
#define REG_ORB_CSR         0xB2
#define REG_BC_CSR          0xB0
#define REG_BG3_CSR         0xA6
#define REG_BG2_CSR         0xA4
#define REG_BG1_CSR         0xA2
#define REG_BG0_CSR         0xA0
#define REG_TR3_CSR         0x96
#define REG_TR2_CSR         0x94
#define REG_TR1_CSR         0x92
#define REG_L1A_CSR         0x90
#define REG_INTID           0x86
#define REG_INTCSR          0x84
#define REG_SWIRQ           0x82
#define REG_SWRST           0x80


//Globals
u_int ret, cont;
int handle = 999;
static VME_MasterMap_t master_map = {LTP_BASE, 0x1000, VME_A24, 0};
static struct sigaction sa, sa2;


//Prototypes
u_int vw16(u_int offset, u_short data);
u_int vr16(u_int offset, u_short *data);
u_int test_register(u_int offset, u_int size, char *name);
void fifostate(void);
void rwfifo(void);
void sw_pulse(void);
void patgen(void);
void chkevcnt(void);
void tstmodreg(void);
void rdallregs(void);
void swrst(void);
void rdallstat(void);
void init_tltp(void);
void readprom4(u_int offset, u_int *concat);
void read_prom(void);
void write_prom(void);
void writeprom4(u_int offset, u_int concat);
void test_menu(void);
void test_sequential(void);
void test_1(void);
void test_2(void);
void test_3(void);
void test_4(void);
void test_5(void);
void test_6(void);
void test_7(void);
void test_8(void);
void test_9(void);
void test_10(void);
void test_11(void);
void test_12(void);
void test_13(void);
void test_14(void);
void test_15(void);
void test_16(void);
void test_17(void);
void test_18(void);
void SigQuitHandler(int signum);
void vme_signal_handler(int signum);


/************************************/
u_int vw16(u_int offset, u_short data)
/************************************/
{
  if (handle == 999) 
  {
    printf("The LTP is currently not mapped.\n"); 
    return (999);
  }  
  
  ret = VME_WriteSafeUShort(handle, offset, data); 
  if (ret) 
  { 
    VME_ErrorPrint(ret); 
    printf("The program will continue but you may want to terminate it anyway\n");
    return (ret);
  } 
  return(0); 
}


/*************************************/
u_int vr16(u_int offset, u_short *data)
/*************************************/
{
  if (handle == 999) 
  {
    printf("The LTP is currently not mapped.\n"); 
    return (999);
  }  
  
  ret = VME_ReadSafeUShort(handle, offset, data); 
  if (ret) 
  { 
    VME_ErrorPrint(ret); 
    printf("The program will continue but you may want to terminate it anyway\n");
    return (ret);
  }  
  return(0); 
}


/************/
int main(void)
/************/
{
  int stat;
  char mode[9];
  
  ret = ts_open(1, TS_DUMMY);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }
    
  ret = VME_Open();
  if (ret)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }
  
  ret = VME_MasterMap(&master_map, &handle);  
  if (ret)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }
  
  // Signal handler for ctrl + //
  sigemptyset(&sa2.sa_mask);
  sa2.sa_flags = 0;
  sa2.sa_handler = SigQuitHandler;
  stat = sigaction(SIGQUIT, &sa2, nullptr);
  if (stat < 0)
  {
    printf("Cannot install signal handler for ctrl + // (error=%d)\n", stat);
    exit(1);
  }  

  // Signal handler for VMEbus interrupts
  sigemptyset(&sa.sa_mask) ;
  sa.sa_flags = 0;
  sa.sa_handler =  vme_signal_handler;
  stat = sigaction(SIGNUM, &sa, nullptr);
  if (stat < 0)
  {
    printf("Cannot install signal handler for VMEbus interrupts (error=%d)\n", stat);
    exit(1);
  }   
  
  vw16(REG_INTCSR, 0);  //disable interrupts
  
  printf("START OF TEST PROGRAM\n");
  printf("THE PROGRAM IS EITHER MENU DRIVEN [m] OR SEQUENTIAL [s]\n");
  printf("SELECT [m] or [s] : ");
  fgets(mode, 9, stdin);

  if (mode[0] == 'm' || mode[0] == 'M')
    test_menu();
  else if (mode[0] == 's' || mode[0] == 'S')
    test_sequential(); 
  else
    printf("Have a nice day\n");
  
  ret = ts_close(TS_DUMMY);
  if (ret)
    rcc_error_print(stdout, ret);
  
  ret = VME_MasterUnmap(handle);
  if (ret)
    VME_ErrorPrint(ret);
  
  ret = VME_Close();
  if (ret)
    VME_ErrorPrint(ret);
}


/******************/
void test_menu(void)
/******************/
{
  static int fun = 1;

  while(fun != 0)
  {  
    printf("SELECT ONE OF THE FOLLOWING TASKS:\n");
    printf("CHECK SUPPLY VOLTAGES.........[ 1]\n");
    printf("CHECK REFERENCE VOLTAGES......[ 2]\n");
    printf("CHECK CLOCK DISTRIBUTION......[ 3]\n");
    printf("SELECT MODULE BASE ADDRESS....[ 4]\n");
    printf("SELECT MODULE AM CODE.........[ 5]\n");
    printf("CHECK MODULE VME RESPONSE.....[ 6]\n");
    printf("WR/RD ID-PROM.................[ 7]\n");
    printf("TEST ALL VME REGISTERS........[ 8]\n");
    printf("PAT GEN MEMORY TESTS..........[ 9]\n");
    printf("PAT GEN LOAD/READ/RUN TEST....[10]\n");
    printf("TEST EVENT COUNTER ...........[11]\n");
    printf("TEST ALL COAX OUTPUTS + LEDs..[12]\n");
    printf("TEST ALL COAX INPUTS..........[13]\n");
    printf("DEAD TIME GENERATION TEST.....[14]\n");
    printf("TURN COUNTER TEST.............[15]\n");
    printf("TRIG-TYPE/FIFO TEST...........[16]\n");  
    printf("CHECK VME INTERRUPT...........[17]\n");
    printf("UTILITIES SUB-MENU............[18]\n");
    
    printf("QUIT PROGRAM...................[ 0]\n");
    printf("Your choice: ");
    fun = getdecd(fun);
    if (fun == 1) test_1();
    if (fun == 2) test_2();
    if (fun == 3) test_3();
    if (fun == 4) test_4();
    if (fun == 5) test_5();
    if (fun == 6) test_6();
    if (fun == 7) test_7();
    if (fun == 8) test_8();
    if (fun == 9) test_9();
    if (fun == 10) test_10();
    if (fun == 11) test_11();
    if (fun == 12) test_12();
    if (fun == 13) test_13();
    if (fun == 14) test_14();
    if (fun == 15) test_15();
    if (fun == 16) test_16();
    if (fun == 17) test_17();
    if (fun == 18) test_18();
  }
}


/************************/
void test_sequential(void)
/************************/
{
//MJ: It has to be possible to exit from the sequential test at certain locations
  test_1();
  test_2();
  test_3();
  test_4();
  test_5();
  test_6();
  test_7();
  test_8();
  test_9();
  test_10();
  test_11();
  test_12();
  test_13();
  test_14();
  test_15();
  test_16();
  test_17();
  test_18();
}


/***************/
void test_1(void)
/***************/
{
  printf("                SUPPLY VOLTAGE CHECK\n");
  printf("1. Put the module on an extender and turn ON the crate power\n\n");
  printf("2. Measure between GND and some IC's VCC pins: +5.0V +/- 0.1\n\n");
  printf("3. Measure between GND and some IC's VEE pins: -5.2V +/- 0.1\n");
}


/***************/
void test_2(void)
/***************/
{
  printf("           INPUT REFERENCE VOLTAGE CHECK\n");
  printf("1. Put the module on an extender and turn ON the crate power\n\n");
  printf("2. Measure between GND and IC35/pin-6: -0.4V +/- 0.05\n\n");
  printf("3. Measure between GND and IC44/pin-2: +0.4V +/- 0.05\n");
}


/***************/
void test_3(void)
/***************/
{
  printf("                CLOCK DISTRIBUTION CHECK\n");
  printf("1. Put the module on an extender and turn ON the crate power\n\n");
  printf("2. With an oscilloscope + probe (10Meg/10X) check clock at:\n");
  printf("   IC31/pin-142 and pin 87 of IC's: 9, 20, 21, 38, 45\n");
  printf("   and pin 90 of IC's: 39 and 46\n\n");
  printf("   The clock should be a smooth 40.08 MHz square wave\n");
  printf("   with a 4 Volt swing\n");
}


/***************/
void test_4(void)
/***************/
{  
  printf("             SET/ADJUST THE MODULE BASE ADDRESS\n");

  //Unmap the module
  ret = VME_MasterUnmap(handle);
  if (ret)
    VME_ErrorPrint(ret);

  //Get the new base address
  printf("1. Enter the module base address: ");
  master_map.vmebus_address = gethexd(master_map.vmebus_address);
  
  //Map the module
  ret = VME_MasterMap(&master_map, &handle);  
  if (ret)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }
  
  printf("3. The module base address is set to: 0x%08x\n", master_map.vmebus_address);
  printf("4.    !!!! CHECK SWITCH SETTINGS [SW1..SW4] ON MODULE !!!\n");
}


/***************/
void test_5(void)
/***************/
{
  printf("There is nothing to be done here");
  printf("AM code 0x39 will be used exclusively\n");
}


/***************/
void test_6(void)
/***************/
{
  printf("               CHECK VME ACCESS\n\n");
  printf("   Press <ctrl>+\\ to stop the routine.\n");
 
  cont = 1;
  while(cont)
  {
    ret = vw16(REG_SWRST, 1);    
    if (ret)
    {
      printf("   !!! Recheck ADDRESS and AM settings !!!\n");
      PTO
    }
  }

  if (!ret)
    printf("   VME access is OK\n");

  PTO
}


/***************/
void test_7(void)
/***************/
{
  char mode[9];

  printf("      READ/WRITE IDPROM\n");
  read_prom();
  
  printf("To write IDPROM enter [w], to quit [q]\n");
  fgets(mode, 9, stdin);

  if (mode[0] == 'w' || mode[0] == 'W')
  {
    write_prom();
    read_prom();
  }
}


/***************/
void test_8(void)
/***************/
{
  char name[15];
 
  printf(" ROUTINE TO TEST ALL REGISTERS ONE BY ONE\n"); 

  strcpy(name, "INTCSR");
  ret = test_register(REG_INTCSR, 6, name);
  if (ret)
    return;

  strcpy(name, "STATID");
  ret = test_register(REG_INTID, 8, name);
  if (ret)
    return;

  strcpy(name, "L1ACSR");
  ret = test_register(REG_L1A_CSR, 8, name);
  if (ret)
    return;

  strcpy(name, "TRIG1CSR");
  ret = test_register(REG_TR1_CSR, 8, name);
  if (ret)
    return;

  strcpy(name, "TRIG2CSR");
  ret = test_register(REG_TR2_CSR, 8, name);
  if (ret)
    return;

  strcpy(name, "TRIG2CSR");
  ret = test_register(REG_TR3_CSR, 8, name);
  if (ret)
    return;

  strcpy(name, "BGO0CSR");
  ret = test_register(REG_BG0_CSR, 8, name);
  if (ret)
    return;

  strcpy(name, "BGO1CSR");
  ret = test_register(REG_BG1_CSR, 8, name);
  if (ret)
    return;

  strcpy(name, "BGO2CSR");
  ret = test_register(REG_BG2_CSR, 8, name);
  if (ret)
    return;

  strcpy(name, "BGO3CSR");
  ret = test_register(REG_BG3_CSR, 8, name);
  if (ret)
    return;

  strcpy(name, "BCCSR");
  ret = test_register(REG_BC_CSR, 3, name);
  if (ret)
    return;

  strcpy(name, "ORBCSR");
  ret = test_register(REG_ORB_CSR, 8, name);
  if (ret)
    return;

  strcpy(name, "INHTIMER");
  ret = test_register(REG_INH_TIMER, 12, name);
  if (ret)
    return;

  strcpy(name, "TURNCR");
  ret = test_register(REG_TURN_CR, 5, name);
  if (ret)
    return;

  strcpy(name, "TRIGTREGF0");
  ret = test_register(REG_TTYPE_REGFILE0, 8, name);
  if (ret)
    return;

  strcpy(name, "TRIGTREGF1");
  ret = test_register(REG_TTYPE_REGFILE1, 8, name);
  if (ret)
    return;

  strcpy(name, "TRIGTREGF2");
  ret = test_register(REG_TTYPE_REGFILE2, 8, name);
  if (ret)
    return;

  strcpy(name, "TRIGTREGF3");
  ret = test_register(REG_TTYPE_REGFILE3, 8, name);
  if (ret)
    return;

  strcpy(name, "TRIGTYPCSR");
  ret = test_register(REG_TTYPE_CSR, 10, name);
  if (ret)
    return;

  strcpy(name, "CALCSR");
  ret = test_register(REG_CAL_CSR, 7, name);
  if (ret)
    return;

  strcpy(name, "BUSYCR");
  ret = test_register(REG_BUSY_CR, 16, name);
  if (ret)
    return;

  strcpy(name, "PATGENCSR");
  ret = test_register(REG_PATGEN_CSR, 5, name);
  if (ret)
    return;

  strcpy(name, "FIXVALREG");
  ret = test_register(REG_FIXVAL, 16, name);
  if (ret)
    return;

  strcpy(name, "ADCNTREGH");
  ret = test_register(REG_20BITCNT_MSW, 4, name);
  if (ret)
    return;

  strcpy(name, "ADCNTREGL");
  ret = test_register(REG_20BITCNT_LSW, 11116, name);
  if (ret)
    return;

  printf("All Registers Tested\n"); 
  PTO
}


/***************/
void test_9(void)
/***************/
{
  u_short pgmw0(0x0000), pgmw1(0x5555), pgmw2(0xaaaa), pgmw3(0xffff);
  u_short wpatt(0), rpatt(0), nwpatth(0), nwpattl(0), pgmr0(0), pgmr1(0), pgmr2(0), pgmr3(0), nrpattl(0), nrpatth(0);
  u_int nrpatt(0), errflag(0), mask(0), n(0), l(0), nlines(20), nwpatt(1);
  
  printf("  ROUTINE TO CHECK PATTERN GENERATOR DATA/ADDRESS LINES\n"); 
  printf("     --- DATA LINE TEST ---\n");
  printf("-The PG FSM is now reset,\n");
  printf(" then data written, then read and compared.\n");
  printf("-The data is: 0000/5555/AAAA/FFFF\n");

  vw16(REG_PATGEN_CSR, 0);
  vw16(REG_SWRST, 1);
  vw16(REG_SW_RST, 1);
  vw16(REG_BC_CSR, 3);
  vw16(REG_20BITCNT_MSW, 0);
  vw16(REG_20BITCNT_LSW, 4);
  vw16(REG_MEMIO, pgmw0);
  vw16(REG_MEMIO, pgmw1);
  vw16(REG_MEMIO, pgmw2);
  vw16(REG_MEMIO, pgmw3);
  vr16(REG_MEMIO, &pgmr0);
  vr16(REG_MEMIO, &pgmr1);
  vr16(REG_MEMIO, &pgmr2);
  vr16(REG_MEMIO, &pgmr3);

  printf("position 0 written data: 0x%04x\n", pgmw0);
  printf("position 0 read data:    0x%04x\n", pgmr0);

  if (pgmw0 != pgmr0)
   errflag = 1;
  
  printf("position 1 written data: 0x%04x\n", pgmw1);
  printf("position 1 read data:    0x%04x\n", pgmr1);

  if (pgmw1 != pgmr1)
    errflag = 1;
  
  printf("position 2 written data:0x%04x\n", pgmw2);
  printf("position 2 read data:   0x%04x\n", pgmw2);

  if (pgmw2 != pgmr2)
    errflag = 1;
    
  printf("position 3 written data:0x%04x\n", pgmw3);
  printf("position 3 read data:   0x%04x\n", pgmw3);

  if (pgmw3 != pgmr3)
    errflag = 1;

  if (errflag)
  {
    printf("! PG DATA LINE PROBLEM !\n");
    PTO
  }

  printf("  PATTERN GENERATOR DATA LINE TEST is OK !!!\n");  
  PTO
  printf("     --- 20 ADDRESS LINE TEST ---\n");
  printf("-The PG FSM will be reset,\n");
  printf("memory written with data,\n");
  printf(" each 2**n memory location = 0001, remaining = 0000\n");
  printf("     !!! TEST TAKES 8 min + 4 min !!!\n");

  vw16(REG_L1A_CSR, 3);
  vw16(REG_PATGEN_CSR, 0);
  
  nlines--;
  nwpatt = nwpatt << nlines;
  n = nwpatt;
  mask = nwpatt;
  nwpatth = ((nwpatt >> 16) & 0xFFFF);
  nwpattl = nwpatt;

  vw16(REG_20BITCNT_MSW, nwpatth);
  vw16(REG_20BITCNT_LSW, nwpattl);
  vr16(REG_20BITCNT_MSW, &nrpatth);
  vr16(REG_20BITCNT_LSW, &nrpattl);

  nrpatt = ((nrpatth << 16) | nrpattl) & 0xffffff;

  printf(" Content of Address Counter Shadow Register:\n");
  printf(" Written number: 0x%04x\n", nwpatt); 
  printf(" Read number:    0x%04x\n", nrpatt); 
  printf("           ARE THEY THE SAME ???\n");
  PTO
  vw16(REG_SW_RST, 1);
  printf("Writing memory...\n");

  do
  {
    wpatt = 0;
    if (mask == n)
    {
      wpatt = 1;
      mask = mask >> 1;
    }
    vw16(REG_MEMIO, wpatt);
    n--;
    vr16(REG_PATGEN_CSR, &rpatt);
  } while((rpatt & 0xf800) == 0x7000); 
 
  if (n != 0)
  {
    printf("!!! ERROR - ALL POSITIONS NOT WRITTEN !!!\n");
    printf("    Remaining positions to be written (hex): 0x%04x\n", n);
    PTO
  }
  else
  {
    printf("  All positions successfully written !\n"); 
    PTO
  }

  printf("Reading memory...\n");
  l = nwpatt;

  do
  {
    vr16(REG_MEMIO, &rpatt);

    if (rpatt)
    {
      u_int t = 0;
      float q;

      q = l;

      do
      {
	q = q / 2;
	t++;
      } while(q > 0.5);

      printf("MEM ADDR LINE: %d TRUE\n", t);    
    }

    l--;
    vr16(REG_PATGEN_CSR, &rpatt);
  }
  while((rpatt & 0xf800) == 0xe000);  

  printf("Remaining positions to be read: %d\n", l);
}


/****************/
void test_10(void)
/****************/
{
  u_int loop(0);
  u_short pattdata(0), nrpatt(0), rpatt(0), nwpatt(0), nwpatth(0), nwpattl(0), orbhits(0), noorbhits(0);
  u_short nrpatth(0), nrpattl(0);
  char action[9];
  
  printf("   PATTERN GENERATOR RUN TEST ROUTINE\n");
  printf(" A pattern will be loaded to produce ORBIT pulses\n");
  
  vw16(REG_PATGEN_CSR, 0);
  vw16(REG_SWRST, 1);
  vw16(REG_SW_RST, 1);
  vw16(REG_BC_CSR, 3);
  vw16(REG_ORB_CSR, 0xf);
  vr16(REG_PATGEN_CSR, &rpatt);

  printf(" The PATTERN GENERATOR CSR reads: 0x%04x\n", rpatt);
  printf(" If not equal to x100 there is an hardware error !!!\n");
  printf(" To continue enter [c/C], to quit [q/Q]:\n"); 
  fgets(action, 9, stdin);
  if (action[0] == 'q' || action[0] == 'Q')
    return;
  
  nwpatt =  7998;  
  nwpatth = nwpatth >> 16;
  nwpattl = nwpatt;

  vw16(REG_20BITCNT_MSW, nwpatth);
  vw16(REG_20BITCNT_LSW, nwpattl);
  vr16(REG_20BITCNT_MSW, &nrpatth);
  vr16(REG_20BITCNT_LSW, &nrpattl);

  nrpatt = ((nrpatth << 16) | nrpattl) & 0xffffff;

  printf(" The Pattern Generator address counter is loaded now\n");
  printf(" Written number: 0x%04x\n", nwpatt);
  printf(" Read number:    0x%04x\n", nrpatt);
  printf("           ARE THEY THE SAME ???\n");

  vr16(REG_PATGEN_CSR, &rpatt);

  printf(" The PATTERN GENERATOR CSR reads: 0x%04x\n", rpatt);
  printf(" If not equal to 0 there is an hardware error !!!\n");
  printf(" To continue enter [c/C], to quit [q/Q]:\n"); 
  fgets(action, 9, stdin);
  if (action[0] == 'q' || action[0] == 'Q')
    return;

  printf(" - Data will now be written in the Pat Gen Memory\n"); 

  for (loop = 0; loop < 2000; loop++)
    vw16(REG_MEMIO, pattdata);
  
  pattdata = 1;
  for (loop = 0; loop < 40; loop++)
    vw16(REG_MEMIO, pattdata);  

  pattdata = 0;
  for (loop = 0; loop < 3960; loop++)
    vw16(REG_MEMIO, pattdata);  

  pattdata = 1;
  for (loop = 0; loop < 40; loop++)
    vw16(REG_MEMIO, pattdata);  

  pattdata = 0;
  for (loop = 0; loop < 1958; loop++)
    vw16(REG_MEMIO, pattdata);  

  vr16(REG_PATGEN_CSR, &rpatt);

  if ((rpatt & 0xf800) != 0)
  {
    printf(" All patterns have now been entered and\n");
    printf(" the required memory locations are not filled !!!!\n");
    printf(" To continue enter [c/C], to quit [q/Q]:\n"); 
    fgets(action, 9, stdin);
    if (action[0] == 'q' || action[0] == 'Q')
      return;
  }

  printf(" - Data will now be read from the Pat Gen Memory\n"); 
  for (loop = 0; loop < 7998; loop++)
  {
    vr16(REG_MEMIO, &pattdata);
    if (pattdata & 1)
      orbhits++;
    else
      noorbhits++;
  }

  vr16(REG_PATGEN_CSR, &rpatt);
  if (rpatt & 0xf800)
  {
    printf(" All patterns have now been read back and\n");
    printf(" the required memory locations has not been accessed !!!!\n");
    printf(" To continue enter [c/C], to quit [q/Q]:\n"); 
    fgets(action, 9, stdin);
    if (action[0] == 'q' || action[0] == 'Q')
      return;
  }
  
  printf(" Number of locations with a '1' should be   80,\n");
  printf(" and counts to: %d\n", orbhits);
  printf(" Number of locations with a '0' should be 7918,\n");
  printf(" and counts to: %d\n\n", noorbhits);
  printf("  - The ORBIT o/p will now have a 10 kHz signal,\n");
  printf("    where each ORBIT pulse is 1 microsec long\n"); 
  PTO
  
  vw16(REG_PATGEN_CSR, 5);
  vw16(REG_SINGLSHOT, 1);
  PTO

  vw16(REG_PATGEN_CSR, 0); 
  printf(" To continue enter [c/C], to quit [q/Q]:\n"); 
  fgets(action, 9, stdin);
  if (action[0] == 'q' || action[0] == 'Q')
    return;

  init_tltp();

  printf(" The Pat Gen will now generate all ZEROES and all paths'\n");
  printf(" STATUS registers will be read and displayed\n");
  PTO
    
  vw16(REG_20BITCNT_LSW, 1);
  vw16(REG_FIXVAL, 0);
  vw16(REG_PATGEN_CSR, 2);

  rdallstat();

  printf(" The Pat Gen will now generate all ONES and all paths'\n");
  printf(" STATUS registers will be read and displayed\n");
  PTO
  
  vw16(REG_FIXVAL, 0xFFFF);
  rdallstat();
  init_tltp();
}


/****************/
void test_11(void)
/****************/
{
  char action[9];
  u_int rcount, loop, ecrcnt;
  u_short rcounth, rcountl;

  printf("    CHECK INCREMENT/RESET/READ EVENT/ORBIT COUNTER\n");
  init_tltp();
  printf("-The PAT-GEN will generate ORBITS/TRIGGERS and BGo's at 1 MHz\n");

  vw16(REG_ORB_CSR, 0x000F);
  vw16(REG_20BITCNT_MSW, 0);
  vw16(REG_20BITCNT_LSW, 38);

  for (loop = 1; loop < 4; loop++)
    vw16(REG_MEMIO, 0x079F);

  for (loop = 1; loop < 35; loop++)
    vw16(REG_MEMIO, 0);

  vw16(REG_PATGEN_CSR, 5);
  vw16(REG_SINGLSHOT, 1);

  printf("-The PATTERN GENERATOR should now be running\n");
  printf("-Check PATTERN GENERATOR running status on-board LED\n");
  printf("-Check ORBIT/L1A/TRIG[3:1]/BGo[3:0] NIM o/p's for a 100 ns pulse\n");
  printf(" every ~1 us\n"); 
  PTO
  printf("-The Counter increment source connections and reset function\n");
  printf(" will now be checked in turn\n"); 

  vw16(REG_32BITCNT_RST, 0);

  for (loop = 1; loop < 10; loop++)
  {
    printf("-Counter incremented by: ");
    if (loop == 1) {printf("ORBIT\n");   vw16(REG_ORB_CSR, 0x002F);}
    if (loop == 2) {printf("L1A\n");     vw16(REG_ORB_CSR, 0x0083);}
    if (loop == 3) {printf("TRIG[1]\n"); vw16(REG_ORB_CSR, 0x0093);}
    if (loop == 4) {printf("TRIG[2]\n"); vw16(REG_ORB_CSR, 0x00A3);}
    if (loop == 5) {printf("TRIG[3]\n"); vw16(REG_ORB_CSR, 0x00B3);}
    if (loop == 6) {printf("BGo[0]\n");  vw16(REG_ORB_CSR, 0x0043);}
    if (loop == 7) {printf("BGo[1]\n");  vw16(REG_ORB_CSR, 0x0053);}
    if (loop == 8) {printf("BGo[2]\n");  vw16(REG_ORB_CSR, 0x0063);}
    if (loop == 9) {printf("BGo[3]\n");  vw16(REG_ORB_CSR, 0x0073);}
    
    ts_delay(200000);

    vw16(REG_ORB_CSR, 0xF);
    vr16(REG_32BITCNT_LSW, &rcountl);
    vr16(REG_32BITCNT_MSW, &rcounth);

    rcount = (rcounth << 16) | rcountl;
    printf("The Counter read: %d (0x%08x)\n", rcount, rcount);

    vw16(REG_32BITCNT_RST, 0); 
    vr16(REG_32BITCNT_LSW, &rcountl);
    vr16(REG_32BITCNT_MSW, &rcounth);
    
    rcount = (rcounth << 16) | rcountl;

    printf("The Counter after reset read: %d\n", rcount); 
    printf(" To continue enter [c/C], to quit [q/Q]:\n"); 
    fgets(action, 9, stdin);
    if (action[0] == 'q' || action[0] == 'Q')
      return;
  }

  vw16(REG_PATGEN_CSR, 0);

  printf("-The Counter will now be incremented by the local ORBIT\n");
  printf(" and reset by the ECR = BGo[1] from the PAT-GEN\n"); 
  PTO
  
  vw16(REG_ORB_CSR, 0x37);
  ts_delay(200000);

  vw16(REG_ORB_CSR, 7);
  vr16(REG_32BITCNT_LSW, &rcountl);
  vr16(REG_32BITCNT_MSW, &rcounth);

  rcount = (rcounth << 16) | rcountl;
  printf("The Counter read: %d (0x%08x)\n", rcount, rcount);

  vw16(REG_PATGEN_CSR, 2);
  vw16(REG_FIXVAL, 0x100);
  vw16(REG_FIXVAL, 0);
  vr16(REG_32BITCNT_LSW, &rcountl);
  vr16(REG_32BITCNT_MSW, &rcounth);

  rcount = (rcounth << 16) | rcountl;
  printf("The Counter after reset read: %d\n", rcount); 
  PTO
 
  printf("-The Counter will now be incremented by the TRIG[1]\n");
  printf(" and reset by the ECR = BGo[1] both from the PAT-GEN\n"); 
  printf("-The Counter is in split mode counting ECR and TRIGGERs\n");

  vw16(REG_ORB_CSR, 0xD3);
  vw16(REG_32BITCNT_RST, 0);
  PTO

  vw16(REG_L1A_CSR, 3);
  vw16(REG_TR1_CSR, 0xF);
  vw16(REG_TR2_CSR, 3);
  vw16(REG_TR3_CSR, 3);
  vw16(REG_BG0_CSR, 3);
  vw16(REG_BG1_CSR, 3);
  vw16(REG_BG2_CSR, 3);
  vw16(REG_BG3_CSR, 3);
  vw16(REG_PATGEN_CSR, 5);
  vw16(REG_SINGLSHOT, 1);

  ts_delay(200000);
  
  vw16(REG_PATGEN_CSR, 0);
  vw16(REG_TR1_CSR, 3); 
  vr16(REG_32BITCNT_LSW, &rcountl);
  vr16(REG_32BITCNT_MSW, &rcounth); 

  ecrcnt = (rcounth >> 8) & 0xff;
  rcount = ((rcounth << 16) | rcountl) & 0xffffff;

  printf("The TRIG[1] Counter read: 0x%08x\n", rcount);
  printf(" (i.e. = 0x2E7?)\n");
  printf("The ECR     Counter read: 0x%08x\n", ecrcnt); 
  printf(" (i.e. = 0)\n");
  PTO
  printf("-An ECR (= BGo[1]) is now generated\n\n");
  
  vw16(REG_BG1_CSR, 0xF);
  vw16(REG_PATGEN_CSR, 1);
  vw16(REG_SINGLSHOT, 1);
  vr16(REG_32BITCNT_LSW, &rcountl); 
  vr16(REG_32BITCNT_MSW, &rcounth); 

  ecrcnt = (rcounth >> 8) & 0xff;
  rcount = ((rcounth << 16) | rcountl) & 0xffffff;

  printf("The TRIG[1] Counter read: 0x%08x\n", rcount);
  printf(" (i.e. = 0)\n");
  printf("The ECR     Counter read: 0x%08x\n", ecrcnt); 
  printf(" (i.e. = 1)\n");
  PTO
  printf("- Now incrementing by ECR\n");
  
  for (loop = 1; loop < 0x55; loop++)
    vw16(REG_SINGLSHOT, 1);

  vw16(REG_PATGEN_CSR, 0);
  vr16(REG_32BITCNT_LSW, &rcountl);
  vr16(REG_32BITCNT_MSW, &rcounth);

  ecrcnt = (rcounth >> 8) & 0xff;
  rcount = ((rcounth << 16) | rcountl) & 0xffffff;

  printf("The TRIG[1] Counter read: 0x%08x\n", rcount);
  printf(" (i.e. = 0)\n");
  printf("The ECR     Counter read: 0x%08x\n", ecrcnt); 
  printf(" (i.e. = x55)\n");  
}


/****************/
void test_12(void)
/****************/
{
  printf("      ROUTINE TO CHECK ALL COAX OUTPUT LEVELS\n");
  init_tltp();

  printf(" -Measure output levels with a 50 ohm terminated oscilloscope.\n\n");
  printf(" -All NIM outputs, except BC/CLOCK, should now be at 0 Volt.\n");
  printf(" -All LEDS, except BC/CLOCK, should now be OFF.\n");
  printf(" -The ORBIT+LVL1/ecl output are AC-coupled - NO level change!\n");
  PTO
 
  vw16(REG_PATGEN_CSR, 2);
  vw16(REG_20BITCNT_LSW, 1); 
  vw16(REG_FIXVAL, 0x879F);

  printf(" -All NIM outputs, except BC/CLOCK, should now be at -0.8 Volt.\n");
  printf(" -All LEDS, including BC/CLOCK, should now be ON.\n");
  printf(" -The ORBIT+LVL1/ecl output are AC-coupled - NO level change!\n");
  PTO
  
  vw16(REG_PATGEN_CSR, 0);
  vw16(REG_ORB_CSR, 7);

  printf(" -Fixed output levels are now turned off.\n");
  printf(" -The BC and ORBIT NIM outputs carry signals with 40.079 MHz\n");
  printf("  and 11.245 kHz respectively\n");
  printf(" -The BC and ORBIT ECL AC-coupled signals have a ~0.7 Vpp swing.\n");

  init_tltp();
}


/****************/
void test_13(void)
/****************/
{
 
  printf("      ROUTINE TO CHECK ALL COAX INTPUTS\n");
  init_tltp();

  printf(" -The BUSY/LOCAL/op/nim shall be used to drive the ORBIT,\n");
  printf("  LVL1, TEST-TRIGGERs and B-Go EXT-ip/nim inputs in turn.\n");
  printf(" -Check that the corresponding LED lights up when the jumper\n");
  printf("  is connected to the input.\n");
  printf(" -Verify the output NIM level with a 1 or 10 Mohm terminated\n"); 
  printf("  oscilloscope, connected in parallel using a Y-adapter.\n"); 

  vw16(REG_BC_CSR, 3);
  vw16(REG_ORB_CSR, 0xB);
  vw16(REG_L1A_CSR, 7);
  vw16(REG_TR1_CSR, 7);
  vw16(REG_TR2_CSR, 7);
  vw16(REG_TR3_CSR, 7);
  vw16(REG_BG0_CSR, 7);
  vw16(REG_BG1_CSR, 7);
  vw16(REG_BG2_CSR, 7);
  vw16(REG_BG3_CSR, 7);
  vw16(REG_BUSY_CR, 0x11);

  PTO
  init_tltp();

  vw16(REG_PATGEN_CSR, 2);
  vw16(REG_20BITCNT_LSW, 1);
  vw16(REG_FIXVAL, 0x80);
  vw16(REG_BUSY_CR, 0xD);

  printf(" -Use now the BGo<0>/LOCAL-op/nim to drive the BUSY-ip/nim and\n");
  printf("  check the LED coming on.\n");
  printf(" -To test the BUSY-ip/ttl use a 0 ohm (LEMO) terminator.\n");
  
  PTO
  init_tltp();
  
  printf(" -Use an external BC/NIM source (= TTCvi Tester) and\n");
  printf("  connect to the BC/CLOCK/EXT-ip/nim.\n");
  printf(" -Check the LED and the NIM + ECL outputs.\n");

  vw16(REG_BC_CSR, 7);

  init_tltp();
}


/****************/
void test_14(void)
/****************/
{
  u_int loop;
  u_short wpatt;
  
  printf("      ROUTINE TO CHECK DEAD-TIME GENERATION\n"); 
  printf("                  AND DURATION\n");

  init_tltp();

  vw16(REG_BUSY_CR, 0x60);
  vw16(REG_20BITCNT_MSW, 0);
  vw16(REG_20BITCNT_LSW, 32);

  for (loop = 1; loop < 32; loop++)
    vw16(REG_MEMIO, 0);

  vw16(REG_MEMIO, 0x1E);
  vw16(REG_PATGEN_CSR, 5);
  vw16(REG_SINGLSHOT, 1);

  printf("-The PATTERN GENERATOR should now be running\n");
  printf("-Check PATTERN GENERATOR running status on-board LED\n");
  printf("-Check L1A/TEST-TRIGGER<3..1> NIM outputs for\n");
  printf(" a 25ns pulse every ~850ns\n");
  printf("-The sourcing of the dead-time trigger will be checked now\n");
  printf("-Trigger the scope on the L1A/LOCAL/op/nim\n");
  printf(" and check the BUSY/LOCAL/op/nim: => ~110ns pulse\n");
  PTO
  
  vw16(REG_BUSY_CR, 0x160);

  printf("-Trigger the scope on the TEST-TRIG<1>/LOCAL/op/nim\n");
  printf(" and check the BUSY/LOCAL/op/nim: => ~110ns pulse\n");
  PTO
  
  vw16(REG_BUSY_CR, 0x260);

  printf("-Trigger the scope on the TEST-TRIG<2>/LOCAL/op/nim\n");
  printf(" and check the BUSY/LOCAL/op/nim: => ~110ns pulse\n");
  PTO
  
  vw16(REG_BUSY_CR, 0x360);

  printf("-Trigger the scope on the TEST-TRIG<3>/LOCAL/op/nim\n");
  printf(" and check the BUSY/LOCAL/op/nim: => ~110ns pulse\n");
  PTO
  
  printf("-The dead-time duration will be checked now\n");
  printf("-By hitting the [CR] the duration will increase 25ns\n");

  for (loop = 0; loop < 8; loop++)
  {
    wpatt = 0x60 | (loop << 12);

    vw16(REG_BUSY_CR, wpatt);
   
    printf("-The dead-time should now be: %d * BC + ~10ns\n", loop + 4);
    PTO
  }

  init_tltp();
}


/****************/
void test_15(void)
/****************/
{
  u_int loop, loop2;
  u_short wpatt;
  
  printf("      ROUTINE TO CHECK TURN COUNTER\n");
  init_tltp();
  
  vw16(REG_ORB_CSR, 0xF);
  vw16(REG_TURN_CR, 0x10);
  vw16(REG_20BITCNT_MSW, 0);
  vw16(REG_20BITCNT_LSW, 292);

  for (loop = 1; loop < 5; loop++)
    vw16(REG_MEMIO, 0x0100);
  
  
  for (loop = 1; loop < 25; loop++)
  {
    for (loop2 = 1; loop2 < 8; loop2++)
      vw16(REG_MEMIO, 0);
    for (loop2 = 1; loop2 < 5; loop2++)
      vw16(REG_MEMIO, 0x81);
  } 

  vw16(REG_PATGEN_CSR, 5);
  vw16(REG_SINGLSHOT, 1);

  printf("-The PATTERN GENERATOR should now be running\n");
  printf("-Check PATTERN GENERATOR running status on-board LED\n");
  printf("-Check BGo<1> NIM output for a 100 ns pulse\n");
  printf(" every ~7.35 us and use this as a scope trigger\n");
  printf("-The BGo<0> NIM output will reflect the ORBIT pulse\n");
  printf(" generated by the pattern generator (100 ns every 300 ns)\n");
  printf("-The ORBIT NIM output carries a level between the ORBITs\n");
  printf(" which is displaced one ORBIT when hitting the [CR]\n");  
  PTO
  
  for (loop = 0; loop < 16; loop++)
  {
    wpatt = 0x10 | loop;
    vw16(REG_TURN_CR, wpatt);
    printf("-The TURN number is now: %d\n", loop);
    PTO
  }

  init_tltp();
}


/****************/
void test_16(void)
/****************/
{
  u_int errflag, loop;
  u_short rpatt, dummy, regfw0, regfw1, regfw2, regfw3, regfr0, regfr1, regfr2, regfr3;
  
  printf("      ROUTINE TO CHECK FIFO E/F FLAGS\n");
  printf("-The FIFO will now be reset\n");
  init_tltp();

  vw16(REG_TTYPE_CSR, 7);
  vw16(REG_FIFO_RST, 0);
  vr16(REG_TTYPE_CSR, &rpatt); 

  if (rpatt & 0x2000)
    printf("-The FIFO is now reset and EMPTY\n");
  else  
    printf("-The FIFO is now reset and IS NOT EMPTY = HW ERROR !!!\n");

  printf("-The FIFO will now be written 512 times\n");

  for (loop = 1; loop < 513; loop++)
    vw16(REG_FIFO_IO, 1);
  
  vr16(REG_TTYPE_CSR, &rpatt);

  if (rpatt & 0x1000)
    printf("-The FIFO is now filled and FULL\n"); 
  else
    printf("-The FIFO is now filled and IS NOT FULL = HW ERROR !!!\n");

  printf("-The FIFO will now be read 512 times\n");

  for (loop = 1; loop < 513; loop++)
    vr16(REG_FIFO_IO, &dummy);
  
  vr16(REG_TTYPE_CSR, &rpatt);

  if (rpatt & 0x2000)
    printf("-The FIFO is now read and EMPTY\n");
  else
    printf("-The FIFO is now read and IS NOT EMPTY = HW ERROR !!!\n");
 
  printf("-End of FIFO fill and empty test.\n");
  PTO
  
  printf("      ROUTINE TO CHECK FIFO DATA LINES\n");
  printf("-The FIFO will now be reset and the 4\n");
  printf(" first positions written with data\n"); 
  printf("-The Register File is indexed by the TRIG-TYPE CSR\n");

  regfw0 = 0x00;
  regfw1 = 0x55;
  regfw2 = 0xaa;
  regfw3 = 0xff;

  vw16(REG_TTYPE_REGFILE0, regfw0);
  vw16(REG_TTYPE_REGFILE1, regfw1);
  vw16(REG_TTYPE_REGFILE2, regfw2);
  vw16(REG_TTYPE_REGFILE3, regfw3);
  vw16(REG_FIFO_RST, 0);
  vw16(REG_TTYPE_CSR,  7);
  vw16(REG_FIFO_IO, 1); 
  vw16(REG_TTYPE_CSR, 0x17);
  vw16(REG_FIFO_IO, 1); 
  vw16(REG_TTYPE_CSR, 0x27);
  vw16(REG_FIFO_IO, 1); 
  vw16(REG_TTYPE_CSR, 0x37);
  vw16(REG_FIFO_IO, 1); 
  vr16(REG_FIFO_IO, &regfr0); 
  vr16(REG_FIFO_IO, &regfr1); 
  vr16(REG_FIFO_IO, &regfr2); 
  vr16(REG_FIFO_IO, &regfr3); 

  errflag = 0;

  printf("position 0 written data: 0x%02x\n", regfw0 & 0xFF);
  printf("position 0 read data:    0x%02x\n", regfr0 & 0xFF);

  if((regfw0 & 0xFF) != (regfr0 & 0xFF))
    errflag = 1;

  printf("position 1 written data: 0x%02x\n", regfw1 & 0xFF);
  printf("position 1 read data:    0x%02x\n", regfr1 & 0xFF);
  
  if((regfw1 & 0xFF) != (regfr1 & 0xFF))
    errflag = 1;
    
  printf("position 2 written data: 0x%02x\n", regfw2 & 0xFF);
  printf("position 2 read data:    0x%02x\n", regfr2 & 0xFF);

  if((regfw2 & 0xFF) != (regfr2 & 0xFF))
    errflag = 1;
    
  printf("position 3 written data: 0x%02x\n", regfw3 & 0xFF);
  printf("position 3 read data:    0x%02x\n", regfr3 & 0xFF);

  if((regfw3 & 0xFF) != (regfr3 & 0xFF))
    errflag = 1;
      
  if (errflag)
    printf("!!! FIFO DATA LINE PROBLEM !!!\n");
  else
  {
    printf("  FIFO DATA LINE TEST is OK !!!\n");  
    PTO
    
    init_tltp();
    
    printf("      ROUTINE TO CHECK REGISTER FILE INDEXING\n");
    printf("-The FIFO will now be reset and the 4\n");
    printf(" first positions written with data\n"); 
    printf("-The Register File is indexed by the PAT-GEN\n");

    regfw0 = 0x00;
    regfw1 = 0x55;
    regfw2 = 0xaa;
    regfw3 = 0xff;

    vw16(REG_TTYPE_REGFILE0, regfw0);
    vw16(REG_TTYPE_REGFILE1, regfw1);
    vw16(REG_TTYPE_REGFILE2, regfw2);
    vw16(REG_TTYPE_REGFILE3, regfw3);
    vw16(REG_TTYPE_CSR, 3);
    vw16(REG_FIFO_RST, 0);
    vw16(REG_PATGEN_CSR, 2);
    vw16(REG_20BITCNT_LSW, 1);
    vw16(REG_FIXVAL, 0);
    vw16(REG_FIFO_IO, 1); 
    vw16(REG_FIXVAL, 0x20);
    vw16(REG_FIFO_IO, 1); 
    vw16(REG_FIXVAL, 0x40);
    vw16(REG_FIFO_IO, 1); 
    vw16(REG_FIXVAL, 0x60);
    vw16(REG_FIFO_IO, 1); 
    vr16(REG_FIFO_IO, &regfr0);
    vr16(REG_FIFO_IO, &regfr1); 
    vr16(REG_FIFO_IO, &regfr2); 
    vr16(REG_FIFO_IO, &regfr3); 

    errflag = 0;

    printf("position 0 written data: 0x%02x\n", regfw0 & 0xFF);
    printf("position 0 read data:    0x%02x\n", regfr0 & 0xFF);
    
    if((regfw0 & 0xFF) != (regfr0 & 0xFF))
      errflag = 1;

    printf("position 1 written data: 0x%02x\n", regfw1 & 0xFF);
    printf("position 1 read data:    0x%02x\n", regfr1 & 0xFF);

    if((regfw1 & 0xFF) != (regfr1 & 0xFF))
      errflag = 1;

    printf("position 2 written data: 0x%02x\n", regfw2 & 0xFF);
    printf("position 2 read data:    0x%02x\n", regfr2 & 0xFF);

    if((regfw2 & 0xFF) != (regfr2 & 0xFF))
      errflag = 1;

    printf("position 3 written data: 0x%02x\n", regfw3 & 0xFF);
    printf("position 3 read data:    0x%02x\n", regfr3 & 0xFF);

    if((regfw3 & 0xFF) != (regfr3 & 0xFF))
      errflag = 1;

    if (errflag)
      printf("!!! REG-FILE INDEXING LINE PROBLEM !!!\n"); 
    else
      printf("  REG-FILE INDEXING LINE TEST is OK !!!\n");  

    init_tltp();
    PTO

    printf("      ROUTINE TO CHECK FIFO WRITING ON TRIGGERS\n");
    printf("-The FIFO will now be reset and the 4\n");
    printf(" first positions written with data\n"); 
    printf("-The Register File is indexed by the PAT-GEN\n"); 
    printf("-The triggers are generated by the PAT-GEN\n");

    regfw0 = 0xd0;
    regfw1 = 0xd1;
    regfw2 = 0xd2;
    regfw3 = 0xd3;

    vw16(REG_TTYPE_REGFILE0, regfw0);
    vw16(REG_TTYPE_REGFILE1, regfw1);
    vw16(REG_TTYPE_REGFILE2, regfw2);
    vw16(REG_TTYPE_REGFILE3, regfw3);
    vw16(REG_FIFO_RST, 0);
    vw16(REG_PATGEN_CSR, 2);
    vw16(REG_20BITCNT_LSW, 1);
    vw16(REG_TTYPE_CSR, 3);
    vw16(REG_FIXVAL, 2);
    vw16(REG_FIXVAL, 0);
    vw16(REG_TTYPE_CSR, 0x103);
    vw16(REG_FIXVAL, 0x24);
    vw16(REG_FIXVAL, 0);
    vw16(REG_TTYPE_CSR, 0x203);
    vw16(REG_FIXVAL, 0x48);
    vw16(REG_FIXVAL, 0);
    vw16(REG_TTYPE_CSR, 0x303);
    vw16(REG_FIXVAL, 0x70);
    vw16(REG_FIXVAL, 0);
    vr16(REG_FIFO_IO, &regfr0); 
    vr16(REG_FIFO_IO, &regfr1); 
    vr16(REG_FIFO_IO, &regfr2); 
    vr16(REG_FIFO_IO, &regfr3);

    errflag = 0;

    printf("position 0 written data: 0x%02x\n", regfw0 & 0xFF);
    printf("position 0 read data:    0x%02x\n", regfr0 & 0xFF);
    
    if((regfw0 & 0xFF) != (regfr0 & 0xFF))
      errflag = 1;

    printf("position 1 written data: 0x%02x\n", regfw1 & 0xFF);
    printf("position 1 read data:    0x%02x\n", regfr1 & 0xFF);

    if((regfw1 & 0xFF) != (regfr1 & 0xFF))
      errflag = 1;

    printf("position 2 written data: 0x%02x\n", regfw2 & 0xFF);
    printf("position 2 read data:    0x%02x\n", regfr2 & 0xFF);

    if((regfw2 & 0xFF) != (regfr2 & 0xFF))
      errflag = 1;

    printf("position 3 written data: 0x%02x\n", regfw3 & 0xFF);
    printf("position 3 read data:    0x%02x\n", regfr3 & 0xFF);

    if((regfw3 & 0xFF) != (regfr3 & 0xFF))
      errflag = 1;

    if (errflag)
      printf("!!! FIFO WRITING BY TRIGGERS PROBLEM !!!\n"); 
    else
     printf("  FIFO WRITING BY TRIGGERS TEST is OK !!!\n");  

    init_tltp();
  }
}


/****************/
void test_17(void)
/****************/
{
  int int_handle;
  u_short rpatt, wpatt, irqlev, irqen;
  VME_InterruptList_t irq_list;

  printf(" ROUTINE TO TEST VME INTERRUPTER\n");
  
  vw16(REG_INTID, 0xAC);
  vr16(REG_INTID, &rpatt);

  printf("The value in the STATUS/ID Regiter is now set to: 0x%02x\n", rpatt & 0xfc);

  irqlev = 3;

  vw16(REG_INTCSR, irqlev);
  vr16(REG_INTCSR, &rpatt);

  printf("The VME IRQ Level is now set to: %d\n", rpatt & 0x7);

  irq_list.number_of_items = 1;
  irq_list.list_of_items[0].vector = rpatt & 0xfc;  
  irq_list.list_of_items[0].level = rpatt & 0x7;
  irq_list.list_of_items[0].type = VME_INT_ROAK;
  ret = VME_InterruptLink(&irq_list, &int_handle);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    return;
  }  

  ret = VME_InterruptRegisterSignal(int_handle, SIGNUM);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    return;
  }

  irqen = 0;

  printf("Enter '1' to enable VME interrupter, else [RETURN]:\n");
  irqen = getdecd(0);
  
  irqen = irqen << 3;
  wpatt = irqlev | irqen;

  vw16(REG_INTCSR, wpatt);
  vr16(REG_INTCSR, &rpatt);

  if (rpatt & 0x8)
    printf("The VME Interrupter is now enabled\n"); 
  else
    printf("The VME Interrupter is now disabled\n");

  printf(" If the VME crate has been powered off: RESTART program\n");
  printf(" by pressing ESC then type !RU\n");
  printf(" The IRQ Level should be set to 3 and\n");
  printf(" the IRQ Enable must be set !!\n");
  printf("Hit [RETURN] to generate an Interrupt !!\n");
  PTO2

  vw16(REG_SWIRQ, 0);

  printf("   Press <ctrl>+\\ to stop the routine.\n");
  cont = 1;
  while(cont) {};

  ret = VME_InterruptUnlink(int_handle);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    return;
  }
}


/****************/
void test_18(void)
/****************/
{
  u_int fun = 1;
  
  while(fun)
  {
    printf("    SELECT ONE OF THE FOLLOWING TASKS:\n");
    printf("INITIALIZE MODULE..............[ 1]\n");
    printf("GENERATE MODULE RESET..........[ 2]\n");
    printf("READ MODULE STATUS.............[ 3]\n");
    printf("READ ALL REGISTERS.............[ 4]\n");
    printf("TEST/READ/MODIFY REGISTERS.....[ 5]\n");
    printf("READ/RESET EVENT COUNTER.......[ 6]\n");
    printf("PATTERN GENERATOR SUB-MENU.....[ 7]\n");
    printf("TEST-TRIG/BGo PULSES BY SW.....[ 8]\n");
    printf("WRITE/READ/RST TRIG-TYPE FIFO..[ 9]\n");
    printf("RETURN TO MAIN MENU............[ 0]\n");
    printf("ENTER [0..9]: ");
    fun = getdecd(fun);

    if (fun == 1)
      init_tltp();
      
    if (fun == 2)
      swrst();
      
    if (fun == 3)
      rdallstat();

    if (fun == 4)
      rdallregs();
      
    if (fun == 5)
      tstmodreg();
      
    if (fun == 6)
      chkevcnt();
      
    if (fun == 7)
      patgen();

    if (fun == 8)
      sw_pulse();
      
    if (fun == 9)
      rwfifo();
  }
}


/***************/
void rwfifo(void)
/***************/
{
  char action[9];
  u_short fifoior;
  
  vw16(REG_BC_CSR, 3);
  fifostate();

  printf("  SELECT WRITE [w] / READ [r] / RESET [z] FIFO  :\n");
  fgets(action, 9, stdin);
 
  if ((action[0] == 'w') || (action[0] == 'W'))  
  {
    vw16(REG_FIFO_IO, 0);
    fifostate();
  }
  
  if ((action[0] == 'r') || (action[0] == 'R'))  
  {
    vr16(REG_FIFO_IO, &fifoior); 
    printf("The FIFO read data is: 0x%02x\n", fifoior & 0xff);
    fifostate();
  }
  
  if ((action[0] == 'z') || (action[0] == 'Z'))  
  {
    vw16(REG_FIFO_RST, 0);
    printf(" The FIFO has now been RESET !\n"); 
  }
}


/******************/
void fifostate(void)
/******************/
{
  u_short rpatt;
  
  vr16(REG_TTYPE_CSR, &rpatt);
   
  if (rpatt & 0x2000) 
    printf("  - The FIFO is EMPTY\n"); 
  else 
    printf("  - The FIFO is NOT EMPTY\n"); 
  if (rpatt & 0x1000) 
    printf("   -The FIFO is FULL\n");  
  else 
    printf("  - The FIFO is NOT FULL\n"); 

}


/*****************/
void sw_pulse(void)
/*****************/
{
  u_int loop(0), src(0), dura(0), npuls(0), csrdat(0), offset(0);
  char action[9];
  
  vw16(REG_BC_CSR, 3); 

  do
  {
    printf("  THESE SOURCES CAN GENERATE PULSES :\n");
    printf("         TestTrig<1> = 1\n");
    printf("         TestTrig<2> = 2\n");
    printf("         TestTrig<3> = 3\n");
    printf("         BGo<0>      = 4\n");
    printf("         BGo<1>      = 5\n");
    printf("         BGo<2>      = 6\n");
    printf("         BGo<3>      = 7\n");
    printf("  SELECT SOURCE: ");
    src = getdecd(1);
  } while (src > 7);
    
  do
  {
    printf("  THE PULSE LENGTH IN # BC CAN BE :\n");
    printf("         1 =  25 ns\n");
    printf("         2 =  50 ns\n");
    printf("         4 = 100 ns\n");
    printf("  SELECT LENGTH :\n"); 
    dura = getdecd(1);
  } while ((dura == 3) || (dura > 4));
  
  printf("  ENTER # OF PULSES (0 = INFINITE #): ");
  npuls = getdecd(0);

  if (src == 1)  offset = 0x92;
  if (src == 2)  offset = 0x94;
  if (src == 3)  offset = 0x96;
  if (src == 4)  offset = 0xa0;
  if (src == 5)  offset = 0xa2;
  if (src == 6)  offset = 0xa4;
  if (src == 7)  offset = 0xa6;
  if (dura == 1) csrdat = 0xb;
  if (dura == 2) csrdat = 0x8b;
  if (dura == 4) csrdat = 7;

  vw16(offset, csrdat);

  action[0] = 'r';  
  while ((action[0] =='r') || (action[0] == 'R'))
  {
    printf("PRESS [CR] TO START!\n");
    PTO2

    if (npuls)
    {
      printf("  NOW GENERATING %d PULSES\n", npuls);
      for (loop = 0; loop < npuls; loop++)
      {
	vw16(REG_BG0_CSR, 0x8000);
	vw16(offset, 0x8000); 
      }

      printf("To repeat press [r/R], to quit [q/Q]:\n"); 
      fgets(action, 9, stdin);
    }
    else
      break;
  }
  
  printf("  NOW GENERATING CONTINUOUS PULSES\n"); 
  cont = 1;
  printf("   Press <ctrl>+\\ to stop the routine.\n");

  while (cont)
    vw16(offset, 0x8000); 
}


/***************/
void patgen(void)
/***************/
{
  u_short fixv(0), wcsrpg(0), worbcsr(0), wbgo3csr(0), rpatt(0), pattdata(0), nwpatth(0), nwpattl(0), nrpatth(0), nrpattl(0);
  u_int npatt0(0), nrpatt(0), bit2(0), bit3(0), bit4(0), opm(0), nwpatt(0), npatt1(0), ptocnt(0);

  char action[9];
  
  printf("                   ROUTINES TO:\n");
  printf(" SET A PATTERN IN THE PATTERN GENERATOR FIXED VALUE REGISTER\n");
  printf("                      or to\n");
  printf("      ENTER PATTERNS IN THE PATTERN GENERATOR MEMORY\n");
  printf("                     and to\n");
  printf("             SELECT OPERATING MODE\n");
  printf("                     and to\n");
  printf("         TRIGGER THE PATTERN GENERATOR\n");

  vw16(REG_PATGEN_CSR, 0);
  vw16(REG_SWRST, 1);
  vw16(REG_SW_RST, 1);
  vw16(REG_BC_CSR, 3); 

  printf("              Select an OutPut Mode:\n");
  printf("[0] for FIXED / [1] for SEQEUNCED\n");
  opm = getdecd(0);

  if (opm == 1)
  {
    printf(" SWRST and PATTERN GENERATOR FSM RST have now been issued\n");
    printf("                       and\n");
    printf(" the PATTERN GENERATOR CSR has been set to idle/load mode\n");
    printf("                       and\n");
    printf(" the ADDRESS COUNTER and its SHADOW REG. have been cleared\n");
    printf("                       and\n");
    printf(" the internal BC has been enabled to clock internal logic\n");

    vr16(REG_PATGEN_CSR, &rpatt);

    printf(" The PATTERN GENERATOR CSR reads: 0x%04x\n", rpatt);
    printf(" If not equal to x100 there is an hardware error !!!\n");
    printf(" To continue press [c/C], to quit [q/Q]:\n"); 
    fgets(action, 9, stdin);
    
    if ((action[0] != 'c') && (action[0] != 'C'))
      return;
      
    printf(" Enter now the number of patterns to be generated\n");
    printf(" (max ONE MILLION !!!!)\n");
    nwpatt = getdecd(1);

    npatt1 =  nwpatt;
    nwpatth = ((nwpatt >> 16) & 0xffff);
    nwpattl = (nwpatt & 0xffff);

    vw16(REG_20BITCNT_MSW, nwpatth);
    vw16(REG_20BITCNT_LSW, nwpattl);
    vr16(REG_20BITCNT_MSW, &nrpatth);
    vr16(REG_20BITCNT_LSW, &nrpattl);

    nrpatt = ((nrpatth << 16) | nrpattl) & 0xffffff;

    printf(" Written number: %d\n", nwpatt); 
    printf(" Read number:    %d\n", nrpatt);
    printf("           ARE THEY THE SAME ???\n");

    vr16(REG_PATGEN_CSR, &rpatt);

    printf(" The PATTERN GENERATOR CSR reads: 0x%04x\n", rpatt);
    printf(" If not equal to 0 there is an hardware error !!!\n");
    printf(" To continue press [c/C], to quit [q/Q]:\n"); 
    fgets(action, 9, stdin);
    
    if ((action[0] != 'c') && (action[0] != 'C'))
      return;

    npatt0 = 1;
    ptocnt = 0;

    printf(" A stream of 16-bit patterns are now to be entered\n");
    printf(" in hexadecimal form i.e. 0xNNNN\n");

    do
    {
      printf(" Enter pattern: %d of total: %d\n", npatt0, npatt1);

      printf(" Prevoisly entered pattern: 0x%04x\n", pattdata);
      pattdata = gethexd(pattdata);

      vw16(REG_MEMIO, pattdata);
      vr16(REG_PATGEN_CSR, &rpatt);

      npatt0++;
    } while ((rpatt & 0xf800) == 0x7000);  

    printf(" All patterns have now been entered and\n");
    PTO
    npatt0 = 1; 

    do
    {
      vr16(REG_MEMIO, &pattdata);
      vr16(REG_PATGEN_CSR, &rpatt);

      printf(" Pattern in position: %d of total: %d", npatt0, npatt1);
      printf(" reads: 0x%04x\n", pattdata);

      npatt0++;
      ptocnt++;

      if (ptocnt == 20)
      {
	ptocnt = 0;
	PTO
      }  
    } while ((rpatt & 0xf800) == 0xe000);

    printf(" All patterns have now been read!\n");
    PTO

    printf(" Select Operating Mode:\n"); 
    printf(" Single Shot = 0 / Continuous = 1: ");
    bit2 = getdecd(0);

    printf(" Trigger run on:\n");
    printf(" VME function = 0 / External source = 1: ");
    bit3 = getdecd(0);
    
    if (bit3)
    {
      printf(" Select trigger external source:\n");
      printf(" Orbit = 0 / Pre-Pulse (BGo<3>) = 1: ");
      bit4 = getdecd(0);
    }
    else
      bit4 = 0;

    wcsrpg = 0;

    if(bit2)
      wcsrpg = wcsrpg | 0x4;
    if(bit3)
      wcsrpg = wcsrpg | 0x8;
    if(bit4)
      wcsrpg = wcsrpg | 0x10;

    wcsrpg = wcsrpg | 0x1;

    printf(" The PG CSR will be set to: 0x%04x\n", wcsrpg);
    printf(" Set up registers manually press [m/M] or\n");
    printf(" set up by the program press [p/P]:\n");
    fgets(action, 9, stdin);
    if ((action[0] == 'm') || (action[0] == 'M'))
      tstmodreg(); 
    }  

  if ((action[0] == 'p') || (action[0] == 'P') || (opm == 0))
  {  
    vw16(REG_L1A_CSR, 0xF);
    vw16(REG_TR1_CSR, 0xF);
    vw16(REG_TR2_CSR, 0xF);
    vw16(REG_TR3_CSR, 0xF);
    vw16(REG_BG0_CSR, 0xF);
    vw16(REG_BG1_CSR, 0xF);   
    vw16(REG_BG2_CSR, 0xF);   
    vw16(REG_TTYPE_CSR, 3);   
    vw16(REG_TTYPE_REGFILE0, 0xa1);   
    vw16(REG_TTYPE_REGFILE1, 0xb2);   
    vw16(REG_TTYPE_REGFILE2, 0xc3);   
    vw16(REG_TTYPE_REGFILE3, 0xd4);   
    vw16(REG_CAL_CSR, 3);   
    vw16(REG_BUSY_CR, 0x8001);  

    if (opm == 1)
    {
      if (!bit3)
      {
        worbcsr = 0xf;
	wbgo3csr = 0xf;
      }      
      else
      {
	if(bit4) {
          worbcsr = 0xf;
	  wbgo3csr = 0x87;
	}

	if (bit4) {
          worbcsr = 7;
	  wbgo3csr = 0xf;
	}
      }
            
      vw16(REG_ORB_CSR, worbcsr);
      vw16(REG_BG3_CSR, wbgo3csr);
    }
  }

  if (opm == 1)
  {
    printf(" All registers have now been set and patterns loaded !\n");
    PTO

    if (bit2)
      printf(" => Continuous mode selected where pressing [CR]\n");
    else
      printf(" => Single Shot mode selected where pressing [CR]\n");

    if (!bit3)
      printf("    will trigger sequence(s) asynchronously.\n");
    else 
    {
      printf("    will trigger sequence(s)\n");
      if (!bit4)
        printf("on each ORBIT pulse.\n");
      else
        printf("on each BGo<3> pulse.\n");
    }
    
    printf(" Start sequence(s) [CR]\n");
    PTO2

    vw16(REG_PATGEN_CSR, wcsrpg); 

    if (!bit3)
      vw16(REG_SINGLSHOT, 1);

    cont = 1;
    printf("   Press <ctrl>+\\ to stop the routine.\n");

    while (cont) {};

    vw16(REG_PATGEN_CSR, 0);
  }
   
  vw16(REG_ORB_CSR, 0xF);
  vw16(REG_BG3_CSR, 0xF);
  vr16(REG_FIXVAL, &fixv);
  vw16(REG_PATGEN_CSR, 2);

  printf("Enter a hex value [0xNNNN] in the FIX VAL REG\n");
  printf("or press [CR] to maintain value:\n");
  fixv = gethexd(fixv);
  
  printf("Press [CR] to activate outputs\n");
  PTO2

  vw16(REG_20BITCNT_LSW, 1);
  vw16(REG_FIXVAL, fixv);

  printf("To reset and quit [q/Q], to RETAIN levels on quitting [r/R]\n");
  fgets(action, 9, stdin);
  if ((action[0] != 'r') || (action[0] != 'R'))
  {
    vw16(REG_PATGEN_CSR, 0);
    vw16(REG_20BITCNT_LSW, 0);
  }
}
 

/*****************/
void chkevcnt(void)
/*****************/
{
  char action[9];
  u_short rcounth(0), rcountl(0);
  u_int rcount(0);
  
  printf("       CHECK RESET/READ EVENT/ORBIT COUNTER\n"); 
  printf("-The ORBIT CSR should be correctly initiated\n");
  printf(" before this test\n");
  printf("RESET the counter now press [r/R] /\n");
  printf(" RETAIN value press [v/V]:\n"); 
  fgets(action, 9, stdin);
    
  if (action[0] == 'r' || action[0] == 'R')
  {
    vw16(REG_32BITCNT_RST, 0);
    printf("!!! The EVENT COUNTER has now been RESET !!!\n");
  }
  
  vr16(REG_32BITCNT_LSW, &rcountl);
  vr16(REG_32BITCNT_MSW, &rcounth);  

  rcount = (rcounth << 16) | rcountl;

  printf("The EVENT counter read: %d [0x%08x]\n", rcount, rcount);
}


/******************/
void tstmodreg(void)
/******************/
{
  u_int bitpos(0), errflag(0), dmode(0), value(0), thisreg(0), size(0), shifter(1), maskbitpos(0), bitmask(0);
  u_short wpatt(0), rpatt(0);
  char mode[9];

  printf(" ROUTINE TO TEST/READ/MODIFY A SELECTED REGISTER\n\n");
  printf("  ONE OF FOLLOWING REGISTERS CAN BE SELECTED:\n\n");

  printf(" 1 = Interrupter CSR         |  15 = Trig Type Reg File 0\n");
  printf(" 2 = Interrupter Stat/ID Reg |  16 = Trig Type Reg File 1\n");
  printf(" 3 = L1A CSR                 |  17 = Trig Type Reg File 2\n");
  printf(" 4 = Test-Trig-1 CSR         |  18 = Trig Type Reg File 3\n");
  printf(" 5 = Test-Trig-2 CSR         |  19 = Trig Type CSR\n");
  printf(" 6 = Test-Trig-3 CSR         |  20 = Calibration CSR\n");
  printf(" 7 = BGo-0 CSR               |  21 = Busy CR\n");
  printf(" 8 = BGo-1 CSR               |  22 = Busy SR\n");
  printf(" 9 = BGo-2 CSR               |  23 = Patt Generator CSR\n");
  printf("10 = BGo-3 CSR               |  24 = Patt Gen Fixed Value Reg\n");
  printf("11 = BC CSR                  |  25 = Patt Gen Memory I/O Reg\n");
  printf("12 = ORBIT CSR               |  26 = Patt Gen Addr Cnt Hi Reg\n");
  printf("13 = Inhibit Timer Dur Count |  27 = Patt Gen Addr Cnt Lo Reg\n");
  printf("14 = Turn Counter CR\n");
  
  do
  {
    printf(" ENTER [1..27]: ");  
    value = getdecd(value);

    if (value ==  1) {thisreg = REG_INTCSR;         size = 6;}
    if (value ==  2) {thisreg = REG_INTID;          size = 8;}
    if (value ==  3) {thisreg = REG_L1A_CSR;        size = 8;}
    if (value ==  4) {thisreg = REG_TR1_CSR;        size = 8;}
    if (value ==  5) {thisreg = REG_TR2_CSR;        size = 8;}
    if (value ==  6) {thisreg = REG_TR3_CSR;        size = 8;}
    if (value ==  7) {thisreg = REG_BG0_CSR;        size = 8;}
    if (value ==  8) {thisreg = REG_BG1_CSR;        size = 8;}
    if (value ==  9) {thisreg = REG_BG2_CSR;        size = 8;}
    if (value == 10) {thisreg = REG_BG3_CSR;        size = 8;}
    if (value == 11) {thisreg = REG_BC_CSR;         size = 3;}
    if (value == 12) {thisreg = REG_ORB_CSR;        size = 8;}
    if (value == 13) {thisreg = REG_INH_TIMER;      size = 12;}
    if (value == 14) {thisreg = REG_TURN_CR;        size = 5;}
    if (value == 15) {thisreg = REG_TTYPE_REGFILE0; size = 8;}
    if (value == 16) {thisreg = REG_TTYPE_REGFILE1; size = 8;}
    if (value == 17) {thisreg = REG_TTYPE_REGFILE2; size = 8;}
    if (value == 18) {thisreg = REG_TTYPE_REGFILE3; size = 8;}
    if (value == 19) {thisreg = REG_TTYPE_CSR;      size = 10;}
    if (value == 20) {thisreg = REG_CAL_CSR;        size = 7;}
    if (value == 21) {thisreg = REG_BUSY_CR;        size = 16;}
    if (value == 22) {thisreg = REG_BUSY_SR;        size = 5;}
    if (value == 23) {thisreg = REG_PATGEN_CSR;     size = 5;}
    if (value == 24) {thisreg = REG_FIXVAL;         size = 16;}
    if (value == 25) {thisreg = REG_MEMIO;          size = 16;}
    if (value == 26) {thisreg = REG_20BITCNT_MSW;   size = 4;}
    if (value == 27) {thisreg = REG_20BITCNT_LSW;   size = 16;}
  }
  while (value > 27);

  for (maskbitpos = 0; maskbitpos < size; maskbitpos++)
  {
    bitmask = shifter;
    shifter = shifter | (shifter << 1);
  }

  printf("SELECTED REGISTER HAS OFFSET: 0x%04x\n\n", thisreg);
  printf("To test register press:        [T/t]\n");
  printf("To read only the value press:  [R/r]\n");
  printf("To read and enter value press: [E/e]\n");
  printf("To quit this routine press:    [Q/q]\n");
  fgets(mode, 9, stdin);
  
  if (mode[0] != 't' && mode[0] != 'T' && mode[0] != 'r' && mode[0] != 'R' && mode[0] != 'e' && mode[0] != 'E')
    return;
    
  if (mode[0] == 't' || mode[0] == 'T')
   dmode = 1;
  if (mode[0] == 'r' || mode[0] == 'R')
   dmode = 3;
  if (mode[0] == 'e' || mode[0] == 'E')
   dmode = 2;

  if (dmode == 1)
  {
    wpatt = 1;
    errflag = 0; 
    
    for(bitpos = 0; bitpos < size; bitpos++)
    {
      vw16(thisreg, wpatt);
      vr16(thisreg, &rpatt);
      if ((rpatt & bitmask) != (wpatt & bitmask))
      {
        errflag = 1;
        printf("Error at bit: %d\n", bitpos);
        printf("Write: 0x%0x4   Read: 0x%0x4\n", wpatt & bitmask, rpatt & bitmask);
      }
      wpatt++;	
    }
    if (!errflag)
      printf("No R/W erros detected\n");
  }

  if (dmode == 2)
  {
    vr16(thisreg, &rpatt);
    wpatt = rpatt;
    printf("The register is at address offset: 0x%04x\n", thisreg);
    printf("Enter new value [0x????] or press [CR]: ");
    wpatt = gethexd(wpatt);
    wpatt = wpatt & bitmask;
    vw16(thisreg, wpatt);
  }

  if (dmode > 1)
  {
    vr16(thisreg, &rpatt);
    printf("The new value of the register is: 0x%04x\n", rpatt);
  }
}


/******************/
void rdallregs(void)
/******************/
{
  u_int nrpatt, rcount;
  u_short bccsrrp, bgo0csrrp, bgo1csrrp, bgo2csrrp, bgo3csrrp, busycrrp, busysrrp, calcsrrp; 
  u_short fixvalregrp, inhtimerrp, intcsrrp, keepval, l1acsrrp, nrpatth, nrpattl, orbcsrrp;
  u_short patgencsrrp, rcounth, rcountl, statidrp, trig1csrrp, trig2csrrp, trig3csrrp; 
  u_short trigtregf0rp, trigtregf1rp, trigtregf2rp, trigtregf3rp, trigtypcsrrp, turncrrp; 

  printf("  TLTP REGISTER CONTENT\n"); 

  vr16(REG_INTCSR, &intcsrrp); 
  vr16(REG_INTID, &statidrp);
  vr16(REG_FIXVAL, &keepval);  
  vr16(REG_L1A_CSR, &l1acsrrp); 
  vw16(REG_FIXVAL, keepval);  
  vr16(REG_TR1_CSR, &trig1csrrp); 
  vw16(REG_FIXVAL, keepval);  
  vr16(REG_TR2_CSR, &trig2csrrp); 
  vw16(REG_FIXVAL, keepval);  
  vr16(REG_TR3_CSR, &trig3csrrp); 
  vw16(REG_FIXVAL, keepval);  
  vr16(REG_BG0_CSR, &bgo0csrrp);
  vw16(REG_FIXVAL, keepval); 
  vr16(REG_BG1_CSR, &bgo1csrrp);
  vw16(REG_FIXVAL, keepval); 
  vr16(REG_BG2_CSR, &bgo2csrrp);
  vw16(REG_FIXVAL, keepval); 
  vr16(REG_BG3_CSR, &bgo3csrrp);
  vw16(REG_FIXVAL, keepval); 
  vr16(REG_BC_CSR, &bccsrrp); 
  vr16(REG_ORB_CSR, &orbcsrrp); 
  vr16(REG_INH_TIMER, &inhtimerrp); 
  vr16(REG_32BITCNT_LSW, &rcountl); 
  vr16(REG_32BITCNT_MSW, &rcounth); 

  rcount = (rcounth << 16) | rcountl;

  vr16(REG_TURN_CR, &turncrrp); 
  vr16(REG_TTYPE_REGFILE0, &trigtregf0rp); 
  vr16(REG_TTYPE_REGFILE1, &trigtregf1rp); 
  vr16(REG_TTYPE_REGFILE2, &trigtregf2rp); 
  vr16(REG_TTYPE_REGFILE3, &trigtregf3rp); 
  vr16(REG_TTYPE_CSR, &trigtypcsrrp); 
  vr16(REG_CAL_CSR, &calcsrrp); 
  vr16(REG_BUSY_CR, &busycrrp); 
  vr16(REG_BUSY_SR, &busysrrp); 
  vr16(REG_PATGEN_CSR, &patgencsrrp); 
  vr16(REG_FIXVAL, &fixvalregrp); 
  vr16(REG_20BITCNT_MSW, &nrpatth);
  vr16(REG_20BITCNT_LSW, &nrpattl);

  nrpatt = ((nrpatth << 16) | nrpattl) & 0xffffff;
   
  printf("REGISTER                 OFFSET    CONTENT [hex]\n");
  printf("================================================\n");
  printf("VME Interrupter CSR        84        0x%04x\n", intcsrrp & 0xFF);   
  printf("Interrupt Vector           86        0x%04x\n", statidrp & 0xFF);      
  printf("Level 1 Accept CSR         90        0x%04x\n", l1acsrrp);      
  printf("Test-Trigger[1] CSR        92        0x%04x\n", trig1csrrp);      
  printf("Test-Trigger[2] CSR        94        0x%04x\n", trig2csrrp);
  printf("Test-Trigger[3] CSR        96        0x%04x\n", trig3csrrp);
  printf("B-Go[0] CSR                A0        0x%04x\n", bgo0csrrp);
  printf("B-Go[1] CSR                A2        0x%04x\n", bgo1csrrp);
  printf("B-Go[2] CSR                A4        0x%04x\n", bgo2csrrp);
  printf("B-Go[3] CSR                A6        0x%04x\n", bgo3csrrp);
  printf("BC/Clock CSR               B0        0x%04x\n", bccsrrp);
  printf("ORBIT CSR                  B2        0x%04x\n", orbcsrrp); 
  printf("Inhibit Timer Delay        B4        0x%04x\n", inhtimerrp); 
  printf("Event Counter              B8/BA     0x%08x\n", rcount);
  printf("Turn CR                    BC        0x%04x\n", turncrrp);

  PTO
  
  printf("REGISTER                 OFFSET    CONTENT [hex]\n");
  printf("================================================\n");
  printf("Trigger Type Register[0]   C0        0x%04x\n", trigtregf0rp & 0xFF);
  printf("Trigger Type Register[1]   C2        0x%04x\n", trigtregf1rp & 0xFF);
  printf("Trigger Type Register[2]   C4        0x%04x\n", trigtregf2rp & 0xFF);
  printf("Trigger Type Register[3]   C6        0x%04x\n", trigtregf3rp & 0xFF);
  printf("Trigger Type CSR           C8        0x%04x\n", trigtypcsrrp); 
  printf("Calibration Request CSR    D0        0x%04x\n", calcsrrp); 
  printf("Busy CR                    D2        0x%04x\n", busycrrp); 
  printf("Busy SR                    D4        0x%04x\n", busysrrp); 
  printf("Pattern Generator CSR      E4        0x%04x\n", patgencsrrp); 
  printf("Fixed Value Register       E6        0x%04x\n", fixvalregrp); 
  printf("Address Counter            F0/F2     0x%08x\n", nrpatt); 
}


/**************/
void swrst(void)
/**************/
{
  u_int loop = 0;
  
  printf("!!! This routine will reset the module !!!\n");
  printf("Do you want to run RESET in a LOOP [1/0]: ");
  loop = getdecd(loop);
  
  if (loop)
  {
    printf("   Press <ctrl>+\\ to stop the routine.\n");
    cont = 1;
  }
  else
    cont = 0;
    
  do
    vw16(REG_SWRST, 1);  
  while(cont);
  
  printf("The MODULE SOFTWARE RESET Function has been generated\n");
  PTO
}


/******************/
void rdallstat(void)
/******************/
{
  char selsrc, ext, lin, lout, loc, path[16], ttl, nim, intt, snot[4];    
  u_short fsmst, readst[8], keepval, bccsrst, orbcsrst, l1acsrst, calcsrst, busysrst, patgencsrst;
  u_int loop;

  printf("       TLTP STATUS TABLE\n");  
  printf("  (Active signal = 1 / Inactive signal = 0)\n");

  selsrc = '0'; 
  ext = '0';
  lin = '0';
  lout = '0';
  loc = '0';

  vr16(REG_FIXVAL, &keepval);
  vr16(REG_BC_CSR, &bccsrst);
  vr16(REG_ORB_CSR, &orbcsrst);
  vr16(REG_L1A_CSR, &l1acsrst);
  vw16(REG_FIXVAL, keepval);
  vr16(REG_TR1_CSR, &readst[1]);
  vw16(REG_FIXVAL, keepval);
  vr16(REG_TR2_CSR, &readst[2]);
  vw16(REG_FIXVAL, keepval);
  vr16(REG_TR3_CSR, &readst[3]);
  vw16(REG_FIXVAL, keepval);
  vr16(REG_BG0_CSR, &readst[4]);
  vw16(REG_FIXVAL, keepval);
  vr16(REG_BG1_CSR, &readst[5]);
  vw16(REG_FIXVAL, keepval);
  vr16(REG_BG2_CSR, &readst[6]);
  vw16(REG_FIXVAL, keepval);
  vr16(REG_BG3_CSR, &readst[7]);
  vw16(REG_FIXVAL, keepval);
  vr16(REG_CAL_CSR, &calcsrst);
  vr16(REG_BUSY_SR, &busysrst);
  vr16(REG_PATGEN_CSR, &patgencsrst);

  if (bccsrst & 0x100)
    ext = '1';
  if (bccsrst & 0x200)
    selsrc = '1';

  printf("               EXT-SRC       SEL-SRC\n");
  printf("BC             %c             %c\n\n", ext, selsrc);

  selsrc = '0';
   
  if (orbcsrst & 0x0100)
    selsrc = '1'; 

  printf("ORBIT            -             %c\n", selsrc); 
  
  if (l1acsrst & 0x0100)
    lout = '1'; 

  if (l1acsrst & 0x0200)
    loc = '1'; 

  printf("               LINK-IN      LINK-OUT    LOCAL\n");
  printf("L1A              -             %c         %c", lout, loc);

  for (loop = 1; loop < 8; loop++)
  {
    lin = '0';
    lout = '0';
    loc = '0';
    if (loop == 1) strcpy(path, "TRIG[1]");
    if (loop == 2) strcpy(path, "TRIG[2]");
    if (loop == 3) strcpy(path, "TRIG[3]");
    if (loop == 4) strcpy(path, "BGo[0]");
    if (loop == 5) strcpy(path, "BGo[1]");
    if (loop == 6) strcpy(path, "BGo[2]");
    if (loop == 7) strcpy(path, "BGo[3]");
  
    if (readst[loop] & 0x100) lin = '1';
    if (readst[loop] & 0x200) lout = '1';
    if (readst[loop] & 0x400) loc = '1';
    
    printf("%s             %c          %c         %c\n", path, lin, lout, loc);
  }
  
  lin = '0';
  lout = '0';
  ttl = '0';
  nim = '0';
  intt = '0';
 
  if (busysrst & 0x0001) lin = '1'; 
  if (busysrst & 0x0002) lout = '1'; 
  if (busysrst & 0x0004) ttl = '1';
  if (busysrst & 0x0008) nim = '1';
  if (busysrst & 0x0010) intt = '1';

  printf("BUSY: LINK-IN: %c / LINK-OUT: %c / TTL: %c / NIM: %c / INT: %c\n", lin, lout, ttl, nim, intt); 

  calcsrst = (calcsrst >> 8) & 7;
  printf("CAL-REQ 3-bit status =         0x%04x", calcsrst); 

  fsmst = (patgencsrst >> 11) & 0x1F;

  strcpy(snot, "NOT");

  if (patgencsrst & 0x0100)
    strcpy(snot, "");
 
  printf("PATTERN GENERATOR FSM status = 0x%04x\n", fsmst); 
  printf("ADDRESS COUNTER is %s equal to ZERO\n", snot);
  PTO
}


/******************/
void init_tltp(void)
/******************/
{
  vw16(REG_SWRST, 0);
  vw16(REG_BC_CSR, 3);
  vw16(REG_ORB_CSR, 7);
  vw16(REG_L1A_CSR, 0x0F);
  vw16(REG_TR1_CSR, 0x0F);
  vw16(REG_TR2_CSR, 0x0F);
  vw16(REG_TR3_CSR, 0x0F);
  vw16(REG_BG0_CSR, 0x0F);
  vw16(REG_BG1_CSR, 0x0F);
  vw16(REG_BG2_CSR, 0x0F);
  vw16(REG_BG3_CSR, 0x0F);
  vw16(REG_32BITCNT_RST, 0);
  vw16(REG_TURN_CR, 0);
  vw16(REG_TTYPE_REGFILE0, 0);
  vw16(REG_TTYPE_REGFILE1, 0x55);
  vw16(REG_TTYPE_REGFILE2, 0xAA);
  vw16(REG_TTYPE_REGFILE3, 0xFF);
  vw16(REG_TTYPE_CSR, 3);
  vw16(REG_FIFO_RST, 0);
  vw16(REG_CAL_CSR, 3);
  vw16(REG_BUSY_CR, 0x8001);
  vw16(REG_SW_RST, 0);
  vw16(REG_PATGEN_CSR, 0);
  vw16(REG_FIXVAL, 0);
  vw16(REG_20BITCNT_MSW, 0);
  vw16(REG_20BITCNT_LSW, 0);
}


/*******************************************************/
u_int test_register(u_int offset, u_int size, char *name)
/*******************************************************/
{
  u_int errflag(0), bitpos(0), shifter(1), maskbitpos(0), bitmask(0);
  u_short wpatt(1), rpatt(0);
  char mode[9];
  
  for (maskbitpos = 0; maskbitpos < size; maskbitpos++)
  {
    bitmask = shifter;
    shifter = shifter | (shifter << 1);
  }  

  printf("TESTED REGISTER IS : %s AT OFFSET: 0x%04x\n", name, offset);
  if (offset == 0x86)
    printf("Ignore errors on bit 1..0\n");

  for (bitpos = 0; bitpos < size; bitpos++)
  {
    vw16(offset, wpatt);
    vr16(offset, &rpatt);
    
    if ((rpatt & bitmask) != (wpatt & bitmask))
    {
      errflag = 1;
      printf("Error at bit: %d\n", bitpos);
      printf("Write: 0x%04x   Read: 0x%04x\n", (wpatt & bitmask), (rpatt & bitmask));
    } 
    wpatt = wpatt << 1;
  }

  if (!errflag)
    printf("No R/W erros detected\n");

  if (!(offset & 0x38))
   
  printf("The register reset function will now be tested\n");
  PTO

  vw16(offset, bitmask);
  vw16(REG_SWRST, 1);
  vr16(offset, &rpatt);

  rpatt = rpatt & bitmask;
 
  if (rpatt)
    printf("ERROR - Register after reset: 0x%04x\n", rpatt);
  else
    printf("Register reset works OK\n");

  printf("To test next register [n/N]:\n");
  printf("Go back to main Menu  [m/M]:\n");
  printf("Your choice: ");
  fgets(mode, 9, stdin);

  if (mode[0] == 'n' || mode[0] == 'N')
    return(0);
  else
    return(1);
}    


/*******************/
void write_prom(void)
/*******************/
{
  u_int ma, bo, re;
  u_short data;
  char mode[9];
  
  mode[0] = 'c';
  printf("           WRITE IDPROM\n");
  vr16(0x2, &data);

  while(mode[0] == 'c' || mode[0] == 'C')
  {
    ret = vw16(0x2, data);
    if (ret)
    {
      printf("FIT STRAP ST500 + ENTER [c] TO CONTINUE OR [q] TO QUIT :\n");
      fgets(mode, 9, stdin);
    }
    else
      break;
  }
  
  if (ret)
    return;
    
  printf("The following values are programmed. [CR] to keep value :\n");
  readprom4(MANUFID, &ma);
  readprom4(BOARDID, &bo);
  readprom4(REVISION, &re);
  
  printf("Manufacturer ID: ");
  ma = gethexd(ma);
  printf("Serial number: ");
  bo = gethexd(bo);
  printf("Revision: ");
  re = gethexd(re);
  
  writeprom4(MANUFID, ma);
  writeprom4(BOARDID, bo);
  writeprom4(REVISION, re);
}


/*****************************************/
void writeprom4(u_int offset, u_int concat)
/*****************************************/
{
  u_int loop, data[4];
  
  data[0] = (concat >> 24) & 0xff;
  data[1] = (concat >> 16) & 0xff;
  data[2] = (concat >> 8) & 0xff;
  data[3] = concat & 0xff;

  for(loop = 0; loop < 4; loop++)
  {
    vw16(offset + 2 + 4 * loop, data[loop]);
    ts_delay(5000);
  }
}


/******************/
void read_prom(void)
/******************/
{
  u_int data;

  printf("READ IDPROM\n");
  
  readprom4(MANUFID, &data);
  printf("The manufacturer ID is 0x%08x\n", data);
  if (data != CERNID) 
    printf("For CERN this should be 0x%08x\n", CERNID); 

  readprom4(BOARDID, &data);
  printf("The serial number is 0x%08x\n", data);
  
  readprom4(REVISION, &data);
  printf("The revision number is 0x%08x\n", data);
}


/*****************************************/
void readprom4(u_int offset, u_int *concat)
/*****************************************/
{
  u_int loop;
  u_short data[4];
  
  for(loop = 0; loop < 4; loop++)
  {
    vr16((offset + 2 + loop * 4), &data[loop]);
    data[loop] &= 0xff;
  }

  *concat = (data[0] << 24) | (data[1] << 16) | (data[2] << 8) | data[3];
}


/*********************************/
void vme_signal_handler(int signum)
/*********************************/
{
  u_short rpatt, wpatt;
  
  if (signum == SIGNUM )  
  {

  printf("       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
  printf("         VME INTERRUPT RECEIVED\n");
  printf("       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");

  vr16(REG_INTCSR, &rpatt);
  wpatt = rpatt & 0x7;
  vw16(REG_INTCSR, wpatt);

  printf("         The INTERRUPT is now disabled\n");
  printf("       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"); 
  }
  else 
    printf("ERROR: unexpected signal\n") ;
}


/*****************************/
void SigQuitHandler(int signum)
/*****************************/
{
//   signum = 0;
  cont = 0;
}


