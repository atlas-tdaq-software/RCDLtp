#include <string>
#include <unistd.h>
#include <ctype.h>
#include <stdlib.h>
#include <iostream>
#include <cstdint>

#include "cmdl/cmdargs.h"

#include "rcc_time_stamp/tstamp.h"
#include "rcc_error/rcc_error.h"
#include <signal.h>

#include "RCDLtp/RCDLtp.h"

using namespace RCD;

std::string prog(">> setupLTPClock: ");

void sigint_handler(int) {
  std::cout << prog << "Received signal SIGINT" << std::endl;
  exit(0);
}

// global variable
LTP* ltp;


int main(int argc, char ** argv)
{
  signal(SIGINT,  sigint_handler);

  // Declare arguments
  CmdArgStr base_cmd('a', "vme-address", "vme-address", "VME base address in hex, e.g. \"0xff1000\" or \"ff1000\"", CmdArg::isREQ);
  CmdArgBool verbose_cmd('v', "verbose", "verbose", CmdArg::isOPT);
  CmdArgBool internal_cmd('i',"internal", "False = clock/orbit from the CTP link-in. True = clock/orbit from internal", CmdArg::isOPT);

  // Declare command object and its argument-iterator
  CmdLine cmd(*argv, &base_cmd, &verbose_cmd, &internal_cmd, nullptr);
  CmdArgvIter arg_iter(--argc, ++argv);

  cmd.description("This program sets up the LTP clock and ORBIT selection.");

  // default values
  base_cmd = "0xff1000";
  verbose_cmd = false;
  internal_cmd = false;
 

  // Parse arguments
  cmd.parse(arg_iter);
  std::string base_s(base_cmd);
  bool verbose(verbose_cmd);
  bool internal(internal_cmd);

  uint32_t base(0);
  sscanf(base_s.c_str(), "%x", &base);

  if (verbose) {
    std::cout << prog;
    printf("VME base address = 0x%08x\n", base);
    std::cout << prog << "internal = " << internal << std::endl;
  }

  u_int rtnv(0);

  ltp = new LTP();
  rtnv = ltp->Open(base);
  bool ok = ltp->CheckLTP();
  if ( (rtnv != 0) || !ok) {
    std::cerr << prog << "ERROR: Could not open LTP at address 0x" << std::hex << base << std::dec << std::endl;
    //     printf("0x%08x\n", base);
    exit(-1);
  }

  // -------------------------------------------------------------------------------------------------


  if (internal) {
    // setup clock and orbit
    rtnv |= ltp->BC_local_intern();
    rtnv |= ltp->BC_linkout_from_local();
    rtnv |= ltp->BC_lemoout_from_local();
    rtnv |= ltp->ORB_local_intern();
    rtnv |= ltp->ORB_linkout_from_local();
    rtnv |= ltp->ORB_lemoout_from_local();
  } else {
    rtnv |= ltp->BC_linkout_from_linkin();
    rtnv |= ltp->BC_lemoout_from_linkin();
    rtnv |= ltp->ORB_linkout_from_linkin();
    rtnv |= ltp->ORB_lemoout_from_linkin();
    rtnv |= ltp->ORB_local_no();
  }

  return rtnv;
}

