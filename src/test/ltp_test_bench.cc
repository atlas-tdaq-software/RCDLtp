/****************************************************************/
/* file: ltp_test_bench.cc                                 	*/
/*                                                              */
/* Program to test ATLAS LTP modules in bench mode 		*/
/*								*/
/* Author: Per Gallno						*/
/* Ported from CATY to C by: Markus Joos, PH/ESS                */
/*                                                              */
/*******C 2007 - A nickel program worth a dime*******************/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include "rcc_error/rcc_error.h"
#include "rcc_time_stamp/tstamp.h"
#include "ROSGetInput/get_input.h"
#include "DFDebug/DFDebug.h"
#include "vme_rcc/vme_rcc.h"


//Macros
#define PTO      {char cc[9]; printf("Press<return> to continue\n"); fgets(cc, 9, stdin);}
#define PTO2     {char cc[9]; fgets(cc, 9, stdin);}

//Constants
#define LTP_TBASE           0xFF1000 // LTP Module Base address of LTP under test
#define LTP_SBASE           0xFF2000 // LTP Module Base address of sender   SLTP
#define LTP_RBASE           0xFF3000 // LTP Module Base address of receiver RLT
#define MANUFID             0x20
#define BOARDID             0x30
#define REVISION            0x40
#define SIGNUM              42
#define CERNID              0x00080030
#define TEST                1
#define SNDR                2
#define RCVR                3

#define REG_20BITCNT_LSW    0xF2
#define REG_20BITCNT_MSW    0xF0
#define REG_MEMIO           0xE8
#define REG_FIXVAL          0xE6
#define REG_PATGEN_CSR      0xE4
#define REG_SINGLSHOT       0xE2
#define REG_SW_RST          0xE0
#define REG_BUSY_SR         0xD4
#define REG_BUSY_CR         0xD2
#define REG_CAL_CSR         0xD0
#define REG_FIFO_RST        0xCC
#define REG_FIFO_IO         0xCA
#define REG_TTYPE_CSR       0xC8
#define REG_TTYPE_REGFILE3  0xC6
#define REG_TTYPE_REGFILE2  0xC4
#define REG_TTYPE_REGFILE1  0xC2
#define REG_TTYPE_REGFILE0  0xC0
#define REG_TURN_CR         0xBC
#define REG_32BITCNT_LSW    0xBA
#define REG_32BITCNT_MSW    0xB8
#define REG_32BITCNT_RST    0xB6
#define REG_INH_TIMER       0xB4
#define REG_ORB_CSR         0xB2
#define REG_BC_CSR          0xB0
#define REG_BG3_CSR         0xA6
#define REG_BG2_CSR         0xA4
#define REG_BG1_CSR         0xA2
#define REG_BG0_CSR         0xA0
#define REG_TR3_CSR         0x96
#define REG_TR2_CSR         0x94
#define REG_TR1_CSR         0x92
#define REG_L1A_CSR         0x90
#define REG_INTID           0x86
#define REG_INTCSR          0x84
#define REG_SWIRQ           0x82
#define REG_SWRST           0x80


//Globals
u_int ret, cont;
int handle_t = 999, handle_s = 999, handle_r = 999;
// static VME_MasterMap_t master_map_t = {LTP_TBASE, 0x1000, VME_A24, 0};
static VME_MasterMap_t master_map_s = {LTP_SBASE, 0x1000, VME_A24, 0};
static VME_MasterMap_t master_map_r = {LTP_RBASE, 0x1000, VME_A24, 0};
static struct sigaction sa2;


//Prototypes
u_int vw16(u_int card, u_int offset, u_short data);
u_int vr16(u_int card, u_int offset, u_short *data);
void test_menu(void);
void test_sequential(void);
void test_1(void);
void test_2(void);
void test_3(void);
void test_4(void);
void test_5(void);
void test_6(void);
void test_7(void);
void test_8(void);
void test_9(void);
void SigQuitHandler(int signum);
void init_tltp(void);
void init_sltp(void);
void init_rltp(void);
void prnmess1(void);
void init_all_ltp(void);
void check_bc_status(void);
void check_l1a_status(void);
void check_orbit_status(void);
void check_busy_status(void);
void prbusystat(u_short busystat);
void printbcstat(u_short bcstat);
void printorbstat(u_short orbstat);
void printl1astat(u_short l1astat);
void check_trig_status(u_int trignofset, char schar);
void check_bgo_status(u_int bgonofset, char schar);
void prtrignstat(u_short trinstat, char schar);
void prbgonstat(u_short bgonstat, char schar);


/************************************************/
u_int vw16(u_int card, u_int offset, u_short data)
/************************************************/
{
  if (card == TEST)
  {
    if (handle_t == 999) 
    {
      printf("The LTP under test is currently not mapped.\n"); 
      return (999);
    }  

    ret = VME_WriteSafeUShort(handle_t, offset, data); 
  }
  
  if (card == SNDR)
  {
    if (handle_s == 999) 
    {
      printf("The sender LTP is currently not mapped.\n"); 
      return (999);
    }
    
    ret = VME_WriteSafeUShort(handle_s, offset, data); 
  }

  if (card == RCVR)
  {
    if (handle_r == 999) 
    {
      printf("The receiver LTP is currently not mapped.\n"); 
      return (999);
    }
    
    ret = VME_WriteSafeUShort(handle_r, offset, data); 
  }

  if (ret) 
  { 
    VME_ErrorPrint(ret); 
    printf("The program will continue mut you may want to terminate it anyway\n");
    return (ret);
  } 
  return(0); 
}


/*************************************************/
u_int vr16(u_int card, u_int offset, u_short *data)
/*************************************************/
{
  if (card == TEST)
  {
    if (handle_t == 999) 
    {
      printf("The LTP under test is currently not mapped.\n"); 
      return (999);
    }  

    ret = VME_ReadSafeUShort(handle_t, offset, data); 
  }
  
  if (card == SNDR)
  {
    if (handle_s == 999) 
    {
      printf("The sender LTP is currently not mapped.\n"); 
      return (999);
    }
    
    ret = VME_ReadSafeUShort(handle_s, offset, data); 
  }

  if (card == RCVR)
  {
    if (handle_r == 999) 
    {
      printf("The receiver LTP is currently not mapped.\n"); 
      return (999);
    }
    
    ret = VME_ReadSafeUShort(handle_r, offset, data); 
  } 
  
  if (ret) 
  { 
    VME_ErrorPrint(ret); 
    printf("The program will continue mut you may want to terminate it anyway\n");
    return (ret);
  }  
  return(0); 
}


/*****************************/
void SigQuitHandler(int signum)
/*****************************/
{
//   signum = 0;
  cont = 0;
}


/************/
int main(void)
/************/
{
  int stat;
  char mode[9];
  
  ret = ts_open(1, TS_DUMMY);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }
    
  ret = VME_Open();
  if (ret)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }
  
  ret = VME_MasterMap(&master_map_r, &handle_t);  
  if (ret)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }

  ret = VME_MasterMap(&master_map_s, &handle_s);  
  if (ret)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }
  
  ret = VME_MasterMap(&master_map_r, &handle_r);  
  if (ret)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }
    
  // Signal handler for ctrl + //
  sigemptyset(&sa2.sa_mask);
  sa2.sa_flags = 0;
  sa2.sa_handler = SigQuitHandler;
  stat = sigaction(SIGQUIT, &sa2, nullptr);
  if (stat < 0)
  {
    printf("Cannot install signal handler for ctrl + // (error=%d)\n", stat);
    exit(1);
  }   
  
  printf(" THIS LTP TEST PROGRAM WILL USE 3 LTP MODULES\n\n");
  printf(" SLTP : sending module    (placed to the LEFT)  \n");
  printf("   Base Address: FF2000\n");
  printf(" RLTP : receiving module  (placed to the RIGHT) \n");
  printf("   Base Address: FF3000\n");
  printf(" TLTP : module under test (placed in the MIDDLE)\n");
  printf("   Base Address: FF1000\n\n");
  printf(" CONNECT THE TWO LINK CABLES BETWEEN THE 3 MODULES \n"); 
  PTO
  
  printf("START OF TEST PROGRAM\n");
  printf("THE PROGRAM IS EITHER MENU DRIVEN [m] OR SEQUENTIAL [s]\n");
  printf("SELECT [m] or [s] : ");
  fgets(mode, 9, stdin);

  if (mode[0] == 'm' || mode[0] == 'M')
    test_menu();
  else if (mode[0] == 's' || mode[0] == 'S')
    test_sequential(); 
  else
    printf("Have a nice day\n");
  
  ret = ts_close(TS_DUMMY);
  if (ret)
    rcc_error_print(stdout, ret);
  
  ret = VME_MasterUnmap(handle_t);
  if (ret)
    VME_ErrorPrint(ret);
  
  ret = VME_MasterUnmap(handle_s);
  if (ret)
    VME_ErrorPrint(ret);

  ret = VME_MasterUnmap(handle_r);
  if (ret)
    VME_ErrorPrint(ret);

  ret = VME_Close();
  if (ret)
    VME_ErrorPrint(ret);
}


/******************/
void test_menu(void)
/******************/
{
  static int fun = 1;

  while(fun != 0)
  {  
    printf("SELECT ONE OF THE FOLLOWING TASKS:\n");
    printf("BC PATH FULL TEST.............[ 1]\n");
    printf("ORBIT PATH FULL TEST..........[ 2]\n");
    printf("BUSY PATH + INHIBIT TEST......[ 3]\n");
    printf("L1A PATH TEST.................[ 4]\n");
    printf("TEST TRIGGER <3..1> PATH TEST.[ 5]\n");
    printf("BGo <3..0> PATH TEST..........[ 6]\n"); 
    printf("TRIG-TYPE PATH TEST...........[ 7]\n"); 
    printf("CALIBRATION PATH TEST.........[ 8]\n"); 
    printf("CHECK VME RESPONSE/REG_SWRST......[ 9]\n");
    printf("QUIT PROGRAM................[ 0]\n");
    printf("Your choice: ");
    fun = getdecd(fun);
    if (fun == 1) test_1();
    if (fun == 2) test_2();
    if (fun == 3) test_3();
    if (fun == 4) test_4();
    if (fun == 5) test_5();
    if (fun == 6) test_6();
    if (fun == 7) test_7();
    if (fun == 8) test_8();
    if (fun == 9) test_9();
  }
}


/************************/
void test_sequential(void)
/************************/
{
  //MJ: It should be possible to exit from the sequential test at certain locations
  test_1();
  test_2();
  test_3();
  test_4();
  test_5();
  test_6();
  test_7();
  test_8();
  test_9();
}


/***************/
void test_1(void)
/***************/
{
  init_tltp();
  init_sltp();
  init_rltp();
   
  vw16(TEST, REG_BC_CSR, 7);
  vw16(SNDR, REG_BC_CSR, 7);

  printf(" ROUTINE TO TEST THE BC/CLOCK PATH USING 3 LTP MODULES\n");
  prnmess1();

  printf(" No running BC/CLOCK source is now driving the path\n"); 
  printf(" - Check BC/CLOCK LED's on the 3 modules:  All OFF\n");
  printf(" - Check BC/CLOCK ecl/nim outputs on TLTP: No signal\n");
  PTO
  check_bc_status();
  PTO
  
  vw16(TEST, REG_BC_CSR, 3);

  printf(" The internal TLTP BC/CLOCK source is now driving itself\n"); 
  printf(" and the LINK-OUT, i.e. the RLTP\n"); 
  printf(" - Check BC/CLOCK LED's on the 3 modules:  SLTP OFF\n");
  printf("                                           TLTP ON\n");
  printf("                                           RLTP ON\n"); 
  printf(" - Check BC/CLOCK ecl/nim outputs on TLTP: Signals present\n");
  printf(" - Check frequency of nim output  on TLTP: 40.079 MHz\n\n"); 
  printf(" - Check BC/CLOCK ecl/nim outputs on RLTP: Signals present\n");
  printf(" - Check frequency of nim output  on RLTP: 40.079 MHz\n");
  PTO
  check_bc_status();
  PTO
  
  vw16(TEST, REG_BC_CSR, 7);
  
  printf(" The external BC/CLOCK source will now be driving TLTP \n"); 
  printf(" and the LINK-OUT, i.e. the RLTP\n"); 
  printf(" - Connect a coax: SLTP BC/LOCAL-op/nim => TLTP BC/EXT-ip/nim\n");
  printf(" - Connect a coax: SLTP BC/LOCAL-ip/nim =>\n");
  printf("TTCvi Tester CLOCK/OUT/nim\n");
  PTO

  printf(" - Check BC/CLOCK LED's on the 3 modules:  SLTP ON\n");
  printf("                                           TLTP ON\n");
  printf("                                           RLTP ON\n"); 
  printf(" - Check BC/CLOCK ecl/nim outputs on TLTP: Signals present\n");
  printf(" - Check frequency of nim output  on TLTP: 40.000 MHz\n\n");
  printf(" - Check BC/CLOCK ecl/nim outputs on RLTP: Signals present\n");
  printf(" - Check frequency of nim output  on RLTP: 40.000 MHz\n");
  PTO
  check_bc_status();
  PTO
  printf(" - Remove the coax:SLTP BC/LOCAL-op/nim => TLTP BC/EXT-ip/nim\n");

  vw16(TEST, REG_BC_CSR, 0); 

  printf("   The BC/CLOCK source of the TLTP will now be the LINK-IN,\n"); 
  printf("   which is also bypassed to the LINK-OUT, i.e. the RLTP\n");
  printf(" - Check BC/CLOCK LED's on the 3 modules:  SLTP ON\n");
  printf("                                           TLTP ON\n");
  printf("                                           RLTP ON\n"); 
  printf(" - Check BC/CLOCK ecl/nim outputs on TLTP: Signals present\n");
  printf(" - Check frequency of nim output  on TLTP: 40.000 MHz\n\n");
  printf(" - Check BC/CLOCK ecl/nim outputs on RLTP: Signals present\n");
  printf(" - Check frequency of nim output  on RLTP: 40.000 MHz\n");
  PTO
  check_bc_status();
  PTO

  vw16(TEST, REG_BC_CSR, 2); 

  printf(" The BC/CLOCK source of the TLTP is now the LOCAL OSCILLATOR.\n"); 
  printf(" The LINK-IN is bypassed to the LINK-OUT, i.e. the RLTP\n");
  printf(" - Check BC/CLOCK LED's on the 3 modules:  SLTP ON\n");
  printf("                                           TLTP ON\n");
  printf("                                           RLTP ON\n"); 
  printf(" - Check BC/CLOCK ecl/nim outputs on TLTP: Signals present\n");
  printf(" - Check frequency of nim output  on TLTP: 40.079 MHz\n\n");
  printf(" - Check BC/CLOCK ecl/nim outputs on RLTP: Signals present\n");
  printf(" - Check frequency of nim output  on RLTP: 40.000 MHz\n");
  PTO
  check_bc_status();

  init_tltp();
  init_sltp();
  init_rltp();
}


/***************/
void test_2(void)
/***************/
{
  init_tltp();
  init_sltp();
  init_rltp();

  vw16(TEST, REG_ORB_CSR, 0xB);
  vw16(SNDR, REG_ORB_CSR, 0xB); 
  vw16(RCVR, REG_ORB_CSR, 0);

  printf(" ROUTINE TO TEST THE ORBIT PATH USING 3 LTP MODULES\n");
  prnmess1();

  printf(" No running ORBIT source is now driving the path\n"); 
  printf(" - Check ORBIT LED's on the 3 modules:  All OFF\n");
  printf(" - Check ORBIT ecl/nim outputs on TLTP: No signal\n");
  PTO
  check_orbit_status();
  PTO

  vw16(TEST, REG_ORB_CSR, 7);

  printf(" The internal ORBIT source is now driving the TLTP \n"); 
  printf(" and the LINK-OUT, i.e. the RLTP\n"); 
  printf(" - Check ORBIT LED's on the 3 modules:     SLTP OFF\n");
  printf("                                           TLTP ON\n");
  printf("                                           RLTP ON\n"); 
  printf(" - Check ORBIT ecl/nim outputs   on TLTP:  Signals present\n");
  printf(" - Check frequency of nim output on TLTP:  11.245 kHz\n\n");
  printf(" - Check ORBIT ecl/nim outputs   on RLTP:  Signals present\n");
  printf(" - Check frequency of nim output on RLTP:  11.245 kHz\n");
  PTO
  check_orbit_status();
  PTO

  vw16(TEST, REG_ORB_CSR, 0xB); 

  printf(" An external ORBIT source will now be driving the TLTP \n"); 
  printf(" and its LINK-OUT, i.e. the RLTP\n"); 
  printf(" - Connect a coax: SLTP ORBIT-op/nim => TLTP ORBIT-ip/nim\n");
  printf(" - Connect a coax: SLTP ORBIT-ip/nim =>\n");
  printf("TTCvi Tester ORBIT/OUT/nim\n");
  PTO
  printf(" - Check ORBIT  LED's on the 3 modules:    SLTP ON\n");
  printf("                                           TLTP ON\n");
  printf("                                           RLTP ON\n"); 
  printf(" - Check ORBIT  ecl/nim outputs  on TLTP:  Signals present\n");
  printf(" - Check frequency of nim output on TLTP:  11.223 kHz\n"); 
  printf(" - Check ORBIT ecl/nim outputs   on RLTP:  Signals present\n");
  printf(" - Check frequency of nim output on RLTP:  11.223 kHz\n");
  PTO
  check_orbit_status();
  PTO
  printf(" - Remove the coax: SLTP ORBIT/LOCAL-op/nim \n"); 
  printf("=> TLTP ORBIT/EXT-ip/nim\n"); 

  vw16(TEST, REG_ORB_CSR, 0); 

  printf(" The ORBIT source of the TLTP will now be the LINK-IN,\n"); 
  printf(" which is also bypassed to the LINK-OUT, i.e. the RLTP\n");
  printf(" - Check ORBIT LED's on the 3 modules:     SLTP ON\n");
  printf("                                           TLTP ON\n");
  printf("                                           RLTP ON\n"); 
  printf(" - Check ORBIT  ecl/nim outputs   on TLTP: Signals present\n");
  printf(" - Check frequency of nim output  on TLTP: 11.223 kHz\n"); 
  printf(" - Check ORBIT ecl/nim outputs    on RLTP: Signals present\n");
  printf(" - Check frequency of nim output  on RLTP: 11.223 kHz\n");
  PTO
  check_orbit_status();
  PTO

  vw16(TEST, REG_ORB_CSR, 6); 

  printf(" The ORBIT source of the TLTP is now the LOCAL GENERATOR.\n"); 
  printf(" The LINK-IN is bypassed to the LINK-OUT, i.e. the RLTP\n");
  printf(" - Check ORBIT LED's on the 3 modules:     SLTP ON\n");
  printf("                                           TLTP ON\n");
  printf("                                           RLTP ON\n"); 
  printf(" - Check ORBIT ecl/nim outputs    on TLTP: Signals present\n");
  printf(" - Check frequency of nim output  on TLTP: 11.245 kHz\n\n");
  printf(" - Check ORBIT ecl/nim outputs    on RLTP: Signals present\n");
  printf(" - Check frequency of nim output  on RLTP: 11.223 kHz\n");
  PTO
  check_orbit_status();

  init_tltp();
  init_sltp();
  init_rltp();
}


/***************/
void test_3(void)
/***************/
{
  init_tltp();
  init_sltp();
  init_rltp();

  vw16(TEST, REG_BUSY_CR, 0); 
  vw16(SNDR, REG_BUSY_CR, 0xF);
  vw16(RCVR, REG_BUSY_CR, 0x11);

  printf(" ROUTINE TO TEST THE BUSY PATH USING 3 LTP MODULES\n");

  prnmess1();

  printf(" RLTP BUSY is now driving the path - TLTP in BYPASS mode\n");
  printf(" - Check BUSY LED's on the 3 modules:      SLTP ON\n");
  printf("                                           TLTP OFF\n");
  printf("                                           RLTP ON\n"); 
  printf(" - Check BUSY nim output on TLTP:          No signal\n");
  PTO
  check_busy_status();
  PTO

  vw16(TEST, REG_BUSY_CR, 0x11);  

  printf(" The internal BUSY constant level is now driving the TLTP \n"); 
  printf(" and the LINK-IN, i.e. to the SLTP\n"); 
  printf(" - Check BUSY LED's on the 3 modules:      SLTP ON\n");
  printf("                                           TLTP ON\n");
  printf("                                           RLTP ON\n"); 
  printf(" - Check BUSY nim output on TLTP:          Signal present\n");
  printf(" - Check BUSY nim output on SLTP:          Signal present\n");
  PTO
  check_busy_status();
  PTO

  vw16(TEST, REG_BUSY_CR, 0xF);  
  vw16(SNDR, REG_BUSY_CR, 8);  

  printf(" External BUSY sources will now drive the TLTP. \n"); 
  printf(" - Connect a coax: RLTP BUSY-op/nim => TLTP BUSY-ip/nim\n");
  printf(" - Connect a coax: TLTP BUSY-op/nim => SLTP BUSY-ip/nim\n");
  printf(" - Connect a 0 ohm termination: TLTP BUSY ip/ttl\n"); 
  PTO
  printf(" - Check BUSY  LED's on the 3 modules:     SLTP ON\n");
  printf("                                           TLTP ON\n");
  printf("                                           RLTP ON\n"); 
  printf(" - Check BUSY nim output on TLTP:          Signal present\n");
  printf(" - Check BUSY nim output on SLTP:          Signal present\n");
  PTO
  check_busy_status();
  printf(" - Remove the coax cables and the termination!\n"); 

  vw16(TEST, REG_BUSY_CR, 0x10C1);
  vw16(RCVR, REG_BUSY_CR, 1);
  vw16(SNDR, REG_BUSY_CR, 1);
  vw16(TEST, REG_FIXVAL, 0xC000); 
  vw16(TEST, REG_PATGEN_CSR, 2);

  printf(" The BUSY in the TLTP is now sourced from the PAT-GEN,\n"); 
  printf(" as a BUSY and TRIGGER/BGo mask\n"); 
  printf(" - Check BUSY LED's on the 3 modules:      SLTP OFF\n");
  printf("                                           TLTP ON\n");
  printf("                                           RLTP OFF\n"); 
  printf(" - Check BUSY (ie PG MASK) nim op on TLTP: Signal present\n");
  PTO
  check_busy_status();

  vw16(TEST, REG_BUSY_CR, 0x81); 
  vw16(TEST, REG_INH_TIMER, 0x28);
  vw16(TEST, REG_ORB_CSR, 7); 

  printf(" The INHIBIT signal will now be checked \n"); 
  printf(" - Connect a coax <=> the ORBIT/LOCAL-op/nim and the scope\n");
  printf(" - Connect a coax <=> the  BUSY/LOCAL-op/nim and the scope\n");
  PTO
  printf("   A 100 ns long pulse should appear at the back\n");
  printf("   edge of the ORBIT pulse\n");

  init_tltp();
  init_sltp();
  init_rltp();
}


/***************/
void test_4(void)
/***************/
{
  init_tltp();
  init_sltp();
  init_rltp();

  vw16(TEST, REG_L1A_CSR, 7); 
  vw16(SNDR, REG_L1A_CSR, 0xF); 
  vw16(RCVR, REG_L1A_CSR, 0); 

  printf(" ROUTINE TO TEST THE L1A PATH USING 3 LTP MODULES\n");

  prnmess1();

  vw16(TEST, REG_ORB_CSR, 1); 
  vw16(SNDR, REG_ORB_CSR, 7);

  printf(" CHECKING FRONT PANEL CONNECTIONS AND PFN\n"); 
  printf(" - Connect a coax: SLTP ORBIT/LOCAL-op/nim => \n");
  printf("TLTP L1A/EXT-op/nim\n");
  printf(" - Connect a coax: TLTP ORBIT/LOCAL-op/nim => scope (50 ohm ip)\n");
  printf(" - Connect a coax: TLTP L1A/LOCAL-op/nim   => scope (50 ohm ip)\n");
  printf(" - Trigger scope on LTP L1A/LOCAL-op/nim leading edge !\n"); 
  PTO
  printf(" - Check TLTP L1A/LOCAL-op NIM and ECL outputs: 1 us pulse\n");
  printf(" - Check RLTP L1A/LOCAL-op NIM and ECL outputs: 1 us pulse\n");
  PTO

  vw16(TEST, REG_L1A_CSR, 0xB); 

  printf(" - Check TLTP L1A/LOCAL-op NIM and ECL outputs: 25 ns pulse\n");
  PTO

  vw16(TEST, REG_L1A_CSR, 0x8B); 

  printf(" - Check TLTP L1A/LOCAL-op NIM and ECL outputs: 50 ns pulse\n");
  PTO
  printf("   STATIC DIAGNOSTIC OF L1A PATH\n"); 

  vw16(TEST, REG_L1A_CSR, 0xF); 
  vw16(TEST, REG_PATGEN_CSR, 2); 
  vw16(TEST, REG_FIXVAL, 0);

  printf(" - No signal should be active in SLTP / TLTP / RLTP\n"); 
  printf(" - The TLTP L1A LED should be OFF\n"); 
  PTO
  check_l1a_status();
  PTO

  vw16(TEST, REG_FIXVAL, 2);

  printf(" - The TLTP PAT-GEN is now driving Front-Panel \n");
  printf("and LINK-OUT/RLTP\n");
  printf(" - The TLTP L1A LED should be ON\n");  
  PTO
  check_l1a_status();
  PTO

  vw16(TEST, REG_L1A_CSR, 0);

  printf(" - The TLTP and LINK-OUT/RLTP is now driven from \n");
  printf("an inactive SLTP\n");
  PTO
  check_l1a_status();
  PTO

  vw16(SNDR, REG_FIXVAL, 2); 
  vw16(SNDR, REG_PATGEN_CSR, 2);

  printf(" - The TLTP and LINK-OUT/RLTP is now driven from an active SLTP\n");
  printf(" - The TLTP L1A LED should be ON\n");  
  PTO
  check_l1a_status();
  PTO

  vw16(TEST, REG_L1A_CSR, 2); 

  printf(" - The LINK-OUT/RLTP is now driven from an active SLTP (BYPASS)\n");
  printf(" - The local internal source is turned OFF\n");
  printf(" - The TLTP L1A LED should be OFF\n");  
  PTO
  check_l1a_status();
  PTO

  vw16(TEST, REG_L1A_CSR, 0x40); 
  vw16(TEST, REG_FIXVAL, 0x4000); 

  printf(" - The TLTP and LINK-OUT/RLTP L1A is now MASKED by the PAT-GEN\n");
  printf(" - The TLTP L1A LED should be OFF\n"); 
  PTO
  check_l1a_status();
  PTO

  vw16(TEST, REG_BUSY_CR, 5); 
  vw16(TEST, REG_L1A_CSR, 0x10);

  printf(" - The TLTP L1A can now be MASKED by BUSY\n");
  printf(" - Watch the TLTP L1A and BUSY LEDs and insert a 0 ohm \n");
  printf("   termination in the TLTP BUSY-ip/ttl\n");
  PTO

  vw16(TEST, REG_L1A_CSR, 0x20);
  vw16(TEST, REG_INH_TIMER, 0x28); 
  vw16(TEST, REG_ORB_CSR, 7);

  printf(" - The MASKING of L1A by the INHIBIT signal will now be checked\n\n");
  printf(" - Connect a coax between the ORBIT/LOCAL-op/nim and the scope\n");
  printf(" - Connect a coax between the   L1A/LOCAL-op/nim and the scope\n");
  PTO

  printf("   A 100ns long POSITIVE going pulse should appear at the back\n");
  printf("   edge of the ORBIT pulse\n");

  init_tltp();
  init_sltp();
  init_rltp();
}


/***************/
void test_5(void)
/***************/
{
  u_int trigbit(4), n(0), trignofset(0);
  char schar(' ');
  
  for (n = 1; n < 4; n++)
  {
    if (n == 1)
    {
      schar = '1';
      trignofset = 0x92;
    }

    if (n == 2)
    { 
      schar = '2';
      trignofset = 0x94;
    }

    if (n == 3)
    { 
      schar = '3';
      trignofset = 0x96;
    }

    init_tltp();
    init_sltp();
    init_rltp();

    vw16(TEST, trignofset, 7); 
    vw16(SNDR, trignofset, 0xF); 
    vw16(RCVR, trignofset, 0); 

    printf(" ROUTINE TO TEST THE TEST-TRIG<%c>\n", schar);
    printf("PATH USING 3 LTP MODULES\n");

    prnmess1();

    vw16(TEST, REG_ORB_CSR, 0);
    vw16(SNDR, REG_ORB_CSR, 7); 

    printf(" CHECKING FRONT PANEL CONNECTIONS AND PFN\n"); 
    printf(" - Connect a coax: SLTP ORBIT/LOCAL-op/nim => TLTP \n");
    printf("TRIG<%c>/EXT-op/nim\n", schar);
    printf(" - Connect a coax: TLTP ORBIT/LOCAL-op/nim => scope (50 ohm ip)\n");
    printf(" - Connect a coax: TLTP TRIG<%c>\n", schar);
    printf("/LOCAL-op/nim => scope (50 ohm ip)\n");
    printf(" - Trigger scope on TLTP TRIG<%c>-op/nim leading edge !\n", schar); 
    PTO
    printf(" - Check TLTP TRIG<%c>\n", schar);
    printf("/LOCAL-op NIM output: 1 us pulse\n");
    PTO

    vw16(TEST, trignofset, 0xB);

    printf(" - Check TLTP TRIG<%c>\n", schar);
    printf("/LOCAL-op NIM output: 25 ns pulse\n");
    PTO

    vw16(TEST, trignofset, 0x8B); 

    printf(" - Check TLTP TRIG<%c>\n", schar);
    printf("/LOCAL-op NIM output: 50 ns pulse\n"); 
    PTO
    printf("   STATIC DIAGNOSTIC OF TRIG<%c> PATH\n", schar); 

    vw16(TEST, trignofset, 0xF);
    vw16(TEST, REG_PATGEN_CSR, 2);
    vw16(TEST, REG_FIXVAL, 0);

    printf(" - No signal should be active in SLTP / TLTP / RLTP\n"); 
    printf(" - The TLTP TRIG<%c> LED should be OFF\n", schar); 
    PTO
    check_trig_status(trignofset, schar);
    PTO

    vw16(TEST, REG_FIXVAL, trigbit); 

    printf(" - The TLTP PAT-GEN is now driving Front-Panel \n");
    printf("and LINK-OUT/RLTP\n");
    printf(" - The TLTP TRIG<%c> LED should be ON\n", schar); 
    PTO
    check_trig_status(trignofset, schar);
    PTO

    vw16(TEST, trignofset, 0);

    printf(" - The TLTP and LINK-OUT/RLTP is now driven from \n");
    printf("an inactive SLTP\n");
    PTO
    check_trig_status(trignofset, schar);
    PTO

    vw16(SNDR, REG_FIXVAL, trigbit);
    vw16(SNDR, REG_PATGEN_CSR, 2);

    printf(" - The TLTP and LINK-OUT/RLTP is now driven from an active SLTP\n");
    printf(" - The TLTP TRIG<%c> LED should be ON\n", schar); 
    PTO
    check_trig_status(trignofset, schar);
    PTO

    vw16(TEST, trignofset, 0x40);
    vw16(TEST, REG_FIXVAL, 0x4000);

    printf(" - The TLTP TRIG<%c>\n", schar);; 
    printf("is now MASKED by the TLTP PAT-GEN\n");
    printf(" - The TLTP TRIG<%c> LED should be OFF\n", schar); 
    PTO
    check_trig_status(trignofset, schar);
    PTO

    vw16(TEST, REG_BUSY_CR, 5); 
    vw16(TEST, trignofset, 0x11);

    printf(" - The TLTP TRIG<%c>\n", schar); 
    printf("can now be MASKED by BUSY\n");
    printf(" - Watch the TLTP TRIG<%c> and BUSY LEDs\n", schar); 
    printf("and insert a 0 ohm \n");
    printf("   termination in the TLTP BUSY-ip/ttl\n");
    PTO

    vw16(TEST, trignofset, 0x20);
    vw16(TEST, REG_INH_TIMER, 0x28); 
    vw16(TEST, REG_ORB_CSR, 7); 

    printf(" - The MASKING of TRIG<%c> by the INHIBIT signal\n", schar); 
    printf("will now be checked\n"); 
    printf(" - Connect a coax between the ORBIT/LOCAL-op/nim and the scope\n");
    printf(" - Connect a coax between the TRIG<%c>\n", schar);
    printf("/LOCAL-op/nim and the scope\n");
    PTO
    printf(" - A 100 ns long POSITIVE going pulse should appear at \n");
    printf("   the back edge of the ORBIT pulse\n"); 

    trigbit = trigbit << 1;
  }

  init_tltp();
  init_sltp();
  init_rltp();
}


/***************/
void test_6(void)
/***************/
{
  u_int n(0), trigbit(0x80), bgonofset(0);
  char schar(' ');
  
  for (n = 0; n < 4; n++)
  {
    if (n == 0)
    {
      schar = '0';
      bgonofset = 0xA0;
    }
     
    if (n == 1)
    {
      schar = '1';
      bgonofset = 0xA2;
    }
     
    if (n == 2)
    {
      schar = '2';
      bgonofset = 0xA4;
    }
     
    if (n == 3)
    {
      schar = '3';
      bgonofset = 0xA6;
    }

    init_tltp();
    init_sltp();
    init_rltp();

    vw16(TEST, bgonofset, 7); 
    vw16(SNDR, bgonofset, 0xF); 
    vw16(RCVR, bgonofset, 0); 

    printf(" ROUTINE TO TEST THE BGO<%c>\n", schar);
    printf("PATH USING 3 LTP MODULES\n");

    prnmess1();

    vw16(TEST, REG_ORB_CSR, 1);
    vw16(SNDR, REG_ORB_CSR, 7); 

    printf(" CHECKING FRONT PANEL CONNECTIONS AND PFN\n"); 
    printf(" - Connect a coax: SLTP ORBIT/LOCAL-op/nim => TLTP \n");
    printf("BGO<%c>/EXT-op/nim\n", schar);
    printf(" - Connect a coax: TLTP ORBIT/LOCAL-op/nim => scope (50 ohm ip)\n");
    printf(" - Connect a coax: TLTP BGO<%c>\n", schar);
    printf("/LOCAL-op/nim => scope (50 ohm ip)\n");
    printf(" - Trigger scope on TLTP BGO<%c>-op/nim leading edge !\n", schar);
    PTO
    printf(" - Check TLTP BGO<%c>\n", schar);
    printf("/LOCAL-op NIM output: 1 us pulse\n");
    PTO

    vw16(TEST, bgonofset, 0xB);

    printf(" - Check TLTP BGO<%c>\n", schar);
    printf("/LOCAL-op NIM output: 25 ns pulse\n");
    PTO

    vw16(TEST, bgonofset, 0x8B); 

    printf(" - Check TLTP BGO<%c>\n", schar);
    printf("/LOCAL-op NIM output: 50 ns pulse\n"); 
    PTO
    printf("   STATIC DIAGNOSTIC OF BGO<%c> PATH\n", schar); 

    vw16(TEST, bgonofset, 0xF); 
    vw16(TEST, REG_PATGEN_CSR, 02); 
    vw16(TEST, REG_FIXVAL, 0); 

    printf(" - No signal should be active in SLTP / TLTP / RLTP\n"); 
    printf(" - The TLTP BGO<%c> LED should be OFF\n", schar); 
    PTO
    check_bgo_status(bgonofset, schar);
    PTO

    vw16(TEST, REG_FIXVAL, trigbit); 

    printf(" - The TLTP PAT-GEN is now driving Front-Panel and LINK-OUT/RLTP\n");
    printf(" - The TLTP BGO<%c> LED should be ON\n", schar); 
    PTO
    check_bgo_status(bgonofset, schar);
    PTO

    vw16(TEST, bgonofset, 0); 

    printf(" - The TLTP and LINK-OUT/RLTP is now driven from an inactive SLTP\n");
    PTO
    check_bgo_status(bgonofset, schar);
    PTO

    vw16(SNDR, REG_FIXVAL, trigbit); 
    vw16(SNDR, REG_PATGEN_CSR, 2); 

    printf(" - The TLTP and LINK-OUT/RLTP is now driven from an active SLTP\n");
    printf(" - The TLTP BGO<%c> LED should be ON\n", schar); 
    PTO
    check_bgo_status(bgonofset, schar);
    PTO

    vw16(TEST, bgonofset, 0x40); 
    vw16(TEST, REG_FIXVAL, 0x4000); 

    printf(" - The TLTP BGO<%c>\n", schar); 
    printf("is now MASKED by the TLTP PAT-GEN\n");
    printf(" - The TLTP BGO<%c> LED should be OFF\n", schar); 
    PTO
    check_bgo_status(bgonofset, schar);
    PTO

    vw16(TEST, REG_BUSY_CR, 5); 
    vw16(TEST, bgonofset, 0x11); 

    printf(" - The TLTP BGO<%c>\n", schar); 
    printf("can now be MASKED by BUSY\n");
    printf(" - Watch the TLTP BGO<%c> and BUSY LEDs\n", schar); 
    printf("and insert a 0 ohm \n");
    printf("   termination in the TLTP BUSY-ip/ttl\n");
    PTO

    vw16(TEST, bgonofset, 0x20); 
    vw16(TEST, REG_INH_TIMER, 0x28); 
    vw16(TEST, REG_ORB_CSR, 7); 

    printf(" - The MASKING of BGO<%c> by the INHIBIT signal\n", schar); 
    printf("will now be checked\n"); 
    printf(" - Connect a coax between the ORBIT/LOCAL-op/nim and the scope\n");
    printf(" - Connect a coax between the BGO<%c>\n", schar);
    printf("/LOCAL-op/nim and the scope\n");
    PTO
    printf(" - A 100 ns long POSITIVE going pulse should appear at the back\n");
    printf("   edge of the ORBIT pulse\n"); 

    trigbit = trigbit << 1;
  }

  init_tltp();
  init_sltp();
  init_rltp();
}


/***************/
void test_7(void)
/***************/
{
  u_int errflag;
  u_short regfw0, regfw1, regfw2, regfw3, regfr0, regfr1, regfr2, regfr3; 
  
  init_tltp();
  init_sltp();
  init_rltp();

  printf("     ROUTINE TO CHECK CTP-LINK\n");
  printf("TRIG-TYPE DATA PATH USING 3 LTP MODULES\n");
  prnmess1();
  printf("- The TLTP FIFO will now be reset \n");
  printf("  and the 4 first positions written \n");
  printf("  with data generated by the SLTP\n"); 
  printf("- The data is read by the TLTP and RLTP\n"); 

  vw16(TEST, REG_TTYPE_CSR, 0);
  vw16(SNDR, REG_TTYPE_CSR, 7);
  vw16(RCVR, REG_TTYPE_CSR, 0);

  regfw0 = 0;
  regfw1 = 0x55; 
  regfw2 = 0xaa;
  regfw3 = 0xff;

  vw16(SNDR, REG_TTYPE_REGFILE0, regfw0);
  vw16(SNDR, REG_TTYPE_REGFILE1, regfw1);
  vw16(SNDR, REG_TTYPE_REGFILE2, regfw2);
  vw16(SNDR, REG_TTYPE_REGFILE3, regfw3);
  vw16(TEST, REG_FIFO_RST, 0);
  vw16(RCVR, REG_FIFO_RST, 0);
  vw16(TEST, REG_TTYPE_CSR, 7);  
  vw16(TEST, REG_FIFO_IO, 1);
  vw16(RCVR, REG_FIFO_IO, 1); 
  vw16(TEST, REG_TTYPE_CSR, 0x17); 
  vw16(TEST, REG_FIFO_IO, 1);
  vw16(RCVR, REG_FIFO_IO, 1); 
  vw16(TEST, REG_TTYPE_CSR, 0x27); 
  vw16(TEST, REG_FIFO_IO, 1);
  vw16(RCVR, REG_FIFO_IO, 1); 
  vw16(TEST, REG_TTYPE_CSR, 0x37); 
  vw16(TEST, REG_FIFO_IO, 1);
  vw16(RCVR, REG_FIFO_IO, 1); 
  vr16(TEST, REG_FIFO_IO, &regfr0); 
  vr16(TEST, REG_FIFO_IO, &regfr1); 
  vr16(TEST, REG_FIFO_IO, &regfr2); 
  vr16(TEST, REG_FIFO_IO, &regfr3); 

  errflag = 0;

  printf("- Reading TLTP FIFO\n"); 
  printf("    position 0 written data: 0x%02x\n", regfw0 & 0xFF); 
  printf("    position 0 read data:    0x%02x\n", regfr0 & 0xFF);  

  if ((regfw0 & 0xFF) != (regfr0 & 0xFF))
    errflag = 1;

  printf("    position 1 written data: 0x%02x\n", regfw1 & 0xFF); 
  printf("    position 1 read data:    0x%02x\n", regfr1 & 0xFF ); 

  if ((regfw1 & 0xFF) != (regfr1 & 0xFF))
    errflag = 1;

  printf("    position 2 written data: 0x%02x\n", regfw2 & 0xFF); 
  printf("    position 2 read data:    0x%02x\n", regfr2 & 0xFF);  

  if ((regfw2 & 0xFF) != (regfr2 & 0xFF))
    errflag = 1;

  printf("    position 3 written data: 0x%02x\n", regfw3 & 0xFF); 
  printf("    position 3 read data:    0x%02x\n", regfr3 & 0xFF);  

  if ((regfw3 & 0xFF) != (regfr3 & 0xFF))
    errflag = 1;
    
  if (errflag)
    printf("!!! CTP-LINK/in DATA LINE PROBLEM !!!\n"); 
  else
    printf(" CTP-LINK/in TRIG-TYPE DATA LINE TEST is OK !!!\n");  

  PTO
  
  vr16(RCVR, REG_FIFO_IO, &regfr0); 
  vr16(RCVR, REG_FIFO_IO, &regfr1); 
  vr16(RCVR, REG_FIFO_IO, &regfr2); 
  vr16(RCVR, REG_FIFO_IO, &regfr3); 

  errflag = 0;

  printf("- Reading RLTP FIFO\n"); 
  printf("    position 0 written data: 0x%02x\n", regfw0 & 0xFF); 
  printf("    position 0 read data:    0x%02x\n", regfr0 & 0xFF);  

  if ((regfw0 & 0xFF) != (regfr0 & 0xFF))
    errflag = 1;

  printf("    position 1 written data: 0x%02x\n", regfw1 & 0xFF); 
  printf("    position 1 read data:    0x%02x\n", regfr1 & 0xFF);  

  if ((regfw1 & 0xFF) != (regfr1 & 0xFF))
    errflag = 1;

  printf("    position 2 written data: 0x%02x\n", regfw2 & 0xFF); 
  printf("    position 2 read data:    0x%02x\n", regfr2 & 0xFF);  

  if ((regfw2 & 0xFF) != (regfr2 & 0xFF))
    errflag = 1;
 
  printf("    position 3 written data: 0x%02x\n", regfw3 & 0xFF); 
  printf("    position 3 read data:    0x%02x\n", regfr3 & 0xFF ); 

  if ((regfw3 & 0xFF) != (regfr3 & 0xFF))
    errflag = 1;
    
  if (errflag)
    printf("!!! CTP-LINK/out DATA LINE PROBLEM !!!\n"); 
  else
    printf(" CTP-LINK/out TRIG-TYPE DATA LINE TEST is OK !!!\n");  

  PTO

  init_all_ltp();

  printf("     ROUTINE TO CHECK FRONT PANEL\n");
  printf("EXTERNAL TRIG-TYPE CONNECT\n"); 

  vw16(TEST, REG_TTYPE_CSR, 7);
  vw16(RCVR, REG_TTYPE_CSR, 8);

  printf("- Connect a flat cable between the front panel\n");
  printf("  TRIG-TYPE connectors of the TLTP and RLTP\n"); 

  PTO
  
  regfw0 = 0;
  regfw1 = 0x55; 
  regfw2 = 0xAA;
  regfw3 = 0xFF;

  vw16(TEST, REG_TTYPE_REGFILE0, regfw0);
  vw16(TEST, REG_TTYPE_REGFILE1, regfw1);
  vw16(TEST, REG_TTYPE_REGFILE2, regfw2);
  vw16(TEST, REG_TTYPE_REGFILE3, regfw3);
  vw16(RCVR, REG_FIFO_RST, 0);
  vw16(TEST, REG_TTYPE_CSR, 7);
  vw16(RCVR, REG_FIFO_IO, 1); 
  vw16(TEST, REG_TTYPE_CSR, 0x17);
  vw16(RCVR, REG_FIFO_IO, 1); 
  vw16(TEST, REG_TTYPE_CSR, 0x27);
  vw16(RCVR, REG_FIFO_IO, 1); 
  vw16(TEST, REG_TTYPE_CSR, 0x37);
  vw16(RCVR, REG_FIFO_IO, 1); 
  vr16(RCVR, REG_FIFO_IO, &regfr0); 
  vr16(RCVR, REG_FIFO_IO, &regfr1); 
  vr16(RCVR, REG_FIFO_IO, &regfr2); 
  vr16(RCVR, REG_FIFO_IO, &regfr3); 

  errflag = 0;

  printf("- Data is going from TLTP => RLTP\n");
  printf("- Reading RLTP FIFO\n"); 
  printf("    position 0 written data: 0x%02x\n", regfw0 & 0xFF); 
  printf("    position 0 read data:    0x%02x\n", regfr0 & 0xFF);  

  if ((regfw0 & 0xFF) != (regfr0 & 0xFF))
    errflag = 1;

  printf("    position 1 written data: 0x%02x\n", regfw1 & 0xFF); 
  printf("    position 1 read data:    0x%02x\n", regfr1 & 0xFF);  

  if ((regfw1 & 0xFF) != (regfr1 & 0xFF))
    errflag = 1;

  printf("    position 2 written data: 0x%02x\n", regfw2 & 0xFF); 
  printf("    position 2 read data:    0x%02x\n", regfr2 & 0xFF);  

  if ((regfw2 & 0xFF) != (regfr2 & 0xFF))
    errflag = 1;

  printf("    position 3 written data: 0x%02x\n", regfw3 & 0xFF); 
  printf("    position 3 read data:    0x%02x\n", regfr3 & 0xFF); 

  if ((regfw3 & 0xFF) != (regfr3 & 0xFF))
    errflag = 1;
    
  if (errflag)
    printf("!!! EXT DATA TRANSFER PROBLEM !!!\n");
  else
  printf(" TLTP => RLTP EXT LINE TRANSFER TEST is OK !!!\n");  

  PTO
  
  vw16(RCVR, REG_TTYPE_CSR, 7);
  vw16(TEST, REG_TTYPE_CSR, 8);
  vw16(RCVR, REG_TTYPE_REGFILE0, regfw0);
  vw16(RCVR, REG_TTYPE_REGFILE1, regfw1);
  vw16(RCVR, REG_TTYPE_REGFILE2, regfw2);
  vw16(RCVR, REG_TTYPE_REGFILE3, regfw3);
  vw16(TEST, REG_FIFO_RST, 0);
  vw16(RCVR, REG_TTYPE_CSR, 7);
  vw16(TEST, REG_FIFO_IO, 1); 
  vw16(RCVR, REG_TTYPE_CSR, 0x17);
  vw16(TEST, REG_FIFO_IO, 1); 
  vw16(RCVR, REG_TTYPE_CSR, 0x27);
  vw16(TEST, REG_FIFO_IO, 1); 
  vw16(RCVR, REG_TTYPE_CSR, 0x37);
  vw16(TEST, REG_FIFO_IO, 1); 
  vr16(TEST, REG_FIFO_IO, &regfr0); 
  vr16(TEST, REG_FIFO_IO, &regfr1); 
  vr16(TEST, REG_FIFO_IO, &regfr2); 
  vr16(TEST, REG_FIFO_IO, &regfr3); 

  errflag = 0;

  printf("- Data is going from RLTP => TLTP\n");
  printf("- Reading TLTP FIFO\n"); 
  printf("    position 0 written data: 0x%02x\n", regfw0 & 0xFF); 
  printf("    position 0 read data:    0x%02x\n", regfr0 & 0xFF ); 

  if ((regfw0 & 0xFF) != (regfr0 & 0xFF))
    errflag = 1;

  printf("    position 1 written data: 0x%02x\n", regfw1 & 0xFF); 
  printf("    position 1 read data:    0x%02x\n", regfr1 & 0xFF);  

  if ((regfw1 & 0xFF) != (regfr1 & 0xFF))
    errflag = 1;

  printf("    position 2 written data: 0x%02x\n", regfw2 & 0xFF);
  printf("    position 2 read data:    0x%02x\n", regfr2 & 0xFF);  

  if ((regfw2 & 0xFF) != (regfr2 & 0xFF))
    errflag = 1;

  printf("    position 3 written data: 0x%02x\n", regfw3 & 0xFF); 
  printf("    position 3 read data:    0x%02x\n", regfr3 & 0xFF);  

  if ((regfw3 & 0xFF) != (regfr3 & 0xFF))
    errflag = 1;

  if (errflag)
    printf("!!! EXT DATA TRANSFER PROBLEM !!!\n");
  else
    printf(" RLTP => TLTP EXT LINE TRANSFER TEST is OK !!!\n");  

  init_tltp();
  init_sltp();
  init_rltp();
}


/***************/
void test_8(void)
/***************/
{
  u_int errflag, loop, n;
  u_short wpatt, rpatt;
  
  init_tltp();
  init_sltp();
  init_rltp();

  printf("     ROUTINE TO CHECK CALIBRATION PATH\n");
  printf("USING 3 LTP MODULES\n");
  prnmess1();

  vw16(TEST, REG_CAL_CSR, 1);

  printf("- The internal generation of the CALIBRATION REQUEST \n");
  printf("  will now be checked by setting bits in the TLTP CSR \n"); 

  errflag = 0;
  n = 0;
  
  for (loop = 0; loop < 4; loop++)
  {
    wpatt = 1;   
    wpatt = wpatt | (n << 4);
    
    vw16(TEST, REG_CAL_CSR, wpatt);
    vr16(TEST, REG_CAL_CSR, &rpatt);

    printf("- TLTP REG_CAL_CSR Data written %d:\n", n);
    printf("- TLTP REG_CAL_CSR Data read    %d:\n", (rpatt >> 8) & 0x7);  

    if (n != (u_int)((rpatt >> 8) & 0x7))
      errflag = 1;
    
    n = n << 1; 
    if (n == 0)
      n = 1;
  }

  if (errflag)
    printf("!!! CALIBRATION BITS READ/WRITE PROBLEM !!!\n");  
  else
    printf("!!! CALIBRATION BITS READ/WRITE is OK !!!\n"); 

  PTO
  
  vw16(TEST, REG_FIXVAL, 0);
  vw16(TEST, REG_PATGEN_CSR, 2); 
  vw16(TEST, REG_CAL_CSR, 3);

  printf("- The internal generation of the CALIBRATION REQUEST \n");
  printf("  will now be checked by setting PATTERN GENERATOR bits \n");

  errflag = 0;
  n = 0;

  for (loop = 0; loop < 4; loop++)
  {
    wpatt = 0;   
    wpatt = wpatt | (n << 11);
    
    vw16(TEST, REG_FIXVAL, wpatt); 
    vr16(TEST, REG_CAL_CSR, &rpatt);

    printf("- TLTP PATGEN Data written %d:\n", n);
    printf("- TLTP REG_CAL_CSR Data read    %d:\n", (rpatt >> 8) & 0x7);  

    if (n != (u_int)((rpatt >> 8) & 0x7))
      errflag = 1;
    
    n = n << 1; 
    if (n == 0)
      n = 1;
  }

  if (errflag)
    printf("!!! CALIBRATION BITS READ/WRITE PROBLEM !!!\n");  
  else
    printf("!!! CALIBRATION BITS READ/WRITE is OK !!!\n");

  PTO
  
  vw16(TEST, REG_CAL_CSR, 1);
  vw16(SNDR, REG_CAL_CSR, 0); 

  printf("- The TLTP => SLTP LINK connections of the CALIBRATION REQUEST\n");
  printf("  will now be checked by setting bits in the TLTP CSR \n"); 

  errflag = 0;
  n = 0;

  for (loop = 0; loop < 4; loop++)
  {
    wpatt = 1;   
    wpatt = wpatt | (n << 4);
    
    vw16(TEST, REG_CAL_CSR, wpatt); 
    vr16(SNDR, REG_CAL_CSR, &rpatt);

    printf("- TLTP REG_CAL_CSR Data written %d:\n", n);
    printf("- SLTP REG_CAL_CSR Data read    %d:\n", (rpatt >> 8) & 0x7);  

    if (n != (u_int)((rpatt >> 8) & 0x7))
      errflag = 1;
    
    n = n << 1; 
    if (n == 0)
      n = 1;
  }

  if (errflag)
    printf("!!! TLTP => SLTP CALIBRATION BITS LINK PROBLEM !!!\n");  
  else
    printf("TLTP => SLTP CALIBRATION BITS on LINK is OK !\n"); 

  PTO
  
  vw16(RCVR, REG_CAL_CSR, 1);
  vw16(TEST, REG_CAL_CSR, 0); 

  printf("- The RLTP => TLTP LINK connections of the CALIBRATION REQUEST\n");
  printf("  will now be checked by setting bits in the RLTP CSR \n"); 

  errflag = 0;
  n = 0;

  for (loop = 0; loop < 4; loop++)
  {
    wpatt = 1;   
    wpatt = wpatt | (n << 4);
    
    vw16(RCVR, REG_CAL_CSR, wpatt); 
    vr16(TEST, REG_CAL_CSR, &rpatt);

    printf("- RLTP REG_CAL_CSR Data written %d:\n", n);
    printf("- TLTP REG_CAL_CSR Data read    %d:\n", (rpatt >> 8) & 0x7);  

    if (n != (u_int)((rpatt >> 8) & 0x7))
      errflag = 1;
    
    n = n << 1; 
    if (n == 0)
      n = 1;
  }

  if (errflag)
    printf("!!! RLTP => TLTP CALIBRATION BITS LINK PROBLEM !!!\n");  
  else
    printf("RLTP => TLTP CALIBRATION BITS on LINK is OK !\n"); 

  PTO
  
  vw16(TEST, REG_CAL_CSR, 5); 
  vw16(SNDR, REG_CAL_CSR, 2);

  printf("- The TLTP => SLTP cable connection of the CALIBRATION REQUEST\n");
  printf("  will now be checked by setting bits in the TLTP CSR \n"); 
  printf("- Connect a cable TLTP => SLTP CALIBR REQ LVDS\n");

  PTO

  errflag = 0;
  n = 0;

  for (loop = 0; loop < 4; loop++)
  {
    wpatt = 5;   
    wpatt = wpatt | (n << 4);
    
    vw16(TEST, REG_CAL_CSR, wpatt); 
    vr16(SNDR, REG_CAL_CSR, &rpatt);

    printf("- TLTP REG_CAL_CSR Data written %d:\n", n);
    printf("- SLTP REG_CAL_CSR Data read    %d:\n", (rpatt >> 8) & 0x7);  

    if (n != (u_int)((rpatt >> 8) & 0x7))
      errflag = 1;
    
    n = n << 1; 
    if (n == 0)
      n = 1;
  }
  
  if (errflag)
    printf("!!! TLTP => SLTP CALIBRATION BIT CABLE PROBLEM !!!\n");  
  else
    printf("TLTP => SLTP CALIBRATION BITS on CABLE is OK !\n"); 

  PTO

  vw16(SNDR, REG_CAL_CSR, 5);
  vw16(TEST, REG_CAL_CSR, 2);

  printf("- The SLTP => TLTP cable connection of the CALIBRATION REQUEST\n");
  printf("  will now be checked by setting bits in the SLTP CSR \n"); 
  printf("- Keep the cable TLTP => SLTP CALIBR REQ LVDS\n");

  PTO

  errflag = 0;
  n = 0;

  for (loop = 0; loop < 4; loop++)
  {
    wpatt = 5;   
    wpatt = wpatt | (n << 4);
    
    vw16(SNDR, REG_CAL_CSR, wpatt); 
    vr16(TEST, REG_CAL_CSR, &rpatt);

    printf("- SLTP REG_CAL_CSR Data written %d:\n", n);
    printf("- TLTP REG_CAL_CSR Data read    %d:\n", (rpatt >> 8) & 0x7);  

    if (n != (u_int)((rpatt >> 8) & 0x7))
      errflag = 1;
    
    n = n << 1; 
    if (n == 0)
      n = 1;
  }

  if (errflag)
    printf("!!! SLTP => TLTP CALIBRATION BIT CABLE PROBLEM !!!\n");  
  else
    printf("SLTP => TLTP CALIBRATION BITS on CABLE is OK !\n"); 

  init_tltp();
  init_sltp();
  init_rltp();
}


/***************/
void test_9(void)
/***************/
{
  printf("              CHECK VME ACCESS\n\n"); 

  printf("   Press <ctrl>+\\ to stop the routine.\n");
  cont = 1;

  while(cont)  
  {
    ret = vw16(TEST, REG_SWRST, 1);
    if (ret)
    {
      printf("   !!! Recheck ADDRESS and AM settings !!!\n");
      PTO
    }
  }

  if (!ret)
    printf("   VME access is OK\n");
    
  PTO
}


/*********************/
void init_all_ltp(void)
/*********************/
{
  vw16(TEST, REG_TTYPE_REGFILE0, 0);
  vw16(TEST, REG_TTYPE_REGFILE1, 0x55);
  vw16(TEST, REG_TTYPE_REGFILE2, 0xAA);
  vw16(TEST, REG_TTYPE_REGFILE3, 0xFF);
  vw16(TEST, REG_TTYPE_CSR,      3);
  vw16(TEST, REG_FIFO_RST,       0);
  vw16(TEST, REG_CAL_CSR,        3);
  vw16(TEST, REG_BUSY_CR,        0x8001);
  vw16(TEST, REG_SW_RST,         0);
  vw16(TEST, REG_PATGEN_CSR,     0);
  vw16(TEST, REG_FIXVAL,         0);
  vw16(TEST, REG_20BITCNT_MSW,   0);
  vw16(TEST, REG_20BITCNT_LSW,   1);

  init_sltp();
  init_rltp();
}


/******************/
void init_tltp(void)
/******************/
{
  vw16(TEST, REG_SWRST,          0);
  vw16(TEST, REG_BC_CSR,         3);
  vw16(TEST, REG_ORB_CSR,        0xF);
  vw16(TEST, REG_L1A_CSR,        0xF);
  vw16(TEST, REG_TR1_CSR,        0xF);
  vw16(TEST, REG_TR2_CSR,        0xF);
  vw16(TEST, REG_TR3_CSR,        0xF);
  vw16(TEST, REG_BG0_CSR,        0xF);
  vw16(TEST, REG_BG1_CSR,        0xF);
  vw16(TEST, REG_BG2_CSR,        0xF);
  vw16(TEST, REG_BG3_CSR,        0xF);
  vw16(TEST, REG_TURN_CR,        0);
  vw16(TEST, REG_TTYPE_REGFILE0, 0);
  vw16(TEST, REG_TTYPE_REGFILE1, 0x55);
  vw16(TEST, REG_TTYPE_REGFILE2, 0xAA);
  vw16(TEST, REG_TTYPE_REGFILE3, 0xFF);
  vw16(TEST, REG_TTYPE_CSR,      3);
  vw16(TEST, REG_FIFO_RST,       0);
  vw16(TEST, REG_CAL_CSR,        3);
  vw16(TEST, REG_BUSY_CR,        0x8001);
  vw16(TEST, REG_SW_RST,         0);
  vw16(TEST, REG_PATGEN_CSR,     0);
  vw16(TEST, REG_FIXVAL,         0);
  vw16(TEST, REG_20BITCNT_MSW,   0);
  vw16(TEST, REG_20BITCNT_LSW,   1);
}


/******************/
void init_sltp(void)
/******************/
{
  vw16(SNDR, REG_SWRST,          0);
  vw16(SNDR, REG_BC_CSR,         3);
  vw16(SNDR, REG_ORB_CSR,        7);
  vw16(SNDR, REG_L1A_CSR,        0xF);
  vw16(SNDR, REG_TR1_CSR,        0xF);
  vw16(SNDR, REG_TR2_CSR,        0xF);
  vw16(SNDR, REG_TR3_CSR,        0xF);
  vw16(SNDR, REG_BG0_CSR,        0xF);
  vw16(SNDR, REG_BG1_CSR,        0xF);
  vw16(SNDR, REG_BG2_CSR,        0xF);
  vw16(SNDR, REG_BG3_CSR,        0xF);
  vw16(SNDR, REG_TURN_CR,        0);
  vw16(SNDR, REG_TTYPE_REGFILE0, 0);
  vw16(SNDR, REG_TTYPE_REGFILE1, 0x55);
  vw16(SNDR, REG_TTYPE_REGFILE2, 0xAA);
  vw16(SNDR, REG_TTYPE_REGFILE3, 0xFF);
  vw16(SNDR, REG_TTYPE_CSR,      3);
  vw16(SNDR, REG_FIFO_RST,       0);
  vw16(SNDR, REG_CAL_CSR,        0);
  vw16(SNDR, REG_BUSY_CR,        0);
  vw16(SNDR, REG_PATGEN_CSR,     0);
  vw16(SNDR, REG_FIXVAL,         0);
  vw16(SNDR, REG_20BITCNT_LSW,   1);
}


/******************/
void init_rltp(void)
/******************/
{
  vw16(RCVR, REG_SWRST,     0);
  vw16(RCVR, REG_BC_CSR,    0);
  vw16(RCVR, REG_ORB_CSR,   0);
  vw16(RCVR, REG_L1A_CSR,   0);
  vw16(RCVR, REG_TR1_CSR,   0);
  vw16(RCVR, REG_TR2_CSR,   0);
  vw16(RCVR, REG_TR3_CSR,   0);
  vw16(RCVR, REG_BG0_CSR,   0);
  vw16(RCVR, REG_BG1_CSR,   0);
  vw16(RCVR, REG_BG2_CSR,   0);
  vw16(RCVR, REG_BG3_CSR,   0);
  vw16(RCVR, REG_TURN_CR,   0);
  vw16(RCVR, REG_TTYPE_CSR, 0);
  vw16(RCVR, REG_FIFO_RST,   0);
  vw16(RCVR, REG_CAL_CSR,    1);
  vw16(RCVR, REG_BUSY_CR,    1);
}


/*****************/
void prnmess1(void)
/*****************/
{
  printf("\n       SLTP : sending module    (placed to the LEFT)\n");
  printf("       RLTP : receiving module  (placed to the RIGHT)\n");
  printf("       TLTP : module under test (placed in the MIDDLE)\n\n");
  printf(" CHECK CONNECTION OF LINK CABLES BETWEEN THE 3 MODULES \n");  
  PTO
}


/************************/
void check_bc_status(void)
/************************/
{
  u_short bcstat;
  
  printf(" - The BC CSR status bits of the SLTP show: \n"); 
  vr16(SNDR, REG_BC_CSR, &bcstat);
  printbcstat(bcstat);

  printf(" - The BC CSR status bits of the TLTP show: \n"); 
  vr16(TEST, REG_BC_CSR, &bcstat);
  printbcstat(bcstat);

  printf(" - The BC CSR status bits of the RLTP show: \n"); 
  vr16(RCVR, REG_BC_CSR, &bcstat);
  printbcstat(bcstat);
}


/******************************/
void printbcstat(u_short bcstat)
/******************************/
{
 if (bcstat & 0x100) 
   printf("=> BC/CLOCK external is running\n");
 else  
   printf("=> BC/CLOCK external is NOT running\n");

 if (bcstat & 0x200)
   printf("=> BC/CLOCK is driving this LTP\n");
 else  
   printf("=> BC/CLOCK is NOT driving this LTP\n");
}


/***************************/
void check_orbit_status(void)
/***************************/
{
  u_short orbstat;
  
  printf(" - The ORBIT CSR status bit of the SLTP show: \n"); 
  vr16(SNDR, REG_ORB_CSR, &orbstat); 
  printorbstat(orbstat);

  printf(" - The ORBIT CSR status bit of the TLTP show: \n"); 
  vr16(TEST, REG_ORB_CSR, &orbstat);
  printorbstat(orbstat);

  printf(" - The ORBIT CSR status bit of the RLTP show: \n"); 
  vr16(RCVR, REG_ORB_CSR, &orbstat); 
  printorbstat(orbstat);
}


/********************************/
void printorbstat(u_short orbstat)
/********************************/
{
  if (orbstat & 0x100)
    printf("=> ORBIT is driving this LTP\n");
  else
    printf("=> ORBIT is NOT driving this LTP\n");
}


/**************************/
void check_busy_status(void)
/**************************/
{
  u_short busystat;
  
  printf(" - The BUSY SR status bits of the SLTP show: \n"); 
  vr16(SNDR, REG_BUSY_SR, &busystat);
  prbusystat(busystat);

  printf(" - The BUSY SR status bits of the TLTP show: \n");  
  vr16(TEST, REG_BUSY_SR, &busystat);
  prbusystat(busystat);

  printf(" - The BUSY SR status bits of the RLTP show: \n");  
  vr16(RCVR, REG_BUSY_SR, &busystat);
  prbusystat(busystat);
}


/*******************************/
void prbusystat(u_short busystat)
/*******************************/
{
 if (busystat & 0x2)    
   printf("     => BUSY     present on LINK-IN\n");
 else 
   printf("     => BUSY NOT present on LINK-IN\n");

 if (busystat & 0x1)  
   printf("     => BUSY     present on LINK-OUT\n");
 else
   printf("     => BUSY NOT present on LINK-OUT\n");

 if (busystat & 0x4) 
   printf("     => BUSY     present on TTL i/p\n");
 else 
   printf("     => BUSY NOT present on TTL i/p\n");
 
 if (busystat & 0x8)  
   printf("     => BUSY     present on NIM i/p\n");
 else
   printf("     => BUSY NOT present on NIM i/p\n");

 if (busystat & 0x10)  
   printf("     => BUSY     present to INT LOGIC\n");
 else
   printf("     => BUSY NOT present to INT LOGIC\n");
}


/*************************/
void check_l1a_status(void)
/*************************/
{
  u_short keepval, l1astat;

  printf(" - The L1A CSR status bits of the SLTP show: \n"); 
  vr16(SNDR, REG_FIXVAL, &keepval);
  vr16(SNDR, REG_L1A_CSR, &l1astat);
  printl1astat(l1astat);
  vw16(SNDR, REG_FIXVAL, keepval);

  printf(" - The L1A CSR status bits of the TLTP show: \n"); 
  vr16(TEST, REG_FIXVAL, &keepval);
  vr16(TEST, REG_L1A_CSR, &l1astat);
  printl1astat(l1astat);
  vw16(TEST, REG_FIXVAL, keepval);

  printf(" - The L1A CSR status bits of the RLTP show: \n"); 
  vr16(RCVR, REG_L1A_CSR, &l1astat);
  printl1astat(l1astat);
}


/********************************/
void printl1astat(u_short l1astat)
/********************************/
{
  if (l1astat & 0x100)
    printf("=> L1A signal     present on LINK-OUT \n");
  else
    printf("=> L1A signal NOT present on LINK-OUT \n");

  if (l1astat & 0x200)
    printf("=> L1A signal     present on Front Panel \n");  
  else
    printf("=> L1A signal NOT present on Front Panel \n"); 
}


/**************************************************/
void check_trig_status(u_int trignofset, char schar)
/**************************************************/
{
  u_short keepval, trinstat;
  
  printf(" - The TRIG<%c> CSR status bits of the SLTP show:\n", schar); 
  vr16(SNDR, REG_FIXVAL, &keepval); 
  vr16(SNDR, trignofset, &trinstat);
  vw16(SNDR, REG_FIXVAL, keepval); 
  prtrignstat(trinstat, schar);

  printf(" - The TRIG<%c> CSR status bits of the TLTP show:\n", schar); 
  vr16(TEST, REG_FIXVAL, &keepval); 
  vr16(TEST, trignofset, &trinstat);
  vw16(TEST, REG_FIXVAL, keepval); 
  prtrignstat(trinstat, schar);

  printf(" - The TRIG<%c> CSR status bits of the RLTP show:\n", schar); 
  vr16(RCVR, trignofset, &trinstat); 
  prtrignstat(trinstat, schar);
}


/********************************************/
void prtrignstat(u_short trinstat, char schar)
/********************************************/
{
  if (trinstat & 0x100)
    printf("=> TST-TRG<%c>     present on LINK-IN\n", schar);
  else 
    printf("=> TST-TRG<%c> NOT present on LINK-IN\n", schar);

  if (trinstat & 0x200)
    printf("=> TST-TRG<%c>     present on LINK-OUT\n", schar);
  else 
    printf("=> TST-TRG<%c> NOT present on LINK-OUT\n", schar);

  if (trinstat & 0x400)
    printf("=> TST-TRG<%c>     present LOCALLY\n", schar);
  else 
    printf("=> TST-TRG<%c> NOT present LOCALLY\n", schar);
}


/************************************************/
void check_bgo_status(u_int bgonofset, char schar)
/************************************************/
{
  u_short keepval, bgonstat;
  
  printf(" - The BGO<%c> CSR status bits of the SLTP show:\n", schar); 
  vr16(SNDR, REG_FIXVAL, &keepval); 
  vr16(SNDR, bgonofset, &bgonstat);
  vw16(SNDR, REG_FIXVAL, keepval); 
  prbgonstat(bgonstat, schar);

  printf(" - The BGO<%c> CSR status bits of the TLTP show:\n", schar); 
  vr16(TEST, REG_FIXVAL, &keepval);
  vr16(TEST, bgonofset, &bgonstat);
  vw16(TEST, REG_FIXVAL, keepval); 
  prbgonstat(bgonstat, schar);

  printf(" - The BGO<%c> CSR status bits of the RLTP show:\n", schar); 
  vr16(RCVR, bgonofset, &bgonstat); 
  prbgonstat(bgonstat, schar);
}


/*******************************************/
void prbgonstat(u_short bgonstat, char schar)
/*******************************************/
{
  if (bgonstat & 0x100)
    printf("=> BGO<%c>     present on LINK-IN\n", schar);
  else
    printf("=> BGO<%c> NOT present on LINK-IN\n", schar);

  if (bgonstat & 0x200)
    printf("=> BGO<%c>     present on LINK-OUT\n", schar);
  else
    printf("=> BGO<%c> NOT present on LINK-OUT\n", schar);

  if (bgonstat & 0x400)
    printf("=> BGO<%c>     present LOCALLY\n", schar);
  else
    printf("=> BGO<%c> NOT present LOCALLY\n", schar);
}
