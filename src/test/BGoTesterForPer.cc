//-------------------------------------------------
// file: menuRCDLtp.cc
// desc: test program for Per Gallno to test BGo
//-------------------------------------------------

#include <fstream>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <list>
#include <errno.h>
#include <stdlib.h>

#include <string>
#include <vector>
#include "RCDVme/RCDVme.h"

// #include <boost/date_time/posix_time/posix_time.hpp>
 // #include <sys/time.h>

#include "owl/time.h"
#include "RCDLtp/RCDLtp.h"

#include "rcc_time_stamp/tstamp.h"
#include "rcc_error/rcc_error.h"

#include <time.h>

using namespace std;
using namespace RCD;

int main() {

  int status = 0;
  std::string		in;
  u_int		base;
  std::cout << "<base>?" << std::endl;
  getline(cin,in);
  if(sscanf(in.c_str(),"%x",&base) == EOF) exit(EINVAL);
  if(base < 0x100) base = base << 16;

  LTP* ltp = new LTP();
  status = ltp->Open(base);
  
  u_int board_man = 0;
  u_int board_revision = 0;
  u_int board_id = 0;
  status |= ltp->ReadConfigurationEEPROM(board_man, board_revision, board_id);

  printf("\n");
  printf(" Manufacturer: 0x%08x (CERN = 0x00080030)\n", board_man);
  printf(" Revision    : 0x%08x \n", board_revision);
  printf(" Id          : 0x%08x \n", board_id);

  std::cout << "> This Program checks the time resolution of 2 timers:" << std::endl;
  std::cout << "> nanosleep and rcc_timer" << std::endl;
  std::cout << "> it will produce TestTrigger1 pulses after each wait function" << std::endl;
  std::cout << std::endl;

  int sel=0;
  std::cout << "> Method (0=none, 1=nanosleep, 2=rcc_timer)? " << std::endl;
  getline(cin,in);
  if(sscanf(in.c_str(),"%d",&sel) == EOF) exit(EINVAL);
  if ((sel!=1) && (sel!=2) && (sel!=0)) {
    cout << "ERROR: wrong selection" << endl;
    exit(-1);
  }


  int dtime = 0;
  std::cout << "> Wait time in microseconds? " << std::endl;
  getline(cin,in);
  if(sscanf(in.c_str(),"%d",&dtime) == EOF) exit(EINVAL);

  unsigned long dt = static_cast<unsigned long>(dtime);

  // setup ltp: internal clock
  //            test trigger from PFN, no busy
  status |= ltp->BC_local_intern();
  status |= ltp->BC_lemoout_from_local();
  status |= ltp->IO_lemoout_from_local(LTP::TR1);
  status |= ltp->IO_local_lemo_pfn(LTP::TR1);
  status |= ltp->IO_pfn_width_2bc(LTP::TR1);
  status |= ltp->IO_disable_pg_gating(LTP::TR1);
  status |= ltp->IO_disable_busy_gating(LTP::TR1);
  status |= ltp->IO_disable_inhibit_gating(LTP::TR1);

  // nanosleep
  struct timespec ts;
  ts.tv_sec = 0;
  ts.tv_nsec = dt;

  // rcc_time_stamp
  u_int ret = ts_open(1, TS_DUMMY);
  if (ret) rcc_error_print(stdout, ret);

  while(true) {
    if (sel==1) {
      nanosleep (&ts, nullptr);
    } else if (sel==2) {
      // wait for dtime
      ret = ts_delay(dt);
      if (ret) rcc_error_print(stdout, ret);
    }
//     // make software pulse
//     ltp->IO_sw_pulse(LTP::TR1);
  }

  //   boost::posix_time::ptime now(boost::posix_time::microsec_clock::universal_time());
  //   std::cout << "NOW = " << now << " = " << boost::posix_time::to_simple_string(now) << std::endl;

  //   now = boost::posix_time::microsec_clock::local_time();
  //   std::cout << "now in local time = " << now << std::endl;

  // check: date -u +'%s %N'

  // Try online wide library

  //   OWLTime t0(OWLTime::Microseconds);
  //   std::cout << "T0 = " << t0 << std::endl;
  //   std::cout << "t0 = " << t0.c_time() << " sec, " << t0.mksec()*1000 << "ns" << std::endl;
  //   std::cout << "t0 = " << t0.total_mksec_utc() << " total_mksec_utc()" << std::endl;
  //   std::cout << "t0 = " << t0.mksec() << " total_mksec" << std::endl;
  //   std::cout << "t0 = " << (t0.total_mksec_utc() - t0.mksec())/1000000 << " sec" << std::endl;
  //   unsigned int t0_sec = t0.c_time();
  //   printf("t0 = %d\n", t0_sec);

  //   OWLTime ConvTime(t0_sec);
  //   std::cout << "Converted T0 = " << ConvTime << std::endl;

  //   OWLTime t1(OWLTime::Microseconds);
  //   std::cout << "T1 = " << t1 << std::endl;
  //   std::cout << "t1 = " << t1.c_time() << " sec, " << t1.mksec()*1000 << "ns" << std::endl;

  //   long long diff = t1.total_mksec_utc() - t0.total_mksec_utc();

  //   VME* m_vme;
  //   VMEMasterMap* m_vmm;
  //   u_int	VME_SIZE = 0x0100;
  //   // open VME library
  //   m_vme = VME::Open();
  //   // create VME master mapping
  //   u_int base = 0xff1000;
  //   m_vmm = m_vme->MasterMap(base,VME_SIZE,VME_A24,0);

  //   u_short add, data;

  // // INIT write CSR-Bgo<n> = x008B
  // // LOOP on write CSR-Bgo<n> = x8000

  //   add = 0x00B0;
  //   data = 0x0003;
  //   m_vmm->WriteSafe(add, data);

  //   add = 0x00A2;
  //   data = 0x008B;
  //   m_vmm->WriteSafe(add, data);

  //   data = 0x8000;
  //   while (true) {
  //     int N;
  //     std::cout << "Generate how many BGo-1? (<0=endless) ";
  //     std::cin >> N;
  //     std::cout << std::endl;
  //     std::cout << "Generating BGo-1 " << N << " times" << std::endl;
  //     if (N>0) {
  //       for (int i = 0; i < N; ++i) {
  // 	m_vmm->WriteSafe(add, data);
  //       }
  //     }
  //     else {
  //       while(true) {
  // 	m_vmm->WriteSafe(add, data);
  //       }
  //     }
  //   }
  return 0;
}
