///*****************************************************************************
// file: RCDLtp.cc
// desc: library for LTP Module
///*****************************************************************************

#include <iostream>
#include <stdlib.h>
#include <cstdlib>
#include <cstring>
#include <errno.h>
#include <iomanip>
#include <utility>
#include <list>
#include <vector>
#include <fstream>
#include <string>

#include "RCDLtp/RCDLtp.h"

#include "rcc_time_stamp/tstamp.h"
#include "rcc_error/rcc_error.h"

using namespace RCD;

bool LTP::CheckCERNID() {
  u_int man, rev, id;
  this->ReadConfigurationEEPROM(man, rev, id);
  return ( 0x00080030 == man)? true : false;
}

// performs basic test to increase the likeliness that the vme
// module is actually an LTP module
bool LTP::CheckLTP() {
  bool ret = true;

  // first check CERN ID
  if (!(this->CheckCERNID())) {
    return false;
  }

  int status = 0;
  {  // check VME interrupter CSR: bit 6-7 are R, bit 0-5 R/W

    // first read word
    u_short word;
    status |= m_vmm->ReadSafe(0x84, &word);

    // flip all bits
    u_short flipped = 0xff & (!word);
    status |= m_vmm->WriteSafe(0x84, flipped);

    // read back new word
    u_short back;
    status |= m_vmm->ReadSafe(0x84, &back);

    // check that the R/W bits have changed and the others haven't
    if ( (0x3f & back) != (0x3f & flipped) ) {
      return false;
    }
    if ( (0xc0 & back) != (0xc0 & word) ) {
      return false;
    }

    // restore first word
    status |= m_vmm->WriteSafe(0x84, word);
  }

//   THE FOLLOWING CHECK SCREWS UP THE LTP: IT RESETS THE COUNTER FOR INSTANCE
//   {  // check VME interrupter vector register:  bits 2-7 are R/W, bits 0-1 are R

//     // first read word
//     u_short word;
//     status |= m_vmm->ReadSafe(0x86, &word);

//     // flip all bits
//     u_short flipped = 0xff & (!word);
//     status |= m_vmm->WriteSafe(0x86, flipped);

//     // read back new word
//     u_short back;
//     status |= m_vmm->ReadSafe(0x86, &back);

//     // check that the R/W bits have changed and the others haven't
//     if ( (0xfc & back) != (0xfc & flipped) ) {
//       return false;
//     }
//     if ( (0x03 & back) != (0x03 & word) ) {
//       return false;
//     }
//
//     // restore first word
//     status |= m_vmm->WriteSafe(0x86, word);
//   }

  { // check Id, typical 0x20050029
    // lower 16 bits should be a number between 0 and 0x99
    // upper 16 bits should be between 2004 and 2006

    u_int board_man = 0;
    u_int board_revision = 0;
    u_int board_id = 0;
    status |= this->ReadConfigurationEEPROM(board_man, board_revision, board_id);

    // check lower 16 bits
    u_int board_number = (board_id & 0xffff);
    if (board_number > 0xff) {
      return false;
    }
    // check upper 16 bits
    u_int board_year = (board_id & 0xffff0000) >> 16;
    if ( (board_year < 0x2004) || (board_year > 0x3020) ) {
      return false;
    }
  }
  return ret;
}


int LTP::ReadConfigurationEEPROM(u_int& board_man, u_int& board_revision, u_int& board_id) {

  int status = 0;
  u_short rev1, rev2, rev3, rev4;
  u_short id1, id2, id3, id4;
  u_short man1, man2, man3;

  status |= m_vmm->ReadSafe(0x4e,&rev1);
  status |= m_vmm->ReadSafe(0x4a,&rev2);
  status |= m_vmm->ReadSafe(0x46,&rev3);
  status |= m_vmm->ReadSafe(0x42,&rev4);

  status |= m_vmm->ReadSafe(0x3e,&id1);
  status |= m_vmm->ReadSafe(0x3a,&id2);
  status |= m_vmm->ReadSafe(0x36,&id3);
  status |= m_vmm->ReadSafe(0x32,&id4);

  status |= m_vmm->ReadSafe(0x2e,&man1);
  status |= m_vmm->ReadSafe(0x2a,&man2);
  status |= m_vmm->ReadSafe(0x26,&man3);

  u_int urev1 = static_cast<u_int>(rev1 & 0xff);
  u_int urev2 = (static_cast<u_int>(rev2 & 0xff) << 8) & 0xff00;
  u_int urev3 = (static_cast<u_int>(rev3 & 0xff) << 16) & 0xff0000;
  u_int urev4 = (static_cast<u_int>(rev4 & 0xff) << 24) & 0xff000000;
  board_revision = urev4 | urev3 | urev2 | urev1;

  u_int uid1 = static_cast<u_int>(id1 & 0xff);
  u_int uid2 = (static_cast<u_int>(id2 & 0xff) << 8) & 0xff00;
  u_int uid3 = (static_cast<u_int>(id3 & 0xff) << 16) & 0xff0000;
  u_int uid4 = (static_cast<u_int>(id4 & 0xff) << 24) & 0xff000000;
  board_id = uid4 | uid3 | uid2 | uid1;

  
  u_int uman1 = static_cast<u_int>(man1 & 0xff);
  u_int uman2 = (static_cast<u_int>(man2 & 0xff) << 8) & 0xff00;
  u_int uman3 = (static_cast<u_int>(man3 & 0xff) << 16) & 0xff0000;
  board_man = uman3 | uman2 | uman1;

//   printf(" Manufacturer: 0x%08x \n", board_man);
//   printf(" Revision    : 0x%08x \n", board_revision);
//   printf(" Id          : 0x%08x \n", board_id);

  return status;
}
 

//------------------------------------------------------------------------------
std::string LTP::GetError(int errcode) {

  char message1[100];
  sprintf(message1,"Error %d in package RCDLtp => ",errcode);
  std::string errstring = message1;
  if (errcode == ERROR_INVALID_INTERRUPT_VECTOR ) {
    errstring += "Invalid interrupt vector.";
  }
  else if (errcode == ERROR_INVALID_IO_LOCAL_SEL     ) {
    errstring += "Invalid i/o local selection.";
  }
  else if (errcode == ERROR_INVALID_ORB_SEL          ) {
    errstring += "Invalid orbit selection.";
  }
  else if (errcode == ERROR_INVALID_COUNTER_SEL      ) {
    errstring += "Invalid counter selection.";
  }
  else if (errcode == ERROR_INVALID_TTYPE_REG        ) {
    errstring += "Invalid trigger type register address.";
  }
  else if (errcode == ERROR_INVALID_TTYPE_FIFODELAY  ) {
    errstring += "Invalid trigger type fifo delay.";
  }
  else if (errcode == ERROR_INVALID_TTYPE_FIFOSRC    ) {
    errstring += "Invalid trigger type fifo source.";
  }
  else if (errcode == ERROR_INVALID_CALREQ_SRC       ) {
    errstring += "Invalid calibration request source.";
  }
  else if (errcode == ERROR_INVALID_CALREQ_PRESET    ) {
    errstring += "Invalid calibration request preset value.";
  }
  else if (errcode == ERROR_CALREQ_LINKSTATUS        ) {
    errstring += "Invalid calibration request link status.";
  }
  else if (errcode == ERROR_INVALID_BUSY_LEMOOUT_SRC ) {
    errstring += "Invalid busy lemo-out source.";
  }
  else if (errcode == ERROR_INVALID_BUSY_DEAD_SRC    ) {
    errstring += "Invalid busy deadtime source.";
  }
  else if (errcode == ERROR_INVALID_PG_RUNMODE       ) {
    errstring += "Invalid pattern generation run mode.";
  }
  else if (errcode == ERROR_TOO_LONG_PATTERN       ) {
    errstring += "Too long pattern (>1M).";
  }
  else {
    m_vme->ErrorString(errcode, &errstring);
  }

  return errstring;
}

//------------------------------------------------------------------------------
int LTP::Reset() {
  u_short data = 1;
  u_short reg = REG_SWRST;

  // write register
  return m_vmm->WriteSafe(reg,data);
}


//------------------------------------------------------------------------------
int LTP::pow(int a, int k) {
  int ret = 1;
  for (int i = 0; i < k; ++i) {
    ret *= a;
  }
  return(ret);
}

//------------------------------------------------------------------------------
int LTP::bintoint(char* a) {
  int r=0;
  for (int k = 0; k<4; ++k) {
    if (a[k]=='1') r+=this->pow(2u,3u-k);
    else if (a[k]=='0') ;
    else r = -1;
  }
  return r;
}

//------------------------------------------------------------------------------
int LTP::bin4toint(char* a1,char* a2,char* a3,char* a4) {
  return 0x1000*bintoint(a1)
    + 0x0100*bintoint(a2)
    + 0x0010*bintoint(a3)
    + 0x0001*bintoint(a4);
}


//------------------------------------------------------------------------------
int LTP::Open(u_int base) {

  // open VME library
  m_vme = VME::Open();

  // create VME master mapping
  m_vmm = m_vme->MasterMap(base,VME_SIZE,VME_A24,0);
  return (*m_vmm)();
}

//------------------------------------------------------------------------------
int LTP::Close(void) {

  // unmap VME master mapping
  int status = m_vme->MasterUnmap(m_vmm);

  // close VME library
  VME::Close();

  return status;
}

//------------------------------------------------------------------------------
int LTP::setBits(u_int reg, u_short mask, u_short bits) {
  u_short data = 0;
  int status = 0;
  // read register
  if ( (status = m_vmm->ReadSafe(reg,&data)) != VME_SUCCESS) {
    return status;
  }
     
  // set bits
  data = (data & ~mask) | ( bits & mask);

  // write register
  return m_vmm->WriteSafe(reg,data);
}

//------------------------------------------------------------------------------
int LTP::testBits(u_int reg, u_short mask, bool* result) {
  u_short data;
  int status = 0;
  // read register
  if ( (status = m_vmm->ReadSafe(reg,&data)) != VME_SUCCESS) {
    return status;
  }

  // apply mask
  data = (data & mask);
  if (data>0) *result=true;
  else *result= false;

  return(status);
}

//------------------------------------------------------------------------------
int LTP::getBitsAndPrint(u_int reg, u_short mask, u_short* data) {

  u_short d;
  int status = 0;
  // read register
  if ( (status = m_vmm->ReadSafe(reg,&d)) != VME_SUCCESS) {
    return status;
  }
     
  //  apply read mask
  *data = (d & mask);
  std::cout <<"Read value " 
	    << std::hex << std::setfill('0') << std::setw(4) << *data 
	    << " = " << printBinary16(*data) 
	    << " from register " 
	    << std::hex << std::setfill('0') << std::setw(4) << reg 
	    << std::dec << std::endl;

  return status;
}

//------------------------------------------------------------------------------
int LTP::getBits(u_int reg, u_short mask, u_short* data) {

  u_short d;
  int status = 0;
  // read register
  status = m_vmm->ReadSafe(reg,&d);
     
  //  apply read mask
  *data = (d & mask);
  return status;
}


//------------------------------------------------------------------------------
string LTP::printBinary16(u_short number) {
  int n = number;
  string s;
  for (int i = 15; i >= 0; --i) {
    if ( ((i+1)%4) == 0 ) 
      s = s + " ";
    n = n >> i;
    int bit = n & 0x1;
    if (bit==0) s=s+"0";
    if (bit==1) s=s+"1";
    n = number;
  }

  return s;
}


//------------------------------------------------------------------------------
// return the CSR address for a given channel 
u_short LTP::CSR(enum channel c) {
  u_short ret = 0;
  switch(c) {
  case L1A:
    ret = REG_L1A_CSR;
    break;
  case TR1:
    ret = REG_TR1_CSR;
    break;
  case TR2:
    ret = REG_TR2_CSR;
    break;
  case TR3:
    ret = REG_TR3_CSR;
    break;
  case BG0:
    ret = REG_BG0_CSR;
    break;
  case BG1:
    ret = REG_BG1_CSR;
    break;
  case BG2:
    ret = REG_BG2_CSR;
    break;
  case BG3:
    ret = REG_BG3_CSR;
    break;
  }
  return ret;
}

// -----------------------------
// VME Interrupts
// -----------------------------

int LTP::VMEInt_write_request_level(u_short w) { return setBits(REG_INTCSR, MSK_VMEINT_LVL  , w            );}
int LTP::VMEInt_enable                      () { return setBits(REG_INTCSR, MSK_VMEINT_EN   , VMEINT_EN    );}
int LTP::VMEInt_disable                     () { return setBits(REG_INTCSR, MSK_VMEINT_EN   , VMEINT_DIS   );}
int LTP::VMEInt_enable_source_0             () { return setBits(REG_INTCSR, MSK_VMEINT_S0_EN, VMEINT_S0_EN );}
int LTP::VMEInt_disable_source_0            () { return setBits(REG_INTCSR, MSK_VMEINT_S0_EN, VMEINT_S0_DIS);}
int LTP::VMEInt_enable_source_1             () { return setBits(REG_INTCSR, MSK_VMEINT_S1_EN, VMEINT_S1_EN );}
int LTP::VMEInt_disable_source_1            () { return setBits(REG_INTCSR, MSK_VMEINT_S1_EN, VMEINT_S1_DIS);}

int LTP::VMEInt_set_vector(int w) { 
  if (w<=0 && w>7) {
    return(-1);
  }
  return setBits(REG_SWIRQ, MSK_VMEINT_VEC, w*0x0004   );
}

int LTP::VMEInt_read_request_level(u_short* w) { return getBits(REG_INTCSR , MSK_VMEINT_LVL, w); }

bool LTP::VMEInt_is_enabled() {
  bool r = false;
  testBits(REG_INTCSR , VMEINT_EN, &r);
  return r;
}

bool LTP::VMEInt_source_0_is_enabled() {
  bool r = false;
  testBits(REG_INTCSR , VMEINT_S0_EN, &r);
  return r;
}

bool LTP::VMEInt_source_1_is_enabled() {
  bool r = false;
  testBits(REG_INTCSR , VMEINT_S1_EN, &r);
  return r;
}

bool LTP::VMEInt_request_is_pending() {
  bool r = false;
  testBits(REG_INTCSR , VMEINT_PEND, &r);
  return r;
}

bool LTP::VMEInt_request_is_asserted() {
  bool r = false;
  testBits(REG_INTCSR , VMEINT_ASSERT, &r);
  return r;
}

bool LTP::VMEInt_source_0_is_active() {
  bool r = false;
  testBits(REG_SWIRQ, VMEINT_S0_ACT, &r);
  return r;
}

bool LTP::VMEInt_source_1_is_active() {
  bool r = false;
  testBits(REG_SWIRQ, VMEINT_S1_ACT, &r);
  return r;
}

int LTP::VMEInt_read_vector(u_short* w)  {
  u_short sel;
  int status = getBits(REG_SWIRQ, MSK_VMEINT_VEC, &sel);
  if (status != VME_SUCCESS) {
    return status;
  }
  sel = sel>>2;
  if ( sel<9 ) *w = sel;
  else {
    return ERROR_INVALID_INTERRUPT_VECTOR;
  }
  return status;
}

// -----------------------------
// I/O - L1A, Test Triggers, BGo
// -----------------------------

int LTP::IO_write(channel c, u_short w) { return setBits(CSR(c), MSK_SIG_W    , w); }
int LTP::IO_read(channel c, u_short* w) { return getBits(CSR(c), MSK_SIG_L1A_R, w); } 

// Set methods
int LTP::IO_linkout_from_linkin   (channel c) { 
  return setBits(this->CSR(c), MSK_SIG_LINKOUT , SIG_LINKOUT_LINK   );
}
int LTP::IO_linkout_from_local    (channel c) {
  return setBits(this->CSR(c), MSK_SIG_LINKOUT , SIG_LINKOUT_LOCAL  );
}
int LTP::IO_lemoout_from_linkin    (channel c) {
  return setBits(this->CSR(c), MSK_SIG_LEMOOUT  , SIG_LEMOOUT_LINK   );
}
int LTP::IO_lemoout_from_local     (channel c) {
  return setBits(this->CSR(c), MSK_SIG_LEMOOUT  , SIG_LEMOOUT_LOCAL  );
}
int LTP::IO_local_no_source       (channel c) {
  return setBits(this->CSR(c), MSK_SIG_LOCALSRC, SIG_LOCALSRC_NO    );
}
int LTP::IO_local_lemo_direct      (channel c) {
  return setBits(this->CSR(c), MSK_SIG_LOCALSRC, SIG_LOCALSRC_FP    );
}
int LTP::IO_local_lemo_pfn         (channel c) {
  return setBits(this->CSR(c), MSK_SIG_LOCALSRC, SIG_LOCALSRC_PFN   );
}
int LTP::IO_local_pg              (channel c) {
  return setBits(this->CSR(c), MSK_SIG_LOCALSRC, SIG_LOCALSRC_PG    );
}
int LTP::IO_enable_busy_gating    (channel c) {
  return setBits(this->CSR(c), MSK_SIG_BUSY    , SIG_GATING_BUSY    );
}
int LTP::IO_enable_inhibit_gating (channel c) {
  return setBits(this->CSR(c), MSK_SIG_INHIBIT , SIG_GATING_INHIBIT );
}
int LTP::IO_enable_pg_gating      (channel c) {
  return setBits(this->CSR(c), MSK_SIG_PG      , SIG_GATING_PG      );
}
int LTP::IO_disable_busy_gating   (channel c) {
  return setBits(this->CSR(c), MSK_SIG_BUSY    , SIG_GATING_NO      );
}
int LTP::IO_disable_inhibit_gating(channel c) {
  return setBits(this->CSR(c), MSK_SIG_INHIBIT , SIG_GATING_NO      );
}
int LTP::IO_disable_pg_gating     (channel c) {
  return setBits(this->CSR(c), MSK_SIG_PG      , SIG_GATING_NO      );
}
int LTP::IO_pfn_width_1bc         (channel c) {
  return setBits(this->CSR(c), MSK_SIG_PFN     , SIG_PFN_1BC        );
}
int LTP::IO_pfn_width_2bc         (channel c) {
  return setBits(this->CSR(c), MSK_SIG_PFN     , SIG_PFN_2BC        );
}

int LTP::IO_sw_pulse(channel c) {
  return setBits(this->CSR(c), MSK_SIG_PULSE   , SIG_PULSE);
}


// Get Methods
bool LTP::L1A_link_is_active() {
  bool r = false;
  testBits(this->CSR(LTP::L1A), MSK_SIG_L1A_LINK_STS, &r);
  return r;
}

bool LTP::IO_linkin_is_active(channel c) {
  bool r = false;
  testBits(this->CSR(c), MSK_SIG_NOTL1A_LINKIN_STS  , &r);
  return r;
}

bool LTP::IO_linkout_is_active(channel c) {
  bool r = false;
  testBits(this->CSR(c), MSK_SIG_NOTL1A_LINKOUT_STS, &r);
  return r;
}

bool LTP::IO_local_is_active(channel c) {
  bool r = false;
  testBits(this->CSR(c), MSK_SIG_NOTL1A_LOCAL_STS, &r);
  return r;
}

bool LTP::IO_linkout_is_from_local(channel c) {
  bool r = false;
  testBits(this->CSR(c), MSK_SIG_LINKOUT ,&r);
  return r;
}

bool LTP::IO_linkout_is_from_linkin(channel c) {
  bool r = false;
  testBits(this->CSR(c), MSK_SIG_LINKOUT ,&r);
  return !r;
}

bool LTP::IO_lemoout_is_from_local(channel c) {
  bool r = false;
  testBits(this->CSR(c), MSK_SIG_LEMOOUT  ,&r);
  return r;
}

bool LTP::IO_lemoout_is_from_linkin(channel c) {
  bool r = false;
  testBits(this->CSR(c), MSK_SIG_LEMOOUT  ,&r);
  return !r;
}

int LTP::IO_local_selection(channel c, io_source* source) {
  u_short sel;
  int status = getBits(CSR(c), MSK_SIG_LOCALSRC, &sel);

  if      (sel == SIG_LOCALSRC_NO    ) *source = IO_NO_SOURCE;
  else if (sel == SIG_LOCALSRC_FP    ) *source = IO_FP_LEMO;
  else if (sel == SIG_LOCALSRC_PFN   ) *source = IO_FP_LEMO_PFN;
  else if (sel == SIG_LOCALSRC_PG    ) *source = IO_PATGEN;
  else {
    *source = IO_ERROR;
    status = ERROR_INVALID_IO_LOCAL_SEL;
  }
  return status;
}

bool LTP::IO_busy_gating_is_enabled(channel c) {
  bool r = false;
  testBits(this->CSR(c), MSK_SIG_BUSY, &r);
  return r;
}

bool LTP::IO_inhibit_timer_gating_is_enabled(channel c) {
  bool r = false;
  testBits(this->CSR(c), MSK_SIG_INHIBIT, &r);
  return r;
}

bool LTP::IO_pg_gating_is_enabled(channel c) {
  bool r = false;
  testBits(this->CSR(c), MSK_SIG_PG, &r);
  return r;
}

bool LTP::IO_pfn_width_is_1bc(channel c) {
  bool r = false;
  testBits(this->CSR(c), MSK_SIG_PFN, &r);
  return !r;
}

bool LTP::IO_pfn_width_is_2bc(channel c) {
  bool r = false;
  testBits(this->CSR(c), MSK_SIG_PFN, &r);
  return r;
}
   
// -----------
// Bunch Clock
// -----------
int LTP::BC_write         (u_short w){ return setBits(REG_BC_CSR , MSK_BC_W, w ); } 
int LTP::BC_read         (u_short* w){ return getBits(REG_BC_CSR , MSK_BC_R, w ); } 

// Set Methods
int LTP::BC_linkout_from_linkin    (){ return setBits(REG_BC_CSR , MSK_BC_LINKOUT  , BC_LINKOUT_LINK  ); }
int LTP::BC_linkout_from_local     (){ return setBits(REG_BC_CSR , MSK_BC_LINKOUT  , BC_LINKOUT_LOCAL ); }
int LTP::BC_lemoout_from_linkin    (){ return setBits(REG_BC_CSR , MSK_BC_LEMOOUT   , BC_LEMOOUT_LINK  ); }
int LTP::BC_lemoout_from_local     (){ return setBits(REG_BC_CSR , MSK_BC_LEMOOUT   , BC_LEMOOUT_LOCAL ); }
int LTP::BC_local_intern           (){ return setBits(REG_BC_CSR , MSK_BC_LOCALSRC , BC_LOCAL_INTERN  ); }
int LTP::BC_local_lemo              (){ return setBits(REG_BC_CSR , MSK_BC_LOCALSRC , BC_LOCAL_LEMO     ); }

// Get Methods
bool LTP::BC_linkout_is_from_local() {
  bool r = false;
  testBits(REG_BC_CSR, MSK_BC_LINKOUT,    &r);
  return r;
}

bool LTP::BC_linkout_is_from_linkin() {
  bool r = false;
  testBits(REG_BC_CSR, MSK_BC_LINKOUT,    &r);
  return !r;
}

bool LTP::BC_lemoout_is_from_local() {
  bool r = false;
  testBits(REG_BC_CSR, MSK_BC_LEMOOUT,     &r);
  return r;
}

bool LTP::BC_lemoout_is_from_linkin() {
  bool r = false;
  testBits(REG_BC_CSR, MSK_BC_LEMOOUT,     &r);
  return !r;
}

bool LTP::BC_local_is_from_lemoin() {
  bool r = false;
  testBits(REG_BC_CSR, MSK_BC_LOCALSRC,   &r);
  return r;
}

bool LTP::BC_local_is_from_intern() {
  bool r = false;
  testBits(REG_BC_CSR, MSK_BC_LOCALSRC,   &r);
  return !r;
}

bool LTP::BC_external_is_running() {
  bool r = false;
  testBits(REG_BC_CSR, MSK_BC_STATUS_EXT, &r);
  return r;
}

bool LTP::BC_selected_is_running() {
  bool r = false;
  testBits(REG_BC_CSR, MSK_BC_STATUS_SEL, &r);
  return r;
}

// -----
// Orbit
// -----

int LTP::ORB_write        (u_short w){ return setBits(REG_ORB_CSR, MSK_ORB_W , w ); } 
int LTP::ORB_read        (u_short* w){ return getBits(REG_ORB_CSR, MSK_ORB_R , w ); } 

// Set Methods
int LTP::ORB_linkout_from_linkin(){ return setBits(REG_ORB_CSR, MSK_ORB_LINKOUT , ORB_LINKOUT_LINK    ); }
int LTP::ORB_linkout_from_local (){ return setBits(REG_ORB_CSR, MSK_ORB_LINKOUT , ORB_LINKOUT_LOCAL   ); }
int LTP::ORB_lemoout_from_linkin(){ return setBits(REG_ORB_CSR, MSK_ORB_LEMOOUT  , ORB_LEMOOUT_LINK    ); }
int LTP::ORB_lemoout_from_local (){ return setBits(REG_ORB_CSR, MSK_ORB_LEMOOUT  , ORB_LEMOOUT_LOCAL   ); }
int LTP::ORB_local_no           (){ return setBits(REG_ORB_CSR, MSK_ORB_LOCALSRC, ORB_LOCAL_NO        ); }
int LTP::ORB_local_intern       (){ return setBits(REG_ORB_CSR, MSK_ORB_LOCALSRC, ORB_LOCAL_INTERN    ); }
int LTP::ORB_local_lemo          (){ return setBits(REG_ORB_CSR, MSK_ORB_LOCALSRC, ORB_LOCAL_LEMO       ); }
int LTP::ORB_local_pg           (){ return setBits(REG_ORB_CSR, MSK_ORB_LOCALSRC, ORB_LOCAL_PG        ); }

int LTP::COUNTER_L1A            (){ return setBits(REG_ORB_CSR, MSK_ORB_CNTRSRC , ORB_CNTRSRC_L1A     ); }
int LTP::COUNTER_TR1            (){ return setBits(REG_ORB_CSR, MSK_ORB_CNTRSRC , ORB_CNTRSRC_TR1     ); }
int LTP::COUNTER_TR2            (){ return setBits(REG_ORB_CSR, MSK_ORB_CNTRSRC , ORB_CNTRSRC_TR2     ); }
int LTP::COUNTER_TR3            (){ return setBits(REG_ORB_CSR, MSK_ORB_CNTRSRC , ORB_CNTRSRC_TR3     ); }
int LTP::COUNTER_BG0            (){ return setBits(REG_ORB_CSR, MSK_ORB_CNTRSRC , ORB_CNTRSRC_BG0     ); }
int LTP::COUNTER_BG1            (){ return setBits(REG_ORB_CSR, MSK_ORB_CNTRSRC , ORB_CNTRSRC_BG1     ); }
int LTP::COUNTER_BG2            (){ return setBits(REG_ORB_CSR, MSK_ORB_CNTRSRC , ORB_CNTRSRC_BG2     ); }
int LTP::COUNTER_BG3            (){ return setBits(REG_ORB_CSR, MSK_ORB_CNTRSRC , ORB_CNTRSRC_BG3     ); }
int LTP::COUNTER_orb            (){ return setBits(REG_ORB_CSR, MSK_ORB_CNTRSRC , ORB_CNTRSRC_ORB     ); }
int LTP::COUNTER_orb_ecr        (){ return setBits(REG_ORB_CSR, MSK_ORB_CNTRSRC , ORB_CNTRSRC_ORB_ECR ); }
int LTP::COUNTER_L1A_ecr        (){ return setBits(REG_ORB_CSR, MSK_ORB_CNTRSRC , ORB_CNTRSRC_L1A_ECR ); }
int LTP::COUNTER_TR1_ecr        (){ return setBits(REG_ORB_CSR, MSK_ORB_CNTRSRC , ORB_CNTRSRC_TR1_ECR ); }
int LTP::COUNTER_TR2_ecr        (){ return setBits(REG_ORB_CSR, MSK_ORB_CNTRSRC , ORB_CNTRSRC_TR2_ECR ); }
int LTP::COUNTER_TR3_ecr        (){ return setBits(REG_ORB_CSR, MSK_ORB_CNTRSRC , ORB_CNTRSRC_TR3_ECR ); }
int LTP::COUNTER_none           (){ return setBits(REG_ORB_CSR, MSK_ORB_CNTRSRC , ORB_CNTRSRC_NONE    ); }


// Get Methods
bool LTP::ORB_linkout_is_from_local() {
  bool r = false;
  testBits(REG_ORB_CSR, MSK_ORB_LINKOUT, &r);
  return r;
}

bool LTP::ORB_linkout_is_from_linkin() {
  bool r = false;
  testBits(REG_ORB_CSR, MSK_ORB_LINKOUT, &r);
  return !r;
}

bool LTP::ORB_lemoout_is_from_local() {
  bool r = false;
  testBits(REG_ORB_CSR, MSK_ORB_LEMOOUT, &r);
  return r;
}

bool LTP::ORB_lemoout_is_from_linkin() {
  bool r = false;
  testBits(REG_ORB_CSR, MSK_ORB_LEMOOUT, &r);
  return !r;
}

int LTP::ORB_local_selection(enum orbit_source* source ) {
  u_short sel;
  int status = getBits(REG_ORB_CSR, MSK_ORB_LOCALSRC, &sel);

  if      (sel == ORB_LOCAL_NO    ) *source = ORB_NO_SOURCE    ;
  else if (sel == ORB_LOCAL_INTERN) *source = ORB_INTERNAL     ;
  else if (sel == ORB_LOCAL_LEMO   ) *source = ORB_FP_LEMO_INPUT ;
  else if (sel == ORB_LOCAL_PG    ) *source = ORB_PATGEN       ;
  else {
    *source = ORB_ERROR;
    status = ERROR_INVALID_ORB_SEL;
  }
  return status;
}

int LTP::COUNTER_selection(enum counter_selection* source ) {
  u_short sel;
  int status = getBits(REG_ORB_CSR, MSK_ORB_CNTRSRC, &sel);

  if      (sel == ORB_CNTRSRC_NONE        ) *source = CNT_NO_SOURCE     ;
  else if (sel == ORB_CNTRSRC_ORB         ) *source = CNT_ORBIT         ;
  else if (sel == ORB_CNTRSRC_ORB_ECR     ) *source = CNT_ORBIT_ECR     ;
  else if (sel == ORB_CNTRSRC_BG0         ) *source = CNT_BG0           ;
  else if (sel == ORB_CNTRSRC_BG1         ) *source = CNT_BG1           ;
  else if (sel == ORB_CNTRSRC_BG2         ) *source = CNT_BG2           ;
  else if (sel == ORB_CNTRSRC_BG3         ) *source = CNT_BG3           ;
  else if (sel == ORB_CNTRSRC_L1A         ) *source = CNT_L1A           ;
  else if (sel == ORB_CNTRSRC_TR1         ) *source = CNT_TR1           ;
  else if (sel == ORB_CNTRSRC_TR2         ) *source = CNT_TR2           ;
  else if (sel == ORB_CNTRSRC_TR3         ) *source = CNT_TR3           ;
  else if (sel == ORB_CNTRSRC_L1A_ECR     ) *source = CNT_L1A_ECR       ;
  else if (sel == ORB_CNTRSRC_TR1_ECR     ) *source = CNT_TR1_ECR       ;
  else if (sel == ORB_CNTRSRC_TR2_ECR     ) *source = CNT_TR2_ECR       ;
  else if (sel == ORB_CNTRSRC_TR3_ECR     ) *source = CNT_TR3_ECR       ;
  else {
    *source = CNT_ERROR;
    status = ERROR_INVALID_COUNTER_SEL;
  }
  return status;
}

bool LTP::ORB_selected_is_running() {
  bool r = false;
  testBits(REG_ORB_CSR, MSK_ORB_STATUS_SEL, &r);
  return r;
}

int LTP::INH_writeShadow(u_short w)   { return setBits(REG_INH_TIMER, MSK_INH_W, w); }
int LTP::INH_readShadow(u_short* w)   { return getBits(REG_INH_TIMER, MSK_INH_W, w); }

int LTP::COUNTER_reset             () { return setBits(REG_32BITCNT_RST, 0xffff, 0xffff ); }
int LTP::COUNTER_read_lsw(u_short* w) { return getBits(REG_32BITCNT_LSW, 0xffff, w      ); }
int LTP::COUNTER_read_msw(u_short* w) { return getBits(REG_32BITCNT_MSW, 0xffff, w      ); }

int LTP::COUNTER_read_32(u_int* a) {
  u_short lsw, msw;
  int status = 0;
  if ( (status = this->COUNTER_read_lsw(&lsw)) != VME_SUCCESS) {
    *a = 0;
    return status;
  }
//   if (lsw!=0) {
//     printf("LTP::COUNTER_read_32: lsw=0x%04x\n", lsw);
//     exit(-1);
//   }
  if ( (status = this->COUNTER_read_msw(&msw)) != VME_SUCCESS) {
    *a = 0;
    return status;
  }
//   if (msw!=0) {
//     printf("LTP::COUNTER_read_32: msw=0x%04x\n", msw);
//     exit(-1);
//   }

  *a = (msw*0x10000 + lsw) & 0xffffffff;

  return status;
}

int LTP::COUNTER_read_8_24(u_short* a8, u_int* a24) {
  u_short lsw, msw;
  int status = 0;
  if ( (status = this->COUNTER_read_lsw(&lsw)) != VME_SUCCESS) {
    return status;
  }
  if ( (status = this->COUNTER_read_msw(&msw)) != VME_SUCCESS) {
    return status;
  }
  u_int c = msw*0x10000 + lsw;

  *a24 = ((c|0xff000000) - 0xff000000) & 0xffffff;
  *a8 = (msw>>8) & 0xff;
     
  return status;
}

// ------------
// Trigger Type
// ------------

// Set Methods
int LTP::TTYPE_write_regfile0(u_short w) { return setBits(REG_TTYPE_REGFILE0, 0xff, w); }
int LTP::TTYPE_write_regfile1(u_short w) { return setBits(REG_TTYPE_REGFILE1, 0xff, w); }
int LTP::TTYPE_write_regfile2(u_short w) { return setBits(REG_TTYPE_REGFILE2, 0xff, w); }
int LTP::TTYPE_write_regfile3(u_short w) { return setBits(REG_TTYPE_REGFILE3, 0xff, w); }

int LTP::TTYPE_write      (u_short w) { return setBits(REG_TTYPE_CSR, MSK_TTYPE_W        , w                  ); }
int LTP::TTYPE_linkout_from_linkin() { return setBits(REG_TTYPE_CSR, MSK_TTYPE_LINKOUT   , TTYPE_LINKOUT_LINK ); }
int LTP::TTYPE_linkout_from_local () { return setBits(REG_TTYPE_CSR, MSK_TTYPE_LINKOUT   , TTYPE_LINKOUT_LOCAL); }
int LTP::TTYPE_eclout_from_linkin () { return setBits(REG_TTYPE_CSR, MSK_TTYPE_ECLOUT    , TTYPE_ECLOUT_LINK  ); }
int LTP::TTYPE_eclout_from_local  () { return setBits(REG_TTYPE_CSR, MSK_TTYPE_ECLOUT    , TTYPE_ECLOUT_LOCAL ); }
int LTP::TTYPE_local_from_pg      () { return setBits(REG_TTYPE_CSR, MSK_TTYPE_LOCAL     , TTYPE_LOCAL_PG     ); }
int LTP::TTYPE_local_from_vme     () { return setBits(REG_TTYPE_CSR, MSK_TTYPE_LOCAL     , TTYPE_LOCAL_VME    ); }
int LTP::TTYPE_enable_eclout      () { return setBits(REG_TTYPE_CSR, MSK_TTYPE_ECL_ENABLE, TTYPE_ECL_EN       ); }
int LTP::TTYPE_disable_eclout     () { return setBits(REG_TTYPE_CSR, MSK_TTYPE_ECL_ENABLE, TTYPE_ECL_DIS      ); }
int LTP::TTYPE_regfile_address_0  () { return setBits(REG_TTYPE_CSR, MSK_TTYPE_FILEADDR  , TTYPE_FILEADDR_0   ); }
int LTP::TTYPE_regfile_address_1  () { return setBits(REG_TTYPE_CSR, MSK_TTYPE_FILEADDR  , TTYPE_FILEADDR_1   ); }
int LTP::TTYPE_regfile_address_2  () { return setBits(REG_TTYPE_CSR, MSK_TTYPE_FILEADDR  , TTYPE_FILEADDR_2   ); }
int LTP::TTYPE_regfile_address_3  () { return setBits(REG_TTYPE_CSR, MSK_TTYPE_FILEADDR  , TTYPE_FILEADDR_3   ); }
int LTP::TTYPE_fifodelay_2bc      () { return setBits(REG_TTYPE_CSR, MSK_TTYPE_DELAY     , TTYPE_DELAY_2      ); }
int LTP::TTYPE_fifodelay_3bc      () { return setBits(REG_TTYPE_CSR, MSK_TTYPE_DELAY     , TTYPE_DELAY_3      ); }
int LTP::TTYPE_fifodelay_4bc      () { return setBits(REG_TTYPE_CSR, MSK_TTYPE_DELAY     , TTYPE_DELAY_4      ); }
int LTP::TTYPE_fifodelay_5bc      () { return setBits(REG_TTYPE_CSR, MSK_TTYPE_DELAY     , TTYPE_DELAY_5      ); }
int LTP::TTYPE_fifotrigger_L1A    () { return setBits(REG_TTYPE_CSR, MSK_TTYPE_FIFOSRC   , TTYPE_FIFOSRC_L1A  ); }
int LTP::TTYPE_fifotrigger_TR1    () { return setBits(REG_TTYPE_CSR, MSK_TTYPE_FIFOSRC   , TTYPE_FIFOSRC_TR1  ); }
int LTP::TTYPE_fifotrigger_TR2    () { return setBits(REG_TTYPE_CSR, MSK_TTYPE_FIFOSRC   , TTYPE_FIFOSRC_TR2  ); }
int LTP::TTYPE_fifotrigger_TR3    () { return setBits(REG_TTYPE_CSR, MSK_TTYPE_FIFOSRC   , TTYPE_FIFOSRC_TR3  ); }
int LTP::TTYPE_reset_fifo         () { return setBits(REG_FIFO_RST , 0xffff              , 0xffff             ); }

// Get Methods
int LTP::TTYPE_read_regfile0(u_short* w) { return getBits(REG_TTYPE_REGFILE0, 0xff, w); }
int LTP::TTYPE_read_regfile1(u_short* w) { return getBits(REG_TTYPE_REGFILE1, 0xff, w); }
int LTP::TTYPE_read_regfile2(u_short* w) { return getBits(REG_TTYPE_REGFILE2, 0xff, w); }
int LTP::TTYPE_read_regfile3(u_short* w) { return getBits(REG_TTYPE_REGFILE3, 0xff, w); }

int LTP::TTYPE_read         (u_short* w) { return getBits(REG_TTYPE_CSR, MSK_TTYPE_R , w);}

bool LTP::TTYPE_linkout_is_from_local() {
  bool r = false;
  testBits(REG_TTYPE_CSR, MSK_TTYPE_LINKOUT, &r);
  return r;
}

bool LTP::TTYPE_linkout_is_from_linkin() {
  bool r = false;
  testBits(REG_TTYPE_CSR, MSK_TTYPE_LINKOUT, &r);
  return !r;
}

bool LTP::TTYPE_eclout_is_from_local() {
  bool r = false;
  testBits(REG_TTYPE_CSR, MSK_TTYPE_ECLOUT, &r);
  return r;
}

bool LTP::TTYPE_eclout_is_from_linkin() {
  bool r = false;
  testBits(REG_TTYPE_CSR, MSK_TTYPE_ECLOUT, &r);
  return !r;
}

bool LTP::TTYPE_local_is_controlled_by_vme() {
  bool r = false;
  testBits(REG_TTYPE_CSR, MSK_TTYPE_LOCAL, &r);
  return r;
}

bool LTP::TTYPE_local_is_controlled_by_pg() {
  bool r = false;
  testBits(REG_TTYPE_CSR, MSK_TTYPE_LOCAL, &r);
  return !r;
}

bool LTP::TTYPE_eclout_is_disabled() {
  bool r = false;
  testBits(REG_TTYPE_CSR, MSK_TTYPE_ECL_ENABLE, &r);
  return r;
}

int LTP::TTYPE_regfile_address(enum ttype_file_addr* addr ) {
  u_short sel;
  int status = getBits(REG_TTYPE_CSR, MSK_TTYPE_FILEADDR, &sel);

  if      (sel == TTYPE_FILEADDR_0) *addr = TTYPE_FILE_0;
  else if (sel == TTYPE_FILEADDR_1) *addr = TTYPE_FILE_1;
  else if (sel == TTYPE_FILEADDR_2) *addr = TTYPE_FILE_2;
  else if (sel == TTYPE_FILEADDR_3) *addr = TTYPE_FILE_3;
  else
    {
      *addr = TTYPE_FILE_ERROR;
      status = ERROR_INVALID_TTYPE_REG;
    }
  return status;
}

int LTP::TTYPE_fifodelay(enum ttype_delay* del ) {
  u_short sel;
  int status = getBits(REG_TTYPE_CSR, MSK_TTYPE_DELAY, &sel);

  if      (sel == TTYPE_DELAY_2) *del = TTYPE_DELAY_2BC;
  else if (sel == TTYPE_DELAY_3) *del = TTYPE_DELAY_3BC;
  else if (sel == TTYPE_DELAY_4) *del = TTYPE_DELAY_4BC;
  else if (sel == TTYPE_DELAY_5) *del = TTYPE_DELAY_5BC;
  else
    {
      *del = TTYPE_DELAY_ERROR;
      status = ERROR_INVALID_TTYPE_FIFODELAY;
    }
  return status;
}

int LTP::TTYPE_fifotrigger(enum ttype_fifotrigger* src ) {
  u_short sel;
  int status = getBits(REG_TTYPE_CSR, MSK_TTYPE_FIFOSRC, &sel);

  if      (sel == TTYPE_FIFOSRC_L1A) *src = TTYPE_FIFO_L1A;
  else if (sel == TTYPE_FIFOSRC_TR1) *src = TTYPE_FIFO_TR1;
  else if (sel == TTYPE_FIFOSRC_TR2) *src = TTYPE_FIFO_TR2;
  else if (sel == TTYPE_FIFOSRC_TR3) *src = TTYPE_FIFO_TR3;
  else
    {
      *src = TTYPE_FIFO_ERROR;
      status = ERROR_INVALID_TTYPE_FIFOSRC;
    }
  return status;
}

bool LTP::TTYPE_fifo_is_full() {
  bool r = false;
  testBits(REG_TTYPE_CSR, MSK_TTYPE_FIFO_FULL, &r);
  return r;
}

bool LTP::TTYPE_fifo_is_empty() {
  bool r = false;
  testBits(REG_TTYPE_CSR, MSK_TTYPE_FIFO_EMPTY, &r);
  return r;
}

// only write! Do no read:
int LTP::TTYPE_write_fifo() { 
  u_short data = 0x00ff; 
  u_int reg = REG_FIFO_IO;
  return m_vmm->WriteSafe(reg,data);
}

int LTP::TTYPE_read_fifo (u_short* w) { return getBits(REG_FIFO_IO, 0x00ff, w); }

// ------------
// Turn Counter
// ------------

int LTP::TURN_read_selector_code(u_short* w){ return getBits(REG_TURN_CR, MSK_TURN_SELCODE, w         ); }
int LTP::TURN_write_selector_code(u_short w){ return setBits(REG_TURN_CR, MSK_TURN_SELCODE, w         ); }
int LTP::TURN_orbit_lemoout_from_orbit     (){ return setBits(REG_TURN_CR, MSK_TURN_SEL    , TURN_ORBIT); }
int LTP::TURN_orbit_lemoout_from_turn      (){ return setBits(REG_TURN_CR, MSK_TURN_SEL    , TURN_SEL  ); }

bool LTP::TURN_orbit_lemoout_is_from_orbit() {
  bool r = false;
  testBits(REG_TURN_CR, MSK_TURN_SEL, &r);
  return !r;
}

bool LTP::TURN_orbit_lemoout_is_from_turn() {
  bool r = false;
  testBits(REG_TURN_CR, MSK_TURN_SEL, &r);
  return r;
}


// -------------------
// Calibration Request
// -------------------

// Set Methods
int LTP::CAL_write   (u_short w) { return setBits(REG_CAL_CSR, MSK_CAL_W       , w               ); }
int LTP::CAL_source_link      () { return setBits(REG_CAL_CSR, MSK_CAL_SRC     , CAL_SRC_LINK    ); }
int LTP::CAL_source_preset    () { return setBits(REG_CAL_CSR, MSK_CAL_SRC     , CAL_SRC_PRESET  ); }
int LTP::CAL_source_fp        () { return setBits(REG_CAL_CSR, MSK_CAL_SRC     , CAL_SRC_FP      ); }
int LTP::CAL_source_pg        () { return setBits(REG_CAL_CSR, MSK_CAL_SRC     , CAL_SRC_PG      ); }
int LTP::CAL_enable_testpath  () { return setBits(REG_CAL_CSR, MSK_CAL_TESTPATH, CAL_TESTPATH_EN ); }
int LTP::CAL_disable_testpath () { return setBits(REG_CAL_CSR, MSK_CAL_TESTPATH, CAL_TESTPATH_DIS); }

int LTP::CAL_set_preset_value  (int w) { return setBits(REG_CAL_CSR, MSK_CAL_PRESET  , w*0x0010  ); }

// Get Methods
int LTP::CAL_read   (u_short* w) { return getBits(REG_CAL_CSR, MSK_CAL_R, w ); }

bool LTP::CAL_testpath_is_enabled() {
  bool r = false;
  testBits(REG_CAL_CSR, MSK_CAL_TESTPATH, &r);
  return r;
}

int LTP::CAL_source(enum cal_source* src ) {
  u_short sel;
  int status = getBits(REG_CAL_CSR, MSK_CAL_SRC, &sel);

  if      (sel == CAL_SRC_LINK)   *src = CAL_LINK;
  else if (sel == CAL_SRC_PRESET) *src = CAL_PRESET;
  else if (sel == CAL_SRC_FP)     *src = CAL_FP;
  else if (sel == CAL_SRC_PG)     *src = CAL_PG;
  else
    {
      *src = CAL_ERROR;
      status = ERROR_INVALID_CALREQ_SRC;
    }
  return status;
}

int LTP::CAL_get_preset_value(int* w) {
  u_short sel;
  int status = getBits(REG_CAL_CSR, MSK_CAL_PRESET, &sel);

  sel = sel>>4;
  if ( sel<9 ) *w = sel;
  else {
    status = ERROR_INVALID_CALREQ_PRESET;
  }
  return status;
}

int LTP::CAL_get_linkstatus(int* w) {
  u_short sel;
  int status = getBits(REG_CAL_CSR, MSK_CAL_LINKSTATUS, &sel);

  sel = sel>>8;
  if ( sel<9 )  *w = sel;
  else {
    status = ERROR_CALREQ_LINKSTATUS;
  }
  return status;
}

//-----
// BUSY
// ----

// Set Methods
int LTP::BUSY_write          (u_short w){return setBits(REG_BUSY_CR, MSK_BUSYCR_W      , w                      );}
int LTP::BUSY_linkout_from_linkin     (){return setBits(REG_BUSY_CR, MSK_BUSYCR_LINKOUT, BUSY_LINKOUT_LINK      );}
int LTP::BUSY_linkout_from_local      (){return setBits(REG_BUSY_CR, MSK_BUSYCR_LINKOUT, BUSY_LINKOUT_LOCAL     );}
int LTP::BUSY_local_with_linkin       (){return setBits(REG_BUSY_CR, MSK_BUSYCR_SUMLINK, BUSY_SUMLINK_EN        );}
int LTP::BUSY_local_without_linkin    (){return setBits(REG_BUSY_CR, MSK_BUSYCR_SUMLINK, BUSY_SUMLINK_DIS       );}
int LTP::BUSY_local_with_ttlin        (){return setBits(REG_BUSY_CR, MSK_BUSYCR_SUMTTL , BUSY_SUMTTL_EN         );}
int LTP::BUSY_local_without_ttlin     (){return setBits(REG_BUSY_CR, MSK_BUSYCR_SUMTTL , BUSY_SUMTTL_DIS        );}
int LTP::BUSY_local_with_nimin        (){return setBits(REG_BUSY_CR, MSK_BUSYCR_SUMNIM , BUSY_SUMNIM_EN         );}
int LTP::BUSY_local_without_nimin     (){return setBits(REG_BUSY_CR, MSK_BUSYCR_SUMNIM , BUSY_SUMNIM_DIS        );}
int LTP::BUSY_enable_constant_level   (){return setBits(REG_BUSY_CR, MSK_BUSYCR_CONST  , BUSY_CONST_EN          );}
int LTP::BUSY_disable_constant_level  (){return setBits(REG_BUSY_CR, MSK_BUSYCR_CONST  , BUSY_CONST_DIS         );}
int LTP::BUSY_local_with_deadtime     (){return setBits(REG_BUSY_CR, MSK_BUSYCR_SUMDEAD, BUSY_SUMDEAD_EN        );}
int LTP::BUSY_local_without_deadtime  (){return setBits(REG_BUSY_CR, MSK_BUSYCR_SUMDEAD, BUSY_SUMDEAD_DIS       );}
int LTP::BUSY_lemoout_busy_only       (){return setBits(REG_BUSY_CR, MSK_BUSYCR_LEMOOUT , BUSY_LEMOOUT_BUSYONLY  );}
int LTP::BUSY_lemoout_busy_or_deadtime(){return setBits(REG_BUSY_CR, MSK_BUSYCR_LEMOOUT , BUSY_LEMOOUT_BUSYORDEAD);}
int LTP::BUSY_lemoout_inhibit         (){return setBits(REG_BUSY_CR, MSK_BUSYCR_LEMOOUT , BUSY_LEMOOUT_INH       );}
int LTP::BUSY_lemoout_pg_inhibit      (){return setBits(REG_BUSY_CR, MSK_BUSYCR_LEMOOUT , BUSY_LEMOOUT_PGINH     );}
int LTP::BUSY_deadtime_source_L1A     (){return setBits(REG_BUSY_CR, MSK_BUSYCR_DEADSRC, BUSY_DEADSRC_L1A       );}
int LTP::BUSY_deadtime_source_TR1     (){return setBits(REG_BUSY_CR, MSK_BUSYCR_DEADSRC, BUSY_DEADSRC_TR1       );}
int LTP::BUSY_deadtime_source_TR2     (){return setBits(REG_BUSY_CR, MSK_BUSYCR_DEADSRC, BUSY_DEADSRC_TR2       );}
int LTP::BUSY_deadtime_source_TR3     (){return setBits(REG_BUSY_CR, MSK_BUSYCR_DEADSRC, BUSY_DEADSRC_TR3       );}
int LTP::BUSY_deadtime_duration  (int n){return setBits(REG_BUSY_CR, MSK_BUSYCR_DEADDUR, n<<BUSY_DEADDUR_POS    );}
int LTP::BUSY_enable_from_pg          (){return setBits(REG_BUSY_CR, MSK_BUSYCR_PG     , BUSY_PG_EN             );}
int LTP::BUSY_disable_from_pg         (){return setBits(REG_BUSY_CR, MSK_BUSYCR_PG     , BUSY_PG_DIS            );}

// Get Methods
int LTP::BUSY_read         (u_short* w) { return getBits(REG_BUSY_CR, MSK_BUSYCR_R, w ); }
int LTP::BUSY_read_status  (u_short* w) { return getBits(REG_BUSY_SR, MSK_BUSYSR_R, w ); }

bool LTP::BUSY_linkout_is_from_local() {
  bool r = false;
  testBits(REG_BUSY_CR, MSK_BUSYCR_LINKOUT, &r);
  return r;
}

bool LTP::BUSY_linkout_is_from_linkin() {
  bool r = false;
  testBits(REG_BUSY_CR, MSK_BUSYCR_LINKOUT, &r);
  return !r;
}

bool LTP::BUSY_linkin_is_summed_with_local() {
  bool r = false;
  testBits(REG_BUSY_CR, MSK_BUSYCR_SUMLINK, &r);
  return r;
}

bool LTP::BUSY_ttlin_is_summed_with_local() {
  bool r = false;
  testBits(REG_BUSY_CR, MSK_BUSYCR_SUMTTL , &r);
  return r;
}

bool LTP::BUSY_nimin_is_summed_with_local() {
  bool r = false;
  testBits(REG_BUSY_CR, MSK_BUSYCR_SUMNIM , &r);
  return r;
}

bool LTP::BUSY_deadtime_is_summed_with_local() {
  bool r = false;
  testBits(REG_BUSY_CR, MSK_BUSYCR_SUMDEAD, &r);
  return r;
}

bool LTP::BUSY_constant_level_is_asserted() {
  bool r = false;
  testBits(REG_BUSY_CR, MSK_BUSYCR_CONST  , &r);
  return r;
}

bool LTP::BUSY_from_pg_is_enabled() {
  bool r = false;
  testBits(REG_BUSY_CR, MSK_BUSYCR_PG     , &r);
  return r;
}

int LTP::BUSY_lemoout_source(enum busy_lemo_output* source ) {
  u_short sel;
  int status = getBits(REG_BUSY_CR, MSK_BUSYCR_LEMOOUT, &sel);

  if      (sel == BUSY_LEMOOUT_BUSYONLY   ) *source = BUSY_ONLY;
  else if (sel == BUSY_LEMOOUT_BUSYORDEAD ) *source = BUSY_OR_DEAD;
  else if (sel == BUSY_LEMOOUT_INH        ) *source = BUSY_TIMER_INH;
  else if (sel == BUSY_LEMOOUT_PGINH      ) *source = BUSY_PG_INH;
  else {
    *source = BUSY_ERROR;
    status = ERROR_INVALID_BUSY_LEMOOUT_SRC;
  }
  return status;
}

int LTP::BUSY_deadtime_source(enum busy_dead_source* source ) {
  u_short sel;
  int status = getBits(REG_BUSY_CR, MSK_BUSYCR_DEADSRC, &sel);

  if      (sel == BUSY_DEADSRC_L1A ) *source = DEAD_L1A ; 
  else if (sel == BUSY_DEADSRC_TR1 ) *source = DEAD_TR1 ;
  else if (sel == BUSY_DEADSRC_TR2 ) *source = DEAD_TR2 ;
  else if (sel == BUSY_DEADSRC_TR3 ) *source = DEAD_TR3 ;
  else {
    *source = DEAD_ERROR;
    status = ERROR_INVALID_BUSY_DEAD_SRC;
  }
  return status;
}

int LTP::BUSY_get_deadtime_duration(int* dur ) {
  u_short sel;
  int status = getBits(REG_BUSY_CR, MSK_BUSYCR_DEADDUR, &sel);
  if (status != VME_SUCCESS) {
    return status;
  }
  *dur = sel >> BUSY_DEADDUR_POS;
  return status;
}

bool LTP::BUSY_linkin_is_busy() {    
  bool r = false;
  testBits(REG_BUSY_SR,MSK_BUSYSR_CTPIN, &r);
  return r;
}

bool LTP::BUSY_linkout_is_busy() {
  bool r = false;
  testBits(REG_BUSY_SR,MSK_BUSYSR_CTPOUT, &r);
  return r;
}

bool LTP::BUSY_ttlin_is_busy() {
  bool r = false;
  testBits(REG_BUSY_SR,MSK_BUSYSR_TTL, &r);
  return r;
}

bool LTP::BUSY_nimin_is_busy() {
  bool r = false;
  testBits(REG_BUSY_SR,MSK_BUSYSR_NIM, &r);
  return r;
}

bool LTP::BUSY_internal_is_busy() {
  bool r = false;
  testBits(REG_BUSY_SR,MSK_BUSYSR_INTERN, &r);
  return r;
}

// -----------------
// Pattern Generator
// -----------------
int LTP::PG_runmode_load          (){ return setBits(REG_PATGEN_CSR, MSK_PG_RUNMODE, PG_RUNMODE_LOAD   ); }
int LTP::PG_runmode_fixedvalue    (){ return setBits(REG_PATGEN_CSR, MSK_PG_RUNMODE, PG_RUNMODE_FIXED  ); }
int LTP::PG_runmode_cont          (){ return setBits(REG_PATGEN_CSR, MSK_PG_RUNMODE, PG_RUNMODE_CONT   ); }
int LTP::PG_runmode_notused       (){ return setBits(REG_PATGEN_CSR, MSK_PG_RUNMODE, PG_RUNMODE_NOTUSED); }
int LTP::PG_runmode_singleshot_vme(){ return setBits(REG_PATGEN_CSR, MSK_PG_RUNMODE, PG_RUNMODE_SS_VME ); }
int LTP::PG_runmode_singleshot_orb(){ return setBits(REG_PATGEN_CSR, MSK_PG_RUNMODE, PG_RUNMODE_SS_ORB ); }
int LTP::PG_runmode_singleshot_bgo(){ return setBits(REG_PATGEN_CSR, MSK_PG_RUNMODE, PG_RUNMODE_SS_BGO ); }


int LTP::PG_get_fixed_value(u_short* w) { return getBits(REG_FIXVAL, 0xffff ,w); }
int LTP::PG_set_fixed_value (u_short w) { return setBits(REG_FIXVAL, 0xffff, w); }

//------------------------------------------------------------------------------
int LTP::PG_reset() {
  // reset PG state machine
  m_dats = 0x0001;
  return m_vmm->WriteSafe(REG_SW_RST,m_dats);
}

//------------------------------------------------------------------------------
int LTP::PG_start() {

  // start pg
  m_dats = 0x0001;
  return m_vmm->WriteSafe(REG_SINGLSHOT,m_dats);
}


int LTP::PG_write(u_short w){
return setBits(REG_PATGEN_CSR, 0x001F , w );
}

//------------------------------------------------------------------------------
int LTP::PG_read_from_memory() {

  int status = 0;
  u_int count = 0;

  status = this->PG_read_address_counter(&count);
  if (status != VME_SUCCESS) {
    return status;
  }
  std::cout << "\n\nNumber of patterns in memory: " << count << std::endl;
  
  u_short d;
  for (u_int i = 1; i <= count; ++i) {
    // read memory register
    status |= m_vmm->ReadSafe(REG_MEMIO,&d);
    if (status != VME_SUCCESS) {
      return status;
    }
    std::printf("%7d.pattern: %s\n",i,this->printBinary16(d).c_str());
  }

  std::printf("\n\nNumber of patterns in memory: %d\n\n",count);
  return status;
}

//------------------------------------------------------------------------------
int LTP::PG_load_memory(std::vector<std::string>& pattern_vector){
  char a1[5];
  char a2[5];
  char a3[5];
  char a4[5];
  
  int status = 0;
  std::list< pair<int,int> > l_pm; // list for pattern-multiplicity pairs
  // start with zero pattern first, because the L1A,ORB,BUSY,CALREQ,TTYPE
  // need to be delayed by 1 BC (see later)
  std::pair<int,int> pm(0,1);
  l_pm.push_back(pm);
  int count = 1;

  if (pattern_vector.size()+1 > PG_MAXPATTERN) {
    std::printf("ERROR: LTP::PG_load_memory() -- pattern two long > 1M\n");
    return ERROR_TOO_LONG_PATTERN;
  }

  for (std::vector<std::string>::iterator it = pattern_vector.begin();
       it != pattern_vector.end();
       ++it) {

    // when using the pattern from OKS, it could happen that the string contains quotes
    // if this is the case, the quotes will be stripped off
    if (*(it->begin()) == '\"') it->erase(it->begin());
    if (*(it->end()) == '\"') it->erase(it->end());

    // ignore patterns that start with #
    if (*(it->begin()) != '#') {
      int mult = 0;
      sscanf(it->c_str(),"%4s %4s %4s %4s %d",&a1[0],&a2[0],&a3[0],&a4[0],&mult);
      int pattern = bin4toint(a1,a2,a3,a4);	
      std::pair<int,int> pm(pattern,mult);

      // 	std::printf("Pattern %4s %4s %4s %4s = 0x%04x, Multiplicity %d\n",a1,a2,a3,a4,pattern,mult);
      l_pm.push_back(pm);
      count+=mult;
    }
  }

  std::cout << "Read " << count-1 << " patterns and introduce an additional pattern to align with the L1A" << std::endl;
  std::cout << "In total: " << count << " patterns loaded into memory" << std::endl;
  // put pattern generator into load mode
  if ((status |= PG_runmode_load())!= VME_SUCCESS) {
    return status;
  }
  if ((status |= PG_reset())!= VME_SUCCESS) {
    return status;
  }

  // write number of patterns into counter register
  // if there are <count> patterns, <count>-1 needs to be written into register
  // -- split variable count into least and most significant word
  u_short lsw, msw;
  lsw = (count | 0xffff0000)-0xffff0000;
  msw = ((count>>16) | 0xffff0000) -0xffff0000;
  // std::printf("Check: %04x + %04x = %08x\n",msw, lsw, count);
  if ((status |= m_vmm->WriteSafe(REG_20BITCNT_MSW,msw)) != VME_SUCCESS) {
    return status;
  }
  if ((status |= m_vmm->WriteSafe(REG_20BITCNT_LSW,lsw)) != VME_SUCCESS) {
    return status;
  }
  
  if ((status |= PG_reset())!= VME_SUCCESS) {
    return status;
  }

  // write patterns
  // -- loop over list
  u_short p0 = 0;
  bool first = true;
  std::list< pair<int,int> >::const_iterator it;
  for (it = l_pm.begin(); it != l_pm.end(); ++it) {
    if (first) {
      first = false;
    }
    else {
      int pattern, mult;
      pattern = it->first;
      mult = it->second;
      for (int i = 0; i < mult; ++i) {
	u_short p1 = pattern;
	this->delayPatterns(p0, p1);
	m_dats = p0;
	p0 = p1;
// 	printf("writing pattern: 0x%04x\n",m_dats);
	if ((status |= m_vmm->WriteSafe(REG_MEMIO,m_dats))!= VME_SUCCESS) {
	  return status;
	}
      }
    }
  }
  // write last word
  m_dats = p0;
//   printf("writing pattern: 0x%04x\n",m_dats);
  if ((status |= m_vmm->WriteSafe(REG_MEMIO,m_dats))!= VME_SUCCESS) {
    return status;
  }

  // reset pattern generator state machine
  if ((status |= PG_reset())!= VME_SUCCESS) {
    return status;
  }

  return status;
}

int LTP::delayPatterns(u_short& p0, u_short& p1) {
  
  // For the pattern generator
  // L1A, orb, busy, calreq and TTYPE must be delayed by 1BC wrt BGo and test triggers
  // this function takes the early pattern p0 and late patter p1
  // and produces a pattern to be written into the pattern generator memory (p0)
  // and a rest pattern (p1) to be fed into the same function as p0 in the
  // next iteration

  u_short delay_part_p0 = p0 & 0xF863;
//   u_short now_part_p0 = p0 & 0x79C;

  u_short delay_part_p1 = p1 & 0xF863;
  u_short now_part_p1 = p1 & 0x79C;

  p0 = now_part_p1 + delay_part_p0; 
  p1 = delay_part_p1;

  return 0;
}


//------------------------------------------------------------------------------
int LTP::PG_load_memory_from_file(std::string* filename){
  char buffer[100];
  char a1[5];
  char a2[5];
  char a3[5];
  char a4[5];
  
  int status = 0;
  std::list< pair<int,int> > l_pm; // list for pattern-multiplicity pairs
  // start with zero pattern first, because the L1A,ORB,BUSY,CALREQ,TTYPE
  // need to be delayed by 1 BC (see later)
  std::pair<int,int> pm(0,1);
  l_pm.push_back(pm);
  u_int count = 1;

  std::ifstream tfile(filename->c_str(), ios::in);
  if (!tfile.is_open()) {
    return(-1);
  }
  else {
    std::cout << "Loading file \"" << filename->c_str() << "\" into memory ..." << std::endl;
    while(!tfile.eof()){
      tfile.getline(buffer,100);
      // ignore lines with #
      int pattern, mult;
      if (buffer[0]!='#' && strlen(buffer)>20) {
 	sscanf(buffer,"%4s %4s %4s %4s %d",&a1[0],&a2[0],&a3[0],&a4[0],&mult);

	pattern = bin4toint(a1,a2,a3,a4);	
	std::pair<int,int> pm(pattern,mult);

// 	std::printf("Pattern %4s %4s %4s %4s = 0x%04x, Multiplicity %d\n",a1,a2,a3,a4,pattern,mult);
	l_pm.push_back(pm);

	count+=mult;
      }
    }
  }
  tfile.close();

  if (count+1 > PG_MAXPATTERN) {
    std::printf("ERROR: LTP::PG_load_memory() -- pattern two long > 1M\n");
    return ERROR_TOO_LONG_PATTERN;
  }

  std::cout << "read " << count << " patterns." << std::endl;

  // put pattern generator into load mode
  if ((status |= PG_runmode_load())!= VME_SUCCESS) {
    return status;
  }
  if ((status |= PG_reset())!= VME_SUCCESS) {
    return status;
  }

  // write number of patterns into counter register
  // if there are <count> patterns, <count>-1 needs to be written into register
  // -- split variable count into least and most significant word
  u_short lsw, msw;
  lsw = (count | 0xffff0000)-0xffff0000;
  msw = ((count>>16) | 0xffff0000) -0xffff0000;
  // std::printf("Check: %04x + %04x = %08x\n",msw, lsw, count);
  if ((status |= m_vmm->WriteSafe(REG_20BITCNT_MSW,msw)) != VME_SUCCESS) {
    return status;
  }
  if ((status |= m_vmm->WriteSafe(REG_20BITCNT_LSW,lsw)) != VME_SUCCESS) {
    return status;
  }
  
  if ((status |= PG_reset())!= VME_SUCCESS) {
    return status;
  }

  // write patterns
  // -- loop over list
  u_short p0 = 0;
  bool first = true;
  std::list< pair<int,int> >::const_iterator it;
  for (it = l_pm.begin(); it != l_pm.end(); ++it) {
    if (first) {
      first = false;
    }
    else {
      int pattern, mult;
      pattern = it->first;
      mult = it->second;
      for (int i = 0; i < mult; ++i) {
	u_short p1 = pattern;
	this->delayPatterns(p0, p1);
	m_dats = p0;
	p0 = p1;
// 	printf("writing pattern: 0x%04x\n",m_dats);
	if ((status |= m_vmm->WriteSafe(REG_MEMIO,m_dats))!= VME_SUCCESS) {
	  return status;
	}
      }
    }
  }
  // write last word
  m_dats = p0;
//   printf("writing pattern: 0x%04x\n",m_dats);
  if ((status |= m_vmm->WriteSafe(REG_MEMIO,m_dats))!= VME_SUCCESS) {
    return status;
  }


  // reset pattern generator state machine
  if ((status |= PG_reset())!= VME_SUCCESS) {
    return status;
  }

  return status;
}

int LTP::PG_read                    (u_short* w) {return getBits(REG_PATGEN_CSR  , MSK_PG_R     , w); }
int LTP::PG_read_address_counter_MSW(u_short* w) {return getBits(REG_20BITCNT_MSW, MSK_PG_AC_MSW, w); }
int LTP::PG_read_address_counter_LSW(u_short* w) {return getBits(REG_20BITCNT_LSW, MSK_PG_AC_LSW, w); }

int LTP::PG_read_address_counter(u_int* w) {
  u_short msw, lsw;
  int status = 0;
  if ((status = this->PG_read_address_counter_MSW(&msw)) != VME_SUCCESS) {
    return status;
  }
  if ((status = this->PG_read_address_counter_LSW(&lsw)) != VME_SUCCESS) {
    return status;
  }
  *w = msw*0x10000+lsw;
  return status;
}

int LTP::PG_runmode(enum pg_op_mode* mode ) {
  u_short sel;
  int status = getBits(REG_PATGEN_CSR, MSK_PG_OPMode, &sel);

  if      (sel == PG_RUNMODE_LOAD   ) *mode =  PG_PRE_LOAD ;
  else if (sel == PG_RUNMODE_RUN    ) *mode =  PG_RUNNING  ;
  else if (sel == PG_RUNMODE_FIXED  ) *mode =  PG_FIXED    ;
  else if (sel == PG_RUNMODE_NOTUSED) *mode =  PG_NOTUSED  ;
  else {
    *mode = PG_ERROR;
    status = ERROR_INVALID_PG_RUNMODE;
  }
  return status;
}

bool LTP::PG_is_in_continuous_mode(){
  bool r = false;
  testBits(REG_PATGEN_CSR, MSK_PG_SS_CONT, &r);
  return r;
}

bool LTP::PG_is_in_singleshot_mode(){
  bool r = false;
  testBits(REG_PATGEN_CSR, MSK_PG_SS_CONT, &r);
  return !r;
}

bool LTP::PG_single_shot_trigger_source_is_external(){
  bool r = false;
  testBits(REG_PATGEN_CSR, MSK_PG_SS_TRIG, &r);
  return r;
}

bool LTP::PG_single_shot_trigger_source_is_vme(){
  bool r = false;
  testBits(REG_PATGEN_CSR, MSK_PG_SS_TRIG, &r);
  return !r;
}

bool LTP::PG_external_source_is_BGo3(){
  bool r = false;
  testBits(REG_PATGEN_CSR, MSK_PG_SS_EXT, &r);
  return r;
}

bool LTP::PG_external_source_is_orbit(){
  bool r = false;
  testBits(REG_PATGEN_CSR, MSK_PG_SS_EXT, &r);
  return !r;
}

bool LTP::PG_is_idle(){
  u_short data;
  getBits(REG_PATGEN_CSR, 0xf800, &data);
  return (data == 0);
}


void LTP::OM_slave_print_description() {
  std::printf("\n");
  std::printf("*** Slave Mode ***\n");
  std::printf("\n");
  std::printf(" Features:\n");
  std::printf(" - L1A, Test triggers, BGo from CTP Link\n");
  std::printf(" - BC and Orbit from CTP Link\n");
  std::printf(" - Busy from LEMO and LTP Link\n");
  std::printf("\n");
}

int LTP::OM_slave() {

  int status = 0;

  // reset
  status |= this->Reset();

  // bc
  status |= this->BC_linkout_from_linkin();
  status |= this->BC_lemoout_from_linkin();

  // orbit
  status |= this->ORB_linkout_from_linkin();
  status |= this->ORB_lemoout_from_linkin();
  status |= this->ORB_local_no();

  // link in to link out for all signals
  status |= this->IO_linkout_from_linkin(LTP::L1A);
  status |= this->IO_lemoout_from_linkin(LTP::L1A);
  status |= this->IO_local_no_source(LTP::L1A);
  status |= this->IO_disable_busy_gating(LTP::L1A);
  status |= this->IO_disable_inhibit_gating(LTP::L1A);
  status |= this->IO_disable_pg_gating(LTP::L1A);

  status |= this->IO_linkout_from_linkin(LTP::TR1);
  status |= this->IO_lemoout_from_linkin(LTP::TR1);
  status |= this->IO_local_no_source(LTP::TR1);
  status |= this->IO_disable_busy_gating(LTP::TR1);
  status |= this->IO_disable_inhibit_gating(LTP::TR1);
  status |= this->IO_disable_pg_gating(LTP::TR1);

  status |= this->IO_linkout_from_linkin(LTP::TR2);
  status |= this->IO_lemoout_from_linkin(LTP::TR2);
  status |= this->IO_local_no_source(LTP::TR2);
  status |= this->IO_disable_busy_gating(LTP::TR2);
  status |= this->IO_disable_inhibit_gating(LTP::TR2);
  status |= this->IO_disable_pg_gating(LTP::TR2);

  status |= this->IO_linkout_from_linkin(LTP::TR3);
  status |= this->IO_lemoout_from_linkin(LTP::TR3);
  status |= this->IO_local_no_source(LTP::TR3);
  status |= this->IO_disable_busy_gating(LTP::TR3);
  status |= this->IO_disable_inhibit_gating(LTP::TR3);
  status |= this->IO_disable_pg_gating(LTP::TR3);

  status |= this->IO_linkout_from_linkin(LTP::BG0);
  status |= this->IO_lemoout_from_linkin(LTP::BG0);
  status |= this->IO_local_no_source(LTP::BG0);
  status |= this->IO_disable_busy_gating(LTP::BG0);
  status |= this->IO_disable_inhibit_gating(LTP::BG0);
  status |= this->IO_disable_pg_gating(LTP::BG0);

  status |= this->IO_linkout_from_linkin(LTP::BG1);
  status |= this->IO_lemoout_from_linkin(LTP::BG1);
  status |= this->IO_local_no_source(LTP::BG1);
  status |= this->IO_disable_busy_gating(LTP::BG1);
  status |= this->IO_disable_inhibit_gating(LTP::BG1);
  status |= this->IO_disable_pg_gating(LTP::BG1);

  status |= this->IO_linkout_from_linkin(LTP::BG2);
  status |= this->IO_lemoout_from_linkin(LTP::BG2);
  status |= this->IO_local_no_source(LTP::BG2);
  status |= this->IO_disable_busy_gating(LTP::BG2);
  status |= this->IO_disable_inhibit_gating(LTP::BG2);
  status |= this->IO_disable_pg_gating(LTP::BG2);

  status |= this->IO_linkout_from_linkin(LTP::BG3);
  status |= this->IO_lemoout_from_linkin(LTP::BG3);
  status |= this->IO_local_no_source(LTP::BG3);
  status |= this->IO_disable_busy_gating(LTP::BG3);
  status |= this->IO_disable_inhibit_gating(LTP::BG3);
  status |= this->IO_disable_pg_gating(LTP::BG3);

  // busy
  status |= this->BUSY_lemoout_busy_only();
  status |= this->BUSY_linkout_from_local      ();
  status |= this->BUSY_local_with_linkin       ();
  status |= this->BUSY_local_with_ttlin        ();
  status |= this->BUSY_local_with_nimin        ();
  status |= this->BUSY_local_without_deadtime  ();
  status |= this->BUSY_disable_constant_level  ();

  // trigger type
  status |= this->TTYPE_linkout_from_linkin();
  status |= this->TTYPE_eclout_from_linkin();
  status |= this->TTYPE_enable_eclout();
  status |= this->TTYPE_fifotrigger_L1A();
  status |= this->TTYPE_reset_fifo();

  // cal req
  status |= this->CAL_source_link();
  status |= this->CAL_enable_testpath();
 
  // counter
  status |= this->COUNTER_L1A();
  status |= this->COUNTER_reset();

  // pattern generator
  status |= this->PG_reset();
  status |= this->PG_runmode_notused();

  return status;
}


void LTP::OM_master_lemo_print_description() {
  std::printf("\n");
  std::printf("*** Master Mode (LEMO)\n");
  std::printf("\n");
  std::printf(" Features:\n");
  std::printf(" - L1A, Test triggers, BGo from LEMO inputs\n");
  std::printf(" - BC and Orbit from internal\n");
  std::printf(" - Busy from TTL and NIM inputs\n");
  std::printf(" - Busy gating switched on\n");
  std::printf(" - Calibration request from front-panel input\n");
  std::printf(" - Trigger type from preset register\n");
  std::printf("\n");
}

int LTP::OM_master_lemo() {
  int status = 0;

  // reset
  status |= this->Reset();

  // bc
  status |= this->BC_local_intern();
  status |= this->BC_linkout_from_local();
  status |= this->BC_lemoout_from_local();

  // orbit
  status |= this->ORB_local_intern();
  status |= this->ORB_linkout_from_local();
  status |= this->ORB_lemoout_from_local();

  // link out from local for all signals
  status |= this->IO_linkout_from_local(LTP::L1A);
  status |= this->IO_lemoout_from_local(LTP::L1A);
  status |= this->IO_local_lemo_direct(LTP::L1A);
  status |= this->IO_enable_busy_gating(LTP::L1A);
  status |= this->IO_disable_inhibit_gating(LTP::L1A);
  status |= this->IO_disable_pg_gating(LTP::L1A);

  status |= this->IO_linkout_from_local(LTP::TR1);
  status |= this->IO_lemoout_from_local(LTP::TR1);
  status |= this->IO_local_lemo_direct(LTP::TR1);
  status |= this->IO_enable_busy_gating(LTP::TR1);
  status |= this->IO_disable_inhibit_gating(LTP::TR1);
  status |= this->IO_disable_pg_gating(LTP::TR1);

  status |= this->IO_linkout_from_local(LTP::TR2);
  status |= this->IO_lemoout_from_local(LTP::TR2);
  status |= this->IO_local_lemo_direct(LTP::TR2);
  status |= this->IO_enable_busy_gating(LTP::TR2);
  status |= this->IO_disable_inhibit_gating(LTP::TR2);
  status |= this->IO_disable_pg_gating(LTP::TR2);

  status |= this->IO_linkout_from_local(LTP::TR3);
  status |= this->IO_lemoout_from_local(LTP::TR3);
  status |= this->IO_local_lemo_direct(LTP::TR3);
  status |= this->IO_enable_busy_gating(LTP::TR3);
  status |= this->IO_disable_inhibit_gating(LTP::TR3);
  status |= this->IO_disable_pg_gating(LTP::TR3);

  status |= this->IO_linkout_from_local(LTP::BG0);
  status |= this->IO_lemoout_from_local(LTP::BG0);
  status |= this->IO_local_lemo_direct(LTP::BG0);
  status |= this->IO_disable_busy_gating(LTP::BG0);
  status |= this->IO_disable_inhibit_gating(LTP::BG0);
  status |= this->IO_disable_pg_gating(LTP::BG0);

  status |= this->IO_linkout_from_local(LTP::BG1);
  status |= this->IO_lemoout_from_local(LTP::BG1);
  status |= this->IO_local_lemo_direct(LTP::BG1);
  status |= this->IO_disable_busy_gating(LTP::BG1);
  status |= this->IO_disable_inhibit_gating(LTP::BG1);
  status |= this->IO_disable_pg_gating(LTP::BG1);

  status |= this->IO_linkout_from_local(LTP::BG2);
  status |= this->IO_lemoout_from_local(LTP::BG2);
  status |= this->IO_local_lemo_direct(LTP::BG2);
  status |= this->IO_disable_busy_gating(LTP::BG2);
  status |= this->IO_disable_inhibit_gating(LTP::BG2);
  status |= this->IO_disable_pg_gating(LTP::BG2);

  status |= this->IO_linkout_from_local(LTP::BG3);
  status |= this->IO_lemoout_from_local(LTP::BG3);
  status |= this->IO_local_lemo_direct(LTP::BG3);
  status |= this->IO_disable_busy_gating(LTP::BG3);
  status |= this->IO_disable_inhibit_gating(LTP::BG3);
  status |= this->IO_disable_pg_gating(LTP::BG3);

  // busy
  status |= this->BUSY_linkout_from_local();
  status |= this->BUSY_local_with_linkin();
  status |= this->BUSY_local_with_ttlin();
  status |= this->BUSY_local_with_nimin();
  status |= this->BUSY_disable_from_pg();
  // do not fiddle with constant busy
//   status |= this->BUSY_disable_constant_level();
  status |= this->BUSY_local_without_deadtime();
  status |= this->BUSY_lemoout_busy_only();

  // trigger type
  status |= this->TTYPE_linkout_from_local();
  status |= this->TTYPE_eclout_from_local();
  status |= this->TTYPE_enable_eclout();
  status |= this->TTYPE_local_from_vme();
  status |= this->TTYPE_regfile_address_0();
  status |= this->TTYPE_write_regfile0(0x0001);
  status |= this->TTYPE_write_regfile1(0x0002);
  status |= this->TTYPE_write_regfile2(0x0003);
  status |= this->TTYPE_write_regfile3(0x0004);
  status |= this->TTYPE_fifotrigger_L1A();
  status |= this->TTYPE_reset_fifo();

  // cal req
  status |= this->CAL_source_fp();
  status |= this->CAL_enable_testpath();
 
  // counter
  status |= this->COUNTER_L1A();
  status |= this->COUNTER_reset();

  // pattern generator
  status |= this->PG_reset();
  status |= this->PG_runmode_notused();

  return status;
}
 

void LTP::OM_master_patgen_print_description(std::string& pgfilename) {
  std::printf("\n");
  std::printf("*** Master Mode (Pattern Generator)\n");
  std::printf("\n");
  std::printf(" Features:\n");
  std::printf("  - PG file name: %s (relative to current directory)\n",pgfilename.c_str());
  std::printf("  - BC, Orbit from internal\n");
  std::printf("  - L1A, Test Triggers, BGo from PG\n");
  std::printf("  - Busy from link and LEMO\n");
  std::printf("  - PG in continous mode\n");
  std::printf("\n");
}

int LTP::OM_master_patgen(std::string& filename) {

  int status = 0;

  // reset
  status |= this->Reset();

  // bc
  status |= this->BC_local_intern();
  status |= this->BC_linkout_from_local();
  status |= this->BC_lemoout_from_local();

  // orbit
  status |= this->ORB_local_intern();
  status |= this->ORB_linkout_from_local();
  status |= this->ORB_lemoout_from_local();

  // link out from local for all signals
  status |= this->IO_linkout_from_local(LTP::L1A);
  status |= this->IO_lemoout_from_local(LTP::L1A);
  status |= this->IO_local_pg(LTP::L1A);
  status |= this->IO_disable_pg_gating(LTP::L1A);
  status |= this->IO_enable_busy_gating(LTP::L1A);
  status |= this->IO_disable_inhibit_gating(LTP::L1A);

  status |= this->IO_linkout_from_local(LTP::TR1);
  status |= this->IO_lemoout_from_local(LTP::TR1);
  status |= this->IO_local_pg(LTP::TR1);
  status |= this->IO_disable_pg_gating(LTP::TR1);
  status |= this->IO_enable_busy_gating(LTP::TR1);
  status |= this->IO_disable_inhibit_gating(LTP::TR1);

  status |= this->IO_linkout_from_local(LTP::TR2);
  status |= this->IO_lemoout_from_local(LTP::TR2);
  status |= this->IO_local_pg(LTP::TR2); 
  status |= this->IO_disable_pg_gating(LTP::TR2);
  status |= this->IO_enable_busy_gating(LTP::TR2);
  status |= this->IO_disable_inhibit_gating(LTP::TR2);

  status |= this->IO_linkout_from_local(LTP::TR3);
  status |= this->IO_lemoout_from_local(LTP::TR3);
  status |= this->IO_local_pg(LTP::TR3);
  status |= this->IO_disable_pg_gating(LTP::TR3);
  status |= this->IO_enable_busy_gating(LTP::TR3);
  status |= this->IO_disable_inhibit_gating(LTP::TR3);

  status |= this->IO_linkout_from_local(LTP::BG0);
  status |= this->IO_lemoout_from_local(LTP::BG0);
  status |= this->IO_local_pg(LTP::BG0);
  status |= this->IO_disable_pg_gating(LTP::BG0);
  status |= this->IO_disable_busy_gating(LTP::BG0);
  status |= this->IO_disable_inhibit_gating(LTP::BG0);

  status |= this->IO_linkout_from_local(LTP::BG1);
  status |= this->IO_lemoout_from_local(LTP::BG1);
  status |= this->IO_local_pg(LTP::BG1);
  status |= this->IO_disable_pg_gating(LTP::BG1);
  status |= this->IO_disable_busy_gating(LTP::BG1);
  status |= this->IO_disable_inhibit_gating(LTP::BG1);

  status |= this->IO_linkout_from_local(LTP::BG2);
  status |= this->IO_lemoout_from_local(LTP::BG2);
  status |= this->IO_local_pg(LTP::BG2);
  status |= this->IO_disable_pg_gating(LTP::BG2);
  status |= this->IO_disable_busy_gating(LTP::BG2);
  status |= this->IO_disable_inhibit_gating(LTP::BG2);

  status |= this->IO_linkout_from_local(LTP::BG3);
  status |= this->IO_lemoout_from_local(LTP::BG3);
  status |= this->IO_local_pg(LTP::BG3);
  status |= this->IO_disable_pg_gating(LTP::BG3);
  status |= this->IO_disable_busy_gating(LTP::BG3);
  status |= this->IO_disable_inhibit_gating(LTP::BG3);

  // busy
  status |= this->BUSY_linkout_from_local();
  status |= this->BUSY_local_with_linkin();
  status |= this->BUSY_local_with_ttlin();
  status |= this->BUSY_local_with_nimin();
  status |= this->BUSY_disable_from_pg();
  // do not fiddle with constant busy
//   status |= this->BUSY_disable_constant_level();
  status |= this->BUSY_local_without_deadtime();
  status |= this->BUSY_lemoout_busy_only();
 
  // trigger type
  status |= this->TTYPE_linkout_from_local();
  status |= this->TTYPE_eclout_from_local();
  status |= this->TTYPE_enable_eclout();
  status |= this->TTYPE_local_from_pg();
  status |= this->TTYPE_write_regfile0(0x0001);
  status |= this->TTYPE_write_regfile1(0x0002);
  status |= this->TTYPE_write_regfile2(0x0003);
  status |= this->TTYPE_write_regfile3(0x0004);
  status |= this->TTYPE_fifotrigger_L1A();
  status |= this->TTYPE_reset_fifo();

  // cal req
  status |= this->CAL_source_pg();
  status |= this->CAL_enable_testpath();
 
  // counter
  status |= this->COUNTER_L1A();
  status |= this->COUNTER_reset();

  // load Pattern Generator
  status |= this->PG_reset();
  status |= this->PG_runmode_load();

  status |= this->PG_load_memory_from_file(&filename);

  status |= this->PG_runmode_cont();

  return status;
}


void LTP::OM_master_RPCPulseL1A_print_description(u_int l1a_delay) {
  std::printf("\n");
  std::printf("*** Special RPC Master Mode: pulse followed by L1A\n");
  std::printf("\n");
  std::printf(" Features:\n");
  std::printf("  - BC, Orbit from internal\n");
  std::printf("  - Pulse (BGo-3) from LEMO input, triggering Pattern generator\n");
  std::printf("  - L1A a fixed number of BC (L1A_delay) after pulse\n");
  std::printf("       current value: L1A_delay = %d BC\n", l1a_delay);
  std::printf("  - Busy from ttl input\n");
  std::printf("\n");
}

int LTP::OM_master_RPCPulseL1A(u_int l1a_delay) {
  int status = 0;

  // reset
  status |= this->Reset();

  // bc
  status |= this->BC_local_intern();
  status |= this->BC_linkout_from_local();
  status |= this->BC_lemoout_from_local();

  // orbit
  status |= this->ORB_local_intern();
  status |= this->ORB_linkout_from_local();
  status |= this->ORB_lemoout_from_local();

  // link out from local for all signals
  status |= this->IO_linkout_from_local(LTP::L1A);
  status |= this->IO_lemoout_from_local(LTP::L1A);
  status |= this->IO_local_pg(LTP::L1A);
  status |= this->IO_enable_busy_gating(LTP::L1A);
  status |= this->IO_disable_inhibit_gating(LTP::L1A);
  status |= this->IO_disable_pg_gating(LTP::L1A);

  status |= this->IO_linkout_from_local(LTP::TR1);
  status |= this->IO_lemoout_from_local(LTP::TR1);
  status |= this->IO_local_no_source(LTP::TR1);
  status |= this->IO_disable_busy_gating(LTP::TR1);
  status |= this->IO_disable_inhibit_gating(LTP::TR1);
  status |= this->IO_disable_pg_gating(LTP::TR1);

  status |= this->IO_linkout_from_local(LTP::TR2);
  status |= this->IO_lemoout_from_local(LTP::TR2);
  status |= this->IO_local_no_source(LTP::TR2);
  status |= this->IO_disable_busy_gating(LTP::TR2);
  status |= this->IO_disable_inhibit_gating(LTP::TR2);
  status |= this->IO_disable_pg_gating(LTP::TR2);

  status |= this->IO_linkout_from_local(LTP::TR3);
  status |= this->IO_lemoout_from_local(LTP::TR3);
  status |= this->IO_local_no_source(LTP::TR3);
  status |= this->IO_disable_busy_gating(LTP::TR3);
  status |= this->IO_disable_inhibit_gating(LTP::TR3);
  status |= this->IO_disable_pg_gating(LTP::TR3);

  status |= this->IO_linkout_from_local(LTP::BG0);
  status |= this->IO_lemoout_from_local(LTP::BG0);
  status |= this->IO_local_no_source(LTP::BG0);
  status |= this->IO_disable_busy_gating(LTP::BG0);
  status |= this->IO_disable_inhibit_gating(LTP::BG0);
  status |= this->IO_disable_pg_gating(LTP::BG0);

  status |= this->IO_linkout_from_local(LTP::BG1);
  status |= this->IO_lemoout_from_local(LTP::BG1);
  status |= this->IO_local_no_source(LTP::BG1);
  status |= this->IO_disable_busy_gating(LTP::BG1);
  status |= this->IO_disable_inhibit_gating(LTP::BG1);
  status |= this->IO_disable_pg_gating(LTP::BG1);

  status |= this->IO_linkout_from_local(LTP::BG2);
  status |= this->IO_lemoout_from_local(LTP::BG2);
  status |= this->IO_local_no_source(LTP::BG2);
  status |= this->IO_disable_busy_gating(LTP::BG2);
  status |= this->IO_disable_inhibit_gating(LTP::BG2);
  status |= this->IO_disable_pg_gating(LTP::BG2);

  status |= this->IO_linkout_from_local(LTP::BG3);
  status |= this->IO_lemoout_from_local(LTP::BG3);
  status |= this->IO_local_lemo_direct(LTP::BG3);
  status |= this->IO_disable_busy_gating(LTP::BG3);
  status |= this->IO_disable_inhibit_gating(LTP::BG3);
  status |= this->IO_disable_pg_gating(LTP::BG3);

  // busy
  status |= this->BUSY_linkout_from_local();
  status |= this->BUSY_local_with_linkin();
  status |= this->BUSY_local_with_ttlin();
  status |= this->BUSY_local_without_nimin();
  status |= this->BUSY_disable_from_pg();
  // do not fiddle with constant busy
//   status |= this->BUSY_disable_constant_level();
  status |= this->BUSY_local_without_deadtime();
  status |= this->BUSY_lemoout_busy_only();

  // trigger type
  status |= this->TTYPE_linkout_from_local();
  status |= this->TTYPE_eclout_from_local();
  status |= this->TTYPE_disable_eclout();
  status |= this->TTYPE_local_from_vme();
  status |= this->TTYPE_regfile_address_0();
  status |= this->TTYPE_write_regfile0(0x0001);
  status |= this->TTYPE_write_regfile1(0x0002);
  status |= this->TTYPE_write_regfile2(0x0003);
  status |= this->TTYPE_write_regfile3(0x0004);
  status |= this->TTYPE_fifotrigger_L1A();
  status |= this->TTYPE_reset_fifo();

  // cal req
  status |= this->CAL_source_fp();
  status |= this->CAL_enable_testpath();
 
  // counter
  status |= this->COUNTER_L1A();
  status |= this->COUNTER_reset();

  // pattern generator
  // #  ICC C     TT     O i
  // # Bnaa aBBB BTT     r c
  // # uhll lGGG GYYT TTLb i
  // # siRR Rooo oPPT TT1i t
  // # yb21 0321 0103 21At y
  // #
  status |= this->PG_reset();
  std::vector<std::string> v;
  std::string s = "0000 0000 0000 0000 1";
  // create gap according to l1a_delay
  for (u_int i = 0; i < l1a_delay-6; ++i) v.push_back(s);
  s = "0000 0000 0000 0010 1";
  v.push_back(s);

  status |= this->PG_load_memory(v);
  status |= this->PG_runmode_singleshot_bgo();

  return status;
}

int LTP::OM_GenerateECR() {
  int status = 0;

  status |= this->IO_local_lemo_pfn(LTP::BG1);
  status |= this->IO_pfn_width_1bc(LTP::BG1);

  u_int ret = ts_open(1, TS_DUMMY);
  if (ret) rcc_error_print(stdout, ret);
  
  // switch on busy
  bool const_busy_on = this->BUSY_constant_level_is_asserted();
  if (!const_busy_on) this->BUSY_enable_constant_level();

  // wait a millisecond 
  u_int time_to_wait_musec = 1000;
  ret = ts_delay(time_to_wait_musec);
  if (ret) rcc_error_print(stdout, ret);

  status |= this->IO_sw_pulse(LTP::BG1);

  ret = ts_delay(time_to_wait_musec);
  if (ret) rcc_error_print(stdout, ret);

  // switch off busy
  if (!const_busy_on) this->BUSY_disable_constant_level();

  return status;
}

//-----------------------
// Software busy channels
//-----------------------

int LTP::createBusyChannel() {
  m_busy_channels.push_back(false);
  return m_busy_channels.size()-1;
}


void LTP::update_hwbusy() {
  bool busy_or(false);
  for (unsigned int i = 0; i<m_busy_channels.size(); ++i) {
    if (m_busy_channels[i] == true) {
      busy_or = true;
      break;
    }
  }
  if (busy_or) {
    this->BUSY_enable_constant_level();
  }
  else {
    this->BUSY_disable_constant_level();
  }
}

bool LTP::setBusyChannel(unsigned int c) {
  if ( c>=m_busy_channels.size() ) {
    return false;
  }
  m_busy_channels[c] = true;
  this->update_hwbusy();
  return true;
}

bool LTP::releaseBusyChannel(unsigned int c) {
  if (c>=m_busy_channels.size() ) {
    return false;
  }
  m_busy_channels[c] = false;
  this->update_hwbusy();
  return true;
}

bool LTP::getBusyStatus(unsigned int c, bool& status) {
  if (c>=m_busy_channels.size() ) {
    return false;
  }
  status = m_busy_channels[c];
  return true;
}

bool LTP::getGlobalBusyStatus(bool& status) {
  if ( m_busy_channels.size() == 0) {
    return false;
  }
  status = false;
  for (unsigned int i = 0; i<m_busy_channels.size(); ++i) {
    status |= m_busy_channels[i];
  }
  return true;
}

void LTP::dumpSWBusy() {
  
  std::cout << "-- LTP::dumpSWBusy() --" << std::endl;
  std::cout << ">> found " << m_busy_channels.size() << " sw busy channels" << std::endl;
  bool global = false;
  for (unsigned int i = 0; i<m_busy_channels.size(); ++i) {
    global |= m_busy_channels[i];
    std::cout << ">> Channel " << i;
    if (m_busy_channels[i]) {
      std::cout << " busy" << std::endl;
    }
    else {
      std::cout << " not busy" << std::endl;
    }
  }
  std::cout << ">> Global: ";
  if (global) {
    std::cout << "busy" << std::endl;
  }
  else {
    std::cout << "not busy" << std::endl;
  }
}


void LTP::clearSWBusy() {
  m_busy_channels.clear();
}



